<?php
//echo VIDEOCALLPATH; exit();
require(VIDEOCALLPATH."api/config.php");
require(VIDEOCALLPATH."api/error.php");

header("Content-type: application/json");

class videoAPI {

    public function Createroom($inputarray) {
        GLOBAL $ARR_ERROR;

        $random_name = rand(100000, 999999);

        $roomname = $inputarray['roomname'];
        $description = $inputarray['description'];
        /* Create Room Meta */
        $Room = Array(
            "name" => $roomname . $random_name,
            "owner_ref" => $random_name,
            "settings" => Array(
                "description" => $description,
                "quality" => "SD",
                "mode" => "group",
                "participants" => "10",
                "duration" => "10",
                "scheduled" => false,
                "auto_recording" => true,
                "active_talker" => true,
                "wait_moderator" => true,
                "adhoc" => false
            ),
            "sip" => Array(
                "enabled" => false
            )
        );

        $Room_Meta = json_encode($Room);


        /* Prepare HTTP Post Request */

        $headers = array(
            'Content-Type: application/json',
            'Authorization: Basic ' . base64_encode(APP_ID . ":" . APP_KEY)
        );

        /* CURL POST Request */

        $ch = curl_init(API_URL . "/rooms");

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $Room_Meta);
        $response = curl_exec($ch);

        curl_close($ch);

        return $response;
    }

}

//print "confo.html?roomId=" + data.room_id + "&usertype=" + usertype + "&user_ref=" + user_ref;
//print "confo.html?roomId=5e4e3e9acf8f4978a17691cd&usertype=moderator&user_ref=Mili";