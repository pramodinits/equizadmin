<style>
        select.form-control {
            width: 30%;
            float: left;
            margin-right: 1%;
        }
    </style>
<!--update template design-->
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-wrapper-before"></div>
        <div class="content-header row">
            <div class="content-header-left col-md-4 col-12 mb-2">
                <h3 class="content-header-title"></h3>
            </div>
            <div class="content-header-right col-md-8 col-12">
                <div class="breadcrumbs-top float-md-right">
                    <div class="breadcrumb-wrapper mr-1">
                        <h6><?= $breadcrumbs; ?></h6>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collpase collapse show">
                            <div class="card-body">
                                <?php
                                if ($this->session->flashdata('message')) {
                                    echo $this->session->flashdata('message');
                                }
                                ?>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <form method="get" action="<?php echo site_url('qbank/index/'); ?>">
                                            <select name="cid" class="form-control">
                                                <option value="0"><?php echo $this->lang->line('all_category'); ?></option>
                                                <?php
                                                foreach ($category_list as $key => $val) {
                                                    ?>

                                                    <option value="<?php echo $key; ?>" <?php
                                                    if ($key == $user['cid']) {
                                                        echo 'selected';
                                                    }
                                                    ?> ><?php echo $val; ?></option>
                                                            <?php
                                                        }
                                                        ?>
                                            </select>
                                            <select name="mid" class="form-control">
                                                <option value="0"><?php echo $this->lang->line('all_module'); ?></option>
                                                <?php
                                                foreach ($question_module as $key => $val) {
                                                    ?>

                                                    <option value="<?php echo $key; ?>"  <?php
                                                    if ($key == $user['mid']) {
                                                        echo 'selected';
                                                    }
                                                    ?> ><?php echo $val; ?></option>
                                                            <?php
                                                        }
                                                        ?>
                                            </select>
                                            <button type="submit" class="btn btn-primary"><?php echo $this->lang->line('filter'); ?></button>
                                            <a href="<?= site_url('qbank/'); ?>">
                                            <button type="button" class="btn btn-primary"><?php echo $this->lang->line('reset_button'); ?></button>
                                            </a>
                                            <!--<button class="btn btn-default" type="submit"></button>-->
                                        </form>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th><?php echo $this->lang->line('question'); ?></th>
                                                <th><?php echo $this->lang->line('question_type'); ?></th>
                                                <th>
                                                    <?php echo $this->lang->line('category_name'); ?>
                                                    <!--
                                                    /
                                                    <?php echo $this->lang->line('category_name'); ?> /
                                                    <?php echo $this->lang->line('module_name'); ?>
                                                    -->
                                                </th>

                                                <th style="display: none;"><?php echo $this->lang->line('percent_corrected'); ?></th>
                                                <th><?php echo $this->lang->line('action'); ?> </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            if (count($result) == 0) {
                                                ?>
                                                <tr>
                                                    <td colspan="6"><?php echo $this->lang->line('no_record_found'); ?></td>
                                                </tr>	


                                                <?php
                                            }
                                            $logged_in = $this->session->userdata('logged_in');
                                            $access = json_decode($logged_in['permission_access'], true);
                                            foreach ($result as $key => $val) {
                                                ?>
                                                <tr>
                                                    <td>  <a href="javascript:show_question_stat('<?php echo $val['qid']; ?>');">+</a>  <?php echo $val['qid']; ?></td>
                                                    <td><?php echo substr(strip_tags($val['question']), 0, 40); ?>
                                                        <span style="display:none;" id="stat-<?php echo $val['qid']; ?>">


                                                            <table class="table table-bordered">
                                                                <tr><td><?php echo $this->lang->line('no_times_corrected'); ?></td><td><?php echo $val['no_time_corrected']; ?></td></tr>
                                                                <tr><td><?php echo $this->lang->line('no_times_incorrected'); ?></td><td><?php echo $val['no_time_incorrected']; ?></td></tr>
                                                                <tr><td><?php echo $this->lang->line('no_times_unattempted'); ?></td><td><?php echo $val['no_time_unattempted']; ?></td></tr>

                                                            </table>
                                                        </span>
                                                    </td>
                                                    <td><?php echo $val['question_type']; ?></td>
                                                    <td>
                                                        <?php echo $category_list[$val['question_category']]; ?> /

                                                        <span style="font-size:15px;"><?php echo $question_section[$val['question_section']]; ?></span> 
                                                        / 
                                                        <span style="font-size:15px;"><?php echo $question_module[$val['question_module']]; ?></span></td>

                                                    <td style="display: none;">
                                                        <?php
                                                        if ($val['no_time_served'] != '0') {
                                                            $perc = ($val['no_time_corrected'] / $val['no_time_served']) * 100;
                                                            ?>

                                                            <div style="background:#eeeeee;width:100%;height:10px;"><div style="background:#449d44;width:<?php echo intval($perc); ?>%;height:10px;"></div>
                                                                <span style="font-size:10px;"><?php echo intval($perc); ?>%</span>

                                                                <?php
                                                            } else {
                                                                echo $this->lang->line('not_used');
                                                            }
                                                            ?></td>

                                                    <td width="10%">
                                                        <?php
                                                        $qn = 1;
                                                        if ($val['question_type'] == $this->lang->line('multiple_choice_single_answer')) {
                                                            $qn = 1;
                                                        }
                                                        if ($val['question_type'] == $this->lang->line('multiple_choice_multiple_answer')) {
                                                            $qn = 2;
                                                        }
                                                        if ($val['question_type'] == $this->lang->line('match_the_column')) {
                                                            $qn = 3;
                                                        }
                                                        if ($val['question_type'] == $this->lang->line('short_answer')) {
                                                            $qn = 4;
                                                        }
                                                        if ($val['question_type'] == $this->lang->line('long_answer')) {
                                                            $qn = 5;
                                                        }
                                                        if ($val['question_type'] == $this->lang->line('fill_the_blank')) {
                                                            $qn = 6;
                                                        }
                                                        if ($val['question_type'] == $this->lang->line('sort_the_column')) {
                                                            $qn = 7;
                                                        }
                                                        ?>
                                                        <?php
                                                        if($logged_in['su'] == 7 && empty($access['question_bank']['add_edit']) && empty($access['question_bank']['delete'])) {
                                                            echo "N/A";
                                                        }
                                                        if ($access['question_bank']['add_edit'] || $logged_in['su'] == 1) { ?>
                                                            <a href="<?php echo site_url('qbank/edit_question_' . $qn . '/' . $val['qid']); ?>">
                                                            <!--<img src="<?php echo base_url('images/edit.png'); ?>">-->
                                                            <i class="la la-edit"></i>
                                                        </a>
                                                       <?php } 
                                                       if ($access['question_bank']['delete'] || $logged_in['su'] == 1) { 
                                                        if ($val['no_time_served'] == 1) {
                                                            $status = $val['status'];
                                                            if ($status == 1) {
                                                                ?>
                                                                <a href="javascript:void(0)" onclick="modifyStatus('<?= $val['qid']; ?>', '<?= $status ?>')">
                                                                    <!--<img src="<?php echo base_url('images/tick-icon.png'); ?>" title="Active Status (Click to Inactive)">-->
                                                                    <i class="la la-check-square text-success" title="Active Status (Click to Inactive)"></i>
                                                                </a>
                                                            <?php } else { ?>
                                                                <a href="javascript:void(0)" onclick="modifyStatus('<?= $val['qid']; ?>', '<?= $status ?>')">
                                                                    <img src="<?php // echo base_url('images/grey-tick.png'); ?>" title="Inactive Status (Click to Active)">
                                                                    <i class="la la-check-square text-danger" title="Inactive Status (Click to Active)"></i>
                                                                </a>
                                                            <?php }
                                                            ?>
                                                        <?php } else if ($val['no_time_served'] == 0) { 
                                                            ?>
                                                            <a href="javascript:void(0)" onclick="removeQuestion('<?= $val['qid']; ?>')">
                                                                <i class="la la-trash text-danger"></i>
                                                            </a>
                                                        <?php } 
                                                       }
                                                        ?>

                                                    </td>
                                                </tr>

                                                <?php
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                    <?= $links; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--update template design-->
<script>

    function advanceconfig() {
        $('#advanceconfig').toggle();
    }
    
    //remove question list
    function removeQuestion(qid) {
        var del = confirm('Are you sure to remove this?');
        if (del) {
            var url = "<?php echo site_url('qbank/remove_question'); ?>";
            $.post(url, {"qid": qid}, function (data) {
                if (data == "success") {
                    alert("Question Removed successfully");
                    location.reload();
                } else {
                    return false;
                }
            });
        } else {
            return false;
        }
    }

    function modifyStatus(qid, status) {
        var status_val = (status == 1) ? 2 : 1;
        if (qid) {
            var url = "<?php echo site_url('qbank/modify_status'); ?>";
            $.post(url, {'qid': qid, 'status_val': status_val}, function (data) {

                if (data == "success") {
                    alert("Status Modified successfully");
                    location.reload();
                } else {
                    return false;
                }
            });
        } else {
            return false;
        }
    }


</script>
<br><br><br><br>
