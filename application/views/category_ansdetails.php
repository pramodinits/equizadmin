<table class="table table-bordered" id="assignstudents">
    <tr>
        <th class="text-center"><?php echo $this->lang->line('question'); ?></th>
        <th class="text-center"><?php echo $this->lang->line('score_obtained'); ?></th>
    </tr>
    <?php
    if (count($result) == 0) {
        ?>
        <tr>
            <td colspan="2"><?php echo $this->lang->line('no_record_found'); ?></td>
        </tr>
        <?php
    }
//    print "<pre>";
//    print_r($result);exit();
    foreach ($result as $key => $val) {
        ?>
        <tr>
            <td><?= $val['question'] ?></td>
            <td><?= $val['score_u'] ?></td>
        </tr>
        <?php
    }
    ?>

</table>