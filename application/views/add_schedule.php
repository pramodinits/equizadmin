<!--<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">-->
<!--<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>-->

<!--includeing files-->
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js" type="text/javascript"></script>
<link href="<?php echo base_url(); ?>css/fullcalendar.css" rel="stylesheet">
<script src="<?php echo base_url(); ?>js/fullcalendar.js"></script>
<!--includeing files-->
<style>
    #wrap {
        width: 1100px;
        margin: 0 auto;
    }

    #external-events {
        float: left;
        width: 150px;
        padding: 0 10px;
        text-align: left;
    }

    #external-events h4 {
        font-size: 16px;
        margin-top: 0;
        padding-top: 1em;
    }

    .external-event { /* try to mimick the look of a real event */
        margin: 10px 0;
        padding: 2px 4px;
        background: #3366CC;
        color: #fff;
        font-size: .85em;
        cursor: pointer;
    }

    #external-events p {
        margin: 1.5em 0;
        font-size: 11px;
        color: #666;
    }

    #external-events p input {
        margin: 0;
        vertical-align: middle;
    }

    #calendar {
        /* 		float: right; */
        margin: 0 auto;
        width: 900px;
        background-color: #FFFFFF;
        border-radius: 6px;
        box-shadow: 0 1px 2px #C3C3C3;
        -webkit-box-shadow: 0px 0px 21px 2px rgba(0,0,0,0.18);
        -moz-box-shadow: 0px 0px 21px 2px rgba(0,0,0,0.18);
        box-shadow: 0px 0px 21px 2px rgba(0,0,0,0.18);
    }
</style>
<div class="container">
    <h3><?php echo $title; ?></h3>
    <div class="row">
        <form method="post" id="register_user" name="register_user">
            <div class="col-md-12">
                <div class="login-panel panel panel-default">
                    <div class="panel-body">
                        <?php
                        if ($this->session->flashdata('message')) {
                            echo $this->session->flashdata('message');
                        }
                        ?>
                        <div class="row">
                            <div id='calendar'></div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

 <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>-->
<script>

    $(document).ready(function () {
        var date = new Date();
        var d = date.getDate();
        var m = date.getMonth();
        var y = date.getFullYear();

        /* initialize the external events
         -----------------------------------------------------------------*/
        $('#external-events div.external-event').each(function () {

            // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
            // it doesn't need to have a start or end
            var eventObject = {
                title: $.trim($(this).text()) // use the element's text as the event title
            };
            // store the Event Object in the DOM element so we can get to it later
            $(this).data('eventObject', eventObject);
            // make the event draggable using jQuery UI
            $(this).draggable({
                zIndex: 999,
                revert: true, // will cause the event to go back to its
                revertDuration: 0  //  original position after the drag
            });
        });
        /* initialize the calendar
         -----------------------------------------------------------------*/

        var inputevent = <?= $events ?>;
        var calendar = $('#calendar').fullCalendar({
            header: {
                left: 'title',
                center: 'agendaDay,agendaWeek,month',
                right: 'prev,next today'
            },
            slotMinutes: 60,
            editable: true,
            eventOverlap: false,
            firstDay: 1, //  1(Monday) this can be changed to 0(Sunday) for the USA system
            selectable: true,
            defaultView: 'month',
            events: inputevent,
            selectHelper: true,
            select: function (startStr, endStr, allDay) {
                var title = prompt('Event Title:');
                if (title) {
                    calendar.fullCalendar('renderEvent',
                            {
                                title: title,
                                start: startStr,
                                end: endStr,
                                allDay: allDay
                            },
                            true // make the event "stick"
                            );
                    //save data in db
                    var url = "<?php echo site_url('schedule_time/insert_schedule'); ?>";
                    var quiz_id = <?= $quiz_id; ?>;
                    var startDate = $.fullCalendar.formatDate(endStr, 'yyyy-MM-dd');
                    var start_schedule = $.fullCalendar.formatDate(startStr, 'yyyy-MM-dd hh:mm');
                    var end_schedule = $.fullCalendar.formatDate(endStr, 'yyyy-MM-dd hh:mm');
                    $.post(url, {'schedule_title': title,
                        'quiz_id': quiz_id,
                        "start_schedule": start_schedule,
                        "end_schedule": end_schedule,
                        "schedule_date": startDate,
                        "display_event_start": startStr,
                        "display_event_end": endStr
                    }, function (data) {
                        var res = JSON.parse(data);
                        if (res.Message == "Success") {
                            alert("Schedule added successfully");
                        } else {
                            alert("Error in addition.");
                            return false;
                        }
                    });
                }
                calendar.fullCalendar('unselect');
            },
            droppable: true, // this allows things to be dropped onto the calendar !!!
            drop: function (date, allDay) { // this function is called when something is dropped

                // retrieve the dropped element's stored Event Object
                var originalEventObject = $(this).data('eventObject');
                // we need to copy it, so that multiple events don't have a reference to the same object
                var copiedEventObject = $.extend({}, originalEventObject);
                // assign it the date that was reported
                copiedEventObject.start = date;
                copiedEventObject.allDay = allDay;
                // render the event on the calendar
                // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
                $('#calendar').fullCalendar('renderEvent', copiedEventObject, true);
                // is the "remove after drop" checkbox checked?
                if ($('#drop-remove').is(':checked')) {
                    // if so, remove the element from the "Draggable Events" list
                    $(this).remove();
                }
            },
            eventClick: function (e) {
                            var deleteMsg = confirm("Do you really want to delete?");
                            if (deleteMsg) {
                    $(this).hide();
                                    $.ajax({
                                            type: "POST",
                                            url: "<?php echo site_url('schedule_time/delete_schedule'); ?>",
                                            data: "&schedule_id=" + e.scheduleId,
                                            success: function (response) {
                                                     var res = JSON.parse(response);
                                                     if (res.Message == "Success") {
//                                                            $('#calendar').fullCalendar('removeEvents', e.scheduleId);
                                                            alert("Deleted Successfully");
                                                    } else {
                                                            alert("Error in delete.");
                                                            return false;
                                                    }
                                            }
                                    });
                            }
                    }
        });

    });

</script>