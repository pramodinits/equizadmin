<div class="container">
    <?php
    $lang = $this->config->item('question_lang');
    ?>

    <h3><?php echo $title; ?></h3>



    <div class="row">
        <form method="post" id="qf"  action="<?php echo site_url('qbank/new_question_7/' . $nop . '/' . $para); ?>" enctype="multipart/form-data">

            <div class="col-md-12">
                <br> 
                <div class="login-panel panel panel-default">
                    <div class="panel-body"> 



                        <?php
                        if ($this->session->flashdata('message')) {
                            echo $this->session->flashdata('message');
                        }
                        ?>	



                        <div class="form-group">	 

                            <?php echo $this->lang->line('multiple_choice_multiple_answer'); ?>
                        </div>

                        <div class="form-group">	 
                            <label><?php echo $this->lang->line('select_category'); ?></label> 
                            <select class="form-control" name="question_category">
                                <option value=""><?php echo $this->lang->line('select_category'); ?></option>
                                <?php
                                foreach ($que_category as $key => $val) {
                                    ?>
                                    <option value="<?php echo $key; ?>"><?php echo $val; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label><?php echo $this->lang->line('exam_type'); ?></label> 
                            <select class="form-control" name="test_type">
                                <option value=""><?php echo $this->lang->line('examtype_select'); ?></option>
                                <?php
                                foreach ($exam_type as $key => $val) {
                                    ?>
                                    <option value="<?php echo $key; ?>"><?php echo $val; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label><?php echo $this->lang->line('question_module'); ?></label> 
                            <select class="form-control" name="question_module" onchange="showVideoModule(this)">
                                <option value=""><?php echo $this->lang->line('select_module'); ?></option>
                                <?php
                                foreach ($question_module as $key => $val) {
                                    ?>
                                    <option value="<?php echo $key; ?>"><?php echo $val; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group" id="audiovideo_flag" style="display: none;">	 
                            <label   >Video Flag</label> 
                            <select class="form-control" name="video_flg" id="video_flg" onchange="showVideo(this)">
                                <option value="">Select Media Type</option>
                                <option value="1">Video Upload</option>
                                <option value="2">Video URL</option>
                            </select>
                        </div>
                        <div class="form-group videodiv" id="video_file_div" style="display: none;" >	 
                            <label   >Video</label> 
                            <input type="file" id="video_file" name="video_file"  />
                        </div>
                        <div class="form-group videodiv" id="video_url_div" style="display: none;" >	 
                            <label>Video URL </label> 
                            <input type="text" id="video_url" name="video_url"  />
                        </div>
                        <div class="form-group" style="display: none;">	 
                            <label   ><?php echo $this->lang->line('select_category'); ?></label> 
                            <select class="form-control" name="cid">
                                <?php
                                foreach ($category_list as $key => $val) {
                                    ?>

                                    <option value="<?php echo $val['cid']; ?>"><?php echo $val['category_name']; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group" style="display: none;">	 
                            <label   ><?php echo $this->lang->line('select_level'); ?></label> 
                            <select class="form-control" name="lid">
                                <?php
                                foreach ($level_list as $key => $val) {
                                    ?>

                                    <option value="<?php echo $val['lid']; ?>"><?php echo $val['level_name']; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group">	 
                            <label   >Time (in seconds) </label> 
                            <input class="form-control" type="text" id="time" name="time"  />
                        </div>
                        <?php
                        if ($para == 1) {
                            foreach ($lang as $lkey => $val) {
                                $lno = $lkey;
                                if ($lkey == 0) {
                                    $lno = "";
                                }
                                ?>
                                <div class="col-lg-6">

                                    <div class="form-group">	 
                                        <label for="paragraph"  ><?php echo $this->lang->line('paragraph') . ' : ' . $val; ?></label> 
                                        <textarea  name="paragraph<?php echo $lno; ?>"  class="form-control"   ><?php
                                            if (isset($qp)) {
                                                echo $qp['paragraph' . $lno];
                                            }
                                            ?></textarea>
                                    </div>
                                </div>


                                <?php
                            }
                        }
                        ?>			

                        <?php
                        foreach ($lang as $lkey => $val) {
                            $lno = $lkey;
                            if ($lkey == 0) {
                                $lno = "";
                            }
                            ?>
                            <div class="col-lg-6">			


                                <div class="form-group">	 
                                    <label for="inputEmail"  ><?php echo $this->lang->line('question') . ' : ' . $val; ?></label> 
                                    <textarea  name="question<?php echo $lno; ?>"  class="form-control tinymcetextarea"   ></textarea>
                                </div>
                            </div>		
                            <?php
                        }
                        foreach ($lang as $lkey => $val) {
                            $lno = $lkey;
                            if ($lkey == 0) {
                                $lno = "";
                            }
                            ?>	<div class="col-lg-6">		


                                <div class="form-group">	 
                                    <label for="description"  ><?php echo $this->lang->line('description') . ' : ' . $val; ?></label> 
                                    <textarea  name="description<?php echo $lno; ?>"  class="form-control tinymcetextarea"></textarea>
                                </div>
                            </div>		
                            <?php
                        }
                        ?>
                        <?php
                        for ($i = 1; $i <= $nop; $i++) {
                            ?>
                            <div class="row">
                                <?php
                                foreach ($lang as $lkey => $val) {
                                    $lno = $lkey;
                                    if ($lkey == 0) {
                                        $lno = "";
                                    }
                                    ?>
                                    <div class="col-lg-6">


                                        <div class="form-group">	 
                                            <label for="Option"  ><?php echo $this->lang->line('options'); ?> <?php echo $i; ?>) <?php echo ' : ' . $val; ?></label>
                                            <span style="display: none;">
                                                <?php if ($lkey == 0) { ?><input type="checkbox" name="score[]" value="<?php echo $i - 1; ?>" <?php
                                                    if ($i == 1) {
                                                        echo 'checked';
                                                    }
                                                    ?> > Select Correct Option 
                                                       <?php } ?>
                                            </span>
                                            <textarea  name="option<?php echo $lno; ?>[]"  class="form-control"   ></textarea>
                                        </div>
                                    </div>
                                    <?php
                                }
                                ?></div>
                            <?php
                        }
                        ?>
                        <input type="hidden" name="parag" id="parag" value="0">
                        <button class="btn btn-default" type="submit"><?php echo $this->lang->line('submit'); ?></button>
                        <?php
                        if ($para == 1) {
                            ?>	<button class="btn btn-default"  type="button"  onClick="javascript:parag();"><?php echo $this->lang->line('submit&add'); ?></button>
                        <?php } ?>
                    </div>
                </div>




            </div>
        </form>
    </div>







</div>
<script>
    function parags() {
        $('#parag').val('1');
        $('#qf').submit();
    }

    function showVideoModule(e) {
        if ($(e).val() == 3 || $(e).val() == 2) {
            $("#audiovideo_flag").show();
        } else {
            $("#audiovideo_flag").hide();
            $("#video_url_div").hide();
            $("#video_file_div").hide();
        }
    }
    
    function showVideo(e) {
        if ($(e).val() == '') {
            $('.videodiv').hide();
        } else if ($(e).val() == 1) {
            $('#video_url_div').hide();
            $('#video_file_div').show();
        } else if ($(e).val() == 2) {
            $('#video_url_div').show();
            $('#video_file_div').hide();
        }

    }
</script>
