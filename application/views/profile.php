<style>
    .content-wrapper-before {
        height: 270px !important;
    }
    .card-title {
        border-bottom: none !important;
        line-height: 1rem !important;
    }
    h6.info {
        font-weight: 600;
    }
</style>
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-wrapper-before"></div>
        <div class="content-header row">

        </div>
        <div class="content-body">
            <div id="user-profile">
                <div class="row">
                    <div class="col-sm-12 col-xl-8">
                        <div class="media d-flex m-1">
                            <div class="align-left p-1">
                                <a class="profile-image" href="#">
                                    <img class="rounded-circle img-border height-100" src="<?= base_url('/images/profile.png') ?>">
                                </a>
                            </div>
                            <div class="media-body text-left mt-1">
                                <div class="media-body text-left mt-1">
                                    <h3 class="font-large-1 white"><?= $result['first_name'] . ' ' . $result['last_name'] ?>
                                        <!--<span class="font-medium-1 white">(Project manager)</span>-->
                                    </h3>
                                    <p class="white">
                                        <i class="la la-map-marker white"> </i> <?= $result['city']; ?> 
                                    </p>
                                    <p class="white">
                                        <i class="la la-envelope white"> </i> <?= $result['email']; ?> 
                                    </p>
                                    <p class="white">
                                        <i class="la la-phone-square white"> </i> <?= $result['contact_no']; ?> 
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-3 col-lg-5 col-md-12">
                        <div class="card">
                            <div class="card-header pb-0">
                                <div class="card-title-wrap bar-primary">
                                    <div class="card-title"><?php echo $this->lang->line('profile'); ?></div>
                                    <hr>
                                </div>
                            </div>
                            <div class="card-content">
                                <div class="card-body p-0 pt-0 pb-1">
                                    <ul>
                                        <li>
                                            <strong>
                                                <?php echo $this->lang->line('exam_category'); ?>
                                            </strong><br/>
                                            <?= $que_category[$result['exam_category']] ?>
                                        </li>
                                        <li>
                                            <strong><?php echo $this->lang->line('account_type'); ?></strong>
                                            <?php
                                            if ($result['su'] == 1) {
                                                echo $this->lang->line('administrator');
                                            } else {
                                                echo $this->lang->line('user');
                                            }
                                            ?>
                                        </li>
                                        <li>
                                            <strong>
                                                <?php echo $this->lang->line('joined'); ?>
                                            </strong>
                                            <?= date('d-m-Y', strtotime($result['registered_date'])); ?>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header pb-0">
                                <div class="card-title-wrap bar-primary">
                                    <div class="card-title"><?= $this->lang->line('account_status'); ?></div>
                                    <hr>
                                </div>
                            </div>
                            <div class="card-content">
                                <div class="card-body p-0 pt-0 pb-1">
                                    <ul>
                                        <li>
                                            <strong><?= $this->lang->line('phone_verified'); ?></strong>
                                            <i class="
                                            <?= $result['phone_verify'] == 1 ? "la la-check success" : "la la-close danger" ?>
                                               font-weight-bold font-medium-1"></i>
                                        </li>
                                        <li>
                                            <strong><?= $this->lang->line('email_verified'); ?></strong>
                                            <i class="
                                            <?= $result['email_verify'] == 1 ? "la la-check success" : "la la-close danger" ?>
                                               font-weight-bold font-medium-1"></i>
                                        </li>
                                        <li>
                                            <strong><?= $this->lang->line('doc_verified'); ?></strong>
                                            <i class="
                                            <?= $result['licensedoc_verify'] == 1 ? "la la-check success" : "la la-close danger" ?>
                                               font-weight-bold font-medium-1"></i>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-9 col-lg-7 col-md-12">
                        <div id="timeline">
                            <div class="card">
                                <div class="card-body">
                                    <div class="timeline">
                                        <h4><?= $this->lang->line('user_info'); ?></h4>
                                        <hr>
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <h6 class="text-bold-600">
                                                        <?php echo $this->lang->line('company'); ?> :
                                                    </h6>
                                                </div>
                                                <div class="col-md-7">
                                                    <h6 class="info"><?= $result['company']; ?></h6>
                                                </div>
                                            </div>
                                            <div class="row mt-1">
                                                <div class="col-md-4">
                                                    <h6 class="text-bold-600">
                                                        <?php echo $this->lang->line('license_type'); ?> :
                                                    </h6>
                                                </div>
                                                <div class="col-md-7">
                                                    <h6 class="info"><?= $result['license_type']; ?></h6>
                                                </div>
                                            </div>
                                            <div class="row mt-1">
                                                <div class="col-md-4">
                                                    <h6 class="text-bold-600">
                                                        <?php echo $this->lang->line('license_number'); ?> :
                                                    </h6>
                                                </div>
                                                <div class="col-md-7">
                                                    <h6 class="info"><?= $result['license_number'] ? $result['license_number'] : "N/A"; ?></h6>
                                                </div>
                                            </div>

                                            <div class="row mt-1">
                                                <div class="col-md-4">
                                                    <h6 class="text-bold-600">
                                                        <?php echo $this->lang->line('license_country'); ?> :
                                                    </h6>
                                                </div>
                                                <div class="col-md-7">
                                                    <h6 class="info"><?= $result['license_country']; ?></h6>
                                                </div>
                                            </div>
                                            <div class="row mt-1">
                                                <div class="col-md-4">
                                                    <h6 class="text-bold-600">
                                                        <?php echo $this->lang->line('license_doc'); ?> :
                                                    </h6>
                                                </div>
                                                <div class="col-md-7">
                                                    <h6 class="info">
                                                        <?php if ($result['license_doc']) { ?>
                                                            <a href="<?= base_url('license_document/' . $result['license_doc']) ?>" download="">
                                                                <?= $result['license_doc']; ?>
                                                            </a>
                                                            <?php
                                                        } else {
                                                            echo "N/A";
                                                        }
                                                        ?>
                                                    </h6>
                                                </div>
                                            </div>
                                            <div class="row mt-1">
                                                <div class="col-md-4">
                                                    <h6 class="text-bold-600">
                                                        <?php echo $this->lang->line('vat'); ?> :
                                                    </h6>
                                                </div>
                                                <div class="col-md-7">
                                                    <h6 class="info"><?= $result['vat']; ?></h6>
                                                </div>
                                            </div>
                                            <div class="row mt-1">
                                                <div class="col-md-4">
                                                    <h6 class="text-bold-600">
                                                        <?php echo $this->lang->line('city'); ?> :
                                                    </h6>
                                                </div>
                                                <div class="col-md-7">
                                                    <h6 class="info"><?= $result['city']; ?></h6>
                                                </div>
                                            </div>
                                            <div class="row mt-1">
                                                <div class="col-md-4">
                                                    <h6 class="text-bold-600">
                                                        <?php echo $this->lang->line('address'); ?> :
                                                    </h6>
                                                </div>
                                                <div class="col-md-7">
                                                    <h6 class="info"><?= $result['address']; ?></h6>
                                                </div>
                                            </div>
                                            <div class="row mt-1">
                                                <div class="col-md-4">
                                                    <h6 class="text-bold-600">
                                                        <?php echo $this->lang->line('postal_code'); ?> :
                                                    </h6>
                                                </div>
                                                <div class="col-md-7">
                                                    <h6 class="info"><?= $result['postal_code']; ?></h6>
                                                </div>
                                            </div>
                                            <div class="row mt-1">
                                                <div class="col-md-4">
                                                    <h6 class="text-bold-600">
                                                        <?php echo $this->lang->line('country'); ?> :
                                                    </h6>
                                                </div>
                                                <div class="col-md-7">
                                                    <h6 class="info"><?= $result['country']; ?></h6>
                                                </div>
                                            </div>
                                            <div class="row mt-1">
                                                <div class="col-md-4">
                                                    <h6 class="text-bold-600">
                                                        <?php echo $this->lang->line('dob'); ?> :
                                                    </h6>
                                                </div>
                                                <div class="col-md-7">
                                                    <h6 class="info"><?= date('d-m-Y', strtotime($result['dob'])); ?></h6>
                                                </div>
                                            </div>
                                        </div>
                                        <hr>
<!--                                        <div class="col-md-12">
                                            <h4><?= $this->lang->line('profile_subscription'); ?></h4>
                                        </div>-->
                                    </div>
                                </div>
                            </div>
                        </div>  
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>