<!--<script src="//code.jquery.com/jquery-3.1.0.min.js"></script>-->
<!--<script src="<?php echo base_url(); ?>vendor/jquery/jquery.min.js"></script>-->
<style>
    .btn-default {
        background: #05038b;
        color: #fff;
        font-weight: bold;
        margin: 0 auto;
    }
</style>

<!--updated theme-->
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-wrapper-before"></div>
        <div class="content-header row">
            <div class="content-header-left col-md-4 col-12 mb-2">
                <h3 class="content-header-title"></h3>
            </div>
        </div>
        <div class="content-body">
            <section id="horizontal-form-layouts">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-content collpase collapse show">
                                <div class="card-body">
                                    <form method="post" id="register_user" name="register_user" action="<?php echo site_url('examiner/insert_examiner/'); ?>">
                                        <div class="form-body">
                                            <!--<h4 class="form-section"><?php echo $title; ?></h4>-->
                                            <input type="hidden" name="uid" value="<?= @$result['uid'] ?>"/>
                                            <div class="col-md-12">
                                                <br>
                                                <div class="login-panel panel panel-default">
                                                    <div class="panel-body">
                                                        <?php
                                                        if ($this->session->flashdata('message')) {
                                                            echo $this->session->flashdata('message');
                                                        }
                                                        ?>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group row">
                                                                    <label class="col-md-3 label-control"><?php echo $this->lang->line('first_name'); ?></label> 
                                                                    <div class="col-md-9">
                                                                        <input type="text" value="<?= @$result['first_name'] ?>"  name="examiner[first_name]"  class="form-control border-primary" placeholder="<?php echo $this->lang->line('first_name'); ?>" required  autofocus>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label class="col-md-3 label-control"><?php echo $this->lang->line('contact_no'); ?></label> 
                                                                    <div class="col-md-9">
                                                                        <input type="text" value="<?= @$result['contact_no'] ?>" required name="examiner[contact_no]"  class="form-control border-primary" placeholder="<?php echo $this->lang->line('contact_no'); ?>"   autofocus>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label class="col-md-3 label-control"><?php echo $this->lang->line('email'); ?></label> 
                                                                    <div class="col-md-9">
                                                                        <input type="email" value="<?= @$result['email'] ?>" id="inputEmail" required name="email" class="form-control border-primary" placeholder="<?php echo $this->lang->line('email'); ?>"   autofocus>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row">	 
                                                                    <label class="col-md-3 label-control"><?php echo $this->lang->line('city'); ?></label> 
                                                                    <div class="col-md-9">
                                                                        <select class="form-control border-primary" name="examiner[id_city]" required>
                                                                            <option value=""><?php echo $this->lang->line('select_city'); ?></option>
                                                                            <?php
                                                                            foreach ($city_list as $k => $v) {
                                                                                ?>
                                                                                <option value="<?php echo $k; ?>" <?= @$result['id_city'] == $k ? "selected" : "" ?>><?php echo $v; ?></option>
                                                                                <?php
                                                                            }
                                                                            ?>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group row">
                                                                    <label class="col-md-3 label-control"><?php echo $this->lang->line('last_name'); ?></label> 
                                                                    <div class="col-md-9">
                                                                        <input type="text" value="<?= @$result['last_name'] ?>" name="examiner[last_name]"  class="form-control border-primary" placeholder="<?php echo $this->lang->line('last_name'); ?>" required  autofocus>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row d-none">	 
                                                                    <label class="col-md-3 label-control"><?php echo $this->lang->line('exam_category'); ?></label> 
                                                                    <div class="col-md-9">
                                                                        <select class="form-control" name="examiner[exam_category]" required >
                                                                            <option value=""><?php echo $this->lang->line('select_category'); ?></option>
                                                                            <?php
                                                                            foreach ($exam_category as $key => $val) {
                                                                                ?>
                                                                                <option value="<?php echo $key; ?>" <?= @$result['exam_category'] == $key ? "selected" : "" ?>><?php echo $val; ?></option>
                                                                                <?php
                                                                            }
                                                                            ?>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row <?= @$result['su'] ? "d-none" : "" ?>">	 
                                                                    <label class="col-md-3 label-control"><?php echo $this->lang->line('userrole'); ?></label> 
                                                                    <div class="col-md-9">
                                                                        <select class="form-control border-primary" name="examiner[su]" required>
                                                                            <option value=""><?php echo $this->lang->line('select_userrole'); ?></option>
                                                                            <?php
                                                                            foreach ($examiner_userrole as $key => $val) {
                                                                                ?>
                                                                                <option value="<?php echo $key; ?>" <?= @$result['su'] == $key ? "selected" : "" ?>><?php echo $val; ?></option>
                                                                                <?php
                                                                            }
                                                                            ?>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row">	 
                                                                    <label class="col-md-3 label-control"><?php echo $this->lang->line('select_testcenter'); ?></label> 
                                                                    <div class="col-md-9">
                                                                        <select class="form-control border-primary" name="examiner[id_testcenter]" required>
                                                                            <option value=""><?php echo $this->lang->line('select_testcenter'); ?></option>
                                                                            <option value="1" <?= @$result['id_testcenter'] == 1 ? "selected" : "" ?>><?php echo $this->lang->line('all'); ?></option>
                                                                            <?php
                                                                            foreach ($testcenter_list as $key => $val) {
                                                                                ?>
                                                                                <option value="<?php echo $key; ?>" <?= @$result['id_testcenter'] == $key ? "selected" : "" ?>><?php echo $val; ?></option>
                                                                                <?php
                                                                            }
                                                                            ?>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <?php if (@$result['password']) { ?>
                                                                <div class="form-group text-right" id="show_password">
                                                                    <p><a href="javascript:void(0)">Do you want to change Password?</a></p>
                                                                </div>
                                                                <?php } ?>
                                                                <div id="password" class="form-group row" style="<?= @$result['password'] ? "display:none;" : "" ?>">	 
                                                                    <label class="col-md-3 label-control"><?php echo $this->lang->line('password'); ?></label> 
                                                                    <div class="col-md-9">
                                                                        <input type="password" required id="password" name="password" class="form-control border-primary" placeholder="<?php echo $this->lang->line('password'); ?>" autofocus>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-actions right">
                                            <button class="btn btn-primary" type="submit">
                                                <i class="la la-check-square-o"></i>
                                                <?php echo $this->lang->line('submit'); ?>
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
<!--updated theme-->

<script>
    // getexpiry();
</script>
<!--<script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>-->

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/additional-methods.js"></script>
<script>
    $(document).ready(function () {
        $("#show_password a").click(function () {
            $("#password").toggle();
        });
        
        $('#register_user').validate({// initialize the plugin
            rules: {
                'examiner[first_name]': {
                    required: true,
                    minlength: 3,
                    lettersonly: true
                },
                'examiner[last_name]': {
                    required: true,
                    minlength: 3,
                    lettersonly: true
                },
                'examiner[contact_no]': {
                    required: true,
                    digits: true
                }
            },
            submitHandler: function (form) { // for demo
                $('button[type=submit]').attr('disabled', 'disabled');
                return true; // for demo
            }
        });
    });
</script>