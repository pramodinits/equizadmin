<html class="loading" lang="en" data-textdirection="ltr">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <meta name="description" content="Chameleon Admin is a modern Bootstrap 4 webapp &amp; admin dashboard html template with a large number of components, elegant design, clean and organized code.">
        <meta name="keywords" content="admin template, Chameleon admin template, dashboard template, gradient admin template, responsive admin template, webapp, eCommerce dashboard, analytic dashboard">
        <meta name="author" content="ThemeSelect">
        <title> <?php echo $title; ?></title>
        <link rel="apple-touch-icon" href="<?php echo base_url(); ?>theme-assets/images/ico/apple-icon-120.png">
        <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url(); ?>images/favicon_gaca.ico">
        <link href="https://fonts.googleapis.com/css?family=Muli:300,300i,400,400i,600,600i,700,700i%7CComfortaa:300,400,700" rel="stylesheet">
        <link href="https://maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome.min.css" rel="stylesheet">
        <!-- BEGIN VENDOR CSS-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>theme-assets/css/vendors.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>theme-assets/css/vendors.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>theme-assets/vendors/css/charts/chartist.css">
        <!-- END VENDOR CSS-->
        <!-- BEGIN CHAMELEON  CSS-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>theme-assets/css/app-lite.css">
        <!-- END CHAMELEON  CSS-->
        <!-- BEGIN Page Level CSS-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>theme-assets/css/core/menu/menu-types/vertical-menu.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>theme-assets/css/core/colors/palette-gradient.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>theme-assets/css/pages/dashboard-ecommerce.css">
        <!-- END Page Level CSS-->
        <!-- BEGIN Custom CSS-->
        <!-- END Custom CSS-->
        <script src="//code.jquery.com/jquery-3.1.0.min.js"></script>
        <style>
            body.vertical-layout.vertical-menu.menu-expanded .main-menu {
                width: 280px !important;
            }
            .main-menu .main-menu-content {
                overflow: auto;
            }
            .main-menu.menu-light .navigation > li > a {
                padding: 10px 0px 10px 10px !important;
            }
            .main-menu.menu-light .navigation > li {
                line-height: 0.5rem !important;
            }
            body.vertical-layout.vertical-menu.menu-expanded .main-menu.menu-light .navigation > li > a > i {
                width: 30px !important;
                height: 30px !important;
                margin-right: 5px !important;
                line-height: 32px !important;
                /*display: none;*/
            }
            .main-menu ul {
                height: auto !important;
            }
            .main-menu.menu-light .navigation > li ul li > a {
                padding: 8px 18px 8px 60px !important;
            }
            .main-menu.menu-light .navigation > li.open .hover > a::before,
            .main-menu.menu-light .navigation > li ul .active > a::before {
                left: 55px !important;
            }
            body.vertical-layout[data-color="bg-chartbg"] .navbar-container {
                background: linear-gradient(to right,#9f78ff,#32cafe) !important;
            }
            .content-wrapper-before {
                height: 110px !important;
            }
            .table-responsive > .table-bordered {
                font-size: 14px !important;
            }
            .table th, .table td {
                padding: .75rem 1rem !important;
            }
            .page-link-active {
                position: relative;
                display: block;
                padding: .5rem .75rem;
                margin-left: -1px;
                line-height: 1.25;
                color: #224abe;
                background-color: #d4d9ee;
                border: 1px solid #dddfeb;
            }
            .card-header .card-title {
                line-height: 3rem;
                margin-bottom: 20px;
                color:#2c303b;
                border-bottom: 1px solid #2c303b;
            }
            label.error {
                color: #9f1212 !important;
                margin-top: 1%;
                font-size: 15px !important;
                width: 100% !important;
            }
            .form-control {
                padding: .55rem 1.5rem;
            }
            textarea.error, select.error, input[type=text].error, input[type=email].error, input[type=password].error {
                border: 1px solid #9f1212 !important;
            }
            .fc-event {
                font-size: 1em !important;
                color:#fff !important;
                font-weight: bold;
                background-color: #1a7baa !important;
            }
            .pagination {
                padding: 0 !important;
            }
        </style>
        <script>
            $(function () {
                $('.alert').delay(3000).show().fadeOut('slow');
            });
        </script>
        <!-- BEGIN VENDOR JS-->
        <script src="<?php echo base_url(); ?>theme-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>theme-assets/js/core/app-menu-lite.js" type="text/javascript"></script>
        <!-- BEGIN VENDOR JS-->
        <!-- BEGIN PAGE VENDOR JS-->
        <script src="<?php echo base_url(); ?>theme-assets/vendors/js/charts/chartist.min.js" type="text/javascript"></script>
        <!-- END PAGE VENDOR JS-->
        <!-- BEGIN CHAMELEON  JS-->
        <script src="<?php echo base_url(); ?>theme-assets/js/core/app-menu-lite.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>theme-assets/js/core/app-lite.js" type="text/javascript"></script>
        <!-- END CHAMELEON  JS-->
        <!-- BEGIN PAGE LEVEL JS-->
        <script src="<?php echo base_url(); ?>theme-assets/js/scripts/pages/dashboard-lite.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL JS-->
    </head>
    <?php
    if ($this->session->userdata('logged_in')) {
        $body_class = "vertical-layout vertical-menu 2-columns menu-expanded fixed-navbar";
        $data_col = "2-columns";
    } else {
        $body_class = "vertical-layout vertical-menu 1-column  bg-full-screen-image blank-page blank-page  pace-done";
        $data_col = "1-column";
    }
    $logged_in = $this->session->userdata('logged_in');
    $this->config->load('masterdata');
    $this->config->load('menudata');
    $role_menu = $this->config->item('role_menu');
    $admin_menu = $this->config->item('admin_menu');
    
    $access = json_decode($logged_in['permission_access'], true);
//                    print "<pre>";
//                    print_r($admin_menu);
//                    print_r($access); 
//                    exit();
    ?>

    <body class="<?= $body_class; ?>" data-open="click" data-menu="vertical-menu" data-color="bg-gradient-x-purple-blue" data-col="<?= $data_col ?>">
        <?php
        if ($this->session->userdata('logged_in')) {
            if (($this->uri->segment(1) . '/' . $this->uri->segment(2)) != 'quiz/attempt') {
                $logged_in = $this->session->userdata('logged_in');
                $hquery = $this->db->query(" select * from kamsquiz_setting where setting_name='App_Name' || setting_name='App_title' order by setting_id asc ");
                $hres = $hquery->result_Array();
//                $show_menu = $logged_in['su'] == 1 && $logged_in['su'] < 7 ? "display:block;" : "display:none;";
                if ($logged_in['su'] == 1) {
                    $show_menu = "display:block";
                } elseif ($logged_in['su'] < 7 && $logged_in['su'] != 1) {
                    $show_menu = "display:none";
                } elseif ($logged_in['su'] >= 7) {
                    $role_menu = $this->config->item('role_menu');
                    $access = json_decode($logged_in['permission_access'], true);
//                    print_r($access);exit;
                    foreach ($role_menu as $key => $value) {
                        foreach ($value as $k => $v) {
                            $show_management_menu = array_key_exists($k, $access[str_replace(' ', '_', $key)]) ? ' display:block;' : 'display:none;';
                        }
                    }
                }
                
                ?>
                <!-- fixed-top-->
                <nav class="header-navbar navbar-expand-md navbar navbar-with-menu navbar-without-dd-arrow fixed-top navbar-semi-light">
                    <div class="navbar-wrapper">
                        <div class="navbar-container content">
                            <div class="collapse navbar-collapse show" id="navbar-mobile">
                                <ul class="nav navbar-nav mr-auto float-left">
                                    <li class="nav-item d-block d-md-none"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu"></i></a></li>
                                    <li class="nav-item d-none"><a class="nav-link nav-link-expand" href="#"><i class="ficon ft-maximize"></i></a></li>
                                    <li class="nav-item dropdown navbar-search">
                                        <ul class="dropdown-menu">
                                            <li class="arrow_box">
                                                <form style="display: none;">
                                                    <div class="input-group search-box">
                                                        <div class="position-relative has-icon-right full-width">
                                                            <input class="form-control" id="search" type="text" placeholder="Search here...">
                                                            <div class="form-control-position navbar-search-close"><i class="ft-x">   </i></div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="dropdown nav-item mega-dropdown d-none d-md-block">
                                        <a class="dropdown-toggle nav-link" href="#" data-toggle="dropdown">
                                            <h3 class="text-white"><?= $title; ?></h3>
                                        </a>
                                    </li>
                                </ul>
                                <ul class="nav navbar-nav float-right">
                                    <li class="dropdown dropdown-user nav-item">
                                        <a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown">             
                                            <span class="avatar avatar-online"><img src="<?php echo base_url(); ?>images/profile.png" alt="avatar"><i></i></span>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right width-200">
                                            <div class="arrow_box_right">
                                                <a class="dropdown-item" href="#">
                                                    <span class="avatar avatar-online">
                                                        <!--<img src="<?php echo base_url(); ?>theme-assets/images/portrait/small/avatar-s-19.png" alt="avatar">-->
                                                        <span class="user-name text-bold-700 ml-1"><?= $logged_in['first_name'] . " " . $logged_in['last_name']; ?></span>
                                                    </span>
                                                </a>
                                                <!--                                        <div class="dropdown-divider"></div>
                                                                                        <a class="dropdown-item" href="#"><i class="ft-user"></i> Edit Profile</a>
                                                                                        <a class="dropdown-item" href="#"><i class="ft-mail"></i> My Inbox</a>
                                                                                        <a class="dropdown-item" href="#"><i class="ft-check-square"></i> Task</a>
                                                                                        <a class="dropdown-item" href="#"><i class="ft-message-square"></i> Chats</a>-->
                                                <div class="dropdown-divider"></div><a class="dropdown-item" href="<?php echo site_url('user/logout'); ?>"><i class="ft-power"></i> Logout</a>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </nav>

                <!-- ////////////////////////////////////////////////////////////////////////////-->


                <div class="main-menu menu-fixed menu-light menu-accordion menu-shadow " data-scroll-to-active="true"
                     data-img="<?php echo base_url(); ?>theme-assets/images/backgrounds/02.jpg">

                    <div class="navbar-header">
                        <ul class="nav navbar-nav flex-row">       
                            <li class="nav-item mr-auto">
                                <a class="navbar-brand" href="<?php echo base_url(); ?>dashboard">
                                    <img class="img-responsive" alt="Admin logo" src="<?php echo base_url(); ?>images/logo_blue.png"/>
                                    <h3 class="brand-text d-none">
                                        <?php if ($hres[0]['setting_value'] == "") { ?>GACA<?php
                                        } else {
                                            echo $hres[0]['setting_value'];
                                        }
                                        ?>
                                    </h3>
                                </a>
                            </li>
                            <li class="nav-item d-md-none"><a class="nav-link close-navbar"><i class="ft-x"></i></a></li>
                        </ul>
                    </div>
                    <div class="main-menu-content">
                        
                        <?php 
                        
                        if ($logged_in['su'] < 7) { ?>
                            <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
                                <li style="<?= @$show_menu ?>" class="d-none">
                                    <a href="<?php echo site_url('roles_permission'); ?>"><i class="la la-dashboard"></i>
                                        <span class="menu-title" data-i18n=""><?= $this->lang->line('roles_permission'); ?></span>
                                    </a>
                                </li>
                                <?php //if (in_array('All', explode(',', $logged_in['setting']))) { ?>
                                <li class="nav-item 
                                <?php
                                if (($this->router->class == 'dashboard') && (($this->router->method == 'index'))) {
                                    echo "open";
                                }
                                ?>
                                    ">
                                    <a href="<?php echo site_url('dashboard'); ?>"><i class="la la-dashboard"></i>
                                        <span class="menu-title" data-i18n=""><?= $this->lang->line('dashboard'); ?></span>
                                    </a>
                                </li>
                                <?php //} ?>
                                <!--management user-->
                                <li class="has-sub nav-item
                                <?php
                                if (($this->router->class == 'management_user') && (($this->router->method == 'new_management_user') || ($this->router->method == 'index') || ($this->router->method == 'roles_permission'))) {
                                    echo "open";
                                }
                                ?>
                                    " style="<?= $logged_in['su'] == 1 || $logged_in['su'] == 3 ? "display:block" : "display:none" ?>">
                                    <a href="#">
                                        <i class="la la-user"></i>
                                        <span class="menu-title" data-i18n=""><?= $this->lang->line('management_user'); ?></span>
                                    </a>
                                    <ul class="menu-content">

                                        <li class="<?php
                                        if (($this->router->class == 'management_user') && (($this->router->method == 'new_management_user') || ($this->router->method == 'roles_permission'))) {
                                            echo "active";
                                        }
                                        ?>">
                                            <a class="menu-item" href="<?= site_url('management_user/new_management_user/'); ?>"><?= $this->lang->line('add_new'); ?></a>
                                        </li>

                                        <li class="<?php
                                        if (($this->router->class == 'management_user') && (($this->router->method == 'index'))) {
                                            echo "active";
                                        }
                                        ?>">
                                            <a class="menu-item" href="<?= site_url('management_user'); ?>"><?= $this->lang->line('management_user'); ?> <?= $this->lang->line('list'); ?></a>
                                        </li>
                                    </ul>
                                </li>
                                <!--management user-->
                                <?php
                                //if (in_array('List_all', explode(',', $logged_in['users']))) {
                                    ?>
                                    <li class="nav-item has-sub 
                                    <?php
                                    if (($this->router->class == 'user')  || ($this->router->class == 'user2')
                                            && (($this->router->method == 'view_user') || ($this->router->method == 'index'))) {
                                        echo "open";
                                    }
                                    ?>
                                        ">
                                        <a href="#">
                                            <i class="la la-users"></i>
                                            <span class="menu-title" data-i18n=""><?= $this->lang->line('users'); ?></span>
                                        </a>
                                        <ul class="menu-content">
                                            <?php
                                            if (in_array('Add', explode(',', $logged_in['users']))) {
                                                ?>
                                                <li class="<?php
                                                if (($this->router->class == 'user') && ($this->router->method == 'new_user')) {
                                                    echo "active";
                                                }
                                                ?> d-none">
                                                    <a class="menu-item" href="<?= site_url('user/new_user'); ?>"><?= $this->lang->line('add_new'); ?></a>
                                                </li>
                                            <?php } ?>
                                            <?php
                                            //if (in_array('List', explode(',', $logged_in['users'])) || in_array('List_all', explode(',', $logged_in['users']))) {
                                                // echo $match_val = $this->lang->line('applicant')[$this->lang->line('list')];
                                                ?>
                                                <li class="<?php
                                                if (($this->router->class == 'user') || ($this->router->class == 'user2') 
                                                        && (($this->router->method == 'index') || ($this->router->method == 'view_user'))) {
                                                    echo "active";
                                                }
                                                ?>" style="">
                                                    <a class="menu-item" href="<?= site_url('user'); ?>"><?= $this->lang->line('users'); ?> <?= $this->lang->line('list'); ?></a>
                                                </li>
                                                <?php
                                            //}
                                            ?>
                                        </ul>
                                    </li>
                                    <li class="has-sub nav-item
                                    <?php
                                    if (($this->router->class == 'examiner') && (($this->router->method == 'new_examiner') || ($this->router->method == 'index'))) {
                                        echo "open";
                                    }
                                    ?>
                                        " style="<?= $show_menu ?>">
                                        <a href="#">
                                            <i class="la la-user"></i>
                                            <span class="menu-title" data-i18n=""><?= $this->lang->line('examinerscorer'); ?></span>
                                        </a>
                                        <ul class="menu-content">
                                            <?php
                                            //if (in_array('Add', explode(',', $logged_in['users']))) {
                                                ?>
                                                <li class="<?php
                                                if (($this->router->class == 'examiner') && (($this->router->method == 'new_examiner'))) {
                                                    echo "active";
                                                }
                                                ?>">
                                                    <a class="menu-item" href="<?= site_url('examiner/new_examiner/'); ?>"><?= $this->lang->line('add_new'); ?></a>
                                                </li>
                                            <?php //} ?>
                                            <?php
                                            //if (in_array('List', explode(',', $logged_in['users'])) || in_array('List_all', explode(',', $logged_in['users']))) {
                                                ?>
                                                <li class="<?php
                                                if (($this->router->class == 'examiner') && (($this->router->method == 'index'))) {
                                                    echo "active";
                                                }
                                                ?>" style=" <?= @$show_management_menu ?>">
                                                    <a class="menu-item" href="<?= site_url('examiner'); ?>"><?= $this->lang->line('examinerscorer'); ?> <?= $this->lang->line('list'); ?></a>
                                                </li>
                                            <?php //} ?>
                                        </ul>
                                    </li>
                                    <li class="has-sub nav-item <?php
                                    if (($this->router->class == 'agency') && (($this->router->method == 'new_agency') || ($this->router->method == 'index'))) {
                                        echo "open";
                                    }
                                    ?>" style="<?= $show_menu ?>  <?= @$show_management_menu ?>">
                                        <a href="#">
                                            <i class="la la-user"></i>
                                            <span class="menu-title" data-i18n=""><?= $this->lang->line('agency'); ?></span>
                                        </a>
                                        <ul class="menu-content">
                                            <?php
                                            //if (in_array('Add', explode(',', $logged_in['users']))) {
                                                ?>
                                                <li class="<?php
                                                if (($this->router->class == 'agency') && (($this->router->method == 'new_agency'))) {
                                                    echo "active";
                                                }
                                                ?>" style="<?= @$show_management_menu ?>">
                                                    <a class="menu-item" href="<?= site_url('agency/new_agency/'); ?>"><?= $this->lang->line('add_new'); ?></a>
                                                </li>
                                            <?php //} ?>
                                            <?php
                                            //if (in_array('List', explode(',', $logged_in['users'])) || in_array('List_all', explode(',', $logged_in['users']))) {
                                                ?>
                                                <li class="<?php
                                                if (($this->router->class == 'agency') && (($this->router->method == 'index'))) {
                                                    echo "active";
                                                }
                                                ?>" style="<?= @$show_management_menu ?>">
                                                    <a class="menu-item" href="<?= site_url('agency'); ?>"><?= $this->lang->line('agency'); ?> <?= $this->lang->line('list'); ?></a>
                                                </li>
                                            <?php //} ?>
                                        </ul>
                                    </li>
                                    <li class="has-sub nav-item <?php
                                    if (($this->router->class == 'location') && (($this->router->method == 'new_city') || ($this->router->method == 'index') || ($this->router->method == 'location_list') || ($this->router->method == 'new_location'))) {
                                        echo "open";
                                    }
                                    ?>" style="<?= $show_menu ?>  <?= @$show_management_menu ?>">
                                        <a href="#">
                                            <i class="la la-institution"></i>
                                            <span class="menu-title" data-i18n=""><?= $this->lang->line('location'); ?></span>
                                        </a>
                                        <ul class="menu-content">
                                            <?php
                                            if (in_array('Add', explode(',', $logged_in['users']))) {
                                                ?>
                                                <li class="d-none <?php
                                                if (($this->router->class == 'location') && (($this->router->method == 'new_city'))) {
                                                    echo "active";
                                                }
                                                ?>" style="<?= @$show_management_menu ?>">
                                                    <a class="menu-item" href="<?= site_url('location/new_city/'); ?>"><?= $this->lang->line('add_new') . " " . $this->lang->line('city'); ?></a>
                                                </li>
                                            <?php } ?>
                                            <?php
                                            //if (in_array('List', explode(',', $logged_in['users'])) || in_array('List_all', explode(',', $logged_in['users']))) {
                                                ?>
                                                <li class="<?php
                                                if (($this->router->class == 'location') && (($this->router->method == 'index'))) {
                                                    echo "active";
                                                }
                                                ?>" style="<?= @$show_management_menu ?>">
                                                    <a class="menu-item" href="<?= site_url('location'); ?>"><?= $this->lang->line('city'); ?> <?= $this->lang->line('list'); ?></a>
                                                </li>
                                            <?php //} ?>
                                            <?php
                                            //if (in_array('List', explode(',', $logged_in['users'])) || in_array('List_all', explode(',', $logged_in['users']))) {
                                                ?>
                                                <li class="<?php
                                                if (($this->router->class == 'location') && (($this->router->method == 'location_list') || ($this->router->method == 'new_location'))) {
                                                    echo "active";
                                                }
                                                ?>" style="<?= @$show_management_menu ?>">
                                                    <a class="menu-item" href="<?= site_url('location/location_list'); ?>"><?= $this->lang->line('location_list'); ?></a>
                                                </li>
                                            <?php //} ?>
                                        </ul>
                                    </li>
                                <?php //} ?>

                                <!--question & quiz-->
                                <?php
                                //if (in_array('List', explode(',', $logged_in['questions'])) || in_array('List_all', explode(',', $logged_in['questions']))) {
                                    ?>
                                    <li class="has-sub nav-item <?php
                                    if (($this->router->class == 'qbank') && (($this->router->method == 'pre_new_question') ||
                                            ($this->router->method == 'new_question_1') || ($this->router->method == 'new_question_2') || ($this->router->method == 'new_question_6') || ($this->router->method == 'index') ||
                                            ($this->router->method == 'edit_question_1') || ($this->router->method == 'edit_question_2') || ($this->router->method == 'edit_question_6'))) {
                                        echo "open";
                                    }
                                    ?>" style="<?= $show_menu ?>">
                                        <a href="#">
                                            <i class="la la-question"></i>
                                            <span class="menu-title" data-i18n=""><?= $this->lang->line('qbank'); ?></span>
                                        </a>
                                        <ul class="menu-content">
                                            <?php
                                            //if (in_array('Add', explode(',', $logged_in['questions']))) {
                                                ?>
                                                <li class="<?php
                                                if (($this->router->class == 'qbank') && (($this->router->method == 'pre_new_question') || ($this->router->method == 'new_question_1') || ($this->router->method == 'new_question_2') || ($this->router->method == 'new_question_6') ||
                                                        ($this->router->method == 'edit_question_1') || ($this->router->method == 'edit_question_2') || ($this->router->method == 'edit_question_6'))) {
                                                    echo "active";
                                                }
                                                ?>">
                                                    <a class="menu-item" href="<?= site_url('qbank/pre_new_question/'); ?>"><?= $this->lang->line('add_new'); ?></a>
                                                </li>
                                            <?php //} ?>
                                            <?php
                                            //if (in_array('List', explode(',', $logged_in['questions'])) || in_array('List_all', explode(',', $logged_in['questions']))) {
                                                ?>
                                                <li class="<?php
                                                if (($this->router->class == 'qbank') && (($this->router->method == 'index'))) {
                                                    echo "active";
                                                }
                                                ?>">
                                                    <a class="menu-item" href="<?= site_url('qbank'); ?>"><?= $this->lang->line('question'); ?> <?= $this->lang->line('list'); ?></a>
                                                </li>
                                            <?php //} ?>
                                        </ul>
                                    </li>
                                <?php //} ?>
                                <?php
                                //if (in_array('List', explode(',', $logged_in['quiz'])) || in_array('List_all', explode(',', $logged_in['quiz']))) {
                                    ?>
                                    <li class="has-sub nav-item <?php
                                    if (($this->router->class == 'quiz') && (($this->router->method == 'add_new') || ($this->router->method == 'edit_quiz') || ($this->router->method == 'index'))) {
                                        echo "open";
                                    }
                                    ?>" style="<?= $show_menu ?>">
                                        <a href="#">
                                            <i class="la la-laptop"></i>
                                            <span class="menu-title" data-i18n=""><?= $this->lang->line('quiz'); ?></span>
                                        </a>
                                        <ul class="menu-content">
                                            <?php
                                            //if (in_array('Add', explode(',', $logged_in['quiz']))) {
                                                ?>
                                                <li class="<?php
                                                if (($this->router->class == 'quiz') && (($this->router->method == 'add_new') || ($this->router->method == 'edit_quiz'))) {
                                                    echo "active";
                                                }
                                                ?>">
                                                    <a class="menu-item" href="<?= site_url('quiz/add_new/'); ?>"><?= $this->lang->line('add_new'); ?></a>
                                                </li>
                                            <?php //} ?>
                                            <?php
                                            //if (in_array('List', explode(',', $logged_in['quiz'])) || in_array('List_all', explode(',', $logged_in['quiz']))) {
                                                ?>
                                                <li class="<?php
                                                if (($this->router->class == 'quiz') && (($this->router->method == 'index'))) {
                                                    echo "active";
                                                }
                                                ?>">
                                                    <a class="menu-item" href="<?= site_url('quiz'); ?>"><?= $this->lang->line('quiz'); ?> <?= $this->lang->line('list'); ?></a>
                                                </li>
                                            <?php //} ?>
                                        </ul>
                                    </li>
                                <?php //} ?>
                                <!--question & quiz-->
                                <?php
                                //if (in_array('List', explode(',', $logged_in['quiz'])) && $logged_in['su'] >= 4) {
                                    ?>
                                    <li class="nav-item <?php
                                    if (($this->router->class == 'examiner') && (($this->router->method == 'exam_listing'))) {
                                        echo "open";
                                    }
                                    ?>">
                                        <a href="<?php echo site_url('examiner/exam_listing'); ?>"><i class="la la-list"></i>
                                            <span class="menu-title" data-i18n=""><?= $this->lang->line('exam_listing'); ?></span>
                                        </a>
                                    </li>
                                <?php //} ?>
                                <?php
                                //if (in_array('List', explode(',', $logged_in['quiz'])) && ($logged_in['su'] == 3 || $logged_in['su'] == 1)) {
                                    ?>
                                    <li class="nav-item <?php
                                    if (($this->router->class == 'agency') && (($this->router->method == 'transaction_listing'))) {
                                        echo "open";
                                    }
                                    ?>">
                                        <a href="<?php echo site_url('agency/transaction_listing'); ?>"><i class="la la-usd"></i>
                                            <span class="menu-title" data-i18n=""><?= $this->lang->line('transaction_listing'); ?></span>
                                        </a>
                                    </li>
                                <?php //} ?>
                                <?php
                                //if (in_array('List', explode(',', $logged_in['results'])) || in_array('List_all', explode(',', $logged_in['results']))) {
                                    ?>
                                    <li class="nav-item <?php
                                    if (($this->router->class == 'result') && ($this->router->method == 'mockexam_results')) {
                                        echo "open";
                                    }
                                    ?>">
                                        <a href="<?php echo site_url('result/mockexam_results'); ?>"><i class="la la-registered"></i>
                                            <span class="menu-title" data-i18n="">
                                                <?= $this->lang->line('mockexam_results'); ?>
                                            </span>
                                        </a>
                                    </li>
                                <?php //} ?>
                                <?php
                                //if (in_array('List', explode(',', $logged_in['results'])) || in_array('List_all', explode(',', $logged_in['results']))) {
                                    ?>
                                    <li class="nav-item <?php
                                    if (($this->router->class == 'result') && (($this->router->method == 'index') || ($this->router->method == 'view_result'))) {
                                        echo "open";
                                    }
                                    ?>">
                                        <a href="<?php echo site_url('result'); ?>"><i class="la la-registered"></i>
                                            <span class="menu-title" data-i18n="">
                                                <?= $this->lang->line('result') . " " . $this->lang->line('list'); ?>
                                            </span>
                                        </a>
                                    </li>
                                <?php //} ?>
                                <?php
                                //if (in_array('List', explode(',', $logged_in['quiz']))) {
                                    ?>			
                                    <li class="nav-item <?php
                                    if (($this->router->class == 'student_subscriptionexam') && (($this->router->method == 'index') || ($this->router->method == 'view_result'))) {
                                        echo "open";
                                    }
                                    ?>">
                                        <a href="<?= site_url('student_subscriptionexam'); ?>">
                                            <i class="la la-clock-o"></i>
                                            <span class="menu-title" data-i18n="">
                                                <?php echo $this->lang->line('student_subscription_exam'); ?>
                                            </span>
                                        </a>
                                    </li>
                                <?php //} ?>
                            </ul>
                        <?php
                        } elseif ($logged_in['su'] == 7) {                           
                            include 'admin_management_menu.php';
                        } elseif ($logged_in['su'] == 8) {
                            include 'agency_management_menu.php';
                            } ?>
                    </div>
                    <div class="navigation-background"></div>
                </div>
                <?php
            }
        }
        ?>



