<style>
    .btn-default {
        background: #05038b;
        color: #fff;
        font-weight: bold;
        margin: 0 auto;
    }
</style>

<!--updated template page-->
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-wrapper-before"></div>
        <div class="content-header row">
            <div class="content-header-left col-md-4 col-12 mb-2">
                <h3 class="content-header-title"></h3>
            </div>
        </div>
        <div class="content-body">
            <section id="horizontal-form-layouts">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-content collpase collapse show">
                                <div class="card-body">
                                    <form method="post" id="add_city" name="add_city" action="<?php echo site_url('location/insert_city/'); ?>">
                                        <div class="form-body">
                                            <input type="hidden" name="city_id" value="<?= @$result['id_city'] ?>"/>
                                            <div class="col-md-12">
                                                <br>
                                                <div class="login-panel panel panel-default">
                                                    <div class="panel-body">
                                                        <?php
                                                        if ($this->session->flashdata('message')) {
                                                            echo $this->session->flashdata('message');
                                                        }
                                                        ?>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group row">
                                                                    <label class="col-md-3 label-control"><?php echo $this->lang->line('city_name'); ?></label> 
                                                                    <div class="col-md-9">
                                                                        <input type="text" value="<?= @$result['city_name'] ?>"  name="city[city_name]"  class="form-control border-primary" placeholder="<?php echo $this->lang->line('city_name'); ?>" required>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                               <div class="form-group row">
                                                                    <label class="col-md-3 label-control"><?php echo $this->lang->line('description'); ?></label> 
                                                                    <div class="col-md-9">
                                                                        <textarea rows="3" cols="5" class="form-control border-primary" name="city[description]"><?= @$result['description'] ?></textarea>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-actions right">
                                            <button class="btn btn-primary" type="submit">
                                                <i class="la la-check-square-o"></i>
                                                <?php echo $this->lang->line('submit'); ?>
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
<!--updated template page-->

<script>
    // getexpiry();
</script>
<!--<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.1/dist/jquery.validate.js"></script>-->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
<script>
    $(document).ready(function () {
        $('#add_city').validate({// initialize the plugin
            rules: {
                
            },
            submitHandler: function (form) { // for demo
                $('button[type=submit]').attr('disabled', 'disabled');
                return true; // for demo
            }
        });
    });
</script>