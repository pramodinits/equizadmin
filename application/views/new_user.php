<!--for datepicker-->
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<!--for datepicker-->

<!--for country list-->
<script src="<?php echo base_url(); ?>vendor/jquery/countries.js"></script>
<!--for country list-->

<style>
    .btn-default {
        background: #05038b;
        color: #fff;
        font-weight: bold;
        margin: 0 auto;
    }
    /*    .form-group .error {
            color: #9f1212 !important;
            line-height: 2 !important;
            font-size: 15px !important;
            width: 100% !important;
        }*/
</style>

<!--updated template page-->
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-wrapper-before"></div>
        <div class="content-header row">
            <div class="content-header-left col-md-4 col-12 mb-2">
                <h3 class="content-header-title"></h3>
            </div>
        </div>
        <div class="content-body">
            <section id="horizontal-form-layouts">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-content collpase collapse show">
                                <div class="card-body">
                                    <?php
                                    if ($this->session->flashdata('message')) {
                                        echo $this->session->flashdata('message');
                                    }
                                    ?>
                                    <form method="post" id="register_user" name="register_user" action="<?php echo site_url('user/insert_user/'); ?>" enctype="multipart/form-data">
                                        <!--<h4 class="form-section"><?php echo $title; ?></h4>-->
                                        <div class="col-md-12">
                                            <br>
                                            <div class="login-panel panel panel-default">
                                                <div class="panel-body">

                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label><?php echo $this->lang->line('first_name'); ?></label> 
                                                                <input type="text"  name="user[first_name]"  class="form-control" placeholder="<?php echo $this->lang->line('first_name'); ?>" required  autofocus>
                                                                <!--<label for="first_name" generated="true" class="error text-red" style="display:none;"></label>-->
                                                            </div>
                                                            <div class="form-group">
                                                                <label><?php echo $this->lang->line('last_name'); ?></label> 
                                                                <input type="text" name="user[last_name]"  class="form-control" placeholder="<?php echo $this->lang->line('last_name'); ?>" required  autofocus>
                                                            </div>
                                                            <div class="form-group">	 
                                                                <label><?php echo $this->lang->line('company'); ?></label> 
                                                                <input type="text" name="user[company]" class="form-control" placeholder="<?php echo $this->lang->line('company'); ?>" required  autofocus>
                                                            </div>
                                                            <div class="form-group">	 
                                                                <label><?php echo $this->lang->line('license_type'); ?></label> 
                                                                <input type="text" name="user[license_type]" class="form-control" placeholder="<?php echo $this->lang->line('license_type'); ?>" required  autofocus>
                                                            </div>
                                                            <div class="form-group">	 
                                                                <label><?php echo $this->lang->line('license_number'); ?></label> 
                                                                <input type="text" name="user[license_number]" class="form-control" placeholder="<?php echo $this->lang->line('license_number'); ?>" required autofocus>
                                                            </div>
                                                            <div class="form-group">	 
                                                                <label><?php echo $this->lang->line('license_country'); ?></label> 
                                                                <select id="license_country" name ="user[license_country]" class="form-control" required></select>
                                                            </div>
                                                            <div class="form-group">	 
                                                                <label><?php echo $this->lang->line('license_doc'); ?></label> 
                                                                <input type="file" name="license_doc"  class="form-control" required placeholder="<?php echo $this->lang->line('license_doc'); ?>"   autofocus>
                                                            </div>
                                                            <div class="form-group">	 
                                                                <label><?php echo $this->lang->line('vat'); ?></label> 
                                                                <input type="text" name="user[vat]" class="form-control" required placeholder="<?php echo $this->lang->line('vat'); ?>" autofocus>
                                                            </div>
                                                            <div class="form-group">	 
                                                                <label><?php echo $this->lang->line('account_type'); ?></label> 
                                                                <select class="form-control" name="user[su]">
                                                                    <?php
                                                                    foreach ($account_type as $ak => $val) {
                                                                        if ($val['account_id'] <= 2) {
                                                                            ?>
                                                                            <option value="<?php echo $val['account_id']; ?>"><?php echo $val['account_name']; ?></option>
                                                                            <?php
                                                                        }
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label><?php echo $this->lang->line('email'); ?></label> 
                                                                <input type="email" id="inputEmail" required name="user[email]" class="form-control" placeholder="<?php echo $this->lang->line('email'); ?>"   autofocus>
                                                            </div>
                                                            <div class="form-group">
                                                                <label><?php echo $this->lang->line('city'); ?></label> 
                                                                <input type="text" required name="user[city]" class="form-control" placeholder="<?php echo $this->lang->line('city'); ?>"   autofocus>
                                                            </div>
                                                            <div class="form-group">	 
                                                                <label><?php echo $this->lang->line('address'); ?></label> 
                                                                <textarea name="user[address]" required rows="2" cols="5" class="form-control" placeholder="<?php echo $this->lang->line('address'); ?>"></textarea>
                                                            </div>
                                                            <div class="form-group">	 
                                                                <label><?php echo $this->lang->line('postal_code'); ?></label> 
                                                                <input type="text" required name="user[postal_code]" class="form-control" placeholder="<?php echo $this->lang->line('postal_code'); ?>" >
                                                            </div>
                                                            <div class="form-group">
                                                                <label><?php echo $this->lang->line('country'); ?></label> 
                                                                <select id="country" required name ="user[country]" class="form-control"></select>
                                                            </div>
                                                            <div class="form-group">
                                                                <label><?php echo $this->lang->line('contact_no'); ?></label> 
                                                                <input type="text" required name="user[contact_no]"  class="form-control" placeholder="<?php echo $this->lang->line('contact_no'); ?>"   autofocus>
                                                            </div>
                                                            <div class="form-group">	 
                                                                <label><?php echo $this->lang->line('dob'); ?></label> 
                                                                <input type="date" required id="dob" name="dob" class="form-control" placeholder="<?php echo $this->lang->line('dob'); ?>"   autofocus>
                                                            </div>
                                                            <div class="form-group">	 
                                                                <label><?php echo $this->lang->line('password'); ?></label> 
                                                                <input type="password" required id="password" name="password" class="form-control" placeholder="<?php echo $this->lang->line('password'); ?>" autofocus>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-actions right">
                                                        <button class="btn btn-primary" type="submit">
                                                            <i class="la la-check-square-o"></i>
                                                            <?php echo $this->lang->line('submit'); ?>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
<!--updated template page-->


<script>
    // getexpiry();
</script>
<script>
    populateCountries("license_country");
    populateCountries("country");

    $(function () {
//        $("#dob").datepicker({
//            dateFormat: 'yy-mm-dd',
//            'maxDate': 0,
//            onSelect: function (selected) {
//                $('label#dob-error').hide();
//            }
//        });
    });

</script>
<!--<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.1/dist/jquery.validate.js"></script>-->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/additional-methods.js"></script>
<script>
    $(document).ready(function () {
        //set select value null
//        $("#license_country").val($("#license_country option:first").val(""));
//        $("#country").val($("#country option:first").val(""));
        //set select value null

        $('#register_user').validate({// initialize the plugin
            rules: {
                'user[first_name]': {
                    required: true,
                    minlength: 3,
                    lettersonly: true
                },
                'user[last_name]': {
                    required: true,
                    minlength: 3,
                    lettersonly: true
                },
                'user[contact_no]': {
                    required: true,
                    digits: true
                },
                'user[postal_code]': {
                    required: true,
                    digits: true
                },
                'user[su]': {
                    required: true
                },
                'license_country': {
                    required: true
                },
            },
            submitHandler: function (form) { // for demo
                $('button[type=submit]').attr('disabled', 'disabled');
                return true; // for demo
            }
        });
    });
</script>