<style>
    input.form-control {
        width: 25%;
        float: left;
        margin-right: 1%;
    }
</style>
<!updated template design-->
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-wrapper-before"></div>
        <div class="content-header row">
            <div class="content-header-left col-md-4 col-12 mb-2">
                <h3 class="content-header-title"></h3>
            </div>
        </div>
        <div class="content-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collpase collapse show">
                            <div class="card-body">
                                <?php
                                if ($this->session->flashdata('message')) {
                                    echo $this->session->flashdata('message');
                                }
                                ?>
                                <div class="row">
                                    <div class="col-md-12">
                                        <form method="get" action="<?php echo site_url('user/index/'); ?>">
                                            <input type="text" name="username" class="form-control" value="<?= $user['username'] ?>" placeholder="Search Name">
                                            <input type="text" name="email" class="form-control" value="<?= $user['email'] ?>" placeholder="Search Email">
                                            <input type="text" name="phone" class="form-control" value="<?= $user['phone'] ?>" placeholder="Search Phone">
                                            <button type="submit" class="btn btn-primary"><?= $this->lang->line('filter') ?></button>
                                            <a href="<?= site_url('user/'); ?>">
                                                <button type="button" class="btn btn-primary"><?php echo $this->lang->line('reset_button'); ?></button>
                                            </a>
                                        </form>
                                    </div>
                                </div>
                                <div class="table-responsive">

                                    <table class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th class="text-center"><?php echo $this->lang->line('name'); ?></th>
                                                <th class="text-center"><?php echo $this->lang->line('exam_category'); ?></th>
                                                <th class="text-center"><?php echo $this->lang->line('email'); ?></th>
                                                <th class="text-center"><?php echo $this->lang->line('contact'); ?></th>
                                                <th class="text-center"><?php echo $this->lang->line('account_status'); ?> </th>
                                                <th style="display:none;"><?php echo $this->lang->line('send_notification'); ?> </th>
                                                <th class="text-center"><?php echo $this->lang->line('action'); ?> </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            if (count($result) == 0) {
                                                ?>
                                                <tr>
                                                    <td colspan="7"><?php echo $this->lang->line('no_record_found'); ?></td>
                                                </tr>
                                                <?php
                                            }
                                            $access = json_decode($logged_in['permission_access'], true);
                                            foreach ($result as $key => $val) {
                                                ?>
                                                <tr>
                                                    <td><?php echo $val['uid']; ?></td>
                                                    <td><?php echo $val['first_name'] . " " . $val['last_name']; ?></td>
                                                    <td><?php echo @$exam_category[$val['exam_category']]; ?></td>
                                                    <td><?= $val['email'] ? $val['email'] . ' ' . $val['wp_user'] : "N/A"; ?></td>
                                                    <td><?= $val['contact_no']; ?></td>
                                                    <td class="text-center" id="verify_status">
                                                        <?php if ($val['phone_verify'] != 1 && $access['applicant']['approved']) { ?>
                                                            <a id="phone_verify<?= $val['uid']; ?>" href='javascript:void(0);' onclick='changeVerificationStatus(<?= $val['uid']; ?>,<?= $val['phone_verify']; ?>, "phone_verify");'>
                                                                <i id="phone_verify_<?php echo $val['uid'] ?>" class="la la-phone text-danger" data-toggle='tooltip' data-original-title='Not Verified (Click to Verify)'></i>
                                                            </a>&nbsp;
                                                        <?php } elseif($val['phone_verify'] != 1) { ?>
                                                            <i id="phone_verify_<?php echo $val['uid'] ?>" class="la la-phone text-danger" data-toggle='tooltip' data-original-title='Not Verified'></i>
                                                      <?php  }
                                                        else { ?>
                                                            <i id="phone_verify_<?php echo $val['uid'] ?>" class="la la-phone text-success" data-toggle='tooltip' data-original-title='Verified'></i>&nbsp;
                                                        <?php } ?>
                                                        <?php if ($val['email_verify'] != 1 && $access['applicant']['approved']) { ?>
                                                            <a id="email_verify<?= $val['uid']; ?>" href='javascript:void(0);' onclick='changeVerificationStatus(<?= $val['uid']; ?>,<?= $val['email_verify']; ?>, "email_verify");'>
                                                                <i id="email_verify_<?php echo $val['uid'] ?>" class="la la-envelope text-danger" data-toggle='tooltip' data-original-title='Not Verified (Click to Verify)'></i>
                                                            </a>&nbsp;
                                                        <?php } elseif ($val['email_verify'] != 1) { ?>
                                                             <i id="email_verify_<?php echo $val['uid'] ?>" class="la la-envelope text-danger" data-toggle='tooltip' data-original-title='Not Verified'></i>
                                                       <?php }
                                                        else { ?>
                                                            <i id="email_verify_<?php echo $val['uid'] ?>" class="la la-envelope text-success" data-toggle='tooltip' data-original-title='Verified'></i>&nbsp;
                                                        <?php } ?>
                                                        <?php
                                                        if (empty($val['license_doc'])) {
                                                            echo "N/A";
                                                        } else {
                                                            ?>
                                                            <?php if ($val['licensedoc_verify'] == 1) { ?>
                                                                <i id="licensedoc_verify_<?php echo $val['uid'] ?>" class="la la-file text-success" data-toggle='tooltip' data-original-title='Verified'></i>
                                                            <?php } elseif($val['licensedoc_verify'] != 1 && $access['applicant']['approved']) { ?>
                                                                <a id="licensedoc_verify<?= $val['uid']; ?>" href='javascript:void(0);' onclick='changeVerificationStatus(<?= $val['uid']; ?>,<?= $val['licensedoc_verify']; ?>, "licensedoc_verify");'>
                                                                    <i id="licensedoc_verify_<?php echo $val['uid'] ?>" class="la la-file text-danger" data-toggle='tooltip' data-original-title='Not Verified (Click to Verify)'></i>
                                                                </a>
                                                            <?php } else { ?>
                                                                <i id="licensedoc_verify_<?php echo $val['uid'] ?>" class="la la-file text-danger" data-toggle='tooltip' data-original-title='Not Verified'></i>
                                                          <?php  } 
                                                            } ?>
                                                    </td>
                                                    <td class="text-center" style="display:none;">
                                                        <a href="<?php echo site_url('notification/add_new/' . $val['uid']); ?>">
                                                            <i class="fa fa-envelope" aria-hidden="true"></i>
                                                        </a>
                                                    </td>
                                                    <td>
                                                        <a href="<?php echo site_url('user2/view_user/' . $val['uid']); ?>">
                                                            <i class="la la-eye" title="View Profile"></i>
                                                        </a>
                                                        <!--<a href="<?php echo site_url('user/edit_user/' . $val['uid']); ?>"><img src="<?php echo base_url('images/edit.png'); ?>"></a>-->
                                                        <a class="d-none" href="javascript:void(0)" onclick="removeUser('<?= $val['uid']; ?>')">
                                                            <i class="la la-trash text-danger" title="Remove Applicant" aria-hidden="true"></i>
                                                        </a>
                                                        <?php
                                                        if ($access['applicant']['delete'] || $logged_in['su'] == 1) {
                                                            ?>
                                                            <a href="<?= site_url('user/remove_user/?id_applicant=' . $val['uid']) ?>" onclick="return confirm('Are you sure to remove this?')">
                                                                <i class="la la-trash text-danger" title="Remove Applicant" aria-hidden="true"></i>
                                                            </a>
                                                        <?php }
                                                        ?>

                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                    <?= $links; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!updated template design-->

<script>
    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });

    //remove user list
    function removeUser(uid) {
        var del = confirm('Are you sure to remove this?');
        if (del) {
            var url = "<?php echo site_url('user/remove_user'); ?>";
            $.post(url, {"uid": uid}, function (data) {
                if (data == "success") {
                    alert("Applicant Removed successfully");
                    location.reload();
                } else {
                    return false;
                }
            });
        } else {
            return false;
        }
    }

    function changeVerificationStatus(id_user, status, field) {
        if (field == "licensedoc_verify") {
            var status_val = (status == 1) ? 2 : 1;
        } else {
            var status_val = (status == 1) ? 0 : 1;
        }
        if (status_val == 0) {
            var msg = "Un-Verified";
        } else {
            var msg = "Verified";
        }
        var conf = confirm("Are you sure to change the status");
        if (conf) {
            var url = "<?= base_url('index.php/user/changeVerificationStatus/'); ?>";
            $.post(url, {"uid": id_user, status_val: status_val, "tbl_name": 'kams_users', "update_field": field, "fld_name": 'id_user'}, function (res) {
                setTimeout(function () {
                    alert("User " + msg + " Successfully!");
                    location.reload();
                }, 500);
            });
            return true;
        } else {
            return false;
        }
    }
</script>
<script>
    var getContacts = 12; //remaining fields
    var AddedContacts = 8; //filled fields
    var restContacts = AddedContacts * 100 / getContacts;
    $("#contactsAdded").css({"width": +restContacts + "%"});
    $("#contactsAdded").attr('data-transitiongoal', restContacts);
</script>