<style>
    .fa-pencil-square-o:before {
        content: "\f044";
    }
    .magicsearch-wrapper {
        width: 100% !important;
    }
    input.magicsearch {
        width: 100% !important;
    }
</style>

<!--update template design-->
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-wrapper-before"></div>
        <div class="content-header row">
            <div class="content-header-left col-md-4 col-12 mb-2">
                <h3 class="content-header-title"></h3>
            </div>
        </div>
        <div class="content-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collpase collapse show">
                            <div class="card-body">
                                <?php
                                if ($this->session->flashdata('message')) {
                                    echo $this->session->flashdata('message');
                                }
                                ?>
                                <div class="row pull-right mb-2">
                                    <a href="<?= site_url('location/new_location'); ?>">
                                        <button type="button" class="btn btn-primary">
                                            <?= $this->lang->line('location_new'); ?>
                                        </button>
                                    </a>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th class="text-center"><?php echo $this->lang->line('location_name'); ?></th>
                                                <th class="text-center"><?php echo $this->lang->line('city_name'); ?></th>
                                                <th class="text-center"><?php echo $this->lang->line('action'); ?> </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            if (count($result) == 0) {
                                                ?>
                                                <tr>
                                                    <td colspan="4"><?php echo $this->lang->line('no_record_found'); ?></td>
                                                </tr>
                                                <?php
                                            }
                                            foreach ($result as $key => $val) {
                                                ?>
                                                <tr id="<?= "location_" . $val['id_location'] ?>">
                                                    <td><?php echo $val['id_location']; ?></td>
                                                    <td><?php echo $val['location_name']; ?></td>
                                                    <td><?= $city_list[$val['id_city']]; ?></td>
                                                    <td>
                                                        <a class="float-left" href="<?php echo site_url('location/new_location/' . $val['id_location']); ?>">
                                                            <i class="la la-edit" aria-hidden="true" title="Edit Location"></i>
                                                        </a>
                                                        <a href="javascript:void(0);" onclick="removeLocation('<?= $val['id_location'] ?>')">
                                                            <i class="la la-trash text-danger" aria-hidden="true" title="Remove Location"></i>
                                                        </a>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                    <?= $links; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--update template design-->
<script src="//code.jquery.com/jquery-3.1.0.min.js"></script>
<script src="<?php echo base_url('autocomplete/js/jquery.magicsearch.js') ?>"></script>
<script>
                                                        $(function () {
                                                            $('.alert').delay(3000).show().fadeOut('slow');
                                                        });

                                                        //remove assign student
                                                        function removeLocation(location_id) {
                                                            var del = confirm('Are you sure to remove this?');
                                                            if (del) {
                                                                var url = "<?php echo site_url('location/remove_location'); ?>";
                                                                $.post(url, {'location_id': location_id}, function (data) {

                                                                    if (data == "success") {
                                                                        $("#location_" + location_id).remove();

                                                                        alert("Location removed successfully");
                                                                        location.reload();
                                                                    } else {
                                                                        return false;
                                                                    }
                                                                });
                                                            } else {
                                                                return false;
                                                            }
                                                        }

</script>