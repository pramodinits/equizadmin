<style>
    .fa-clock-o:before {
        content: "\f017";
    }
    select.form-control {
        width: 30%;
        float: left;
        margin-right: 1%;
    }
</style>
<?php
$logged_in = $this->session->userdata('logged_in');
$uid = $logged_in['uid'];
$list_view = "table";
$acp = explode(',', $logged_in['quiz']);
?>

<!--update template-->
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-wrapper-before"></div>
        <div class="content-header row">
            <div class="content-header-left col-md-4 col-12 mb-2">
                <h3 class="content-header-title"></h3>
            </div>
            <div class="content-header-right col-md-8 col-12">
                <div class="breadcrumbs-top float-md-right">
                    <h6><?= $breadcrumbs; ?></h6>
                </div>
            </div>
        </div>
        <div class="content-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collpase collapse show">
                            <div class="card-body">
                                <?php
                                if ($this->session->flashdata('message')) {
                                    echo $this->session->flashdata('message');
                                }
                                ?>
                                <?php if (in_array('List_all', $acp)) { ?>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <form method="get" action="<?php echo site_url('quiz/index/'); ?>">
                                                <select name="cid" class="form-control">
                                                    <option value="0"><?php echo $this->lang->line('all_category'); ?></option>
                                                    <?php
                                                    foreach ($exam_category as $key => $val) {
                                                        ?>

                                                        <option value="<?php echo $key; ?>" <?php
                                                        if ($key == $user['cid']) {
                                                            echo 'selected';
                                                        }
                                                        ?> ><?php echo $val; ?></option>
                                                                <?php
                                                            }
                                                            ?>
                                                </select>
                                                <select name="mid" class="form-control">
                                                    <option value="0"><?php echo $this->lang->line('exam_type'); ?></option>
                                                    <?php
                                                    foreach ($exam_type as $key => $val) {
                                                        ?>

                                                        <option value="<?php echo $key; ?>"  <?php
                                                        if ($key == $user['mid']) {
                                                            echo 'selected';
                                                        }
                                                        ?> ><?php echo $val; ?></option>
                                                                <?php
                                                            }
                                                            ?>
                                                </select>
                                                <button type="submit" class="btn btn-primary"><?= $this->lang->line('filter') ?></button>
                                                <a href="<?= site_url('quiz/'); ?>">
                                                    <button type="button" class="btn btn-primary"><?php echo $this->lang->line('reset_button'); ?></button>
                                                </a>
                                            </form>
                                        </div>
                                    </div>
                                <?php } ?>
                                <div class="row" style="display: none;">
                                    <div class="col-xl-4 col-md-6 col-sm-12">
                                        <div class="card bg-gradient-directional-success text-white box-shadow-0 bg-info">
                                            <div class="card-content collapse show">
                                                <div class="card-body">
                                                    <h4 class="card-title text-white"><?php echo $this->lang->line('active'); ?> <?php echo $this->lang->line('quiz'); ?></h4>
                                                    <h1 class="text-right mb-0"><?php echo $active; ?></h1>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-4 col-md-6 col-sm-12">
                                        <div class="card bg-gradient-directional-info text-white box-shadow-0 bg-info">
                                            <div class="card-content collapse show">
                                                <div class="card-body">
                                                    <h4 class="card-title text-white"><?php echo $this->lang->line('archived'); ?> <?php echo $this->lang->line('quiz'); ?></h4>
                                                    <h1 class="text-right mb-0"><?php echo $archived; ?></h1>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-4 col-md-6 col-sm-12">
                                        <div class="card bg-gradient-directional-warning text-white box-shadow-0 bg-info">
                                            <div class="card-content collapse show">
                                                <div class="card-body">
                                                    <h4 class="card-title text-white"><?php echo $this->lang->line('upcoming'); ?> <?php echo $this->lang->line('quiz'); ?></h4>
                                                    <h1 class="text-right mb-0"><?php echo $upcoming; ?></h1>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div> 
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th><?php echo $this->lang->line('quiz_name'); ?></th>
                                                <th><?php echo $this->lang->line('exam_category'); ?></th>
                                                <th><?php echo $this->lang->line('exam_type'); ?></th>
                                                <th><?php echo $this->lang->line('price_'); ?></th>
                                                <!--<th><?php echo $this->lang->line('quiz_media'); ?></th>-->
                                                <th><?php echo $this->lang->line('video_conference'); ?></th>
                                                <th><?php echo $this->lang->line('action'); ?> </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            if (count($result) == 0) {
                                                ?>
                                                <tr>
                                                    <td colspan="7"><?php echo $this->lang->line('no_record_found'); ?></td>
                                                </tr>
                                                <?php
                                            }
                                            $logged_in = $this->session->userdata('logged_in');
                                            $access = json_decode($logged_in['permission_access'], true);
                                            foreach ($result as $key => $val) {
                                                ?>
                                                <tr>
                                                    <td><?php echo $val['quid']; ?></td>
                                                    <td><?php echo substr(strip_tags($val['quiz_name']), 0, 50); ?></td>
                                                    <td><?php echo $exam_category[$val['exam_category']]; ?></td>
                                                    <td><?php echo $exam_type[$val['exam_type']]; ?></td>
                                                    <td><?= $val['exam_type'] == 1 ? 0 : $val['quiz_price'] ?></td>
<!--                                                    <td>
                                                        <?php if ($val['photo']) { ?>
                                                        <img class="img-sm" src="<?= base_url('quiz_media/') . $val['photo']; ?>">
                                                        <?php } else {
                                                            echo "N/A";
                                                        } ?>
                                                    </td>-->
                                                    <td class="text-center">
                                                        <a style="display: none;" href="<?= site_url('schedule_time/addSchedule') . "/" . $val['quid'] ?>">
                                                            <i style="color: blue;" class="fa fa-clock-o" aria-hidden="true"></i>
                                                        </a>
                                                        <?php
                                                        //$val['exam_type'] == 2 && 
                                                        if ($val['video_conference'] == 1) {
                                                            echo "<span class='text-success'>" . $this->lang->line('enable_conf') . "</span>";
                                                        } else {
                                                            echo "<span class='text-danger'>" . $this->lang->line('disable_conf') . "</span>";
                                                        }
                                                        ?>
                                                    </td>
                                                    <td>
                                                        <?php
                                                        $acp = explode(',', $logged_in['quiz']);
                                                        //if (in_array('List_all', $acp)) {
                                                            ?>
                                                        <?php
                                                        if($logged_in['su'] == 7 && empty($access['exam']['add_edit']) && empty($access['exam']['action_status'])) {
                                                            echo "N/A";
                                                        }
                                                        if ($access['exam']['add_edit'] || $logged_in['su'] == 1) { ?>
                                                        <a href="<?php echo site_url('quiz/edit_quiz/' . $val['quid']); ?>"><i class="la la-edit"></i></a>
                                                        <?php } 
                                                        if ($access['exam']['action_status'] || $logged_in['su'] == 1) { ?>
                                                            <?php if ($val['status'] == 1) { ?>
                                                                <a href="javascript:void(0)" onclick="modifyStatus('<?= $val['quid']; ?>', '<?= $val['status'] ?>')">
                                                                    <i class="la la-check-square text-success" title="Active Status (Click to Inactive)"></i>
                                                                </a>
                                                            <?php } else { ?>
                                                                <a href="javascript:void(0)" onclick="modifyStatus('<?= $val['quid']; ?>', '<?= $val['status'] ?>')">
                                                                    <i class="la la-check-square text-danger" title="Inactive Status (Click to Active)"></i>
                                                                </a>
                                                            <?php } 
                                                            }
                                                        //}
                                                        ?>
                                                    </td>
                                                </tr>

                                                <?php
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                    <?= $links; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--update template-->

<script>
    function modifyStatus(quid, status) {
        var status_val = (status == 1) ? 2 : 1;
        var conf = confirm("Do you want to change the status?");
        if (conf) {
            var url = "<?php echo site_url('quiz/modify_status'); ?>";
            $.post(url, {'quid': quid, 'status_val': status_val}, function (data) {
                if (data == "success") {
                    alert("Status Modified successfully");
                    location.reload();
                } else {
                    return false;
                }
            });
        } else {
            return false;
        }
    }
</script>