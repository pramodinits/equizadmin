<link href='<?php echo base_url(); ?>fullcalendar/fullcalendar.min.css' rel='stylesheet' />
<link href='<?php echo base_url(); ?>fullcalendar/fullcalendar.print.min.css' rel='stylesheet' media='print' />
<script src='<?php echo base_url(); ?>fullcalendar/lib/moment.min.js'></script>
<script src='<?php echo base_url(); ?>fullcalendar/fullcalendar.min.js'></script>
<!--includeing files-->
<style>
    #wrap {
        width: 1100px;
        margin: 0 auto;
    }

    #external-events {
        float: left;
        width: 150px;
        padding: 0 10px;
        text-align: left;
    }

    #external-events h4 {
        font-size: 16px;
        margin-top: 0;
        padding-top: 1em;
    }

    .external-event { /* try to mimick the look of a real event */
        margin: 10px 0;
        padding: 2px 4px;
        background: #3366CC;
        color: #fff;
        font-size: .85em;
        cursor: pointer;
    }

    #external-events p {
        margin: 1.5em 0;
        font-size: 11px;
        color: #666;
    }

    #external-events p input {
        margin: 0;
        vertical-align: middle;
    }

    #calendar {
        /* 		float: right; */
        margin: 0 auto;
        width: 900px;
        padding: 2% 1%;
        background-color: #FFFFFF;
        border-radius: 6px;
        box-shadow: 0 1px 2px #C3C3C3;
        -webkit-box-shadow: 0px 0px 21px 2px rgba(0,0,0,0.18);
        -moz-box-shadow: 0px 0px 21px 2px rgba(0,0,0,0.18);
        box-shadow: 0px 0px 21px 2px rgba(0,0,0,0.18);
    }
    .modal-header{
        background:#05038B;
        color:#fff;
    }
    .modal-header .close {
        color:#fff !important;
        opacity: .7 !important;
    }
    .searchForm input.form-control, .searchForm select.form-control {
        width: 25%;
        float: left;
        margin-right: 1%;
    }
</style>
<!--updated template page-->
<?php 
//print "<pre>";
//print_r($examiner_list);exit;
?>
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-wrapper-before"></div>
        <div class="content-header row">
            <div class="content-header-left col-md-4 col-12 mb-2">
                <h3 class="content-header-title"></h3>
            </div>
        </div>
        <div class="content-body">
            <section id="horizontal-form-layouts">
                <div class="row">

                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-content collpase collapse show">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <form class="searchForm" method="get" action="<?php echo site_url('student_subscriptionexam/index/'); ?>">
                                                <select class="form-control" name="exam_id">
                                                    <option value=""><?php echo $this->lang->line('select') . " " . $this->lang->line('exam_category'); ?></option>
                                                    <?php
                                                    foreach ($exam_category as $key => $val) {
                                                        ?>
                                                        <option value="<?php echo $key; ?>" <?= $user['exam_id'] == $key ? "selected" : "" ?>><?php echo $val; ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                                <input type="text" name="studentname" class="form-control" value="<?= $user['studentname'] ?>" placeholder="Search Student Name">
                                                <input type="date" name="subscriptiondate" class="form-control" value="<?= $user['subscriptiondate'] ?>" placeholder="Search Date">
                                                <button type="submit" class="btn btn-primary"><?= $this->lang->line('filter') ?></button>
                                                <a href="<?= site_url('student_subscriptionexam/'); ?>">
                                                    <button type="button" class="btn btn-primary"><?php echo $this->lang->line('reset_button'); ?></button>
                                                </a>
                                            </form>
                                        </div>
                                    </div>
                                    <form method="post" id="register_user" name="register_user">
                                        <div class="form-body">
                                            <div class="col-md-12">
                                                <br>
                                                <div class="login-panel panel panel-default">
                                                    <div class="panel-body">
                                                        <?php
                                                        if ($this->session->flashdata('message')) {
                                                            echo $this->session->flashdata('message');
                                                        }
                                                        ?>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <form></form>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div id='calendar'></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
<!--updated template page-->

<!---update schedule modal-->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-info">
                <h4 class="modal-title pull-left text-white">
                    <?php echo $this->lang->line('student_subscription_exam'); ?>
                </h4>
                <button type="button" class="close pull-right text-white" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body" id="details_list">
                <div class="row">
                    <div class="col-md-12">
                        <label>
                            <strong><?php echo $this->lang->line('quiz_name'); ?></strong>
                        </label> : <span id="quiz_name"></span>
                    </div>
                    <div class="col-md-12">
                        <label>
                            <strong><?php echo $this->lang->line('applicant_name'); ?></strong>
                        </label> : <span id="student_name"></span>
                    </div> 
                    <div class="col-md-12">
                        <label>
                            <strong><?php echo $this->lang->line('examiner_name'); ?></strong>
                        </label> : <span id="examiner_name"></span>
                    </div> 
                    <div class="col-md-12">
                        <label>
                            <strong><?php echo $this->lang->line('scorer_name'); ?></strong>
                        </label> : <span id="scorer_name"></span>
                    </div> 
                    <div class="col-md-12">
                        <label>
                            <strong><?php echo $this->lang->line('start_time'); ?></strong>
                        </label> : <span id="start_time_show"></span>
                    </div> 
                    <div class="col-md-12">
                        <label>
                            <strong><?php echo $this->lang->line('end_time'); ?></strong>
                        </label> : <span id="end_time_show"></span>
                    </div> 
                </div>

            </div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>
<!---update schedule modal-->

<!---Assign Examiner/Scorer modal-->
<div class="modal fade" id="assignExaminer" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title pull-left text-white"><?php echo $this->lang->line('assign_examiner'); ?></h4>
                <button type="button" class="close pull-right" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body" id="details_list">
                <form method="post" name="assign_user" id="assign_user" action="<?= site_url('student_subscriptionexam/assign_user') ?>">
                    <input type="hidden" id="subscription_id" value="" name="subscription_id"/>
                    <input type="hidden" id="student_id" value="" name="student_id"/>
                    <input type="hidden" id="quiz_price" value="" name="quiz_price"/>
                    <div class="col-md-12">
                        <label>
                            <strong><?php echo $this->lang->line('applicant_name'); ?></strong>
                        </label> : <span id="student_name_form"></span>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label><?php echo $this->lang->line('select_examiner'); ?></label> 
                            <select class="form-control" name="examiner_id" id="examiner_id" required>
                                <option value=""><?php echo $this->lang->line('select_examiner'); ?></option>
                                <?php
                                foreach ($examiner_list as $key => $val) {
                                    ?>
                                    <option value="<?php echo $val['uid']; ?>"><?php echo $val['examiner_name']; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group">	 
                            <label><?php echo $this->lang->line('select_scorer'); ?></label> 
                            <select class="form-control" name="scorer_id" id="scorer_id" required>
                                <option value=""><?php echo $this->lang->line('select_scorer'); ?></option>
                                <?php
                                foreach ($scorer_list as $key => $val) {
                                    ?>
                                    <option value="<?php echo $val['uid']; ?>"><?php echo $val['scorer_name']; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label><?php echo $this->lang->line('update_date'); ?></label> 
                            <input type="date" onkeydown="return false" id="start_date" name="start_date" value="" class="form-control">
                        </div>
                        <div class="form-group">
                            <label><?php echo $this->lang->line('update_time'); ?></label> 
                            <select class="form-control" id="start_time_update" name="start_time">
                                <option><?= $this->lang->line('select_time') ?></option>
                                <option value="09:00:00"> 9:00 </option>
                                <option value="10:00:00"> 10:00 </option>
                                <option value="11:00:00"> 11:00 </option>
                                <option value="12:00:00"> 12:00 </option>
                                <option value="13:00:00"> 13:00 </option>
                                <option value="14:00:00"> 14:00 </option>
                                <option value="15:00:00"> 15:00 </option>
                                <option value="16:00:00"> 16:00 </option>
                            </select>
                            <!--<input type="time" onkeydown="return false" id="time" name="time" value="" class="form-control">-->
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>
                                            <strong><?php echo $this->lang->line('available_balance'); ?></strong>
                                        </label> : <span id="available_balance"></span>
                                    </div> 
                                    <div class="col-md-6 text-right">
                                        <label>
                                            <strong><?php echo $this->lang->line('exam_price'); ?></strong>
                                        </label> : <span id="exam_price"></span>
                                    </div>
                                </div>
                            </div>                     
                        </div>
                        <div class="form-actions">
                            <input type="submit" class="btn btn-default btn-success" value="Assign" id="submit">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>
<!---Assign Examiner/Scorer modal-->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
<script>
                                $(document).ready(function () {

                                    $('#assign_user').validate({
                                        rules: {

                                        },
                                        submitHandler: function (form) { // for demo
                                            $('button[type=submit]').attr('disabled', 'disabled');
                                            return true; // for demo
                                        }
                                    });
                                });
</script>

<!--<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js" type="text/javascript"></script>-->
<script>
    $(function () {
        $('.alert').delay(3000).show().fadeOut('slow');
    });

//        $('.fc-button-group').find('button.fc-custom1-button').removeClass("fc-state-active");
    $(document).ready(function () {
        $("button.fc-button").on('click', function (d) {
            alert("clicked");
        });

        var date = new Date();
        var d = date.getDate();
        var m = date.getMonth();
        var y = date.getFullYear();
        /* initialize the external events
         -----------------------------------------------------------------*/
        $('#external-events div.external-event').each(function () {

            // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
            // it doesn't need to have a start or end
            var eventObject = {
                title: $.trim($(this).text()) // use the element's text as the event title
            };
            // store the Event Object in the DOM element so we can get to it later
            $(this).data('eventObject', eventObject);
            // make the event draggable using jQuery UI
            $(this).draggable({
                zIndex: 999,
                revert: true, // will cause the event to go back to its
                revertDuration: 0  //  original position after the drag
            });
        });
        /* initialize the calendar
         -----------------------------------------------------------------*/

        var inputevent = <?= $events ?>;
        var calendar = $('#calendar').fullCalendar({
            header: {
                left: 'title',
                center: 'agendaDay,agendaWeek,listYear,custom1',
                right: 'prev,next, today'
            },
            customButtons: {
                custom1: {
                    text: 'Table',
                    click: function () {
                        $('.fc-button-group').find('.fc-button').removeClass("fc-state-active");
                        $('.fc-custom1-button').addClass("fc-state-active");
                        var tbl = $("<table/>").attr({
                            id: "mytable",
                            class: "table table-bordered table-striped"
                        });
                        $(".fc-view").html(tbl);
                        var tableEvents = <?= $table_events; ?>;
                        var tableHeading = "<thead>\n\
                                <tr class='text-center font-weight-bold'>\n\
                                <td>Quiz Name</td>\n\
                                <td>Applicant Name</td>\n\
                                <td>Start Date & Time</td>\n\
                                <td>Price</td>\n\
                                <td style='width:10%;'>Activity</td>\n\
                                </tr>\n\
                                </thead>";
                        $("#mytable").append(tableHeading);
                        for (var i = 1; i < tableEvents.length; i++)
                        {
//                            alert(tableEvents[i]["start"]);
                            var tr = "<tr>";
                            var td1 = "<td>" + tableEvents[i]["title"] + "</td>";
                            var td2 = "<td>" + tableEvents[i]["student_name"] + "</td>";
                            var td3 = "<td>" + tableEvents[i]["start"] + "</td>";
                            var td4 = "<td class='text-center'>" + tableEvents[i]["quiz_price"] + "</td>";
                            var td5 = "<td class='text-center'>\n\
                        <i class='la la-eye cursor-pointer' style='color:" + tableEvents[i]["eventTextColor"] + "' data-subscriptionid ='" + tableEvents[i]["subscription_id"] + "'></i>\n\
                        </td></tr>";
                            $("#mytable").append(tr + td1 + td2 + td3 + td4 + td5);
                        }
                        //event click function
                        $("td i").click(function () {
                            var eventId = $(this).attr('data-subscriptionid');
                            var url = "<?php echo site_url('student_subscriptionexam/getEventInfo'); ?>";
                            $.post(url, {
                                'subscriptionid': eventId
                            },
                                    function (data) {
                                        var e = JSON.parse(data);
                                        if (e.usertype == 3 && e.approved_status != 1) {
                                            if (parseInt(e.availableBalance, 10) >= parseInt(e.quiz_price, 10)) {
                                                $("#assignExaminer").modal('show');
                                            } else {
                                                alert("Available balance is low to approve this subscription.");
                                                return false;
                                            }
                                        } else {
                                            $('#myModal').modal('show');
                                        }
                                        var examinerName = (e.examiner_name) ? e.examiner_name : "No examiner assigned Yet.";
                                        var scorerName = (e.scorer_name) ? e.scorer_name : "No rater assigned Yet.";
                                        $("#examiner_name").text(examinerName);
                                        $("#student_name").text(e.student_name);
                                        $("#student_name_form").text(e.student_name);
                                        $("#scorer_name").text(scorerName);
                                        $("#quiz_name").text(e.quiz_name);
                                        $("#available_balance").text(e.availableBalance);
                                        $("#exam_price").text(e.quiz_price);
                                        $("#subscription_id").val(e.subscription_id);
                                        $("#start_date").val(e.start_date);
                                        $("#student_id").val(e.student_id);
                                        $("#quiz_price").val(e.quiz_price);
                                        $('#start_time_update').val(e.start_time);
                                        $("#start_time_show").text(e.start_datetime);
                                        $("#end_time_show").text(e.end_datetime);
                                    }
                            );
                        });
                    }
                }
            },
            slotDuration: "01:00",
            editable: true,
            eventOverlap: false,
            firstDay: 1, //  1(Monday) this can be changed to 0(Sunday) for the USA system
            selectable: true,
            defaultView: 'listYear',
            //defaultView: 'month',
            events: inputevent,
            selectHelper: true,
            /*select: function (startStr, endStr, allDay) {
             
             var title = prompt('Event Title:');
             //                var title = "";
             if (title) {
             calendar.fullCalendar('renderEvent',
             {
             title: title,
             start: startStr,
             end: endStr,
             allDay: allDay
             },
             true // make the event "stick"
             );
             //save data in db
             var url = "<?php echo site_url('schedule_time/insert_schedule'); ?>";
             var startDate = $.fullCalendar.formatDate(endStr, 'yyyy-MM-dd');
             var start_schedule = $.fullCalendar.formatDate(startStr, 'yyyy-MM-dd hh:mm');
             var end_schedule = $.fullCalendar.formatDate(endStr, 'yyyy-MM-dd hh:mm');
             $.post(url, {'schedule_title': title,
             'quiz_id': quiz_id,
             "start_schedule": start_schedule,
             "end_schedule": end_schedule,
             "schedule_date": startDate,
             "display_event_start": startStr,
             "display_event_end": endStr
             }, function (data) {
             var res = JSON.parse(data);
             if (res.Message == "Success") {
             alert("Schedule added successfully");
             } else {
             alert("Error in addition.");
             return false;
             }
             });
             }
             calendar.fullCalendar('unselect');
             },*/
            droppable: true, // this allows things to be dropped onto the calendar !!!
            drop: function (date, allDay) { // this function is called when something is dropped

                // retrieve the dropped element's stored Event Object
                var originalEventObject = $(this).data('eventObject');
                // we need to copy it, so that multiple events don't have a reference to the same object
                var copiedEventObject = $.extend({}, originalEventObject);
                // assign it the date that was reported
                copiedEventObject.start = date;
                copiedEventObject.allDay = allDay;
                // render the event on the calendar
                // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
                $('#calendar').fullCalendar('renderEvent', copiedEventObject, true);
                // is the "remove after drop" checkbox checked?
                if ($('#drop-remove').is(':checked')) {
                    // if so, remove the element from the "Draggable Events" list
                    $(this).remove();
                }
            },
            eventClick: function (e) {
                if (e.usertype == 3 && e.approved_status != 1)
                {
                    if (parseInt(e.availableBalance, 10) >= parseInt(e.quiz_price, 10)) {
                        $("#assignExaminer").modal('show');
                    } else {
                        alert("Available balance is low to approve this subscription.");
                        return false;
                    }
                } else {
                    $('#myModal').modal('show');
                }
                            
                var examinerName = (e.examiner_name) ? e.examiner_name : "No examiner assigned Yet.";
                var scorerName = (e.scorer_name) ? e.scorer_name : "No rater assigned Yet.";
                $("#examiner_name").text(examinerName);
                $("#student_name").text(e.student_name);
                $("#student_name_form").text(e.student_name);
                $("#scorer_name").text(scorerName);
                $("#quiz_name").text(e.title);
                $("#available_balance").text(e.availableBalance);
                $("#exam_price").text(e.quiz_price);
                $("#subscription_id").val(e.subscription_id);
                $("#start_date").val(e.start_date);
                $("#student_id").val(e.student_id);
                $("#quiz_price").val(e.quiz_price);
                $('#start_time_update').val(e.start_time);
//                if (e.examiner_id) {
//                    $("#examiner_id").val(e.examiner_id);
//                }
//                if (e.scorer_id) {
//                    $("#scorer_id").val(e.scorer_id);
//                }
                var start_schedule = $.fullCalendar.formatDate(e.start, 'DD-MM-YYYY hh:mm');
                var end_schedule = $.fullCalendar.formatDate(e.end, 'DD-MM-YYYY hh:mm');
                $("#start_time_show").text(start_schedule);
                $("#end_time_show").text(end_schedule);
                    }
        });
        $(".fc-listYear-button").on('click', function (e) {
//            alert("clicked");
//            $('button').find('.fc-custom1-button').removeClass("fc-state-active");
            $('.fc-button-group').find('button.fc-custom1-button').removeClass("fc-state-active");
        });
    });

</script>

