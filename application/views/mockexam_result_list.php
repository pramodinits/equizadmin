<!--for datepicker-->
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<!--for datepicker-->

<!--update template design-->
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-wrapper-before"></div>
        <div class="content-header row">
            <div class="content-header-left col-md-4 col-12 mb-2">
                <h3 class="content-header-title"></h3>
            </div>
        </div>
        <div class="content-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collpase collapse show">
                            <div class="card-body">
                                <?php
                                if ($this->session->flashdata('message')) {
                                    echo $this->session->flashdata('message');
                                }
                                ?>
                                <?php
                                if ($logged_in['su'] == '1') {
                                    ?>
                                    <div class='alert alert-danger'><?php echo $this->lang->line('pending_message_admin'); ?></div>		
                                    <?php
                                }
                                ?>
                                <?php
                                $logged_in = $this->session->userdata('logged_in');
                                ?>
                                <?php
                                //if ($logged_in['su'] == '1') {
                                ?>
                                <div class="col-lg-12">
                                    <div class="row">
                                        <div class="col-md-11">
                                            <form method="post" action="<?php echo site_url('result/generate_report/'); ?>"> 
                                                <div class="row">
                                                    <!--<div> <h3><?php echo $this->lang->line('generate_report'); ?> </h3> </div> <br>-->
                                                    <select name="category_id" class="form-control col-md-2 mr-1">
                                                        <option value="0"><?php echo $this->lang->line('all_category'); ?></option>
                                                        <?php
                                                        foreach ($category_type as $key => $val) {
                                                            ?>
                                                            <option value="<?php echo $key; ?>"><?php echo $val; ?></option>
                                                            <?php
                                                        }
                                                        ?>
                                                    </select>

                                                    <select name="quid" class="form-control col-md-2 mr-1">
                                                        <option value="0"><?php echo $this->lang->line('select_quiz'); ?></option>
                                                        <?php
                                                        foreach ($quiz_list as $qk => $quiz) {
                                                            ?>
                                                            <option value="<?php echo $quiz['quid']; ?>"><?php echo $quiz['quiz_name']; ?></option>
                                                            <?php
                                                        }
                                                        ?>
                                                    </select>

                                                    <select name="gid" style="display: none;">
                                                        <option value="0"><?php echo $this->lang->line('select_group'); ?></option>
                                                        <?php
                                                        foreach ($group_list as $gk => $group) {
                                                            ?>
                                                            <option value="<?php echo $group['gid']; ?>"><?php echo $group['group_name']; ?></option>
                                                            <?php
                                                        }
                                                        ?>
                                                    </select>
                                                    <input type="text" class="form-control col-md-2 mr-1 singledate" onkeydown="return false" autocomplete="false" name="date1" id="start_date" value="" placeholder="<?php echo $this->lang->line('date_from'); ?>">

                                                    <input type="text" class="form-control col-md-2 mr-1" onkeydown="return false" name="date2" id="end_date" value="" placeholder="<?php echo $this->lang->line('date_to'); ?>">

                                                    <button class="btn btn-info font-weight-bold" type="submit"><?php echo $this->lang->line('generate_report'); ?></button>	
                                                </div><!-- /input-group -->
                                            </form>
                                        </div>
                                        <div class="col-md-1">
                                            <a href="">
                                                <i class="la la-refresh pull-right text-danger font-weight-bold mt-1" title="Refresh"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div><!-- /.col-lg-6 -->

                                <?php
                                //}
                                ?>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th><?php echo $this->lang->line('result_id'); ?></th>
                                                <th><?php echo $this->lang->line('name'); ?></th>
                                                <th><?php echo $this->lang->line('quiz_name'); ?></th>
                                                <th class="d-none"><?php echo $this->lang->line('status'); ?></th>
                                                <th><?php echo $this->lang->line('percentage_obtained'); ?></th>
                                                <th><?php echo $this->lang->line('action'); ?> </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            if (count($result) == 0) {
                                                ?>
                                                <tr>
                                                    <td colspan="6"><?php echo $this->lang->line('no_record_found'); ?></td>
                                                </tr>
                                                <?php
                                            }
                                            $logged_in = $this->session->userdata('logged_in');
                                            $access = json_decode($logged_in['permission_access'], true);
                                            foreach ($result as $key => $val) {
                                                ?>
                                                <tr>
                                                    <td><?php echo $val['rid']; ?></td>
                                                    <td><?php echo $val['first_name']; ?> <?php echo $val['last_name']; ?></td>
                                                    <td><?php echo $val['quiz_name']; ?></td>
                                                    <td  class="d-none" ><?php echo $val['result_status']; ?></td>
                                                    <td><?php echo number_format($val['percentage_obtained'], 2); ?>%</td>
                                                    <td>
                                                        <?php
                                                        $roomID = $val['video_room_id'];
                                                        $VrecordID = "";
                                                        if ($roomID) {
                                                            $login = '5e5a605c90ef80ca2a783172';
                                                            $password = 'unu9eMupena6ezy3ava2epeQuLuVaMyzeUuH';
                                                            $url = 'https://api.enablex.io/v1/archive/room/' . $roomID;
                                                            $ch = curl_init();
                                                            curl_setopt($ch, CURLOPT_URL, $url);
                                                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                                                            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
                                                            curl_setopt($ch, CURLOPT_USERPWD, "$login:$password");
                                                            $result = curl_exec($ch);

                                                            curl_close($ch);
                                                            $res = json_decode($result, true);
                                                            $VrecordID = @$res['archive'][0]['transcoded'][0]['url'];
                                                            if ($VrecordID && $logged_in['su'] != 4) {
                                                                ?>
                                                                <a href="javascript:void(0)" data-roomid ="<?= $VrecordID ?>" onclick="showVideo(this)">
                                                                    <i class="la la-file-video-o" title="Show Recording"></i>
                                                                </a>


                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                        <?php if ($val['video_conference_flag'] > 1 && $VrecordID  && $logged_in['su'] == 5) { ?>
                                                        <a data-resultid="<?= $val['rid'] ?>"
                                                           data-vocabulary_flag="<?= $val['vocabulary_flag'] ?>"
                                                           data-structure_flag="<?= $val['structure_flag'] ?>"
                                                           data-fluency_flag="<?= $val['fluency_flag'] ?>"
                                                           data-pronunciation_flag="<?= $val['pronunciation_flag'] ?>"
                                                           data-comprehension_flag="<?= $val['comprehension_flag'] ?>"
                                                           data-quizname="<?= $val['quiz_name'] ?>" data-studentname="<?= $val['first_name'] . " " . $val['last_name'] ?>" data-quizid="<?= $val['quid'] ?>" data-studentid="<?= $val['uid'] ?>"
                                                           data-toggle="modal" data-target="#markExam" onclick="markExam(this)" type="submit">
                                                            <i class="la la-check-square-o" title="Mark"></i>
                                                        </a>
                                                        <?php
                                                        }
                                                        ?>

                                                        <?php
                                                        // echo "++".$val['video_conference_flag']."++";
                                                        if (!$VrecordID && $val['video_conference_flag'] > 1 && $val['video_room_id'] != "" && $logged_in['su'] == 4) {

                                                            $roomID = $val['video_room_id'];

                                                            $usertype = "moderator";
                                                            // $usertype = "participant";

                                                            $roomURL = "https://www.thoughtspheres.com/video_call/client/confo.html?";
                                                            $roomURL .= "roomId=" . $roomID;
                                                            $roomURL .= "&usertype=" . $usertype;
                                                            $roomURL .= "&user_ref=Examiner";
                                                            //$roomURL = "https://www.thoughtspheres.com/testvideo/index.html";
                                                            ?>
                                                            <a href="javascript:void(0)" data-roomid ="<?= $roomURL ?>" onclick="showVideo(this)">
                                                                <i class="la la-video-camera" title="Interactions"></i>
                                                            </a>

                                                        <?php } else if ($val['result_status'] != 'Open' && ($access['mock_exam_result_list']['view_result'] || $logged_in['su'] == 1)) { ?>
                                                            <a href="<?php echo site_url('result/view_result/' . $val['rid']); ?>" >
                                                                <i class="la la-eye" title="<?php echo $this->lang->line('view'); ?>"></i>
                                                            </a>
                                                        <?php } ?>

                                                        <?php
                                                        if ($logged_in['su'] == '1') {
                                                            ?>
                                                            <a class="d-none" href="javascript:remove_entry('result/remove_result/<?php echo $val['rid']; ?>');">
                                                                <i class="la la-trash text-danger" title="Remove Result"></i>
                                                                <!--<img src="<?php echo base_url('images/cross.png'); ?>">-->
                                                            </a>
                                                            <?php
                                                        }
                                                        ?>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                    <?= $links; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--update template design-->

<div class="modal fade" id="markExam" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title pull-left"></h4>
                <button type="button" class="close pull-right" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <label>
                            <strong>Student Name</strong>
                        </label> : <span id="studentname"></span>
                    </div>
                </div>
                <form method="post" name="mark_exam" id="mark_exam" action="<?php echo site_url('examiner/update_mark/'); ?>">
                    <input type="hidden" id="quiz_id" name="quiz_id" value=""/>
                    <input type="hidden" id="student_id" name="student_id" value=""/>
                    <input type="hidden" id="result_id" name="result_id" value=""/>
                    <input type="hidden" id="video_exam" name="video_exam" value="1"/>
                    <div class="row">
                        <div class="form-group col-md-6" id="vocabulary_score_div">
                            <label>Vocabulary </label>
                            <input class="form-control" required type="number" max="10" id="vocabulary_score" name="vocabulary_score" onkeyup="summark()" />
                        </div>
                        <div class="form-group col-md-6">
                            <label>Comment </label>
                            <textarea name="vocabulary_comment"></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6" id="structure_score_div">
                            <label>Structure </label>
                            <input class="form-control" required type="number" max="10" id="structure_score" name="structure_score" onkeyup="summark()" />
                        </div>
                        <div class="form-group col-md-6">
                            <label>Comment </label>
                            <textarea name="structure_comment"></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6" id="fluency_score_div">
                            <label>Fluency </label>
                            <input class="form-control" required type="number" max="10" id="fluency_score" name="fluency_score" onkeyup="summark()" />
                        </div>
                        <div class="form-group col-md-6">
                            <label>Comment </label>
                            <textarea name="fluency_comment"></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6" id="pronunciation_score_div">
                            <label>Pronunciation </label>
                            <input class="form-control" required type="number" max="10" id="pronunciation_score" name="pronunciation_score" onkeyup="summark()" />
                        </div>
                        <div class="form-group col-md-6">
                            <label>Comment </label>
                            <textarea name="pronunciation_comment"></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6" id="comprehension_score_div">
                            <label>Comprehension </label>
                            <input class="form-control" required type="number" max="10" id="comprehension_score" name="comprehension_score" onkeyup="summark()" />
                        </div>
                        <div class="form-group col-md-6">
                            <label>Comment </label>
                            <textarea name="comprehension_comment"></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Mark Obtained</label>
                        <input class="form-control" readonly required type="number" id="score" max="10" name="score" />
                    </div>
                    <div class="form-actions">
                        <input type="submit" class="btn btn-success" value="Save" id="submit">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="container" style="display: none;">


    <hr>
    <h3><?php echo $title; ?></h3>

    <div class="row">

        <div class="col-lg-6">
            <form method="post" action="<?php echo site_url('result/index/'); ?>">
                <div class="input-group">
                    <input type="text" class="form-control" name="search" placeholder="<?php echo $this->lang->line('search'); ?>...">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="submit"><?php echo $this->lang->line('search'); ?></button>
                    </span>


                </div><!-- /input-group -->
            </form>
        </div><!-- /.col-lg-6 -->
    </div><!-- /.row -->
</div>

<script>
    $(function () {
        $("#start_date").datepicker({
            dateFormat: 'yy-mm-dd',
            'maxDate': 0,
            onSelect: function (selected) {
                $("#end_date").datepicker("option", "minDate", selected)
            }
        });
    });
    $(function () {
        $("#end_date").datepicker({
            dateFormat: 'yy-mm-dd',
            'maxDate': 0
        });
    });

    function showVideo(e) {
        var roomId = $(e).attr('data-roomid');
        window.open(roomId, "Exam Room", "_blank");
    }

    function summark() {
        var flg = 0;

        var vocabulary_score = 0;
        if ($("#vocabulary_score_div").is(":visible")) {
            flg = flg + 1;
            var vocabulary_score = parseInt($('#vocabulary_score').val(), 10);

        }

        var structure_score = 0;
        if ($("#structure_score_div").is(":visible")) {
            flg = flg + 1;
            var structurescore = $('#structure_score').val() ? $('#structure_score').val() : 0;
            var structure_score = parseInt(structurescore, 10);

        }
        console.log(structure_score);
        var fluency_score = 0;
        if ($("#fluency_score_div").is(":visible")) {
            flg = flg + 1;
            var fluencyscore = $('#fluency_score').val() ? $('#fluency_score').val() : 0
            var fluency_score = parseInt(fluencyscore, 10);
        }
        var pronunciation_score = 0;
        if ($("#pronunciation_score_div").is(":visible")) {
            flg = flg + 1;
            var pronunciationscore = $('#pronunciation_score').val() ? $('#pronunciation_score').val() : 0;
            var pronunciation_score = parseInt(pronunciationscore, 10);

        }

        var comprehension_score = 0;
        if ($("#comprehension_score_div").is(":visible")) {
            flg = flg + 1;
            var comprehensionscore = $('#comprehension_score').val() ? $('#comprehension_score').val() : 0;
            var comprehension_score = parseInt(comprehensionscore, 10);

        }

        var total = vocabulary_score + structure_score + fluency_score + pronunciation_score + comprehension_score;
        $('#score').val(total / flg);
    }

    function markExam(e) {
        var resultid = $(e).attr('data-resultid');
        var studentid = $(e).attr('data-studentid');
        var quizid = $(e).attr('data-quizid');
        var quizname = $(e).attr('data-quizname');
        var studentname = $(e).attr('data-studentname');
        var vocabulary_flag = $(e).attr('data-vocabulary_flag');
        var structure_flag = $(e).attr('data-structure_flag');
        var fluency_flag = $(e).attr('data-fluency_flag');
        var pronunciation_flag = $(e).attr('data-pronunciation_flag');
        var comprehension_flag = $(e).attr('data-comprehension_flag');

        if (vocabulary_flag) {
            $('#vocabulary_score_div').show();
        } else {
            $('#vocabulary_score_div').hide();
        }

        if (structure_flag) {
            $('#structure_score_div').show();
        } else {
            $('#structure_score_div').hide();
        }

        if (fluency_flag) {
            $('#fluency_score_div').show();
        } else {
            $('#fluency_score_div').hide();
        }
        if (pronunciation_flag) {
            $('#pronunciation_score_div').show();
        } else {
            $('#pronunciation_score_div').hide();
        }
        if (comprehension_flag) {
            $('#comprehension_score_div').show();
        } else {
            $('#comprehension_score_div').hide();
        }

        $("#student_id").val(studentid);
        $("#quiz_id").val(quizid);
        $("#result_id").val(resultid);
        $("h4.modal-title").text(quizname);
        $("#studentname").text(studentname);
    }
</script>