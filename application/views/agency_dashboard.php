<link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/animate.min.css">
<script src="<?= base_url() ?>theme-assets/vendors/js/animation/jquery.appear.js" type="text/javascript"></script>
<script src="<?= base_url() ?>js/animation.js" type="text/javascript"></script>
<!--update template design-->
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-wrapper-before"></div>
        <div class="content-header row">
            <div class="content-header-left col-md-4 col-12 mb-2">
                <!--<h3 class="content-header-title"><?= $title; ?></h3>-->
            </div>
        </div>
        <div class="content-body">
            <section id="ecommerce-stats">
                <div class="row">
                    <div class="col-xl-4 col-lg-6 col-md-12 zoomIn">
                        <div class="card pull-up ecom-card-1 bg-gradient-x-purple-red animated zoomIn">
                            <div class="card-content ecom-card2 height-150">
                                <div class="card-body">
                                    <div class="media d-flex">
                                        <div class="align-self-bottom mt-3">
                                            <i class="la la-usd icon-opacity text-white font-large-4 float-left"></i>
                                        </div>
                                        <div class="align-self-top media-body text-white text-right ">
                                            <span class="d-block mb-1 font-medium-1 font-weight-bold"><?= $this->lang->line('card_balance') ?></span>
                                            <h1 class="text-white mb-0"><?php echo $total_balance; ?></h1>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-6 col-md-12">
                        <div class="card pull-up ecom-card-1 bg-gradient-x-orange-yellow animated zoomIn">
                            <div class="card-content ecom-card2 height-150">
                                <div class="card-body">
                                    <div class="media d-flex">
                                        <div class="align-self-bottom mt-3">
                                            <i class="la la-laptop icon-opacity text-white font-large-4 float-left"></i>
                                        </div>
                                        <div class="align-self-top media-body text-white text-right ">
                                            <span class="d-block mb-1 font-medium-1 font-weight-bold"><?= $this->lang->line('card_completedexam') ?></span>
                                            <h1 class="text-white mb-0"><?php echo $completed_exam; ?></h1>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-12">
                        <div class="card pull-up ecom-card-1 bg-gradient-x-blue-green animated zoomIn">
                            <div class="card-content ecom-card2 height-150 zoomIn">
                                <div class="card-body">
                                    <div class="media d-flex">
                                        <div class="align-self-bottom mt-3">
                                            <i class="la la-clock-o icon-opacity text-white font-large-4 float-left"></i>
                                        </div>
                                        <div class="align-self-top media-body text-white text-right ">
                                            <span class="d-block mb-1 font-medium-1 font-weight-bold"><?= $this->lang->line('card_subscribedexam') ?></span>
                                            <h1 class="text-white mb-0"><?php echo $subscribed_exam; ?></h1>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-content collpase collapse show">
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-striped">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th class="text-center"><?= $this->lang->line('dashboard_username') ?></th>
                                                    <th class="text-center"><?php echo $this->lang->line('description'); ?></th>
                                                    <th class="text-center"><?php echo $this->lang->line('transaction_type'); ?> </th>
                                                    <th class="text-center"><?php echo $this->lang->line('amount'); ?> </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                if (count($result) == 0) {
                                                    ?>
                                                    <tr>
                                                        <td colspan="5"><?php echo $this->lang->line('no_record_found'); ?></td>
                                                    </tr>
                                                    <?php
                                                }
                                                foreach ($result as $key => $val) {
                                                    ?>
                                                    <tr>
                                                        <td><?php echo $val['id']; ?></td>
                                                        <td><?= $val['center_name'] ?></td>
                                                        <td><?= $val['description'] ? $val['description'] : "N/A" ?></td>
                                                        <td>
                                                            <?= $val['flag'] == 1 ? "Cr" : "Dr"; ?>
                                                        </td>
                                                        <td><?= $val['amount']; ?></td>
                                                    </tr>
                                                    <?php
                                                }
                                                ?>
                                            </tbody>
                                        </table>
                                        <div class="pull-right">
                                            <a href="<?= base_url('agency/transaction_listing'); ?>">
                                                <button type="button" class="btn btn-primary">View All</button>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>   
        </div>
    </div>
</div>
</div>


