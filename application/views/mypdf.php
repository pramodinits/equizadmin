<!DOCTYPE html>

<html>
    <head>
        <title>Result PDF</title>
        <style>
            .text-center {
                text-align: center;
            }
            .col-md-12 {
                width: 100%;
            }
            .font-weight-bold {
                font-weight: bold;
            }
            h2, h4, h3 {
                font-weight: normal;    
            }
            .row {
                float: left;
            }
            .col-md-4 {
                width: 33.33333%;
                width: 33.33333%;
                float: left;
            }
            .table {
                width: 100%;
            }
            .table thead th {
                vertical-align: bottom;
                border-top: 1px solid #e3ebf3;
                border-bottom: 2px solid #e3ebf3;
            }
            .table td, .table th{
                border: 1px solid #e3ebf3;
                padding: 9px 8px !important;
                text-align: center;
            }
        </style>
    </head>

    <body>
        <div class="app-content content">
            <div class="content-wrapper">
                <div class="content-body">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <img src="/var/www/html/equizAdmindesign/images/logo_blue.png" class="img-fluid">
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-md-12 text-center">
                            <h2>English Language Proficiency Test Certificate</h2>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-md-12 text-center">
                            <h2>English Language Department</h2>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-md-12 text-center">
                            <h3 class="font-weight-bold">Certificate awarded to</h3>
                            <h1 class="font-weight-bold">
                                <?= $result['first_name'] . " " . $result['last_name'] ?>
                            </h1>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-md-12">
                            <div class="row" style="display: inline-block">
                                <h3>
                                    <span class="text-center">
                                        <strong>Email ID : </strong>
                                        <?= $result['email']; ?>
                                    </span>
                                    <span style="padding-left: 20%;">
                                        <strong>Contact No : </strong>
                                        <?= $result['contact_no']; ?>
                                    </span>
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-md-12 text-center">
                            <h3>
                                <strong>Date of Birth : </strong>
                                <?php 
//                                date_default_timezone_set('Asia/Kolkata');
                                echo $dob; 
                                ?>
                            </h3>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-md-12 text-center">
                            <h3>
                                For achieving ICAO equivalency
                                <br/><br/>
                                Level 5
                            </h3>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-md-12 text-center">
                            <div class="row" style="display: inline-block">
                                <h3>
                                    <span style="margin-left: -1%;">
                                        <strong>Start Date : </strong>
                                        <?= $start_date; ?>
                                    </span>
                                    <span style="padding-left: 20%;">
                                        <strong>Expiry Date : </strong>
                                        <?php
                                        echo $expirydate;
                                        ?>
                                    </span>
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-md-12 text-center">
                            <h3>
                                <strong>Exam Category : </strong>
                                <?= $exam_category[$result['exam_type']]; ?>
                            </h3>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-md-12 text-center">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <?php if (!empty($result['pronunciation_q_ids'])) { ?>
                                                <th class="text-center">Pronunciation</th>
                                            <?php } ?>
                                            <?php if (!empty($result['structure_q_ids'])) { ?>
                                                <th class="text-center">Structure</th>
                                            <?php } ?>
                                            <?php if (!empty($result['vocabulary_q_ids'])) { ?>
                                                <th class="text-center">Vocabulary</th>
                                            <?php } ?>
                                            <?php if (!empty($result['fluency_q_ids'])) { ?>
                                                <th class="text-center">Fluency</th>
                                            <?php } ?>
                                            <?php if (!empty($result['comprehension_q_ids'])) { ?>
                                                <th class="text-center">Comprehension</th>
                                            <?php } ?>
                                            <?php if ($result['video_conference_flag'] == 1) { ?>
                                                <th class="text-center">Interactions</th> 
                                                <?php } ?>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <?php if (!empty($result['pronunciation_q_ids'])) { ?>
                                                <td><?= $result['pronunciation_percentage'] ? intval($result['pronunciation_percentage']/10) : 0 ?></td>
                                            <?php } ?>
                                            <?php if (!empty($result['structure_q_ids'])) { ?>
                                                <td><?= $result['structure_percentage'] ? intval($result['structure_percentage']/10) : 0 ?></td>
                                            <?php } ?>
                                            <?php if (!empty($result['vocabulary_q_ids'])) { ?>
                                                <td><?= $result['vocabulary_percentage'] ? intval($result['vocabulary_percentage']/10) : 0 ?></td>
                                            <?php } ?>
                                            <?php if (!empty($result['fluency_q_ids'])) { ?>
                                                <td><?= $result['fluency_percentage'] ? intval($result['fluency_percentage']/10) : 0 ?></td>
                                            <?php } ?>
                                            <?php if (!empty($result['comprehension_q_ids'])) { ?>
                                                <td><?= $result['comprehension_percentage'] ? intval($result['comprehension_percentage']/10) : 0 ?></td>
                                            <?php } ?>
                                            <?php if ($result['video_conference_flag'] == 1) { ?>
                                                <td><?= intval($result['video_conference_percentage']/10) ?></td>
                                            <?php } ?>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>

</html>
