<style>
    .fa-pencil-square-o:before {
        content: "\f044";
    }
    .magicsearch-wrapper {
        width: 100% !important;
    }
    input.magicsearch {
        width: 100% !important;
    }
</style>
<script src="//code.jquery.com/jquery-3.1.0.min.js"></script>

<!--update template design-->
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-wrapper-before"></div>
        <div class="content-header row">
            <div class="content-header-left col-md-4 col-12 mb-2">
                <h3 class="content-header-title"></h3>
            </div>
        </div>
        <div class="content-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collpase collapse show">
                            <div class="card-body">
                                <?php
                                if ($this->session->flashdata('message')) {
                                    echo $this->session->flashdata('message');
                                }
                                ?>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th class="text-center"><?php echo $this->lang->line('date'); ?></th>
                                                <th class="text-center"><?php echo $this->lang->line('description'); ?></th>
                                                <th class="text-center"><?php echo $this->lang->line('transaction_type'); ?> </th>
                                                <th class="text-center"><?php echo $this->lang->line('amount'); ?> </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                if (count($result) == 0) {
                    ?>
                    <tr>
                        <td colspan="5"><?php echo $this->lang->line('no_record_found'); ?></td>
                    </tr>
                    <?php
                }
                foreach ($result as $key => $val) {
                    ?>
                    <tr>
                        <td><?php echo $val['id']; ?></td>
                        <td><?php echo date('d-m-Y h:i:s', strtotime($val['add_date'])); ?></td>
                        <td><?= $val['description'] ? $val['description'] : "N/A" ?></td>
                        <td>
                            <?= $val['flag'] == 1 ? "Cr" : "Dr"; ?>
                        </td>
                        <td><?= $val['amount']; ?></td>
                    </tr>
                    <?php
                }
                ?>
                                        </tbody>
                                    </table>
                                    <?= $links; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--update template design-->
<script src="//code.jquery.com/jquery-3.1.0.min.js"></script>
<script>
                                                        $(function () {
                                                            $('.alert').delay(3000).show().fadeOut('slow');
                                                        });


</script>