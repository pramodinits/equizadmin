<style>
    .fa-pencil-square-o:before {
        content: "\f044";
    }
    .magicsearch-wrapper {
        width: 100% !important;
    }
    input.magicsearch {
        width: 100% !important;
    }
    .exam-card {
        border-radius: 7px;
        background:#fff;
        box-shadow: 0px 0px 10px -2px #ccc;
        padding: 10% 5%;
        min-height: 230px;
    }
    .exam-card h6 {
        color:#05038B;
        text-align: center;
        line-height: 25px;
    }
    .exam-card h5 {
        color:#05038B;
        text-align: center;
        line-height: 25px;
        font-size: 1.15rem;
    }
    hr {
        border: 1.5px solid #05038B;
        margin-bottom: 10%;
    }
    .btn-default {
        padding: .30rem .55rem !important;
    }
</style>
<div class="container">
    <h3><?php echo $title; ?></h3>
    <div class="row" style="margin-top: 3%;">
        <?php
        if (count($result) == 0) {
            foreach ($exam_list as $k => $v) {
                $student_name = $v['first_name'] . " " . $v['last_name'];
                $start_time = strtotime($v['start_date'] . " " . $v['start_time']);
                $end_time = strtotime($v['end_date'] . " " . $v['end_time']);
                //echo $end_time - $start_time; 
                $exam_status = $start_time - time();
                if ($exam_status > 0) {
                    $border = "style = 'border: 2px solid grey !important'"; //in future
                } elseif (($start_time <= time()) && ($end_time >= time())) {
                    $border = "style = 'border: 2px solid green !important'"; //ongoing exam
                } elseif ($exam_status < 0) {
                    $border = "style = 'border: 2px solid red !important'"; //past exam
                }
                ?>
                <div class="col-md-4">
                    <div class="card exam-card mb-4" <?= $border ?>>
                        <h5 style="color:#05038B" class="font-weight-bold text-center">
                            <?= $v['quiz_name']; ?>
                        </h5><br/>
                        <h5><?= $student_name; ?> | 60 min
                            <?= $v['video_conference'] == 1 ? "| <i class='fa fa-film'></i>" : "" ?>
                        </h5><br/>
                        <div class="row" style="display: none;">
                            <div class="col-md-6">
                                <h6><b>Start Time</b><br>
                                <?= date('d-m-Y', strtotime($v['start_date'])) . " " . date('H:m A', strtotime($v['start_time'])); ?>
                                </h6>
                            </div>
                            <div class="col-md-6">
                                <h6>
                                    <b>End Time</b><br>
                        <?= date('d-m-Y', strtotime($v['end_date'])) . " " . date('H:m A', strtotime($v['end_time'])); ?>
                                </h6>
                            </div>
                        </div>

                        <hr>
                        <!--<div class="card-footer"></div>-->
                        <?php if ($v['video_conference'] == 1) { ?>
                            <div class="row" style="margin: 0 auto;">
                                <button id="startExam" class="btn btn-default text-center float-left mr-1" type="submit">Start</button>
                                <button class="btn btn-default text-center" data-examinerid="<?= $v['examiner_id'] ?>"
                                        data-quizname="<?= $v['quiz_name'] ?>" data-studentname="<?= $student_name ?>" data-quizid="<?= $v['quiz_id'] ?>" data-studentid="<?= $v['student_id'] ?>"
                                        data-toggle="modal" data-target="#markExam" onclick="markExam(this)" type="submit">Mark</button>
                            </div>
                        <?php } else { ?>

                        <?php } ?>
                        
                    </div>
                </div>
                <?php
            }
        } else {
            echo "No exam found.";
        }
        ?>
    </div><!-- /.row -->  


    <div class="modal fade" id="markExam" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title pull-left"></h4>
                    <button type="button" class="close pull-right" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label>
                                <strong>Student Name</strong>
                            </label> : <span id="studentname"></span>
                        </div>
                    </div>
                    <form method="post" name="mark_exam" id="mark_exam" action="<?php echo site_url('examiner/update_mark/');?>">
                        <input type="hidden" id="quiz_id" name="quiz_id" value=""/>
                        <input type="hidden" id="student_id" name="student_id" value=""/>
                        <input type="hidden" id="examiner_id" name="examiner_id" value=""/>
                        <div class="form-group">
                            <label>Mark Obtained</label>
                            <input class="form-control" required type="text" name="score" />
                        </div>
                        <div class="form-actions">
                            <input type="submit" class="btn btn-success" value="Save" id="submit">
                        </div>
                    </form>
                    <span id="examiner_id"></span>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>    
    <script>
                                            $(document).ready(function () {
                                                $('#mark_exam').validate({
                                                    rules: {
                                                        'score': {required: true}
                                                    },
                                                    submitHandler: function (form) { // for demo
                                                        $('button[type=submit]').attr('disabled', 'disabled');
                                                        return true; // for demo
                                                    }
                                                });

                                                $("button#startExam").click(function () {
                                                    window.open('https://www.thoughtspheres.com/testvideo/index.html', "test", "_blank");
                                                });
                                            });
                                            function markExam(e) {
                                                var examinerid = $(e).attr('data-examinerid');
                                                var studentid = $(e).attr('data-studentid');
                                                var quizid = $(e).attr('data-quizid');
                                                var quizname = $(e).attr('data-quizname');
                                                var studentname = $(e).attr('data-studentname');
                                                $("#student_id").val(studentid);
                                                $("#quiz_id").val(quizid);
                                                $("#examiner_id").val(examinerid);
                                                $("h4.modal-title").text(quizname);
                                                $("#studentname").text(studentname);
                                            }
    </script>