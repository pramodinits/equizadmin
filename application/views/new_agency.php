<!--for datepicker-->
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<!--for datepicker-->

<!--for country list-->
<script src="<?php echo base_url(); ?>vendor/jquery/countries.js"></script>
<!--for country list-->

<style>
    .btn-default {
        background: #05038b;
        color: #fff;
        font-weight: bold;
        margin: 0 auto;
    }
</style>

<!--updated template page-->
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-wrapper-before"></div>
        <div class="content-header row">
            <div class="content-header-left col-md-4 col-12 mb-2">
                <h3 class="content-header-title"></h3>
            </div>
        </div>
        <div class="content-body">
            <section id="horizontal-form-layouts">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-content collpase collapse show">
                                <div class="card-body">
                                    <?php
                                    if ($this->session->flashdata('message')) {
                                        echo $this->session->flashdata('message');
                                    }
                                    ?>
                                    <form method="post" id="register_user" name="register_user" action="<?php echo site_url('agency/insert_agency/'); ?>">
                                        <div class="form-body">
                                            <input type="hidden" name="uid" value="<?= @$result['uid'] ?>"/>
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group row">
                                                            <label class="col-md-3 label-control"><?php echo $this->lang->line('agency_name'); ?></label> 
                                                            <div class="col-md-9">
                                                                <input type="text" value="<?= @$result['company'] ?>"  name="agency[company]"  class="form-control border-primary" placeholder="<?php echo $this->lang->line('agency_name'); ?>" required  autofocus>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">	 
                                                            <label class="col-md-3 label-control"><?php echo $this->lang->line('city'); ?></label> 
                                                            <div class="col-md-9">
                                                                <select class="form-control border-primary" name="agency[id_city]" id="id_city" required onchange="getLocation('id_city', 'kams_location')">
                                                                    <option value=""><?php echo $this->lang->line('select_city'); ?></option>
                                                                    <?php
                                                                    foreach ($city_list as $k => $v) {
                                                                        ?>
                                                                        <option value="<?php echo $k; ?>" <?= @$result['id_city'] == $k ? "selected" : "" ?>><?php echo $v; ?></option>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-md-3 label-control"><?php echo $this->lang->line('location'); ?></label> 
                                                            <div class="col-md-9">
                                                                <select class="form-control border-primary" id="id_location" name="agency[id_location]" required>
                                                                    <option value="">Select Location</option>
                                                                    <?php foreach($location_list as $k => $v) { ?>
                                                                    <option value="<?= $k ?>" <?= $k == $result['id_location'] ? "selected" : "" ?>><?= $v; ?></option>
                                                                    <?php } ?>
                                                                </select>
                                                                <!--<textarea class="form-control" required name="agency[location]"><?= @$result['location'] ?></textarea>-->
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-md-3 label-control"><?php echo $this->lang->line('address'); ?></label> 
                                                            <div class="col-md-9">
                                                                <textarea class="form-control border-primary" required name="agency[address]"><?= @$result['address'] ?></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group row">
                                                            <label class="col-md-3 label-control"><?php echo $this->lang->line('agency_approval_number'); ?></label> 
                                                            <div class="col-md-9">
                                                                <input type="text" value="<?= @$result['license_number'] ?>"  name="agency[license_number]"   class="form-control border-primary" placeholder="<?php echo $this->lang->line('agency_approval_number'); ?>" required  autofocus>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-md-3 label-control"><?php echo $this->lang->line('validity_date'); ?></label> 
                                                            <div class="col-md-9">
                                                                <input type="date" onkeydown="return false;" name="agency[valid_upto]" value="<?= @$result['valid_upto'] ?>" required  min="<?php echo date('Y-m-d');?>"  class="form-control border-primary" placeholder="<?php echo $this->lang->line('validity_date'); ?>" required  autofocus>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-md-3 label-control"><?php echo $this->lang->line('status'); ?></label> 
                                                             <div class="col-md-9">
                                                                <select class="form-control border-primary" name="agency[user_status]" required>
                                                                    <option value=""><?php echo $this->lang->line('status'); ?></option>
                                                                    <option value="Active" <?= (@$result['user_status'] =="Active" )? "selected":"" ?> ><?php echo $this->lang->line('active'); ?></option>
                                                                    <option value="Inactive" <?= (@$result['user_status'] =="Inactive") ? "selected":"" ?> ><?php echo $this->lang->line('inactive'); ?></option>
                                                                   
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-md-3 label-control"><?php echo $this->lang->line('markup_price'); ?></label> 
                                                            <div class="col-md-9">
                                                                <input type="text" value="<?= @$result['markup_price'] ? @$result['markup_price'] : 0 ?>"  name="agency[markup_price]"  class="form-control border-primary" placeholder="<?php echo $this->lang->line('markup_price'); ?>" required  autofocus>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="col-md-12">
                                                <br>
                                                <div class="login-panel panel panel-default">
                                                    <div class="panel-body">
                                                        <?php
                                                        if ($this->session->flashdata('message')) {
                                                            echo $this->session->flashdata('message');
                                                        }
                                                        ?>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group row">
                                                                    <label class="col-md-3 label-control"><?php echo $this->lang->line('first_name'); ?></label> 
                                                                    <div class="col-md-9">
                                                                        <input type="text" value="<?= @$result['first_name'] ?>"  name="agency[first_name]"  class="form-control border-primary" placeholder="<?php echo $this->lang->line('first_name'); ?>" required  autofocus>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label class="col-md-3 label-control"><?php echo $this->lang->line('contact_no'); ?></label> 
                                                                    <div class="col-md-9">
                                                                        <input type="text" value="<?= @$result['contact_no'] ?>" required name="agency[contact_no]"  class="form-control border-primary" placeholder="<?php echo $this->lang->line('contact_no'); ?>" autofocus>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label class="col-md-3 label-control"><?php echo $this->lang->line('email'); ?></label> 
                                                                    <div class="col-md-9">
                                                                        <input type="email" value="<?= @$result['email'] ?>" id="inputEmail" required name="email" class="form-control border-primary" placeholder="<?php echo $this->lang->line('email'); ?>"   autofocus>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group row">
                                                                    <label class="col-md-3 label-control"><?php echo $this->lang->line('last_name'); ?></label> 
                                                                    <div class="col-md-9">
                                                                        <input type="text" value="<?= @$result['last_name'] ?>" name="agency[last_name]"  class="form-control border-primary" placeholder="<?php echo $this->lang->line('last_name'); ?>" required  autofocus>
                                                                    </div>
                                                                </div>
                                                                <?php if (@$result['password']) { ?>
                                                                    <div class="form-group text-right" id="show_password">
                                                                        <p><a href="javascript:void(0)">Do you want to change Password?</a></p>
                                                                    </div>
                                                                <?php } ?>
                                                                <div id="password" class="form-group row" style="<?= @$result['password'] ? "display:none;" : "" ?>">	 
                                                                    <label class="col-md-3 label-control"><?php echo $this->lang->line('password'); ?></label> 
                                                                    <div class="col-md-9">
                                                                        <input type="password" required id="password_info" name="password" class="form-control border-primary" placeholder="<?php echo $this->lang->line('password'); ?>" autofocus>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row" style="<?= @$result['password'] ? "display:none;" : "" ?>">	 
                                                                    <label class="col-md-3 label-control"><?php echo $this->lang->line('confirm_password'); ?></label> 
                                                                    <div class="col-md-9">
                                                                        <input type="password" required id="confirm_password" name="confirm_password" class="form-control" placeholder="<?php echo $this->lang->line('confirm_password'); ?>" autofocus>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-actions right">
                                            <button class="btn btn-primary" type="submit">
                                                <i class="la la-check-square-o"></i>
                                                <?php echo $this->lang->line('submit'); ?>
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
<!--updated template page-->
<script>
    // getexpiry();
</script>
<!--<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.1/dist/jquery.validate.js"></script>-->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/additional-methods.js"></script>
<script>
    $(document).ready(function () {
        $("#show_password a").click(function () {
            $("#password").toggle();
        });

        $('#register_user').validate({// initialize the plugin
            rules: {
                'agency[company]': {
                    required: true
                },
                'agency[first_name]': {
                    required: true,
                    minlength: 3,
                    lettersonly: true
                },
                'agency[last_name]': {
                    required: true,
                    minlength: 2,
                    lettersonly: true
                },
                'agency[contact_no]': {
                    required: true,
                    digits: true
                },
                'agency[markup_price]': {
                  digits: true  
                },
                confirm_password: {
                    equalTo: "#password_info"
                }
            },
            submitHandler: function (form) { // for demo
                $('button[type=submit]').attr('disabled', 'disabled');
                return true; // for demo
            }
        });
    });
    
    function getLocation(id, tbl) {
        if (id == "id_city") {
            $('#id_location').html("<option value='' >Select Location</option>");
        }
        var selected_val = $('select#' + id + ' option:selected').val();
        var url = "<?= site_url('agency/getLocation/'); ?>";
        $.post(url, {"selected_val": selected_val, "id": id, "tbl": tbl}, function (res) {
            $("#id_location").html(res);
        });
    }
</script>