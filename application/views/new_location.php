<style>
    .btn-default {
        background: #05038b;
        color: #fff;
        font-weight: bold;
        margin: 0 auto;
    }
</style>

<!--updated template page-->
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-wrapper-before"></div>
        <div class="content-header row">
            <div class="content-header-left col-md-4 col-12 mb-2">
                <h3 class="content-header-title"></h3>
            </div>
        </div>
        <div class="content-body">
            <section id="horizontal-form-layouts">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-content collpase collapse show">
                                <div class="card-body">
                                    <form method="post" id="add_location" name="add_location" action="<?php echo site_url('location/insert_location/'); ?>">
                                        <div class="form-body">
                                            <input type="hidden" name="id_location" value="<?= @$result['id_location'] ?>"/>
                                            <div class="col-md-12">
                                                <br>
                                                <div class="login-panel panel panel-default">
                                                    <div class="panel-body">
                                                        <?php
                                                        if ($this->session->flashdata('message')) {
                                                            echo $this->session->flashdata('message');
                                                        }
                                                        ?>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                               <div class="form-group row">
                                                                    <label class="col-md-3 label-control"><?php echo $this->lang->line('city_name'); ?></label> 
                                                                    <div class="col-md-9">
                                                                        <select class="form-control border-primary" name="location[id_city]" required>
                                                                            <option value=""><?= $this->lang->line('select_city') ?></option>
                                                                            <?php foreach ($city_list as $k => $v) { ?>
                                                                            <option value="<?= $k ?>" <?= @$result['id_city'] == $k ? "selected" : "" ?>><?= $v ?></option>
                                                                           <?php } ?>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group row">
                                                                    <label class="col-md-3 label-control"><?php echo $this->lang->line('location_name'); ?></label> 
                                                                    <div class="col-md-9">
                                                                        <input type="text" value="<?= @$result['location_name'] ?>"  name="location[location_name]"  class="form-control border-primary" placeholder="<?php echo $this->lang->line('location_name'); ?>" required>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-actions right">
                                            <button class="btn btn-primary" type="submit">
                                                <i class="la la-check-square-o"></i>
                                                <?php echo $this->lang->line('submit'); ?>
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
<!--updated template page-->

<script>
    // getexpiry();
</script>
<!--<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.1/dist/jquery.validate.js"></script>-->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
<script>
    $(document).ready(function () {
        $('#add_location').validate({// initialize the plugin
            rules: {
                
            },
            submitHandler: function (form) { // for demo
                $('button[type=submit]').attr('disabled', 'disabled');
                return true; // for demo
            }
        });
    });
</script>