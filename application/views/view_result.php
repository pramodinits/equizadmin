<?php
$logged_in = $this->session->userdata('logged_in');

function ordinal($number) {
    $ends = array('th', 'st', 'nd', 'rd', 'th', 'th', 'th', 'th', 'th', 'th');
    if ((($number % 100) >= 11) && (($number % 100) <= 13))
        return $number . 'th';
    else
        return $number . $ends[$number % 10];
}

$score_obtained = 0;
if (!empty($result['pronunciation_q_ids'])) {
    $score_obtained += $result['pronunciation_score'];
}
if (!empty($result['structure_q_ids'])) {
    $score_obtained += $result['structure_score'];
}
if (!empty($result['vocabulary_q_ids'])) {
    $score_obtained += $result['vocabulary_score'];
}
if (!empty($result['fluency_q_ids'])) {
    $score_obtained += $result['fluency_score'];
}
if (!empty($result['comprehension_q_ids'])) {
    $score_obtained += $result['comprehension_score'];
}
if ($result['video_conference_flag'] == 1) {
    $score_obtained += intval($result['video_conference_percentage']/10);
}


//individual score
$ind_score = explode(',', $result['score_individual']);
?>
<script src="//code.jquery.com/jquery-3.1.0.min.js"></script>

<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-wrapper-before"></div>
        <div class="content-header row">
            <div class="content-header-left col-md-4 col-12 mb-2">
                <h3 class="content-header-title"></h3>
            </div>
        </div>
        <div class="content-body">
            <section id="horizontal-form-layouts">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="row card-title">
                                    <div class="col-md-6"><h4><?= $result['quiz_name']; ?></h4></div>
                                    <div class="col-md-6 mb-1">
                                        <div class="pull-right text-right">
                                            <i class="la la-file-pdf-o font-large-1" title="Download Result PDF"></i>
                                            <a href="<?= base_url('result_pdf/') . $result['report_pdf'] ?>" target="_blank" download="">
                                            <?= $result['report_pdf'] ?>
                                        </a>
                                    </div>
                                    </div>
                                </div>
                                <span class="text-medium-1 info line-height-2 text-uppercase text-bold-600">
                                    <?= $result['first_name'] . " " . $result['last_name'] ?>
                                </span>  
                                <span>
                                    (<?php echo str_replace('{attempt_no}', ordinal($attempt), $this->lang->line('title_result')); ?>)
                                </span>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body pt-0 pb-1">
                                    <p></p>
                                    <div class="row mb-1">
                                        <div class="col-6 col-sm-3 col-md-6 col-lg-2 border-right-blue-grey border-right-lighten-5 text-center">
                                            <p class="font-medium-1 mb-1 teal"><?= $this->lang->line('score_obtained'); ?></p>
                                            <p class="font-medium-3 text-bold-400"><?= $score_obtained; ?></p>
                                        </div>
                                        <div class="col-6 col-sm-4 col-md-4 col-lg-4 border-right-blue-grey border-right-lighten-5 text-center">
                                            <p class="font-medium-1 mb-1 teal"><?= $this->lang->line('attempt_time'); ?></p>
                                            <p class="font-medium-3 text-bold-400"><?= date('Y-m-d H:i:s', $result['start_time']); ?></p>
                                        </div>

                                        <div class="col-6 col-sm-3 col-md-6 col-lg-3 border-right-blue-grey border-right-lighten-5 text-center">
                                            <p class="font-medium-1 mb-1 teal"><?= $this->lang->line('percentage_obtained') ?></p>
                                            <p class="font-medium-3 text-bold-400"><?= number_format($result['percentage_obtained'], 2); ?>%</p>
                                        </div>
                                        <div class="d-none col-6 col-sm-3 col-md-6 col-lg-2 border-right-blue-grey border-right-lighten-5 text-center">
                                            <p class="font-medium-1 mb-1 teal"><?= $this->lang->line('percentile_obtained'); ?></p>
                                            <p class="font-medium-3 text-bold-400"><?= substr(((($percentile[1] + 1) / $percentile[0]) * 100), 0, 5); ?>%</p>
                                        </div>
                                        <div class="col-6 col-sm-3 col-md-6 col-lg-2 border-right-blue-grey border-right-lighten-5 text-center">
                                            <p class="font-medium-1 mb-1 teal"><?= $this->lang->line('status'); ?></p>
                                            <p class="font-medium-3 text-bold-400"><?= $result['result_status'] ?></p>
                                        </div>
                                    </div>
                                    <div class="media d-flex">
                                        <div class="align-self-center">
                                            <h6 class="text-bold-600 mt-2"> <?= $this->lang->line('result_id') ?>:
                                                <span class="info"><?= $result['rid']; ?></span>
                                            </h6>
                                            <h6 class="text-bold-600 mt-1"> <?= $this->lang->line('email') ?>:
                                                <span class="blue-grey"><?= $result['email']; ?></span>
                                            </h6>
                                        </div>
                                    </div>
                                </div>
                                <hr/>
                                <!---category wise analysis-->
                                <div class="row">
                                    <div class="card-text col-md-12">
                                        <h3 class="font-weight-800 my-1 text-success text-center"><?= $this->lang->line('categorywise'); ?></h3>
                                    </div>
                                    <div class="table-responsive col-md-9" style="margin: 0 auto;">
                                        <table class="table table-bordered">
                                            <thead class="bg-primary white">
                                                <tr>
                                                    <th><?php echo $this->lang->line('category_name'); ?></th>
                                                    <th><?php echo $this->lang->line('score_obtained'); ?></th>
                                                    <th style="width: 20%;"><?php echo $this->lang->line('show_details'); ?></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                if (!empty($result['pronunciation_q_ids'])) {
                                                    ?>
                                                    <tr>
                                                        <td>
                                                            <?php echo $this->lang->line('section_pronunciation'); ?>
                                                        </td>
                                                        <td><?php echo $result['pronunciation_score']; ?></td>
                                                        <td>
                                                            <a onclick="questionDetails('<?= $result['pronunciation_q_ids'] ?>')" href="#" data-toggle="modal" data-target="#myModal">
                                                                <i class="la la-eye" aria-hidden="true" title="Show Details"></i>
                                                            </a>&nbsp;
                                                        </td>
                                                    </tr>
                                                    <?php
                                                } if (!empty($result['structure_q_ids'])) {
                                                    ?>
                                                    <tr>
                                                        <td>
                                                            <?php echo $this->lang->line('section_structure'); ?>
                                                        </td>
                                                        <td><?php echo $result['structure_score']; ?></td>
                                                        <td>
                                                            <a onclick="questionDetails('<?= $result['structure_q_ids'] ?>')" href="#" data-toggle="modal" data-target="#myModal">
                                                                <i class="la la-eye" aria-hidden="true" title="Show Details"></i>
                                                            </a>&nbsp;
                                                        </td>
                                                    </tr>
                                                    <?php
                                                } if (!empty($result['vocabulary_q_ids'])) {
                                                    ?>
                                                    <tr>
                                                        <td>
                                                            <?php echo $this->lang->line('section_vocabulary'); ?>
                                                        </td>
                                                        <td><?php echo $result['vocabulary_score']; ?></td>
                                                        <td>
                                                            <a onclick="questionDetails('<?= $result['vocabulary_q_ids'] ?>')" href="#" data-toggle="modal" data-target="#myModal">
                                                                <i class="la la-eye" aria-hidden="true" title="Show Details"></i>
                                                            </a>&nbsp;
                                                        </td>
                                                    </tr>
                                                    <?php
                                                } if (!empty($result['fluency_q_ids'])) {
                                                    ?>
                                                    <tr>
                                                        <td>
                                                            <?php echo $this->lang->line('section_fluency'); ?>
                                                        </td>
                                                        <td><?php echo $result['fluency_score']; ?></td>
                                                        <td>
                                                            <a onclick="questionDetails('<?= $result['fluency_q_ids'] ?>')" href="#" data-toggle="modal" data-target="#myModal">
                                                                <i class="la la-eye" aria-hidden="true" title="Show Details"></i>
                                                            </a>&nbsp;
                                                        </td>
                                                    </tr>
                                                <?php } if (!empty($result['comprehension_q_ids'])) {
                                                    ?>
                                                    <tr>
                                                        <td>
                                                            <?php echo $this->lang->line('section_comprehension'); ?>
                                                        </td>
                                                        <td><?php echo $result['comprehension_score']; ?></td>
                                                        <td>
                                                            <a onclick="questionDetails('<?= $result['comprehension_q_ids'] ?>')" href="#" data-toggle="modal" data-target="#myModal">
                                                                <i class="la la-eye" aria-hidden="true" title="Show Details"></i>
                                                            </a>&nbsp;
                                                        </td>
                                                    </tr>
                                                <?php }
                                                if($result['video_conference_flag'] == 1){
                                                    ?>
                                                    
                                                     <tr>
                                                        <td>
                                                            Interactions
                                                        </td>
                                                        <td><?php echo intval($result['video_conference_percentage']/10); ?></td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                    
                                                    <?php
                                                }
                                                ?>
                                            </tbody>
                                            <thead> 
                                                <tr>
                                                    <th style="background:#337ab7;color:#ffffff;"><?php echo $this->lang->line('total'); ?></th>
                                                    <th colspan="2" style="background:#337ab7;color:#ffffff;"><?php echo $score_obtained; ?>
                                                    </th>

                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                                <hr/>
                                <!---category wise analysis-->
                                <!--category wise report-->
                                <?php
                                if ($this->config->item('google_chart') == true) {
                                    ?> 
                                    <div class="row <?= $result['video_conference_flag'] == 1 ? "d-block" : "d-none" ?>">
                                        <div class="card-text col-md-12">
                                            <h3 class="font-weight-800 my-1 text-success text-center">
                                                <?= $this->lang->line('categorywise_report'); ?>
                                            </h3>
                                        </div>
                                        <div class="col-md-12">
                                            <canvas id="column-chart" style="width:800px; height: 500px;"></canvas>
                                        </div>
                                    </div>
                                    <div class="row <?= $result['video_conference_flag'] != 1 ? "d-block" : "d-none" ?>">
                                        <div class="card-text col-md-12">
                                            <h3 class="font-weight-800 my-1 text-success text-center">
                                                <?= $this->lang->line('categorywise_report'); ?>
                                            </h3>
                                        </div>
                                        <div class="col-md-12">
                                            <script type="text/javascript" src="https://www.google.com/jsapi"></script>
                                            <script type="text/javascript">
                                                                //                                google.load("visualization", "1", {packages: ["corechart"]});
                                                                //                                google.setOnLoadCallback(drawChart);
                                                                google.load("visualization", "1", {packages: ["corechart"]});
                                                                google.setOnLoadCallback(drawChart);
                                                                function drawChart() {
                                                                    // var data = google.visualization.arrayToDataTable(["Element", "Density", { role: "style" } ],["Copper", 8.94, "#b87333"],["Silver", 10.49, "silver"]);
                                                                    // var data = google.visualization.arrayToDataTable(<?= $qtime; ?>);
                                                                    var data = google.visualization.arrayToDataTable([
                                                                        ['Element', 'Percentage', {role: 'style'}],
    <?php foreach ($graph as $key => $val) {
        ?>
                                                                            ["<?= $key ?>", <?= $val['percent']; ?>, "<?= $val['color']; ?>"],
    <?php }
    ?>

                                                                    ]);
                                                                    var options = {
                                                                        title: '<?php echo $this->lang->line('categorywise_report'); ?>',
                                                                       colors: ['green','red'],
                                                                    };

                                                                    var chart = new google.visualization.ColumnChart(document.getElementById('chart_div2'));
                                                                    chart.draw(data, options);
                                                                }
                                            </script>
                                            <div id="chart_div2" style="width:800px; height: 500px;"></div>
                                        </div>
                                    </div>
                                    <hr/>
                                    <div class="row">
                                        <div class="card-text col-md-12">
                                            <h3 class="font-weight-800 my-1 text-success text-center">
                                                <?= $this->lang->line('top_10_result'); ?> <?= $result['quiz_name']; ?>
                                            </h3>
                                        </div>
                                        <div class="col-md-12 mb-2">
                                            <script type="text/javascript">
                                                google.load("visualization", "1", {packages: ["corechart"]});
                                                google.setOnLoadCallback(drawChart);
                                                function drawChart() {
                                                    var data = google.visualization.arrayToDataTable(<?php echo $value; ?>);

                                                    var options = {
                                                        //title: '<?php echo $this->lang->line('top_10_result'); ?> <?php echo $result['quiz_name']; ?>',
                                                        hAxis: {title: '<?php echo $this->lang->line('quiz'); ?>(<?php echo $this->lang->line('user'); ?>)', titleTextStyle: {color: 'red'}}
                                                    };

                                                    var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
                                                    chart.draw(data, options);
                                                }
                                            </script>
                                            <div id="chart_div" style="width: 800px; height: 500px;"></div>
                                        </div>
                                    </div>
                                <?php } ?>

                                <!--category wise report-->
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>


<!---details modal-->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title pull-left"><?php echo $this->lang->line('show_catdetails'); ?></h4>
                <button type="button" class="close pull-right" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body" id="details_list">

            </div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>
<!---details modal-->
<script type="text/javascript">
    function questionDetails(question_ids) {
        var result_id = <?= $result['rid'] ?>;
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('result/getQuestionDetails'); ?>",
            data: {"question_ids": question_ids, "result_id": result_id},
            cache: false,
            success: function (result) {
                $("#details_list").html(result);
            }
        });
    }
</script>

<script src="<?= base_url() ?>theme-assets/vendors/js/charts/chart.min.js" type="text/javascript"></script>
<script src="https://themeselection.com/demo/chameleon-admin-template/app-assets/js/scripts/charts/chartjs/bar/bar.min.js" type="text/javascript"></script>
<script>
    $(window).on("load", function () {

        //Get the context of the Chart canvas element we want to select
        var ctx = $("#column-chart");

        // Chart Options
        var chartOptions = {
            // Elements options apply to all of the options unless overridden in a dataset
            // In this case, we are setting the border of each bar to be 2px wide and green
            elements: {
                rectangle: {
                    borderWidth: 2,
                    borderColor: 'rgb(0, 255, 0)',
                    borderSkipped: 'bottom'
                }
            },
            responsive: true,
            maintainAspectRatio: false,
            responsiveAnimationDuration: 500,

            legend: {
                position: 'top',
            },
            scales: {
                xAxes: [{
                        display: true,
                        gridLines: {
                            color: "#f3f3f3",
                            drawTicks: false,
                        },
                        scaleLabel: {
                            display: true,
                        }
                    }],
                yAxes: [{
                        display: true,
                        gridLines: {
                            color: "#f3f3f3",
                            drawTicks: false,
                        },
                        scaleLabel: {
                            display: true,
                        }
                    }]
            },
            title: {
                display: false,
                text: 'Chart.js Bar Chart'
            }
        };
        // Chart Data

        var chartData = {
            labels:<?php echo '["' . implode('", "', $barchart['labels']) . '"]'; ?>,
            datasets: [{
                    label: "Exam",
                    data: <?php echo '["' . implode('", "', $barchart['percent']) . '"]'; ?>,
                    backgroundColor: "#28D094",
                    hoverBackgroundColor: "rgba(40,208,148,.9)",
                    borderColor: "transparent"
                }, {
                    label: "Interaction",
                    data: <?php echo '["' . implode('", "', $barchart['interaction']) . '"]' ?>,
                    backgroundColor: "#FF4961",
                    hoverBackgroundColor: "rgba(255,73,97,.9)",
                    borderColor: "transparent",
                    maxBarThickness: 8,
                }]

        };

        var config = {
            type: 'bar',

            // Chart Options
            options: chartOptions,

            data: chartData,
        };

        // Create the chart
        var lineChart = new Chart(ctx, config);
    });
</script>