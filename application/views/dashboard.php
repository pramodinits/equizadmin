<?php
$dataPoints = array(
    array("label" => "Verified User", "y" => $verified_users),
    array("label" => "Un-Verified user", "y" => $unverified_users),
    array("label" => "Total Registered Applicants", "y" => $num_users)
);

$dataPointsCategory = array(
    array("label" => "Pilots", "y" => $count_pilots),
    array("label" => "Ramp Agents", "y" => $count_agents),
    array("label" => "ATCOS", "y" => $count_atcos),
    array("label" => "Air Technicians", "y" => $count_technicians)
        );

        ?>
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/animate.min.css">
<script src="<?= base_url() ?>theme-assets/vendors/js/animation/jquery.appear.js" type="text/javascript"></script>
<script src="<?= base_url() ?>js/animation.js" type="text/javascript"></script>
<!--update template design-->
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-wrapper-before"></div>
        <div class="content-header row">
            <div class="content-header-left col-md-4 col-12 mb-2">
                <!--<h3 class="content-header-title"><?= $title; ?></h3>-->
            </div>
        </div>
        <div class="content-body">
            <section id="ecommerce-stats">
                <div class="row">
                    <div class="col-xl-4 col-lg-6 col-md-12 zoomIn">
                        <div class="card pull-up ecom-card-1 bg-gradient-x-purple-red animated zoomIn">
                            <div class="card-content ecom-card2 height-150">
                                <div class="card-body">
                                    <div class="media d-flex">
                                        <div class="align-self-bottom mt-3">
                                            <i class="la la-users icon-opacity text-white font-large-4 float-left"></i>
                                        </div>
                                        <div class="align-self-top media-body text-white text-right ">
                                            <span class="d-block mb-1 font-medium-1 font-weight-bold">
                                                <?= $this->lang->line('registered_applicants') ?>
                                            </span>
                                            <h1 class="text-white mb-0"><?php echo $num_users; ?></h1>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-6 col-md-12">
                        <div class="card pull-up ecom-card-1 bg-gradient-x-orange-yellow animated zoomIn">
                            <div class="card-content ecom-card2 height-150">
                                <div class="card-body">
                                    <div class="media d-flex">
                                        <div class="align-self-bottom mt-3">
                                            <i class="la la-user icon-opacity text-white font-large-4 float-left"></i>
                                        </div>
                                        <div class="align-self-top media-body text-white text-right ">
                                            <span class="d-block mb-1 font-medium-1 font-weight-bold">
                                                <?= $this->lang->line('total_centers') ?>
                                            </span>
                                            <h1 class="text-white mb-0"><?php echo $total_agency; ?></h1>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-12">
                        <div class="card pull-up ecom-card-1 bg-gradient-x-blue-green animated zoomIn">
                            <div class="card-content ecom-card2 height-150 zoomIn">
                                <div class="card-body">
                                    <div class="media d-flex">
                                        <div class="align-self-bottom mt-3">
                                            <i class="la la-clock-o icon-opacity text-white font-large-4 float-left"></i>
                                        </div>
                                        <div class="align-self-top media-body text-white text-right ">
                                            <span class="d-block mb-1 font-medium-1 font-weight-bold">
                                                <?= $this->lang->line('total_subscriptions') ?>
                                            </span>
                                            <h1 class="text-white mb-0"><?php echo $total_subscription; ?></h1>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-4 col-lg-6 col-md-12">
                        <div class="card pull-up ecom-card-1 bg-gradient-directional-success animated zoomIn">
                            <div class="card-content ecom-card2 height-150">
                                <div class="card-body">
                                    <div class="media d-flex">
                                        <div class="align-self-bottom mt-3">
                                            <i class="la la-laptop icon-opacity text-white font-large-4 float-left"></i>
                                        </div>
                                        <div class="align-self-top media-body text-white text-right ">
                                            <span class="d-block mb-1 font-medium-1 font-weight-bold">
                                                Total <?= $this->lang->line('examtype_dummy') ?>
                                            </span>
                                            <h1 class="text-white mb-0"><?php echo $num_quiz_mock; ?></h1>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-6 col-md-12">
                        <div class="card pull-up ecom-card-1 bg-gradient-directional-info animated zoomIn">
                            <div class="card-content ecom-card2 height-150">
                                <div class="card-body">
                                    <div class="media d-flex">
                                        <div class="align-self-bottom mt-3">
                                            <i class="la la-laptop icon-opacity text-white font-large-4 float-left"></i>
                                        </div>
                                        <div class="align-self-top media-body text-white text-right ">
                                            <span class="d-block mb-1 font-medium-1 font-weight-bold">
                                                Total <?= $this->lang->line('examtype_exam') ?>
                                            </span>
                                            <h1 class="text-white mb-0"><?php echo $num_quiz_exam; ?></h1>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-12">
                        <div class="card pull-up ecom-card-1 bg-gradient-directional-danger animated zoomIn">
                            <div class="card-content ecom-card2 height-150">
                                <div class="card-body">
                                    <div class="media d-flex">
                                        <div class="align-self-bottom mt-3">
                                            <i class="la la-question-circle icon-opacity text-white font-large-4 float-left"></i>
                                        </div>
                                        <div class="align-self-top media-body text-white text-right">
                                            <span class="d-block mb-1 font-medium-1 font-weight-bold">
                                                <?= $this->lang->line('total_questions') ?>
                                            </span>
                                            <h1 class="text-white mb-0"><?php echo $num_qbank; ?></h1>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section id="chartjs-pie-charts">
                <div class="row">
                    <div class="col-md-3 col-sm-12">
                        <div class="card">
                            <div class="card-header pb-1">
                                <h4 class="card-title"><?= $this->lang->line('applicant_report') ?></h4>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body" style="padding: 0rem 0.5rem 1.5rem">
                                    <div class="height-300">
                                        <canvas id="simple-pie-chart"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-12">
                        <div class="card">
                            <div class="card-header pb-1">
                                <h4 class="card-title"><?= $this->lang->line('center_report') ?></h4>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body" style="padding: 0rem 0.5rem 1.5rem">
                                    <div class="height-300">
                                        <canvas id="simple-doughnut-chart"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5 col-sm-12">
                        <div class="card">
                            <div class="card-header pb-1">
                                <h4 class="card-title"><?= $this->lang->line('center_subscriptions') ?></h4>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body" style="padding: 0rem 0.5rem 1.5rem">
                                    <div class="height-300">
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th><?= $this->lang->line('dashboard_username') ?></th>
                                                    
                                                    <th><?= $this->lang->line('dashboard_subscription') ?></th>
						<th><?= $this->lang->line('dashboard_balance') ?></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php if (count($agency_listing) == 0) { ?>
                                                    <tr>
                                                        <td colspan="3"><?php echo $this->lang->line('no_record_found'); ?></td>
                                                    </tr>
                                                <?php } 
                                                foreach ($agency_listing as $key => $val) { ?>
                                                    <tr>
                                                    <td><?= $val['first_name'] . " " . $val['last_name'] ?></td>
                                                    <td><?= $val['totalexamsubscription'] ?></td>
                                                    <td><?= $val['wallet_balance']; ?></td>
                                                </tr>
                                               <?php }
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </section>    
        </div>
        </section>
    </div>
</div>
</div>
<!--update template design-->
<script src="<?= base_url() ?>theme-assets/vendors/js/charts/chart.min.js" type="text/javascript"></script>
<script>
    $(window).on("load", function () {
        var a = $("#simple-pie-chart");
        new Chart(a,
                {type: "pie",
                    options: {responsive: !0, maintainAspectRatio: !1, responsiveAnimationDuration: 500},
                    data: {
                        labels: ["Verified Applicant", "Un-Verified Applicant", "Total Registered Applicant"],
                        datasets: [{label: "User Info", data: [<?= $verified_users ?>,<?= $unverified_users ?>,<?= $num_users ?>],
                                backgroundColor: ["#28D094", "#FF4961", "#666EE8"]
                            }]}
                })

        var a = $("#simple-doughnut-chart");
        new Chart(a,
                {
                    type: "doughnut",
                    options: {responsive: !0, maintainAspectRatio: !1, responsiveAnimationDuration: 500},
                    data: {labels: ["Pilots", "Ramp Agents", "ATCOS", "Air Technicians"],
                        datasets: [{label: "Exam category Info", data: [<?= $count_pilots ?>,<?= $count_agents ?>,<?= $count_atcos ?>,<?= $count_technicians ?>],
                                backgroundColor: ["#666EE8", "#28D094", "#FF4961", "#1E9FF2"]}
                        ]}
                })
    });
</script>


