<table class="table table-bordered" id="assignstudents">
    <tr>
        <th class="text-center"><?php echo $this->lang->line('student_name'); ?></th>
        <th class="text-center"><?php echo $this->lang->line('contact_no'); ?></th>
        <th class="text-center"><?php echo $this->lang->line('action'); ?></th>
    </tr>
    <?php
    if (count($result) == 0) {
        ?>
        <tr>
            <td colspan="3"><?php echo $this->lang->line('no_record_found'); ?></td>
        </tr>
        <?php
    }
    foreach ($result as $key => $val) {
        ?>
        <tr id="student_<?= $val['student_id']; ?>">
            <td><?= $val['student_name'] ?></td>
            <td><?= $val['contact_no'] ?></td>
            <td>
                <a href="javascript:void(0);" onclick="removeStudent(<?= $val['student_id']; ?>,<?= $val['examiner_id']; ?>)">
                    <i class="fa fa-trash text-danger" aria-hidden="true" title="Remove Examiner"></i>
                </a>
            </td>
        </tr>
        <?php
    }
    ?>

</table>