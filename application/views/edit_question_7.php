<div class="container">


    <h3><?php echo $title; ?></h3>

    <?php
    $lang = $this->config->item('question_lang');
    ?>

    <div class="row">
        <form method="post" action="<?php echo site_url('qbank/edit_question_7/' . $question['qid']); ?>">

            <div class="col-md-12">
                <br> 
                <div class="login-panel panel panel-default">
                    <div class="panel-body"> 



                        <?php
                        if ($this->session->flashdata('message')) {
                            echo $this->session->flashdata('message');
                        }
                        ?>	



                        <div class="form-group">	 

                            <?php echo $this->lang->line('multiple_choice_multiple_answer'); ?>
                        </div>

                        <div class="form-group">	 
                            <label><?php echo $this->lang->line('select_category'); ?></label> 
                            <select class="form-control" name="question_category">
                                <option value=""><?php echo $this->lang->line('select_category'); ?></option>
                                <?php
                                foreach ($que_category as $key => $val) {
                                    ?>
                                    <option value="<?php echo $key; ?>" <?= $question['question_category'] ==  $key ? "selected" : ""?>><?php echo $val; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label><?php echo $this->lang->line('exam_type'); ?></label> 
                            <select class="form-control" name="test_type">
                                <option value=""><?php echo $this->lang->line('examtype_select'); ?></option>
                                <?php
                                foreach ($exam_type as $key => $val) {
                                    ?>
                                    <option value="<?php echo $key; ?>" <?= $question['test_type'] ==  $key ? "selected" : ""?>><?php echo $val; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label><?php echo $this->lang->line('question_module'); ?></label> 
                            <select class="form-control" name="question_module">
                                <option value=""><?php echo $this->lang->line('select_module'); ?></option>
                                <?php
                                foreach ($question_module as $key => $val) {
                                    ?>
                                    <option value="<?php echo $key; ?>" <?= $question['question_module'] ==  $key ? "selected" : ""?>><?php echo $val; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group" style="display: none;">	 
                            <label   ><?php echo $this->lang->line('select_category'); ?></label> 
                            <select class="form-control" name="cid">
                                <?php
                                foreach ($category_list as $key => $val) {
                                    ?>

                                    <option value="<?php echo $val['cid']; ?>"  <?php if ($question['cid'] == $val['cid']) {
                                    echo 'selected';
                                } ?> ><?php echo $val['category_name']; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>


                        <div class="form-group">	 
                            <label   ><?php echo $this->lang->line('select_level'); ?></label> 
                            <select class="form-control" name="lid">
                                <?php
                                foreach ($level_list as $key => $val) {
                                    ?>

                                    <option value="<?php echo $val['lid']; ?>" <?php if ($question['lid'] == $val['lid']) {
                                    echo 'selected';
                                } ?> ><?php echo $val['level_name']; ?></option>
    <?php
}
?>
                            </select>
                        </div>
                        <div class="form-group">	 
                            <label   >Time (in seconds) </label> 
                            <input class="form-control" type="text" id="time" name="time" value="<?php echo $question['time']; ?>"  />
                        </div>
                        <div class="form-group">
                            <?php if ($question['video_flg'] == 2) { ?>
                                Video URL: 
                                <a href="<?= $question['video_url']; ?>" target="_blank"><?= $question['video_url']; ?></a>
                            <?php } else if ($question['video_flg'] == 1) { ?>
                                <video width="320" height="240" controls>
                                    <source src="<?= base_url() . 'videos/' . $question['video_url'] ?>" type="video/mp4">
                                </video>
                            <?php } else if ($question['video_flg'] == 0) { ?>
                                <b>No video available.</b>
<?php }
?>
                        </div>


                        <?php
                        if (strip_tags($question['paragraph']) != "") {
                            foreach ($lang as $lkey => $val) {
                                $lno = $lkey;
                                if ($lkey == 0) {
                                    $lno = "";
                                }
                                ?>
                                <div class="col-lg-6">
                                    <div class="form-group">	 
                                        <label for="inputEmail"  ><?php echo $this->lang->line('paragraph') . ' : ' . $val; ?></label> 
                                        <textarea  name="paragraph<?php echo $lno; ?>"  class="form-control"   ><?php echo $question['paragraph' . $lno]; ?></textarea>
                                    </div>


                                </div>			 

                                <?php
                            }
                        }
                        ?>			

                        <?php
                        foreach ($lang as $lkey => $val) {
                            $lno = $lkey;
                            if ($lkey == 0) {
                                $lno = "";
                            }
                            ?>
                            <div class="col-lg-6">			

                                <div class="form-group">	 
                                    <label for="inputEmail"  ><?php echo $this->lang->line('question') . ' : ' . $val; ?></label> 
                                    <textarea  name="question<?php echo $lno; ?>"  class="form-control"   ><?php echo $question['question' . $lno]; ?></textarea>
                                </div></div>
                            <?php
                        }
                        foreach ($lang as $lkey => $val) {
                            $lno = $lkey;
                            if ($lkey == 0) {
                                $lno = "";
                            }
                            ?>	<div class="col-lg-6">		


                                <div class="form-group">	 
                                    <label for="inputEmail"  ><?php echo $this->lang->line('description') . ' : ' . $val; ?></label> 
                                    <textarea  name="description<?php echo $lno; ?>"  class="form-control"><?php echo $question['description' . $lno]; ?></textarea>
                                </div>

                            </div>		
                            <?php
                        }
                        ?>
                        <?php
                        foreach ($options as $key => $val) {
                            ?>
                            <div class="row">
                                <?php
                                foreach ($lang as $lkey => $la) {
                                    $lno = $lkey;
                                    if ($lkey == 0) {
                                        $lno = "";
                                    }
                                    ?>
                                    <div class="col-lg-6">



                                        <div class="form-group">	 
                                            <label for="inputEmail"  ><?php echo $this->lang->line('options'); ?> <?php echo $key + 1; ?>) <?php echo ' : ' . $la; ?></label>
                                            <span style="display: none;">
                                                <?php
                                                if ($lkey == 0) {
                                                    ?><input type="checkbox" name="score[]" value="<?php echo $key; ?>" <?php if ($val['score'] >= 0.1) {
                                            echo 'checked';
                                        } ?> > Select Correct Option <?php } ?>
                                            </span>
                                            <textarea  name="option<?php echo $lno; ?>[]"  class="form-control"  ><?php echo $val['q_option' . $lno]; ?></textarea>
                                        </div>
                                    </div>
                                    <?php
                            }
                            ?></div>
                            <?php
                        }
                        ?>

                        <button class="btn btn-default" type="submit"><?php echo $this->lang->line('submit'); ?></button>

                    </div>
                </div>




            </div>
        </form>
        <div class="col-md-3">


            <div class="form-group">	 
                <table class="table table-bordered">
                    <tr><td><?php echo $this->lang->line('no_times_corrected'); ?></td><td><?php echo $question['no_time_corrected']; ?></td></tr>
                    <tr><td><?php echo $this->lang->line('no_times_incorrected'); ?></td><td><?php echo $question['no_time_incorrected']; ?></td></tr>
                    <tr><td><?php echo $this->lang->line('no_times_unattempted'); ?></td><td><?php echo $question['no_time_unattempted']; ?></td></tr>

                </table>

            </div>


        </div>
    </div>





</div>
