<?php
foreach ($admin_menu as $key => $value) {
    foreach ($value as $k => $v) {
//        $show_management_menu = array_key_exists($k, $access[str_replace(' ', '_', $key)]) ? ' display:block;' : 'display:none;';
        if (array_key_exists($k, $access[str_replace(' ', '_', $key)])) {
            // echo $v . "<br/>";
        }
    }
}
//print "<pre>";print_r(@$access);exit();
?>
<ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
    <li class="nav-item 
    <?php
    if (($this->router->class == 'dashboard') && (($this->router->method == 'index'))) {
        echo "open";
    }
    ?>
        ">
        <a href="<?php echo site_url('dashboard'); ?>"><i class="la la-dashboard"></i>
            <span class="menu-title" data-i18n=""><?= $this->lang->line('dashboard'); ?></span>
        </a>
    </li>

    <?php
    foreach ($admin_menu as $key => $val) {
        
        if (@$access[str_replace(' ', '_', $key)]) {
//            foreach(@$access[str_replace(' ', '_', $key)] as $k => $v) {
//                echo $k . "<br>";
//            }
            ?>
            <li class="nav-item <?= $val['submenu'] ? 'has-sub' : ''; ?> <?php
            if ((in_array($this->router->class, $val['class'])) && (in_array($this->router->method, $val['method']))) {
                echo "open";
            }
            ?>
                ">
                <a href="<?= $val['href'] ?>"> 
                    <i class="<?= $val['mainfont'] ?>"></i>
                    <span class="menu-title" data-i18n=""><?= $val['mainlable']; //$this->lang->line('users');    ?></span>
                </a>
                <?php
                if ($val['submenu']) {
                    ?>
                    <ul class="menu-content">
                        <?php
                        foreach ($val['submenu'] as $k => $v) {
                            
                           // $show_management_menu = array_key_exists($k, @$access[str_replace(' ', '_', $key)]) ? ' display:block;' : 'display:none;';
                            $show_management_menu = @$access[str_replace(' ', '_', $key)][$k] ? ' display:block;' : 'display:none;';
                            ?>
                            <li style="<?= $show_management_menu; ?>" class="<?php
                            if (($this->router->class == $v['class']) && ($this->router->method == $v['method'])) {
                                echo "active";
                            }
                            ?> ">
                                <a class="menu-item" href="<?= site_url($v['href']); ?>"><?= $v['label']; ?></a>
                            </li>
                            <?php
                        }
                        ?>
                    </ul>
                    <?php
                }
                ?>
            </li>
            <?php
        }
    }
    ?>
    

</ul>