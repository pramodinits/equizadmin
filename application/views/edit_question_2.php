<?php
$lang = $this->config->item('question_lang');
?>
<!--updated template page-->
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-wrapper-before"></div>
        <div class="content-header row">
            <div class="content-header-left col-md-4 col-12 mb-2">
                <h3 class="content-header-title"></h3>
            </div>
            <div class="content-header-right col-md-8 col-12">
                <div class="breadcrumbs-top float-md-right">
                    <h6><?= $breadcrumbs; ?></h6>
                </div>
            </div>
        </div>
        <div class="content-body">
            <section id="horizontal-form-layouts">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-content collpase collapse show">
                                <div class="card-body">
                                    <form method="post" id="edit_question" action="<?php echo site_url('qbank/edit_question_2/' . $question['qid']); ?>" enctype="multipart/form-data">
                                        <div class="form-body">
                                            <h4 class="form-section"><?php echo $title; ?></h4>
                                            <div class="login-panel panel panel-default">
                                                <div class="panel-body">
                                                    <?php
                                                    if ($this->session->flashdata('message')) {
                                                        echo $this->session->flashdata('message');
                                                    }
                                                    ?>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">	 
                                                                <?php echo $this->lang->line('multiple_choice_multiple_answer'); ?>
                                                            </div>
                                                            <div class="form-group">	 
                                                                <label><?php echo $this->lang->line('select_category'); ?></label> 
                                                                <select class="form-control" name="question_category" required>
                                                                    <option value=""><?php echo $this->lang->line('select_category'); ?></option>
                                                                    <?php
                                                                    foreach ($que_category as $key => $val) {
                                                                        ?>
                                                                        <option value="<?php echo $key; ?>" <?= $question['question_category'] == $key ? "selected" : "" ?>><?php echo $val; ?></option>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                            <div class="form-group">
                                                                <label><?php echo $this->lang->line('exam_type'); ?></label> 
                                                                <select class="form-control" name="test_type" required onchange="showVideoModule(this)">
                                                                    <option value=""><?php echo $this->lang->line('examtype_select'); ?></option>
                                                                    <?php
                                                                    foreach ($exam_type as $key => $val) {
                                                                        ?>
                                                                        <option value="<?php echo $key; ?>" <?= $question['test_type'] == $key ? "selected" : "" ?>><?php echo $val; ?></option>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                            <div class="form-group">	 
                                                                <label   >Time (in seconds) </label> 
                                                                <input class="form-control" type="text" id="time" name="time" value="<?php echo $question['time']; ?>"  />
                                                            </div> 
                                                            <div class="form-group">
                                                                <label>Question Section</label> 
                                                                <select class="form-control" name="question_section" required>
                                                                    <option value=""><?php echo $this->lang->line('select_section'); ?></option>
                                                                    <?php
                                                                    foreach ($question_section as $key => $val) {
                                                                        ?>
                                                                        <option value="<?php echo $key; ?>" <?= $question['question_section'] == $key ? "selected" : "" ?> ><?php echo $val; ?></option>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                            <div class="form-group">
                                                                <label><?php echo $this->lang->line('question_module'); ?></label> 
                                                                <select class="form-control" required name="question_module" onchange="showVideoModule(this)">
                                                                    <option value=""><?php echo $this->lang->line('select_module'); ?></option>
                                                                    <?php
                                                                    foreach ($question_module as $key => $val) {
                                                                        ?>
                                                                        <option value="<?php echo $key; ?>" <?= $question['question_module'] == $key ? "selected" : "" ?>><?php echo $val; ?></option>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                            <div class="form-group text-right" id="show_media">
                                                                <p><a href="javascript:void(0)">Do you want to change?</a></p>
                                                            </div>
                                                            <div class="form-group" id="audiovideo_flag" style="display: none;">	 
                                                                <label><?php echo $this->lang->line('media_flag'); ?></label> 
                                                                <select class="form-control" name="video_flg" id="video_flg" onchange="showVideo(this)" required>
                                                                    <option value=""><?php echo $this->lang->line('media_select'); ?></option>
                                                                    <option value="1"><?php echo $this->lang->line('media_browse'); ?></option>
                                                                    <option value="2"><?php echo $this->lang->line('media_url'); ?></option>
                                                                </select>
                                                            </div>
                                                            <div class="form-group videodiv" id="video_file_div" style="display: none;">	 
                                                                <label><?php echo $this->lang->line('media_upload'); ?></label>
                                                                <input type="file" class="form-control" required id="video_file" name="video_file" accept ="video/*, audio/*" />
                                                            </div>
                                                            <div class="form-group videodiv" id="video_url_div" style="display: none;">	 
                                                                <label><?php echo $this->lang->line('media_input_url'); ?></label> 
                                                                <input type="text" class="form-control" required id="video_url" name="video_url"  />
                                                            </div>
                                                            <div class="form-group" id="available_flag">
                                                                <?php if ($question['video_flg'] == 2) { ?>
                                                                    URL: 
                                                                    <a href="<?= $question['video_url']; ?>" target="_blank"><?= $question['video_url']; ?></a>
                                                                <?php } else if ($question['video_flg'] == 1) { ?>
                                                                    <a href="<?= base_url() . 'videos/' . $question['video_url'] ?>" target="_blank">
                                                                        <?= $question['video_url'] ?>
                                                                    </a>
                                                                <?php } else { ?>
                                                                    <b>No video available.</b>
                                                                <?php }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <?php
                                                            if (strip_tags($question['paragraph']) != "") {
                                                                foreach ($lang as $lkey => $val) {
                                                                    $lno = $lkey;
                                                                    if ($lkey == 0) {
                                                                        $lno = "";
                                                                    }
                                                                    ?>
                                                                    <div class="col-lg-6">
                                                                        <div class="form-group">	 
                                                                            <label for="inputEmail"  ><?php echo $this->lang->line('paragraph') . ' : ' . $val; ?></label> 
                                                                            <textarea  name="paragraph<?php echo $lno; ?>"  class="form-control tinymcetextarea" required  ><?php echo $question['paragraph' . $lno]; ?></textarea>
                                                                        </div>
                                                                    </div>			 
                                                                    <?php
                                                                }
                                                            }
                                                            ?>			

                                                            <?php
                                                            foreach ($lang as $lkey => $val) {
                                                                $lno = $lkey;
                                                                if ($lkey == 0) {
                                                                    $lno = "";
                                                                }
                                                                ?>
                                                                <div class="col-lg-6">			

                                                                    <div class="form-group">	 
                                                                        <label for="inputEmail"  ><?php echo $this->lang->line('question') . ' : ' . $val; ?></label> 
                                                                        <textarea  name="question<?php echo $lno; ?>"  class="form-control" required  ><?php echo $question['question' . $lno]; ?></textarea>
                                                                    </div></div>
                                                                <?php
                                                            }
                                                            foreach ($lang as $lkey => $val) {
                                                                $lno = $lkey;
                                                                if ($lkey == 0) {
                                                                    $lno = "";
                                                                }
                                                                ?>	<div class="col-lg-6">		


                                                                    <div class="form-group">	 
                                                                        <label for="inputEmail"  ><?php echo $this->lang->line('description') . ' : ' . $val; ?></label> 
                                                                        <textarea  name="description<?php echo $lno; ?>" class="form-control"><?php echo $question['description' . $lno]; ?></textarea>
                                                                    </div>

                                                                </div>		
                                                                <?php
                                                            }
                                                            ?>
                                                            <?php
                                                            foreach ($options as $key => $val) {
                                                                ?>
                                                                <div class="row">
                                                                    <?php
                                                                    foreach ($lang as $lkey => $la) {
                                                                        $lno = $lkey;
                                                                        if ($lkey == 0) {
                                                                            $lno = "";
                                                                        }
                                                                        ?>
                                                                        <div class="col-lg-6">
                                                                            <div class="form-group">	 
                                                                                <label for="inputEmail"><?php echo $this->lang->line('options'); ?> <?php echo $key + 1; ?>) <?php echo ' : ' . $la; ?></label> <br>
                                                                                <?php
                                                                                if ($lkey == 0) {
                                                                                    ?><input type="checkbox" name="score[]" value="<?php echo $key; ?>" <?php
                                                                                    if ($val['score'] >= 0.1) {
                                                                                        echo 'checked';
                                                                                    }
                                                                                    ?> > Select Correct Option <?php } ?>
                                                                                <br><textarea  name="option<?php echo $lno; ?>[<?php echo $key; ?>]"  class="form-control"  ><?php echo $val['q_option' . $lno]; ?></textarea>
                                                                            </div>
                                                                        </div>
                                                                        <?php
                                                                    }
                                                                    ?></div>
                                                                <?php
                                                            }
                                                            ?>
                                                        </div>
                                                    </div>
                                                    <div class="form-group" style="display: none;"  >
                                                        <input type="checkbox" name="with_paragraph" value="1"> <?php echo $this->lang->line('with_paragraph'); ?></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-actions right">
                                            <button class="btn btn-primary" type="submit"><i class="la la-check-square-o"></i><?php echo $this->lang->line('submit'); ?></button>
                                        </div>
                                    </form>
                                    <div class="col-md-3" style="display: none;">
                                        <div class="form-group">	 
                                            <table class="table table-bordered">
                                                <tr><td><?php echo $this->lang->line('no_times_corrected'); ?></td><td><?php echo $question['no_time_corrected']; ?></td></tr>
                                                <tr><td><?php echo $this->lang->line('no_times_incorrected'); ?></td><td><?php echo $question['no_time_incorrected']; ?></td></tr>
                                                <tr><td><?php echo $this->lang->line('no_times_unattempted'); ?></td><td><?php echo $question['no_time_unattempted']; ?></td></tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
<!--updated template page-->

<script>

    $(document).ready(function () {
        var media_flag = $('select[name=question_module]').val();
        var flag_value = <?= $question['video_flg'] ?>;
        /*if (media_flag == 3 || media_flag == 4) {
         $("#audiovideo_flag").show();
         $('#video_flg').find('option[value="' + flag_value + '"]').attr('selected', 'selected')
         if (flag_value == 1) {
         $("#video_file_div").show();
         } else {
         $("#video_url_div").show();
         }
         }*/
        $("#show_media a").click(function () {
            $("#audiovideo_flag").show();
        });
    });

    function showVideoModule(e) {
        $("#available_flag").hide();
        if ($(e).val() == 3 || $(e).val() == 4) {
            $("#audiovideo_flag").show();
        } else {
            $("#audiovideo_flag").hide();
            $("#video_url_div").hide();
            $("#video_file_div").hide();
        }
    }

    function showVideo(e) {
        if ($(e).val() == '') {
            $('.videodiv').hide();
        } else if ($(e).val() == 1) {
            $('#video_url_div').hide();
            $('#video_file_div').show();
        } else if ($(e).val() == 2) {
            $('#video_url_div').show();
            $('#video_file_div').hide();
        }

    }
</script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/additional-methods.js"></script>
<script>
    $(document).ready(function () {
        $('#edit_question').validate({// initialize the plugin
        rules: {
        'time': {
        digits: true
        },
<?php for ($i = 0; $i < count($options); $i++) { ?>
            'option[<?php echo $i; ?>]': {
            required: true
            },
<?php } ?>
        },
                submitHandler: function (form) { // for demo
                $('button[type=submit]').attr('disabled', 'disabled');
                        return true; // for demo
                }
        });
    });
</script>