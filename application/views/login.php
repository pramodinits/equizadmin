<?php include 'header.php'; ?>
<?php
$hquery = $this->db->query(" select * from kamsquiz_setting where setting_name='App_Name' || setting_name='App_title' order by setting_id asc ");
$hres = $hquery->result_Array();
?>
<style>
    .form-horizontal i {
        line-height: 2;   
    }
    .blank-page .content-wrapper .flexbox-container {
        height: 90vh !important;
    }
</style>
<!--updated template page-->
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-wrapper-before"></div>
        <div class="content-header row">
            <div class="content-header-left col-md-4 col-12 mb-2">
                <h3 class="content-header-title"></h3>
            </div>
        </div>
        <div class="content-body">
            <section class="flexbox-container">
                <div class="col-12 d-flex align-items-center justify-content-center">
                    <div class="col-lg-4 col-md-6 col-10 box-shadow-2 p-0">
                        <div class="card border-grey border-lighten-3 px-1 py-1 m-0">
                            <div class="card-header border-0">
                                <div class="text-center mb-0">
                                    <img class="brand-logo" alt="Branding logo" src="<?php echo base_url(); ?>images/logo_blue.png"/>
                                    <h1 style="display: none;">
                                        <?php if ($hres[0]['setting_value'] == "") { ?>GACA<?php
                                        } else {
                                            echo $hres[0]['setting_value'];
                                        }
                                        ?> 
                                    </h1>
                                </div>
                                <!--<div class="font-large-1  text-center">      </div>-->
                            </div>
                            <?php
                            if ($this->session->flashdata('message')) {
                                ?>
                                <div class="alert alert-danger">
                                <?php echo str_replace('{resend_url}', site_url('login/resend'), $this->session->flashdata('message')); ?>
                                </div>
                                <?php
                            }
                            ?>
                            <div class="card-content">

                                <div class="card-body">
                                    <form class="form-horizontal" method="post" action="<?php echo site_url('login/verifylogin'); ?>" novalidate="">
                                        <fieldset class="form-group position-relative has-icon-left">
                                            <!--<input type="text" class="form-control round" id="user-name" placeholder="Your Username" required="">-->
                                            <input type="email" name="email" class="form-control round" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="<?php echo $this->lang->line('email_address');?>">
                                            <div class="form-control-position">
                                                <i class="la la-user"></i>
                                            </div>
                                        </fieldset>
                                        <fieldset class="form-group position-relative has-icon-left">
                                            <input type="password" name="password" class="form-control round" id="exampleInputPassword" placeholder="<?php echo $this->lang->line('password');?>">
                                            <!--<input type="password" class="form-control round" id="user-password" placeholder="Enter Password" required="">-->
                                            <div class="form-control-position">
                                                <i class="la la-lock"></i>
                                            </div>
                                        </fieldset>
                                        <div class="form-group text-center">
                                            <button type="submit" class="btn round btn-block btn-glow btn-bg-gradient-x-purple-blue col-12 mr-1 mb-1">Login</button>    
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
<!--updated template page-->
</body>
</html>