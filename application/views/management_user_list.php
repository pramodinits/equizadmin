<style>
    .searchForm input.form-control {
        width: 25%;
        float: left;
        margin-right: 1%;
    }
</style>
<script src="//code.jquery.com/jquery-3.1.0.min.js"></script>
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-wrapper-before"></div>
        <div class="content-header row">
            <div class="content-header-left col-md-4 col-12 mb-2">
                <h3 class="content-header-title"></h3>
            </div>
        </div>
        <div class="content-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        
                        <div class="card-content collpase collapse show">
                            <div class="card-body">
                                <?php
                                if ($this->session->flashdata('message')) {
                                    echo $this->session->flashdata('message');
                                }
                                ?>
                                <div class="row">
                                    <div class="col-md-12">
                                        <form class="searchForm" method="get" action="<?php echo site_url('management_user/index/'); ?>">
                                            <input type="text" name="username" class="form-control" value="<?= $user['username'] ?>" placeholder="Search Name">
                                            <input type="text" name="email" class="form-control" value="<?= $user['email'] ?>" placeholder="Search Email">
                                            <input type="text" name="phone" class="form-control" value="<?= $user['phone'] ?>" placeholder="Search Phone">
                                            <button type="submit" class="btn btn-primary"><?= $this->lang->line('filter') ?></button>
                                            <a href="<?= site_url('management_user/'); ?>">
                                                <button type="button" class="btn btn-primary"><?php echo $this->lang->line('reset_button'); ?></button>
                                            </a>
                                        </form>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th class="text-center"><?php echo $this->lang->line('name'); ?></th>
                                                <th class="text-center"><?php echo $this->lang->line('email'); ?></th>
                                                <th class="text-center"><?php echo $this->lang->line('contact_no'); ?> </th>
                                                <th class="text-center"><?php echo $this->lang->line('city'); ?> </th>
                                                <th class="text-center"><?php echo $this->lang->line('userrole'); ?> </th>
                                                <th class="text-center"><?php echo $this->lang->line('roles_permission'); ?> </th>
                                                <!--<th class="text-center"><?php  echo $this->lang->line('exam_category'); ?> </th>-->
                                                <th class="text-center"><?php echo $this->lang->line('action'); ?> </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            if (count($result) == 0) {
                                                ?>
                                                <tr>
                                                    <td colspan="8"><?php echo $this->lang->line('no_record_found'); ?></td>
                                                </tr>
                                                <?php
                                            }
                                            foreach ($result as $key => $val) {
                                                ?>
                                                <tr>
                                                    <th><?php echo $val['uid']; ?></th>
                                                    <td><?php echo $val['first_name'] . " " . $val['last_name']; ?></td>
                                                    <td><?= $val['email'] ?></td>
                                                    <td><?= $val['contact_no']; ?></td>
                                                    <td><?= $city_list[$val['id_city']]; ?></td>
                                                    <td width="15%;"><?= $usertype[$val['su']]; ?></td>
                                                    <!--<td><?= $exam_category[$val['exam_category']]; ?></td>-->
                                                    <!--<td><?= $val['user_status']; ?></td>-->
                                                    <td class="text-center">
                                                        <a href="<?php echo site_url('management_user/roles_permission/' . $val['uid']); ?>">
                                                            <i class="la la-check-square-o" title="Assign Roles & permissions"></i>
                                                        </a>
                                                    </td>
                                                    <td>
                                                        <a href="<?php echo site_url('management_user/new_management_user/' . $val['uid']); ?>">
                                                            <i class="la la-edit" aria-hidden="true" title="Edit Examiner"></i>
                                                        </a>
                                                        <a style="display: none;" onclick="userDetails(<?= $val['uid'] ?>)" href="#" data-toggle="modal" data-target="#myModal" data-id ="<?= $val['uid'] ?>">
                                                            <i class="fa fa-user-plus" aria-hidden="true" title="Assign Students"></i>
                                                        </a>
                                                        <a class="d-none" href="javascript:remove_entry('management_user/remove_examiner/<?php echo $val['uid']; ?>');">
                                                            <i class="la la-trash text-danger" aria-hidden="true" title="Remove Examiner"></i>
                                                        </a>
                                                        <a href="<?= site_url('management_user/remove_management/?id_management='.$val['uid']) ?>" onclick="return confirm('Are you sure to remove this?')">
                                                        <i class="la la-trash text-danger" title="Remove User" aria-hidden="true"></i>
                                                        </a>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                    <?= $links; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var getContacts = 12; //remaining fields
    var AddedContacts = 8; //filled fields
    var restContacts = AddedContacts * 100 / getContacts;
    $("#contactsAdded").css({"width": +restContacts + "%"});
    $("#contactsAdded").attr('data-transitiongoal', restContacts);
</script>

<script src="//code.jquery.com/jquery-3.1.0.min.js"></script>
<script src="<?php echo base_url('autocomplete/js/jquery.magicsearch.js') ?>"></script>
<script>
    $(function () {
        $('.alert').delay(3000).show().fadeOut('slow');
    });
    //assign students list modal
    function userDetails(uid) {
        //var uid = $(e).attr('data-id');
        $("#uid").val(uid);

        $.ajax({
            type: "POST",
            url: "<?php echo site_url('examiner/getAssignStudents'); ?>",
            data: {"examiner_id": uid},
            cache: false,
            //dataType: 'json',
            success: function (result) {
                $("#assign_students_list").html(result);
            }
        });
    }

//remove assign student
    function removeStudent(student_id, eid) {
        var del = confirm('Are you sure to remove this?');
        if (del) {
            var url = "<?php echo site_url('examiner/remove_students'); ?>";
            $.post(url, {'student_id': student_id, "eid": eid}, function (data) {

                if (data == "success") {
                    $("#student_" + student_id).remove();

                    alert("Student removed successfully");
                } else {
                    return false;
                }
            });
        } else {
            return false;
        }
    }

    //add assign students
    function saveData() {
        var student_ids = $('#basic').attr('data-id');
        var uid = $('#uid').val();
        if (student_ids == '') {
            alert("Add students to assign");
            return false;
        }
        return false;
        $.ajax({
            type: 'POST',
            dataType: "json",
            //data: use,
            url: "<?php echo site_url('examiner/assign_students'); ?>",
            data: {'student_id': student_ids, 'examiner_id': uid},
            success: function (res) {
                console.log(res);
                userDetails(uid);
            }
        });
        ;


    }




    $(function () {
        var dataSource = [
<?php foreach ($students_list as $k => $v) { ?>
                {id: <?= $v['uid'] ?>, firstName: "<?= $v['first_name'] ?>", lastName: "<?= $v['last_name'] ?>"},
<?php } ?>
        ];
        $('#basic').magicsearch({
            dataSource: dataSource,
            fields: ['firstName', 'lastName'],
            id: 'id',
            format: '%firstName% · %lastName%',
            multiple: true,
            multiField: 'firstName',
            multiStyle: {
                space: 5,
                width: 80
            }
        });
        $('#set-btn').click(function () {
            $('#basic').trigger('set', {id: '3,4'});
        });
    });
</script>