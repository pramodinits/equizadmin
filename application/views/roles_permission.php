<style>
    input.form-control {
        width: 25%;
        float: left;
        margin-right: 1%;
    }
</style>
<!updated template design-->
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-wrapper-before"></div>
        <div class="content-header row">
            <div class="content-header-left col-md-4 col-12 mb-2">
                <h3 class="content-header-title"></h3>
            </div>
        </div>
        <div class="content-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <?php
                        $access = json_decode($result['permission_access'], true);
                        ?>
                        <form method="post" name="add_permission" id="add_permission" action="<?= site_url('management_user/insert_permission/'); ?>">
                            <input type="hidden" name="id_user" value="<?= $id_user ?>">
                            <input type="hidden" name="id_permission" value="<?= @$result['id_permission'] ?>">
                            <table class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th class="text-center">#</th>
                                        <th class="text-center"> Menu Name </th>
                                        <th class="text-center">Action </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i = 1;
                                    foreach ($role_menu as $key => $value) {
                                        ?>
                                        <tr>
                                            <td class="text-center"><?= $i; ?></td>
                                            <td class="text-center">
                                                <?= ucwords($key) ?>
                                            </td>
                                            <td>
                                                <table class="table table-striped">
                                                    <tr>
                                                        <?php
                                                        foreach ($value as $k => $v) {
                                                            $checked = array_key_exists($k, $access[str_replace(' ', '_', $key)]) ? ' checked' : '';
                                                            ?>
                                                            <td class="text-center">
                                                                <label class="label-control"><?= $v; ?></label><br/>
                                                                <input type="checkbox" value="1" name="role_menu[<?= str_replace(' ', '_', $key) ?>][<?= $k ?>]"
                                                                       <?= $checked; ?> >
                                                            </td>
                                                            <?php
                                                        }
                                                        ?>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <?php
                                        $i++;
                                    }
                                    ?>
                                </tbody>
                            </table>
                            <div class="card-footer float-right border-top-0">
                                <div class="right">
                                    <button class="btn btn-primary" type="submit">
                                        <i class="la la-check-square-o"></i>
                                        <?php echo $this->lang->line('submit'); ?>
                                    </button>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>