<link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/animate.min.css">
<script src="<?= base_url() ?>theme-assets/vendors/js/animation/jquery.appear.js" type="text/javascript"></script>
<script src="<?= base_url() ?>js/animation.js" type="text/javascript"></script>
<!--update template design-->
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-wrapper-before"></div>
        <div class="content-header row">
            <div class="content-header-left col-md-4 col-12 mb-2">
                <!--<h3 class="content-header-title"><?= $title; ?></h3>-->
            </div>
        </div>
        <div class="content-body">
            <section id="ecommerce-stats">
                <div class="row">
                    <div class="col-xl-4 col-lg-6 col-md-12 zoomIn">
                        <div class="card pull-up ecom-card-1 bg-gradient-x-purple-red animated zoomIn">
                            <div class="card-content ecom-card2 height-150">
                                <div class="card-body">
                                    <div class="media d-flex">
                                        <div class="align-self-bottom mt-3">
                                            <i class="la la-laptop icon-opacity text-white font-large-4 float-left"></i>
                                        </div>
                                        <div class="align-self-top media-body text-white text-right">
                                            <span class="d-block mb-1 font-medium-1 font-weight-bold"><?= $this->lang->line('card_assignedexam') ?></span>
                                            <h1 class="text-white mb-0"><?php echo $assigned_exam; ?></h1>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-6 col-md-12">
                        <div class="card pull-up ecom-card-1 bg-gradient-x-orange-yellow animated zoomIn">
                            <div class="card-content ecom-card2 height-150">
                                <div class="card-body">
                                    <div class="media d-flex">
                                        <div class="align-self-bottom mt-3">
                                            <i class="la la-laptop icon-opacity text-white font-large-4 float-left"></i>
                                        </div>
                                        <div class="align-self-top media-body text-white text-right ">
                                            <span class="d-block mb-1 font-medium-1 font-weight-bold"><?= $this->lang->line('card_completedexam') ?></span>
                                            <h1 class="text-white mb-0"><?php echo $completed_exam; ?></h1>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-12">
                        <div class="card pull-up ecom-card-1 bg-gradient-x-blue-green animated zoomIn">
                            <div class="card-content ecom-card2 height-150 zoomIn">
                                <div class="card-body">
                                    <div class="media d-flex">
                                        <div class="align-self-bottom mt-3">
                                            <i class="la la-star icon-opacity text-white font-large-4 float-left"></i>
                                        </div>
                                        <div class="align-self-top media-body text-white text-right ">
                                            <span class="d-block mb-1 font-medium-1 font-weight-bold"><?= $this->lang->line('card_rating') ?></span>
                                            <h1 class="text-white mb-0"><?php echo $rating; ?></h1>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-content collpase collapse show">
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-striped">
                                            <thead>
                                                <tr>
                                                <th><?php echo $this->lang->line('result_id'); ?></th>
                                                <th><?php echo $this->lang->line('name'); ?></th>
                                                <th><?php echo $this->lang->line('quiz_name'); ?></th>
                                                <th><?php echo $this->lang->line('status'); ?>
                                                    <select class="d-none" onChange="sort_result('<?php echo $limit; ?>', this.value);">
                                                        <option value="0"><?php echo $this->lang->line('all'); ?></option>
                                                        <option value="<?php echo $this->lang->line('pass'); ?>" <?php
                                                        if ($status == $this->lang->line('pass')) {
                                                            echo 'selected';
                                                        }
                                                        ?> ><?php echo $this->lang->line('pass'); ?></option>
                                                        <option value="<?php echo $this->lang->line('fail'); ?>" <?php
                                                        if ($status == $this->lang->line('fail')) {
                                                            echo 'selected';
                                                        }
                                                        ?> ><?php echo $this->lang->line('fail'); ?></option>
                                                        <option style="display: none;" value="<?php echo $this->lang->line('pending'); ?>" <?php
                                                        if ($status == $this->lang->line('pending')) {
                                                            echo 'selected';
                                                        }
                                                        ?> ><?php echo $this->lang->line('pending'); ?></option>
                                                    </select>
                                                </th>
                                                <th><?php echo $this->lang->line('percentage_obtained'); ?></th>
                                                <th><?php echo $this->lang->line('action'); ?> </th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                            if (count($result) == 0) {
                                                ?>
                                                <tr>
                                                    <td colspan="6"><?php echo $this->lang->line('no_record_found'); ?></td>
                                                </tr>	


                                                <?php
                                            }

                                            foreach ($result as $key => $val) {
                                                ?>
                                                <tr>
                                                    <td><?php echo $val['rid']; ?></td>
                                                    <td><?php echo $val['first_name']; ?> <?php echo $val['last_name']; ?></td>
                                                    <td><?php echo $val['quiz_name']; ?></td>
                                                    <td><?php echo $val['result_status']; ?></td>
                                                    <td><?php echo number_format($val['percentage_obtained'], 2); ?>%</td>
                                                    <td>
                                                        <a href="<?php echo site_url('result/view_result/' . $val['rid']); ?>" >
                                                            <i class="la la-eye" title="<?php echo $this->lang->line('view'); ?>"></i>
                                                        </a>
                                                        <?php
                                                        if ($logged_in['su'] == '1') {
                                                            ?>
                                                            <a href="javascript:remove_entry('result/remove_result/<?php echo $val['rid']; ?>');">
                                                                <i class="la la-trash text-danger" title="Remove Result"></i>
                                                                <!--<img src="<?php echo base_url('images/cross.png'); ?>">-->
                                                            </a>
                                                            <?php
                                                        }
                                                        ?>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                            ?>
                                            </tbody>
                                        </table>
                                        <div class="pull-right">
                                            <a href="<?= base_url('result'); ?>">
                                                <button type="button" class="btn btn-primary">View All</button>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>   
        </div>
    </div>
</div>
</div>


