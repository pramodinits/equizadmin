<?php include SITE_ROOT . 'views/mail/header.php' ?>
<!-- START CENTERED WHITE CONTAINER -->
<table role="presentation" class="main">

    <!-- START MAIN CONTENT AREA -->
    <tr>
        <td class="wrapper">
            <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <p>Dear <?= $name; ?>,</p>
                        <p>Thank you for register with us.
                        </p>
                        <?php if (@$phone_verify) {
                            ?>
                            <p>Your Phone Verified Successfully.</p>

                            <?php }
                        ?>
                        <?php if (@$email_verify) {
                            ?>
                            <p>Your Email Verified Successfully.</p>

                            <?php }
                        ?>

                        <p>Regards,
                            <br/>
                            GACA Elpt </p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>

    <!-- END MAIN CONTENT AREA -->
</table>
<!-- END CENTERED WHITE CONTAINER -->
<?php include SITE_ROOT . 'views/mail/footer.php' ?>