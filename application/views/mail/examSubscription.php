<?php include SITE_ROOT . 'views/mail/header.php' ?>
 <!-- START CENTERED WHITE CONTAINER -->
            <table role="presentation" class="main">

              <!-- START MAIN CONTENT AREA -->
              <tr>
                <td class="wrapper">
                  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                      <td>
                        <p>Dear <?= $name;?>,</p>
                        <p>Thank you for subscription <?=$examname?>  , <?=$examdatetime;?>.
                            </p>
                        
                        <p>Your activation code is <b><?=$activationcode;?> </b> .</p>
                        <p>Regards,<br/>
                        Awaed Law</p>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>

            <!-- END MAIN CONTENT AREA -->
            </table>
            <!-- END CENTERED WHITE CONTAINER -->
<?php include SITE_ROOT . 'views/mail/footer.php' ?>