z<div class="container">
    <?php
    $lang = $this->config->item('question_lang');
    ?>

    <h3><?php echo $title; ?></h3>



    <div class="row">
        <form method="post" id="add_question" action="<?php echo site_url('qbank/new_question_3/' . $nop . '/' . $para); ?>" enctype="multipart/form-data">

            <div class="col-md-8">
                <br> 
                <div class="login-panel panel panel-default">
                    <div class="panel-body"> 
                        <?php
                        if ($this->session->flashdata('message')) {
                            echo $this->session->flashdata('message');
                        }
                        ?>
                        <div class="form-group">	 
                            <?php echo $this->lang->line('match_the_column'); ?>

                        </div>

                        <div class="form-group">	 
                            <label><?php echo $this->lang->line('select_category'); ?></label> 
                            <select class="form-control" name="question_category" required>
                                <option value=""><?php echo $this->lang->line('select_category'); ?></option>
                                <?php
                                foreach ($que_category as $key => $val) {
                                    ?>
                                    <option value="<?php echo $key; ?>"><?php echo $val; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label><?php echo $this->lang->line('exam_type'); ?></label> 
                            <select class="form-control" name="test_type" required>
                                <option value=""><?php echo $this->lang->line('examtype_select'); ?></option>
                                <?php
                                foreach ($exam_type as $key => $val) {
                                    ?>
                                    <option value="<?php echo $key; ?>"><?php echo $val; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label><?php echo $this->lang->line('question_module'); ?></label> 
                            <select class="form-control" name="question_module" required onchange="showVideoModule(this)">
                                <option value=""><?php echo $this->lang->line('select_module'); ?></option>
                                <?php
                                foreach ($question_module as $key => $val) {
                                    ?>
                                    <option value="<?php echo $key; ?>"><?php echo $val; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group" id="audiovideo_flag" style="display: none;">	 
                            <label><?php echo $this->lang->line('media_flag'); ?></label> 
                            <select class="form-control" name="video_flg" id="video_flg" onchange="showVideo(this)" required>
                                <option value=""><?php echo $this->lang->line('media_select'); ?></option>
                                <option value="1"><?php echo $this->lang->line('media_browse'); ?></option>
                                <option value="2"><?php echo $this->lang->line('media_url'); ?></option>
                            </select>
                        </div>
                        <div class="form-group videodiv" id="video_file_div" style="display: none;">	 
                            <label><?php echo $this->lang->line('media_upload'); ?></label>
                            <input type="file" class="form-control" required id="video_file" name="video_file"  />
                        </div>
                        <div class="form-group videodiv" id="video_url_div" style="display: none;">	 
                            <label><?php echo $this->lang->line('media_input_url'); ?></label> 
                            <input type="text" class="form-control" required id="video_url" name="video_url"  />
                        </div>
                        <div class="form-group" style="display: none;">	 
                            <label   ><?php echo $this->lang->line('select_category'); ?></label> 
                            <select class="form-control" name="cid">
                                <?php
                                foreach ($category_list as $key => $val) {
                                    ?>

                                    <option value="<?php echo $val['cid']; ?>"><?php echo $val['category_name']; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group" style="display: none;">	 
                            <label   ><?php echo $this->lang->line('select_level'); ?></label> 
                            <select class="form-control" name="lid">
                                <?php
                                foreach ($level_list as $key => $val) {
                                    ?>

                                    <option value="<?php echo $val['lid']; ?>"><?php echo $val['level_name']; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group">	 
                            <label>Time (in seconds) </label> 
                            <input class="form-control" required type="text" id="time" name="time"  />
                        </div>
                        <?php
                        if ($para == 1) {
                            ?>

                            <div class="form-group">	 
                                <label for="inputEmail"  ><?php echo $this->lang->line('paragraph'); ?></label> 
                                <textarea  name="paragraph"  class="form-control tinymcetextarea" required  ><?php
                                    if (isset($qp)) {
                                        echo $qp['paragraph'];
                                    }
                                    ?></textarea>
                            </div>


                            <?php
                        }
                        ?>			

                        <div class="form-group">	 
                            <label for="inputEmail"  ><?php echo $this->lang->line('question'); ?></label> 
                            <textarea  name="question"  class="form-control"  required ></textarea>
                        </div>
                        <div class="form-group">	 
                            <label for="inputEmail"  ><?php echo $this->lang->line('description'); ?></label> 
                            <textarea  name="description"  class="form-control" required></textarea>
                        </div>
                        <?php
                        for ($i = 1; $i <= $nop; $i++) {
                            ?>
                            <div class="form-group">	 
                                <label for="inputEmail"><?php echo $this->lang->line('options'); ?> <?php echo $i; ?>)</label> <br>
                                <input type="text" style="width: 40%;" name="option[<?php echo $i - 1; ?>]" value="" > =	<input type="text" style="width: 40%;" name="option2[<?php echo $i - 1; ?>]" value=""  > 
                            </div>
                            <?php
                        }
                        ?>

                        <input type="hidden" name="parag" id="parag" value="0">
                        <button class="btn btn-default" type="submit"><?php echo $this->lang->line('submit'); ?></button>

                        <?php
                        if ($para == 1) {
                            ?>	<button class="btn btn-default"  type="button" onClick="javascript:parag();"><?php echo $this->lang->line('submit&add'); ?></button>
                        <?php } ?>

                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    function parags() {
        $('#parag').val('1');
        $('#qf').submit();
    }
    function showVideoModule(e) {
        if ($(e).val() == 3 || $(e).val() == 2) {
            $("#audiovideo_flag").show();
        } else {
            $("#audiovideo_flag").hide();
            $("#video_url_div").hide();
            $("#video_file_div").hide();
        }
    }
    function showVideo(e) {
        if ($(e).val() == '') {
            $('.videodiv').hide();
        } else if ($(e).val() == 1) {
            $('#video_url_div').hide();
            $('#video_file_div').show();
        } else if ($(e).val() == 2) {
            $('#video_url_div').show();
            $('#video_file_div').hide();
        }

    }
</script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/additional-methods.js"></script>
<script>
    $(document).ready(function () {
        $('#add_question').validate({// initialize the plugin
            rules: {
                'time': {
                    digits: true
                },
                <?php for($i=0;$i<$nop;$i++){?>
                'option[<?php echo $i;?>]': {
                    required: true
                },
                        'option2[<?php echo $i;?>]': {
                    required: true
                },
                <?php }?>
            },
            submitHandler: function (form) { // for demo
                $('button[type=submit]').attr('disabled', 'disabled');
                return true; // for demo
            }
        });
    });
</script>