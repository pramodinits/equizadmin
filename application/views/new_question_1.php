<?php
$lang = $this->config->item('question_lang');
?>
<!--updated template page-->
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-wrapper-before"></div>
        <div class="content-header row">
            <div class="content-header-left col-md-4 col-12 mb-2">
                <h3 class="content-header-title"></h3>
            </div>
            <div class="content-header-right col-md-8 col-12">
                <div class="breadcrumbs-top float-md-right">
                    <h6><?= $breadcrumbs; ?></h6>
                </div>
            </div>
        </div>
        <div class="content-body">
            <section id="horizontal-form-layouts">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-content collpase collapse show">
                                <div class="card-body">
                                    <form method="post" id="add_question" action="<?php echo site_url('qbank/new_question_1/' . $nop . '/' . $para); ?>" enctype="multipart/form-data" >
                                        <div class="form-body">
                                            <div class="login-panel panel panel-default">
                                                <div class="panel-body">
                                                    <?php
                                                    if ($this->session->flashdata('message')) {
                                                        echo $this->session->flashdata('message');
                                                    }
                                                    ?>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <h5 class="text-info"><?php echo $this->lang->line('multiple_choice_single_answer'); ?></h5>
                                                            </div>
                                                            <div class="form-group">	 
                                                                <label><?php echo $this->lang->line('select_category'); ?></label> 
                                                                <select class="form-control" name="question_category" required>
                                                                    <option value=""><?php echo $this->lang->line('select_category'); ?></option>
                                                                    <?php
                                                                    foreach ($que_category as $key => $val) {
                                                                        ?>
                                                                        <option value="<?php echo $key; ?>"><?php echo $val; ?></option>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                            <div class="form-group">
                                                                <label><?php echo $this->lang->line('exam_type'); ?></label> 
                                                                <select class="form-control" name="test_type" required>
                                                                    <option value=""><?php echo $this->lang->line('examtype_select'); ?></option>
                                                                    <?php
                                                                    foreach ($exam_type as $key => $val) {
                                                                        ?>
                                                                        <option value="<?php echo $key; ?>"><?php echo $val; ?></option>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                            <div class="form-group">	 
                                                                <label>Time (in seconds) </label> 
                                                                <input class="form-control" required type="text" id="time" name="time"  />
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Question Section</label> 
                                                                <select class="form-control" name="question_section" required>
                                                                    <option value=""><?php echo $this->lang->line('select_section'); ?></option>
                                                                    <?php
                                                                    foreach ($question_section as $key => $val) {
                                                                        ?>
                                                                        <option value="<?php echo $key; ?>"><?php echo $val; ?></option>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>

                                                            <div class="form-group">
                                                                <label><?php echo $this->lang->line('question_module'); ?></label> 
                                                                <select class="form-control" name="question_module" required onchange="showVideoModule(this)" >
                                                                    <option value=""><?php echo $this->lang->line('select_module'); ?></option>
                                                                    <?php
                                                                    foreach ($question_module as $key => $val) {
                                                                        ?>
                                                                        <option value="<?php echo $key; ?>"><?php echo $val; ?></option>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                            <div class="form-group" id="audiovideo_flag" style="display: none;">	 
                                                                <label><?php echo $this->lang->line('media_flag'); ?></label> 
                                                                <select class="form-control" name="video_flg" id="video_flg" onchange="showVideo(this)" required>
                                                                    <option value=""><?php echo $this->lang->line('media_select'); ?></option>
                                                                    <option value="1"><?php echo $this->lang->line('media_browse'); ?></option>
                                                                    <option value="2"><?php echo $this->lang->line('media_url'); ?></option>
                                                                </select>
                                                            </div>
                                                            <div class="form-group videodiv" id="video_file_div" style="display: none;">	 
                                                                <label><?php echo $this->lang->line('media_upload'); ?></label>
                                                                <input type="file" class="form-control" required id="video_file" name="video_file"  />
                                                            </div>
                                                            <div class="form-group videodiv" id="video_url_div" style="display: none;">	 
                                                                <label><?php echo $this->lang->line('media_input_url'); ?></label> 
                                                                <input type="text" class="form-control" required id="video_url" name="video_url"  />
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <?php
                                                            if ($para == 1) {
                                                                foreach ($lang as $lkey => $val) {
                                                                    $lno = $lkey;
                                                                    if ($lkey == 0) {
                                                                        $lno = "";
                                                                    }
                                                                    ?>
                                                                    <div class="col-lg-12">
                                                                        <div class="form-group">	 
                                                                            <label for="paragraph"  ><?php echo $this->lang->line('paragraph') . ' : ' . $val; ?></label> 
                                                                            <textarea  name="paragraph<?php echo $lno; ?>"  class="form-control tinymcetextarea" required  autofocus><?php
                                                                                if (isset($qp)) {
                                                                                    echo $qp['paragraph' . $lno];
                                                                                }
                                                                                ?></textarea>
                                                                        </div>
                                                                    </div>
                                                                    <?php
                                                                }
                                                            }
                                                            ?>			

                                                            <?php
                                                            foreach ($lang as $lkey => $val) {
                                                                $lno = $lkey;
                                                                if ($lkey == 0) {
                                                                    $lno = "";
                                                                }
                                                                ?>
                                                                <div class="col-lg-12">
                                                                    <div class="form-group">	 
                                                                        <label for="question"  ><?php echo $this->lang->line('question') . ' : ' . $val; ?></label> 
                                                                        <textarea  name="question<?php echo $lno; ?>"  class="form-control" required></textarea><br>


                                                                    </div>
                                                                </div>		
                                                                <?php
                                                            }
                                                            foreach ($lang as $lkey => $val) {
                                                                $lno = $lkey;
                                                                if ($lkey == 0) {
                                                                    $lno = "";
                                                                }
                                                                ?>	<div class="col-lg-12">		
                                                                    <div class="form-group">	 
                                                                        <label for="inputEmail"  ><?php echo $this->lang->line('description') . ' : ' . $val; ?></label> 
                                                                        <textarea  name="description<?php echo $lno; ?>"  class="form-control"></textarea>
                                                                    </div>
                                                                </div>		
                                                                <?php
                                                            }
                                                            ?>
                                                            <?php
                                                            for ($i = 1; $i <= $nop; $i++) {
                                                                ?>
                                                                <div class="row">
                                                                    <?php
                                                                    foreach ($lang as $lkey => $val) {
                                                                        $lno = $lkey;
                                                                        if ($lkey == 0) {
                                                                            $lno = "";
                                                                        }
                                                                        ?>
                                                                        <div class="col-lg-12">
                                                                            <div class="form-group">	 
                                                                                <label for="inputEmail"  ><?php echo $this->lang->line('options'); ?> <?php echo $i; ?>) <?php echo ' : ' . $val; ?></label> <br>
                                                                                <?php
                                                                                if ($lkey == 0) {
                                                                                    ?>	<input type="radio" name="score" value="<?php echo $i - 1; ?>" <?php
                                                                                    if ($i == 1) {
                                                                                        echo 'checked';
                                                                                    }
                                                                                    ?> > Select Correct Option 
                                                                                       <?php } else { ?>  <?php }
                                                                                       ?>		
                                                                                <br><textarea class="form-control required" required="" name="option<?php echo $lno; ?>[<?php echo $i - 1; ?>]"     ></textarea>
                                                                            </div>
                                                                        </div>
                                                                        <?php
                                                                    }
                                                                    // echo $lno."++";
                                                                    ?></div>
                                                                <?php
                                                            }
                                                            ?>
                                                            <input type="hidden" name="parag" id="parag" value="0">
                                                        </div>
                                                    </div>
                                                    <div class="form-group" style="display: none;"  >
                                                        <input type="checkbox" name="with_paragraph" value="1"> <?php echo $this->lang->line('with_paragraph'); ?></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-actions right">
                                            <button class="btn btn-primary" type="submit"><i class="la la-check-square-o"></i><?php echo $this->lang->line('submit'); ?></button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
<!--updated template page-->
<script>
    function parags() {
        $('#parag').val('1');
        $('#qf').submit();
    }
    function showVideoModule(e) {
        if ($(e).val() == 2 || $(e).val() == 3 || $(e).val() == 4) {
            $("#audiovideo_flag").show();
        } else {
            $("#audiovideo_flag").hide();
            $("#video_url_div").hide();
            $("#video_file_div").hide();
        }
    }
    function showVideo(e) {
        if ($(e).val() == '') {
            $('.videodiv').hide();
        } else if ($(e).val() == 1) {
            $('#video_url_div').hide();
            $('#video_file_div').show();
        } else if ($(e).val() == 2) {
            $('#video_url_div').show();
            $('#video_file_div').hide();
        }

    }

</script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/additional-methods.js"></script>
<script>
    $(document).ready(function () {
        $('#add_question').validate({// initialize the plugin
        rules: {
        'time': {
        digits: true
        },
<?php for ($i = 0; $i < $nop; $i++) { ?>
            'option[<?php echo $i; ?>]': {
            required: true
            },
<?php } ?>
        },
                submitHandler: function (form) { // for demo
                $('button[type=submit]').attr('disabled', 'disabled');
                        return true; // for demo
                }
        });
    });
</script>