<!--updated template page-->
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-wrapper-before"></div>
        <div class="content-header row">
            <div class="content-header-left col-md-4 col-12 mb-2">
                <h3 class="content-header-title"></h3>
            </div>
            <div class="content-header-right col-md-8 col-12">
                <div class="breadcrumbs-top float-md-right">
                    <h6><?= $breadcrumbs; ?></h6>
                </div>
            </div>
        </div>
        <div class="content-body">
            <section id="horizontal-form-layouts">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-content collpase collapse show">
                                <div class="card-body">
                                    <form method="post" action="<?php echo site_url('qbank/pre_new_question/'); ?>">
                                        <div class="form-body">
                                            <div class="login-panel panel panel-default">
                                                <div class="panel-body">
                                                    <?php
                                                    if ($this->session->flashdata('message')) {
                                                        echo $this->session->flashdata('message');
                                                    }
                                                    ?>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">	 
                                                                <label><?php echo $this->lang->line('select_question_type'); ?></label> 
                                                                <select class="form-control" name="question_type" onChange="hidenop(this.value);">
                                                                    <option value="1"><?php echo $this->lang->line('multiple_choice_single_answer'); ?></option>
                                                                    <option value="2"><?php echo $this->lang->line('multiple_choice_multiple_answer'); ?></option>
                                                                    <!--<option value="3"><?php echo $this->lang->line('match_the_column'); ?></option>-->
                                    <!--						<option value="4"><?php echo $this->lang->line('short_answer'); ?></option>
                                                                    <option value="5"><?php echo $this->lang->line('long_answer'); ?></option>-->
                                                                    <option value="6"><?php echo $this->lang->line('fill_the_blank'); ?></option>
                                                                    <!--<option value="7"><?php echo $this->lang->line('sort_the_column'); ?></option>-->
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" id="nop" >	 
                                                                <label for="inputEmail"  ><?php echo $this->lang->line('nop'); ?></label> 
                                                                <input type="text"   name="nop"  class="form-control" value="4"   >
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group" style="display: none;"  >
                                                        <input type="checkbox" name="with_paragraph" value="1"> <?php echo $this->lang->line('with_paragraph'); ?></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-actions right">
                                            <button class="btn btn-primary" type="submit"><?php echo $this->lang->line('next'); ?></button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
<!--updated template page-->


