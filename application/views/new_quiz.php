<link href="<?php echo base_url('css/select2.min.css'); ?>" rel="stylesheet" />
<script src="<?php echo base_url('js/select2.min.js'); ?>"></script>
<!-- Latest compiled and minified CSS -->
<!--for datepicker-->
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<!--for datepicker-->

<!--for country list-->
<script src="<?php echo base_url(); ?>vendor/jquery/countries.js"></script>
<!--for country list-->
<style>
    .ui-timepicker-standard a {
        text-align: left !important;
        font-size: 1em !important;
    }

    /*    .breadcrumb {
            background-color: #f4f5f7 !important;
        }*/
</style>
<!-- Optional theme -->

<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-wrapper-before"></div>
        <div class="content-header row">
            <div class="content-header-left col-md-4 col-12 mb-2">
                <h3 class="content-header-title"></h3>
            </div>
            <div class="content-header-right col-md-8 col-12">
                <div class="breadcrumbs-top float-md-right">
                    <div class="breadcrumb-wrapper mr-1">
                        <h6><?= $breadcrumbs; ?></h6>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <section id="horizontal-form-layouts">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-content collpase collapse show">
                                <div class="card-body">
                                    <form method="post" id="add_quiz" name="add_quiz" enctype="multipart/form-data" action="<?php echo site_url('quiz/insert_quiz/'); ?>">
                                        <div class="form-body">
                                            <div class="col-md-12">
                                                <div class="login-panel panel panel-default">
                                                    <div class="panel-body">
                                                        <?php
                                                        if ($this->session->flashdata('message')) {
                                                            echo $this->session->flashdata('message');
                                                        }
                                                        ?>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">	 
                                                                    <label><?php echo $this->lang->line('exam_category'); ?></label> 
                                                                    <select class="form-control" name="exam_category" required>
                                                                        <option value=""><?php echo $this->lang->line('examcategory_select'); ?></option>
                                                                        <?php
                                                                        foreach ($exam_category as $key => $val) {
                                                                            ?>
                                                                            <option value="<?php echo $key; ?>"><?php echo $val; ?></option>
                                                                            <?php
                                                                        }
                                                                        ?>
                                                                    </select>
                                                                </div>
                                                                <div class="form-group">	 
                                                                    <label for="inputEmail"><?php echo $this->lang->line('quiz_name'); ?></label> 
                                                                    <input type="text"  name="quiz_name"  class="form-control" placeholder="<?php echo $this->lang->line('quiz_name'); ?>"  required>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label><?php echo $this->lang->line('quiz_media'); ?></label>
                                                                    <input type="file" class="form-control" name="photo" accept="image/*" />
                                                                </div>

                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label><?php echo $this->lang->line('exam_type'); ?></label> 
                                                                    <select class="form-control" name="exam_type" required onchange="showPrice(this)">
                                                                        <option value=""><?php echo $this->lang->line('examtype_select'); ?></option>
                                                                        <?php
                                                                        foreach ($exam_type as $key => $val) {
                                                                            ?>
                                                                            <option value="<?php echo $key; ?>"><?php echo $val; ?></option>
                                                                            <?php
                                                                        }
                                                                        ?>
                                                                    </select>
                                                                </div>
                                                                <div class="form-group" style="display:none;" id="quiz_price">	 
                                                                    <label for="inputEmail" ><?php echo $this->lang->line('quiz_price'); ?></label> <br>
                                                                    <input type="text" name="quiz_price" class="form-control" placeholder="<?php echo $this->lang->line('quiz_price'); ?>"  required >
                                                                </div>
                                                                <div class="form-group">	 
                                                                    <label for="inputEmail"  ><?php echo $this->lang->line('description'); ?></label> 
                                                                    <textarea   name="description" required class="form-control" ></textarea> <!-- tinymce_textarea -->
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <div class="row">
                                                                    <label><?php echo $this->lang->line('exam_section'); ?></label> 
                                                                    <div class="col-md-12">
                                                                        <div class="row">
                                                                            <div class="col-xl-4 col-md-6 col-sm-12">
                                                                                <div class="card text-white box-shadow-0 bg-info">
                                                                                    <div class="card-content collapse show">
                                                                                        <div class="card-body">
                                                                                            <h4 class="card-title text-white">Structure
                                                                                            </h4>
                                                                                            <input type="text" name="structure_flag" class="form-control" placeholder="<?php echo $this->lang->line('question_nos'); ?>" autofocus>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            
                                                                             <div class="col-xl-4 col-md-6 col-sm-12">
                                                                                <div class="card text-white box-shadow-0 bg-info">
                                                                                    <div class="card-content collapse show">
                                                                                        <div class="card-body">
                                                                                            <h4 class="card-title text-white">Vocabulary</h4>
                                                                                            <input type="text" name="vocabulary_flag" class="form-control" placeholder="<?php echo $this->lang->line('question_nos'); ?>" autofocus>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            
                                                                             <div class="col-xl-4 col-md-6 col-sm-12">
                                                                                <div class="card text-white box-shadow-0 bg-info">
                                                                                    <div class="card-content collapse show">
                                                                                        <div class="card-body">
                                                                                            <h4 class="card-title text-white">Fluency</h4>
                                                                                            <input type="text" name="fluency_flag" class="form-control" placeholder="<?php echo $this->lang->line('question_nos'); ?>" autofocus>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            
                                                                             <div class="col-xl-4 col-md-6 col-sm-12">
                                                                                <div class="card text-white box-shadow-0 bg-info">
                                                                                    <div class="card-content collapse show">
                                                                                        <div class="card-body">
                                                                                            <h4 class="card-title text-white">Comprehension</h4>
                                                                                            <input type="text" name="comprehension_flag" class="form-control" placeholder="<?php echo $this->lang->line('question_nos'); ?>" autofocus>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            
                                                                            <div class="col-xl-4 col-md-6 col-sm-12">
                                                                                <div class="card text-white box-shadow-0 bg-info">
                                                                                    <div class="card-content collapse show">
                                                                                        <div class="card-body">
                                                                                            <h4 class="card-title text-white">Pronunciation
                                                                                            </h4>
                                                                                            <input type="text" name="pronunciation_flag" class="form-control" placeholder="<?php echo $this->lang->line('question_nos'); ?>" autofocus>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            
                                                                           
                                                                           
                                                                           
                                                                            <div class="col-xl-4 col-md-6 col-sm-12">
                                                                                <div class="card text-white box-shadow-0 bg-info">
                                                                                    <div class="card-content collapse show">
                                                                                        <div class="card-body">
                                                                                            <h4 class="card-title text-white">Interactions</h4>
                                                                                            <div class="col-md-6 mb-2 text-center bold" style="float: left;">
                                                                                                <input type="radio" class="form-control"   name="video_conference" value="0"> NO
                                                                                            </div>
                                                                                            <div class="col-md-6 mb-2 text-center bold " style="float: right;">
                                                                                                <input type="radio" class="form-control"  name="video_conference" value="1"> YES 
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-actions right">
                                            <button class="btn btn-primary" type="submit">
                                                <i class="la la-check-square-o"></i>
                                                <?php echo $this->lang->line('submit'); ?>
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>

<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
<script>
                                                                        populateCountries("country");
                                                                        $(document).ready(function () {
                                                                            var max_fields = 5; //maximum input boxes allowed
                                                                            var wrapper = $(".input-duration-wrapper"); //Fields wrapper
                                                                            var add_button = $(".add_field_button"); //Add button ID

                                                                            var x = 1; //initlal text box count
                                                                            $(add_button).click(function (e) { //on add input button click
                                                                                e.preventDefault();
//        if (x < max_fields){ //max input box allowed
                                                                                x++; //text box increment
                                                                                $(wrapper).append('<div class="input-duration-wrapper mb-1">\n\
<input style="float: left; width: 90%;" type="text" name="duration[]"  value="" class="duration mb-1 form-control" placeholder="" >\n\
<a style="float: right;margin-top: 1.5%;" href="#" class="remove_field"><i class="fa fa-times text-danger" aria-hidden="true"></i></a>\n\
</div>'); //add input box
//}
                                                                                $('.duration').timepicker({
                                                                                    timeFormat: 'h:mm p',
                                                                                    interval: 60,
                                                                                    minTime: '10',
                                                                                    maxTime: '6:00pm',
                                                                                    defaultTime: '11',
                                                                                    startTime: '10:00',
                                                                                    dynamic: false,
                                                                                    dropdown: true,
                                                                                    scrollbar: true
                                                                                });
                                                                            });
                                                                            $(wrapper).on("click", ".remove_field", function (e) { //user click on remove text
                                                                                e.preventDefault();
                                                                                $(this).parent('div').remove();
                                                                                x--;
                                                                            })

//set up timepicker
//$(function () {
//                $('.duration').datetimepicker({
//                    format: 'LT'
//                });
//            });
                                                                            $('.duration').timepicker({
                                                                                timeFormat: 'h:mm p',
                                                                                interval: 60,
                                                                                minTime: '10',
                                                                                maxTime: '6:00pm',
                                                                                defaultTime: '11',
                                                                                startTime: '10:00',
                                                                                dynamic: false,
                                                                                dropdown: true,
                                                                                scrollbar: true
                                                                            });
                                                                        });
                                                                        $(function () {
                                                                            $("#end_date").datepicker({
                                                                                dateFormat: 'yy-mm-dd',
                                                                                'minDate': 0
                                                                            });
                                                                        });
                                                                        $(function () {
                                                                            $("#start_date").datepicker({
                                                                                dateFormat: 'yy-mm-dd',
                                                                                'minDate': 0,
                                                                                onSelect: function (selected) {
                                                                                    $("#end_date").datepicker("option", "minDate", selected)
                                                                                }
                                                                            });
                                                                        });
</script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
<script>
                                                                        $(document).ready(function () {
                                                                            $('#add_quiz').validate({
                                                                                rules: {
                                                                                    quiz_price: {
                                                                                        digits: true
                                                                                    }
                                                                                },
                                                                                submitHandler: function (form) { // for demo
                                                                                    $('button[type=submit]').attr('disabled', 'disabled');
                                                                                    return true; // for demo
                                                                                }
                                                                            });
                                                                        });

                                                                        function showPrice(e) {
                                                                            var examType = $(e).val();
                                                                            if (examType == 2) {
                                                                                $("#quiz_price").show();
                                                                            } else {
                                                                                $("#quiz_price").hide();
                                                                            }
                                                                        }
</script>
