<!--for datepicker-->
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<!--for datepicker-->

<!--update template design-->
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-wrapper-before"></div>
        <div class="content-header row">
            <div class="content-header-left col-md-4 col-12 mb-2">
                <h3 class="content-header-title"></h3>
            </div>
        </div>
        <div class="content-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collpase collapse show">
                            <div class="card-body">
                                <?php
                                if ($this->session->flashdata('message')) {
                                    echo $this->session->flashdata('message');
                                }
                                ?>
                                <?php
                                if ($logged_in['su'] == '1') {
                                    ?>
                                    <div class='alert alert-danger'><?php echo $this->lang->line('pending_message_admin'); ?></div>		
                                    <?php
                                }
                                ?>
                                <?php
                                $logged_in = $this->session->userdata('logged_in');
                                ?>
                                <?php
                                if ($logged_in['su'] == '1') {
                                    ?>
                                    <div class="col-lg-12">
                                        <form method="post" action="<?php echo site_url('result/generate_report/'); ?>"> 
                                            <div class="row">
                                                <!--<div> <h3><?php echo $this->lang->line('generate_report'); ?> </h3> </div> <br>-->
                                                <select name="category_id" class="form-control col-md-2 mr-1">
                                                    <option value="0"><?php echo $this->lang->line('all_category'); ?></option>
                                                    <?php
                                                    foreach ($category_type as $key => $val) {
                                                        ?>
                                                        <option value="<?php echo $key; ?>"><?php echo $val; ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>

                                                <select name="quid" class="form-control col-md-2 mr-1">
                                                    <option value="0"><?php echo $this->lang->line('select_quiz'); ?></option>
                                                    <?php
                                                    foreach ($quiz_list as $qk => $quiz) {
                                                        ?>
                                                        <option value="<?php echo $quiz['quid']; ?>"><?php echo $quiz['quiz_name']; ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>

                                                <select name="gid" style="display: none;">
                                                    <option value="0"><?php echo $this->lang->line('select_group'); ?></option>
                                                    <?php
                                                    foreach ($group_list as $gk => $group) {
                                                        ?>
                                                        <option value="<?php echo $group['gid']; ?>"><?php echo $group['group_name']; ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                                <input type="text" class="form-control col-md-2 mr-1 singledate" onkeydown="return false" autocomplete="false" name="date1" id="start_date" value="" placeholder="<?php echo $this->lang->line('date_from'); ?>">

                                                <input type="text" class="form-control col-md-2 mr-1" onkeydown="return false" name="date2" id="end_date" value="" placeholder="<?php echo $this->lang->line('date_to'); ?>">

                                                <button class="btn btn-info font-weight-bold" type="submit"><?php echo $this->lang->line('generate_report'); ?></button>	
                                            </div><!-- /input-group -->
                                        </form>
                                    </div><!-- /.col-lg-6 -->
                                    <?php
                                }
                                ?>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th><?php echo $this->lang->line('result_id'); ?></th>
                                                <th><?php echo $this->lang->line('name'); ?></th>
                                                <th><?php echo $this->lang->line('quiz_name'); ?></th>
                                                <th><?php echo $this->lang->line('status'); ?>
                                                    <select class="d-none" onChange="sort_result('<?php echo $limit; ?>', this.value);">
                                                        <option value="0"><?php echo $this->lang->line('all'); ?></option>
                                                        <option value="<?php echo $this->lang->line('pass'); ?>" <?php
                                                        if ($status == $this->lang->line('pass')) {
                                                            echo 'selected';
                                                        }
                                                        ?> ><?php echo $this->lang->line('pass'); ?></option>
                                                        <option value="<?php echo $this->lang->line('fail'); ?>" <?php
                                                        if ($status == $this->lang->line('fail')) {
                                                            echo 'selected';
                                                        }
                                                        ?> ><?php echo $this->lang->line('fail'); ?></option>
                                                        <option style="display: none;" value="<?php echo $this->lang->line('pending'); ?>" <?php
                                                        if ($status == $this->lang->line('pending')) {
                                                            echo 'selected';
                                                        }
                                                        ?> ><?php echo $this->lang->line('pending'); ?></option>
                                                    </select>
                                                </th>
                                                <th><?php echo $this->lang->line('percentage_obtained'); ?></th>
                                                <th><?php echo $this->lang->line('action'); ?> </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            if (count($result) == 0) {
                                                ?>
                                                <tr>
                                                    <td colspan="6"><?php echo $this->lang->line('no_record_found'); ?></td>
                                                </tr>	


                                                <?php
                                            }
                                            $logged_in = $this->session->userdata('logged_in');
                                            $access = json_decode($logged_in['permission_access'], true);
                                            foreach ($result as $key => $val) {
                                                ?>
                                                <tr>
                                                    <td><?php echo $val['rid']; ?></td>
                                                    <td><?php echo $val['first_name']; ?> <?php echo $val['last_name']; ?></td>
                                                    <td><?php echo $val['quiz_name']; ?></td>
                                                    <td><?php echo $val['result_status']; ?></td>
                                                    <td><?php echo number_format($val['percentage_obtained'], 2); ?>%</td>
                                                    <td>
                                                        <?php
                                                        if ($logged_in['su'] == 7 && empty($access['result_list']['view_result']) && empty($access['result_list']['delete_result'])) {
                                                            echo "N/A";
                                                        }
                                                        if ($access['result_list']['view_result'] || $logged_in['su'] == 1) {
                                                            ?>
                                                            <a href="<?php echo site_url('result/view_result/' . $val['rid']); ?>" >
                                                                <i class="la la-eye" title="<?php echo $this->lang->line('view'); ?>"></i>
                                                            </a>
                                                        <?php }
                                                        ?>

                                                        <?php
                                                        if ($access['result_list']['delete_result'] || $logged_in['su'] == '1') {
                                                            ?>
                                                            <a href="javascript:remove_entry('result/remove_result/<?php echo $val['rid']; ?>');">
                                                                <i class="la la-trash text-danger" title="Remove Result"></i>
                                                                <!--<img src="<?php echo base_url('images/cross.png'); ?>">-->
                                                            </a>
                                                            <?php
                                                        }
                                                        ?>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                            ?>
                                        </tbody>
                                    </table>
<?= $links; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--update template design-->


<div class="container" style="display: none;">


    <hr>
    <h3><?php echo $title; ?></h3>

    <div class="row">

        <div class="col-lg-6">
            <form method="post" action="<?php echo site_url('result/index/'); ?>">
                <div class="input-group">
                    <input type="text" class="form-control" name="search" placeholder="<?php echo $this->lang->line('search'); ?>...">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="submit"><?php echo $this->lang->line('search'); ?></button>
                    </span>


                </div><!-- /input-group -->
            </form>
        </div><!-- /.col-lg-6 -->
    </div><!-- /.row -->
</div>

<script>
    $(function () {
        $("#start_date").datepicker({
            dateFormat: 'yy-mm-dd',
            'maxDate': 0,
            onSelect: function (selected) {
                $("#end_date").datepicker("option", "minDate", selected)
            }
        });
    });
    $(function () {
        $("#end_date").datepicker({
            dateFormat: 'yy-mm-dd',
            'maxDate': 0
        });
    });
</script>