<style>
    .searchForm input.form-control {
        width: 25%;
        float: left;
        margin-right: 1%;
    }
</style>
<link href="<?php echo base_url('autocomplete/css/jquery.magicsearch.css'); ?>" rel="stylesheet">
<script src="//code.jquery.com/jquery-3.1.0.min.js"></script>

<!--updated template design-->
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-wrapper-before"></div>
        <div class="content-header row">
            <div class="content-header-left col-md-4 col-12 mb-2">
                <h3 class="content-header-title"></h3>
            </div>
        </div>
        <div class="content-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">

                        <div class="card-content collpase collapse show">
                            <div class="card-body">
                                <?php
                                if ($this->session->flashdata('message')) {
                                    echo $this->session->flashdata('message');
                                }
                                ?>
                                <div class="row">
                                    <div class="col-md-12">
                                        <form class="searchForm" method="get" action="<?php echo site_url('agency/index/'); ?>">
                                            <input type="text" name="username" class="form-control" value="<?= $user['username'] ?>" placeholder="Search Name">
                                            <input type="text" name="email" class="form-control" value="<?= $user['email'] ?>" placeholder="Search Email">
                                            <input type="text" name="phone" class="form-control" value="<?= $user['phone'] ?>" placeholder="Search Phone">
                                            <button type="submit" class="btn btn-primary"><?= $this->lang->line('filter') ?></button>
                                            <a href="<?= site_url('agency/'); ?>">
                                                <button type="button" class="btn btn-primary"><?php echo $this->lang->line('reset_button'); ?></button>
                                            </a>
                                        </form>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th class="text-center"><?= $this->lang->line('unique_id'); ?></th>
                                                <th class="text-center"><?= $this->lang->line('agency_name'); ?></th>
                                                <th class="text-center"><?php echo $this->lang->line('contact_info'); ?></th>
                                                <th class="text-center"><?php echo $this->lang->line('center_location'); ?> </th>
                                                <th class="text-center"><?php echo $this->lang->line('markup_price'); ?> </th>
                                                <th class="text-center"><?php echo $this->lang->line('add_wallet'); ?> </th>
                                                <th class="text-center"><?php echo $this->lang->line('action'); ?> </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            if (count($result) == 0) {
                                                ?>
                                                <tr>
                                                    <td colspan="8"><?php echo $this->lang->line('no_record_found'); ?></td>
                                                </tr>
                                                <?php
                                            }
                                            $logged_in = $this->session->userdata('logged_in');
                                            $access = json_decode($logged_in['permission_access'], true);
                                            foreach ($result as $key => $val) {
                                                $student_name = $val['first_name'] . " " . $val['last_name'];
                                                ?>
                                                <tr id="<?= "agency_" . $val['uid'] ?>">
                                                    <td><?php echo $val['uid']; ?></td>
                                                    <td><?= $val['unique_id'] ? $val['unique_id'] : "N/A" ?></td>
                                                    <td style="width: 15%"><?php echo $val['company'] ? $val['company'] : "N/A"; ?></td>
                                                    <td style="width:30%">
                                                        <span><i class="la la-user text-primary"></i>&nbsp;<?= $student_name ?></span><br/>
                                                        <span><i class="la la-envelope text-primary"></i>&nbsp;<?= $val['email'] ?></span><br/>
                                                        <span><i class="la la-phone-square text-primary"></i>&nbsp;<?= $val['contact_no'] ?></span>
                                                    </td>
                                                    <td style="width: 15%">
                                                        <span>
                                                            <span class="text-primary">
                                                                <?= $this->lang->line('city') . ": " ?>
                                                            </span> <?= $city_list[$val['id_city']] ?><br/>
                                                            <span class="text-primary">
                                                                <?= $this->lang->line('location') . ": " ?>
                                                            </span> <?= $val['id_location'] ? $location_list[$val['id_location']] : "N/A" ?><br/>
                                                            <span class="text-primary">
                                                                <?= $this->lang->line('address') . ": " ?>
                                                            </span> <?= $val['address'] ?>
                                                        </span>
                                                    </td>
                                                    <td class="text-center"><?= $val['markup_price'] ? $val['markup_price'] : 0; ?></td>
                                                    <td class="text-center" width="10%">
                                                        <span>
                                                            <i class="la la-briefcase mb-1 text-primary" title="Wallet balance"></i>&nbsp;
                                                            <?= $val['wallet_balance'] ?>
                                                        </span>
                                                        <?php
                                                        if ($access['training_center']['add_wallet'] || $logged_in['su'] == 1) {
                                                        ?>
                                                        <br/>
                                                        <button class="btn btn-primary btn-sm btn-min-width text-center" data-userid="<?= $val['uid'] ?>"
                                                                data-agencyname="<?= $student_name ?>" data-walletbalance="<?= $val['wallet_balance'] ? $val['wallet_balance'] : "0.00" ?>"
                                                                data-toggle="modal" data-target="#addWallet" onclick="addWallet(this)" type="submit">
                                                            Add Wallet
                                                        </button>
                                                        <?php } ?>
                                                    </td>
                                                    <td>
                                                        <?php
                                                        if ($logged_in['su'] == 7 && empty($access['training_center']['add_edit']) && empty($access['training_center']['delete'])) {
                                                            echo "N/A";
                                                        }
                                                        if ($access['training_center']['add_edit'] || $logged_in['su'] == 1) {
                                                            ?>
                                                            <a href="<?php echo site_url('agency/new_agency/' . $val['uid']); ?>">
                                                                <i class="la la-edit" aria-hidden="true" title="Edit Agency"></i>
                                                            </a>
                                                        <?php }
                                                        if ($access['training_center']['delete'] || $logged_in['su'] == 1) {
                                                            ?>
                                                            <a href="javascript:void(0);" onclick="removeAgency('<?= $val['uid'] ?>')">
                                                                <i class="la la-trash text-danger" aria-hidden="true" title="Remove Agency"></i>
                                                            </a>
                                                            <?php } ?>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                            ?>
                                        </tbody>
                                    </table>
<?= $links; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--updated template design-->


<!--Modal--->
<div class="modal fade" id="addWallet" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title pull-left"><?= $this->lang->line('add_wallet'); ?></h4>
                <button type="button" class="close pull-right" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <label>
                            <strong><?= $this->lang->line('current_wallet_balance'); ?></strong>
                        </label> : <span id="walletbalance"></span>
                    </div>
                </div>
                <form class="form-horizontal" method="post" name="add_balance" id="add_balance" action="<?php echo site_url('agency/update_wallet/'); ?>">
                    <input type="hidden" id="uid" name="uid" value=""/>
                    <input type="hidden" id="current_balance" name="current_balance" value=""/>
                    <div class="form-group">
                        <label class="label-control"><?= $this->lang->line('select_type'); ?></label>
                        <select class="form-control" name="transaction_type" required="">
                            <option value=""><?= $this->lang->line('select_type'); ?></option>
                            <option value="1"><?= $this->lang->line('select_add'); ?></option>
                            <option value="2"><?= $this->lang->line('select_deduct'); ?></option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="label-control"><?= $this->lang->line('add_balance'); ?></label>
                        <input class="form-control" required type="text" name="wallet_balance" >
                    </div>
                    <div class="form-group">
                        <label><?= $this->lang->line('city_description'); ?></label>
                        <textarea name="description" rows="3" cols="5" class="form-control"></textarea>
                    </div>
                    <div class="form-actions">
                        <input type="submit" class="btn btn-success" value="Save" id="submit">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!--Modal--->


<script src="//code.jquery.com/jquery-3.1.0.min.js"></script>
<script src="<?php echo base_url('autocomplete/js/jquery.magicsearch.js') ?>"></script>
<script>
                                                                $(function () {
                                                                    $('.alert').delay(3000).show().fadeOut('slow');
                                                                });

                                                                //remove assign student
                                                                function removeAgency(uid) {
                                                                    var del = confirm('Are you sure to remove this?');
                                                                    if (del) {
                                                                        var url = "<?php echo site_url('agency/remove_agency'); ?>";
                                                                        $.post(url, {'uid': uid}, function (data) {

                                                                            if (data == "success") {
                                                                                $("#agency_" + uid).remove();
                                                                                alert("Agency removed successfully");
                                                                                location.reload();
                                                                            } else {
                                                                                return false;
                                                                            }
                                                                        });
                                                                    } else {
                                                                        return false;
                                                                    }
                                                                }

                                                                function addWallet(e) {
                                                                    var userid = $(e).attr('data-userid');
                                                                    var walletbalance = $(e).attr('data-walletbalance');
                                                                    $("#uid").val(userid);
                                                                    $("#walletbalance").text(walletbalance);
                                                                    $("#current_balance").val(walletbalance);
                                                                }

</script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
<script>
                                                                $(document).ready(function () {
                                                                    $('#add_balance').validate({
                                                                        rules: {
                                                                            'wallet_balance': {
                                                                                required: true,
                                                                                digits: true
                                                                            }
                                                                        },
                                                                        submitHandler: function (form) { // for demo
                                                                            $('button[type=submit]').attr('disabled', 'disabled');
                                                                            return true; // for demo
                                                                        }
                                                                    });
                                                                });
</script>