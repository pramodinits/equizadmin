<link href="<?php echo base_url('css/select2.min.css'); ?>" rel="stylesheet" />
<script src="<?php echo base_url('js/select2.min.js'); ?>"></script>
<!--for datepicker-->
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<!--for datepicker-->

<!--for country list-->
<script src="<?php echo base_url(); ?>vendor/jquery/countries.js"></script>
<!--for country list-->
<style>
    .ui-timepicker-standard a {
        text-align: left !important;
        font-size: 1em !important;
    }
    textarea.error, select.error, input[type=text].error {
        border: 1px solid #9f1212 !important;
    }
</style>
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-wrapper-before"></div>
        <div class="content-header row">
            <div class="content-header-left col-md-4 col-12 mb-2">
                <h3 class="content-header-title"></h3>
            </div>
            <div class="content-header-right col-md-8 col-12">
                <div class="breadcrumbs-top float-md-right">
                    <h6><?= $breadcrumbs; ?></h6>
                </div>
            </div>
        </div>
        <div class="content-body">
            <section id="horizontal-form-layouts">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-content collpase collapse show">
                                <div class="card-body">
                                    <form method="post" id="edit_quiz" name="edit_quiz" enctype="multipart/form-data" action="<?php echo site_url('quiz/update_quiz/' . $quiz['quid']); ?>">
                                        <div class="form-body">
                                            <h4 class="form-section">
                                                <?= !$this->session->flashdata('addquestion') ? $title : $quiz['quiz_name']; ?>
                                            </h4>
                                            <div class="col-md-12">
                                                <div class="login-panel panel panel-default">
                                                    <div class="panel-body">
                                                        <?php
                                                        if ($this->session->flashdata('message')) {
                                                            echo $this->session->flashdata('message');
                                                        }
                                                        ?>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">	 
                                                                    <label><?php echo $this->lang->line('exam_category'); ?></label> 
                                                                    <select class="form-control" name="exam_category" required>
                                                                        <option value=""><?php echo $this->lang->line('examcategory_select'); ?></option>
                                                                        <?php
                                                                        foreach ($exam_category as $key => $val) {
                                                                            ?>
                                                                            <option value="<?php echo $key; ?>" <?= $quiz['exam_category'] == $key ? "selected" : "" ?>><?php echo $val; ?></option>
                                                                            <?php
                                                                        }
                                                                        ?>
                                                                    </select>
                                                                </div>
                                                                <div class="form-group">	 
                                                                    <label for="inputEmail"><?php echo $this->lang->line('quiz_name'); ?></label> 
                                                                    <input type="text"  name="quiz_name"  value="<?php echo $quiz['quiz_name']; ?>" class="form-control" placeholder="<?php echo $this->lang->line('quiz_name'); ?>"  required>
                                                                </div>
                                                                <div class="form-group text-right" id="show_media">
                                                                    <p><a href="javascript:void(0)">Do you want to change image?</a></p>
                                                                </div>
                                                                <div class="form-group" id="quiz_media" style="display: none;">
                                                                    <label><?php echo $this->lang->line('quiz_media'); ?></label>
                                                                    <input type="file" class="form-control" name="photo" accept="image/*" />
                                                                </div>
                                                                <div class="form-group">
                                                                    <?php if ($quiz['photo']) { ?>
                                                                    <a href="<?= base_url() . 'quiz_media/' . $quiz['photo'] ?>" download="" target="_blank">
                                                                            <?= $quiz['photo'] ?>
                                                                        </a>
                                                                    <?php
                                                                    } else {
                                                                        echo "No image available.";
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label><?php echo $this->lang->line('exam_type'); ?></label> 
                                                                    <select class="form-control" name="exam_type" required onchange="showPrice(this)">
                                                                        <option value=""><?php echo $this->lang->line('examtype_select'); ?></option>
                                                                        <?php
                                                                        foreach ($exam_type as $key => $val) {
                                                                            ?>
                                                                            <option value="<?php echo $key; ?>" <?= $quiz['exam_type'] == $key ? "selected" : "" ?>><?php echo $val; ?></option>
                                                                            <?php
                                                                        }
                                                                        ?>
                                                                    </select>
                                                                </div>
                                                                <div class="form-group" style="display:none;" id="quiz_price">	 
                                                                    <label for="inputEmail" ><?php echo $this->lang->line('quiz_price'); ?></label> <br>
                                                                    <input type="text" name="quiz_price" class="form-control" value="<?php echo $quiz['quiz_price']; ?>" required>
                                                                </div>
                                                                <div class="form-group">	 
                                                                    <label for="inputEmail"  ><?php echo $this->lang->line('description'); ?></label> 
                                                                    <textarea   name="description" required class="form-control tinymce_textarea_NOT" ><?php echo $quiz['description']; ?></textarea>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <div class="row">
                                                                    <label><?php echo $this->lang->line('exam_section'); ?></label> 
                                                                    <div class="col-md-12">
                                                                        <div class="row">
                                                                            <div class="col-xl-4 col-md-6 col-sm-12">
                                                                                <div class="card text-white box-shadow-0 bg-info">
                                                                                    <div class="card-content collapse show">
                                                                                        <div class="card-body">
                                                                                            <h4 class="card-title text-white">Pronunciation
                                                                                            </h4>
                                                                                            <input type="text" name="pronunciation_flag" value="<?php echo $quiz['pronunciation_flag']; ?>" class="form-control" placeholder="<?php echo $this->lang->line('question_nos'); ?>" autofocus>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-xl-4 col-md-6 col-sm-12">
                                                                                <div class="card text-white box-shadow-0 bg-info">
                                                                                    <div class="card-content collapse show">
                                                                                        <div class="card-body">
                                                                                            <h4 class="card-title text-white">Structure
                                                                                            </h4>
                                                                                            <input type="text" name="structure_flag" value="<?php echo $quiz['structure_flag']; ?>" class="form-control" placeholder="<?php echo $this->lang->line('question_nos'); ?>" autofocus>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-xl-4 col-md-6 col-sm-12">
                                                                                <div class="card text-white box-shadow-0 bg-info">
                                                                                    <div class="card-content collapse show">
                                                                                        <div class="card-body">
                                                                                            <h4 class="card-title text-white">Vocabulary</h4>
                                                                                            <input type="text" name="vocabulary_flag" value="<?php echo $quiz['vocabulary_flag']; ?>" class="form-control" placeholder="<?php echo $this->lang->line('question_nos'); ?>" autofocus>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-xl-4 col-md-6 col-sm-12">
                                                                                <div class="card text-white box-shadow-0 bg-info">
                                                                                    <div class="card-content collapse show">
                                                                                        <div class="card-body">
                                                                                            <h4 class="card-title text-white">Fluency</h4>
                                                                                            <input type="text" name="fluency_flag" value="<?php echo $quiz['fluency_flag']; ?>" class="form-control" placeholder="<?php echo $this->lang->line('question_nos'); ?>" autofocus>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-xl-4 col-md-6 col-sm-12">
                                                                                <div class="card text-white box-shadow-0 bg-info">
                                                                                    <div class="card-content collapse show">
                                                                                        <div class="card-body">
                                                                                            <h4 class="card-title text-white">Comprehension</h4>
                                                                                            <input type="text" name="comprehension_flag" value="<?php echo $quiz['comprehension_flag']; ?>" class="form-control" placeholder="<?php echo $this->lang->line('question_nos'); ?>" autofocus>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-xl-4 col-md-6 col-sm-12">
                                                                                <div class="card text-white box-shadow-0 bg-info">
                                                                                    <div class="card-content collapse show">
                                                                                        <div class="card-body">
                                                                                            <h4 class="card-title text-white">Interactions</h4>
                                                                                            <div class="col-md-6 mb-2 text-center bold" style="float: left;">
                                                                                                <input type="radio" class="form-control"   name="video_conference" value="0" <?php echo ($quiz['video_conference'] == 0) ? "checked" : ""; ?>> NO
                                                                                            </div>
                                                                                            <div class="col-md-6 mb-2 text-center bold " style="float: right;">
                                                                                                <input type="radio" class="form-control"  name="video_conference" value="1" <?php echo ($quiz['video_conference'] == 1) ? "checked" : ""; ?>> YES 
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-actions right">
                                            <button class="btn btn-primary" type="submit">
                                                <i class="la la-check-square-o"></i>
<?php echo $this->lang->line('submit'); ?>
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>

<script src="//code.jquery.com/jquery-3.1.0.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
<script>
                                                                        $(document).ready(function () {
                                                                            $(function () {
                                                                                $('.alert').delay(3000).show().fadeOut('slow');
                                                                            });
                                                                            //show quiz price
                                                                            var examType = "<?= $quiz['exam_type'] ?>";
                                                                            if (examType == 2) {
                                                                                $("#quiz_price").show();
                                                                            }
                                                                            $("#show_media a").click(function () {
                                                                                $("#quiz_media").toggle();
                                                                            });

                                                                            $('#edit_quiz').validate({
                                                                                rules: {

                                                                                },
                                                                                submitHandler: function (form) { // for demo
                                                                                    $('button[type=submit]').attr('disabled', 'disabled');
                                                                                    return true; // for demo
                                                                                }
                                                                            });


                                                                        });
</script>
<script>

    function showPrice(e) {
        var examType = $(e).val();
        if (examType == 2) {
            $("#quiz_price").show();
        } else {
            $("#quiz_price").hide();
        }
    }
</script>