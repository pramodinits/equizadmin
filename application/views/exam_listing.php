<style>
    .listing-card {
        min-height: 200px;
    }
    hr {
        /*border: 1px solid #05038B;*/
        margin-bottom: 10%;
    }
    .btn-default {
        padding: .30rem .55rem !important;
    }
</style>

<!--updated template-->
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-wrapper-before"></div>
        <div class="content-header row">
            <div class="content-header-left col-md-4 col-12 mb-2">
                <h3 class="content-header-title"></h3>
            </div>
        </div>
        <div class="content-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collpase collapse show">
                            <div class="card-body">
                                <?php
                                if ($this->session->flashdata('message')) {
                                    echo $this->session->flashdata('message');
                                }
                                ?>
                                <div class="row">
                                    <?php
                                    if (count($exam_list) != 0) {
                                        foreach ($exam_list as $k => $v) {
                                            $student_name = $v['first_name'] . " " . $v['last_name'];
                                            $start_time = strtotime($v['start_date'] . " " . $v['start_time']);
                                            $end_time = strtotime($v['end_date'] . " " . $v['end_time']);
                                            //echo $end_time - $start_time; 
                                            $exam_status = $start_time - time();
                                            if ($exam_status > 0) {
                                                $border = "bg-gradient-x-primary"; //in future
                                            } elseif (($start_time <= time()) && ($end_time >= time())) {
                                                $border = "bg-gradient-x-success"; //ongoing exam
                                            } elseif ($exam_status < 0) {
                                                $border = "bg-gradient-x-danger"; //past exam
                                            }
                                            ?>
                                            <div class="col-xl-4 col-md-6 col-sm-12">
                                                <div class="card listing-card text-white box-shadow-0 <?= $border ?>">
                                                    <div class="card-content collapse show">
                                                        <div class="card-body">
                                                            <h4 class="card-title text-white"><?= $v['quiz_name']; ?></h4>
                                                            <p class="card-text font-weight-bold"><?= $student_name; ?> | 60 min
                                                                <?= $v['video_conference'] == 1 ? "| <i class='la la-film'></i>" : "" ?>
                                                            </p>
                                                            <hr/>
                                                            <?php if ($v['video_conference_flag'] > 1) {
                                                                ?>
                                                                <div class="row" style="margin: 0 auto;">
                                                                    <?php if ($logged_in['uid'] == $v['examiner_id']) { ?>
                                                                        <button id="startExam" class="btn btn-dark text-center text-white mr-1" type="submit">Start</button>
                                                                    <?php } ?>
                                                                    <?php if ($logged_in['uid'] == $v['scorer_id']) { ?>
                                                                        <button class="btn btn-dark text-center" data-resultid="<?= $v['rid'] ?>"
                                                                                data-vocabulary_flag="<?= $v['vocabulary_flag'] ?>"
                                                                                data-structure_flag="<?= $v['structure_flag'] ?>"
                                                                                data-fluency_flag="<?= $v['fluency_flag'] ?>"
                                                                                data-pronunciation_flag="<?= $v['pronunciation_flag'] ?>"
                                                                                data-comprehension_flag="<?= $v['comprehension_flag'] ?>"
                                                                                data-quizname="<?= $v['quiz_name'] ?>" data-studentname="<?= $student_name ?>" data-quizid="<?= $v['quid'] ?>" data-studentid="<?= $v['uid'] ?>"
                                                                                data-toggle="modal" data-target="#markExam" onclick="markExam(this)" type="submit">Mark</button>
                                                                            <?php } ?>
                                                                </div>
                                                            <?php } else { ?>

                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php
                                        }
                                    } else {
                                        echo "No exam found.";
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--updated template-->

<div class="modal fade" id="markExam" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title pull-left"></h4>
                <button type="button" class="close pull-right" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <label>
                            <strong>Student Name</strong>
                        </label> : <span id="studentname"></span>
                    </div>
                </div>
                <form method="post" name="mark_exam" id="mark_exam" action="<?php echo site_url('examiner/update_mark/'); ?>">
                    <input type="hidden" id="quiz_id" name="quiz_id" value=""/>
                    <input type="hidden" id="student_id" name="student_id" value=""/>
                    <input type="hidden" id="result_id" name="result_id" value=""/>
                    <div class="form-group" id="vocabulary_score_div">
                        <label>Vocabulary </label>
                        <input class="form-control" required type="number" max="10" id="vocabulary_score" name="vocabulary_score" onkeyup="summark()" />
                    </div>
                    <div class="form-group" id="structure_score_div">
                        <label>Structure </label>
                        <input class="form-control" required type="number" max="10" id="structure_score" name="structure_score" onkeyup="summark()" />
                    </div>
                    <div class="form-group" id="fluency_score_div">
                        <label>Fluency </label>
                        <input class="form-control" required type="number" max="10" id="fluency_score" name="fluency_score" onkeyup="summark()" />
                    </div>
                    <div class="form-group" id="pronunciation_score_div">
                        <label>Pronunciation </label>
                        <input class="form-control" required type="number" max="10" id="pronunciation_score" name="pronunciation_score" onkeyup="summark()" />
                    </div>
                    <div class="form-group" id="comprehension_score_div">
                        <label>Comprehension </label>
                        <input class="form-control" required type="number" max="10" id="comprehension_score" name="comprehension_score" onkeyup="summark()" />
                    </div>
                    <div class="form-group">
                        <label>Mark Obtained</label>
                        <input class="form-control" readonly required type="number" id="score" max="10" name="score" />
                    </div>
                    <div class="form-actions">
                        <input type="submit" class="btn btn-success" value="Save" id="submit">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>    
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/additional-methods.js"></script>
<script>
                            $(document).ready(function () {
//                                    $('#mark_exam').validate({
//                                        rules: {
//                                            'score': {
//                                                required: true,
//                                                digits: true
//                                            }
//                                        },
//                                        submitHandler: function (form) { // for demo
//                                            $('button[type=submit]').attr('disabled', 'disabled');
//                                            return true; // for demo
//                                        }
//                                    });

                                $("button#startExam").click(function () {
                                    window.open('https://www.thoughtspheres.com/testvideo/index.html', "test", "_blank");
                                });
                            });
                            $(function () {
                                $('.alert').delay(3000).show().fadeOut('slow');
                            });
                            function markExam(e) {
                                var resultid = $(e).attr('data-resultid');
                                var studentid = $(e).attr('data-studentid');
                                var quizid = $(e).attr('data-quizid');
                                var quizname = $(e).attr('data-quizname');
                                var studentname = $(e).attr('data-studentname');
                                var vocabulary_flag = $(e).attr('data-vocabulary_flag');
                                var structure_flag = $(e).attr('data-structure_flag');
                                var fluency_flag = $(e).attr('data-fluency_flag');
                                var pronunciation_flag = $(e).attr('data-pronunciation_flag');
                                var comprehension_flag = $(e).attr('data-comprehension_flag');

                                if (vocabulary_flag) {
                                    $('#vocabulary_score_div').show();
                                } else {
                                    $('#vocabulary_score_div').hide();
                                }

                                if (structure_flag) {
                                    $('#structure_score_div').show();
                                } else {
                                    $('#structure_score_div').hide();
                                }

                                if (fluency_flag) {
                                    $('#fluency_score_div').show();
                                } else {
                                    $('#fluency_score_div').hide();
                                }
                                if (pronunciation_flag) {
                                    $('#pronunciation_score_div').show();
                                } else {
                                    $('#pronunciation_score_div').hide();
                                }
                                if (comprehension_flag) {
                                    $('#comprehension_score_div').show();
                                } else {
                                    $('#comprehension_score_div').hide();
                                }

                                $("#student_id").val(studentid);
                                $("#quiz_id").val(quizid);
                                $("#result_id").val(resultid);
                                $("h4.modal-title").text(quizname);
                                $("#studentname").text(studentname);
                            }

                            function summark() {
                                var flg = 0;
                                
                                var vocabulary_score = 0;
                                if ($("#vocabulary_score_div").is(":visible")) {
                                    flg = flg+1;
                                    var vocabulary_score = parseInt($('#vocabulary_score').val(), 10);

                                }
                               
                                var structure_score = 0;
                                if ($("#structure_score_div").is(":visible")) {
                                    flg = flg+1;
                                    var structurescore = $('#structure_score').val() ? $('#structure_score').val():0 ;
                                    var structure_score = parseInt(structurescore, 10);

                                }
                                 console.log(structure_score);
                                var fluency_score = 0;
                                if ($("#fluency_score_div").is(":visible")) {
                                    flg = flg+1;
                                    var fluencyscore = $('#fluency_score').val() ? $('#fluency_score').val() :0
                                    var fluency_score = parseInt(fluencyscore, 10);

                                }
                                var pronunciation_score = 0;
                                if ($("#pronunciation_score_div").is(":visible")) {
                                    flg = flg+1;
                                   var pronunciationscore = $('#pronunciation_score').val() ? $('#pronunciation_score').val() : 0;
                                    var pronunciation_score = parseInt(pronunciationscore, 10);

                                }
                                
                                var comprehension_score = 0;
                                if ($("#comprehension_score_div").is(":visible")) {
                                    flg = flg+1;
                                    var comprehensionscore = $('#comprehension_score').val() ? $('#comprehension_score').val() :0;
                                    var comprehension_score = parseInt(comprehensionscore, 10);

                                }
                                
                                var total = vocabulary_score + structure_score + fluency_score + pronunciation_score + comprehension_score; 
                                $('#score').val(total/flg);
                            }
</script>