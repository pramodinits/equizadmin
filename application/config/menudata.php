<?php

$config['admin_menu'] = array(
    "applicant" => array(
        "mainlable" => "Applicant",
        "mainfont" => "la la-users",
        "class" => array("user"),
        "method" => array("new_user", "index"),
        "href" => "#",
        "submenu" => array(
            "list"=>array(
                "label" => 'Applicants List',
                "href" => 'user',
                'method' => 'index',
                'class' => 'user',
            )
        )
    ),
    "examiner rater list" => array(
        "mainlable" => "Examiner / Rater",
        "mainfont" => "la la-user",
        "class" => array("examiner"),
        "method" => array("new_examiner", "index"),
        "href" => "#",
        "submenu" => array(
           "add_edit" => array(
                "label" => 'Add New',
                "href" => 'examiner/new_examiner/',
                'method' => 'new_examiner',
                'class' => 'examiner',
            ),
            "list" => array(
                "label" => 'Examiner / Rater List',
                "href" => 'examiner',
                'method' => 'index',
                'class' => 'examiner',
            )
        )
    ),
    "training center" => array(
        "mainlable" => "Testing Center",
        "mainfont" => "la la-user",
        "class" => array("agency"),
        "method" => array("new_agency", "index"),
        "href" => "#",
        "submenu" => array(
           "add_edit" => array(
                "label" => 'Add New',
                "href" => 'agency/new_agency/',
                'method' => 'new_agency',
                'class' => 'agency',
            ),
            "list" => array(
                "label" => 'Testing Center List',
                "href" => 'agency',
                'method' => 'index',
                'class' => 'agency',
            )
        )
    ),
    "question bank" => array(
        "mainlable" => "Question Bank",
        "mainfont" => "la la-question",
        "class" => array("qbank"),
        "method" => array("pre_new_question", "index"),
        "href" => "#",
        "submenu" => array(
           "add_edit" => array(
                "label" => 'Add New',
                "href" => 'qbank/pre_new_question/',
                'method' => 'pre_new_question',
                'class' => 'qbank',
            ),
            "list" => array(
                "label" => 'Question List',
                "href" => 'qbank',
                'method' => 'index',
                'class' => 'qbank',
            )
        )
    ),
    "exam" => array(
        "mainlable" => "Exam",
        "mainfont" => "la la-laptop",
        "class" => array("quiz"),
        "method" => array("add_new", "index"),
        "href" => "#",
        "submenu" => array(
           "add_edit" => array(
                "label" => 'Add New',
                "href" => 'quiz/add_new/',
                'method' => 'add_new',
                'class' => 'quiz',
            ),
            "list" => array(
                "label" => 'Exam List',
                "href" => 'quiz',
                'method' => 'index',
                'class' => 'quiz',
            )
        )
    ),
    "transaction listing" => array(
        "mainlable" => "Transaction Listing",
        "mainfont" => "la la-usd",
        "class" => array("agency"),
        "method" => array("transaction_listing"),
        "href" => "agency/transaction_listing",
    ),
    "mock exam result list" => array(
        "mainlable" => "Mock Exam Result List",
        "mainfont" => "la la-registered",
        "class" => array("result"),
        "method" => array("mockexam_results"),
        "href" => "result/mockexam_results",
    ),
    "result list" => array(
        "mainlable" => "Result List",
        "mainfont" => "la la-registered",
        "class" => array("result"),
        "method" => array("index"),
        "href" => "result",
    ),
    "applicant exam registration" => array(
        "mainlable" => "Applicant Exam Registration",
        "mainfont" => "la la-clock-o",
        "class" => array("student_subscriptionexam"),
        "method" => array("index"),
        "href" => "student_subscriptionexam",
    )
);

$config['admin_menu_2'] = array(
    "applicant" => array(
        "list" => 'Applicants List',
//        "add_edit"=>'ADD / EDIT',
        "delete" => 'DELETE',
        "approved" => "Approve"
    ),
    "examiner rater list" => array(
        "list" => 'Examiner / Rater List',
        "add_edit" => 'Add New',
        "delete" => 'DELETE',
//        "approved" => "Approve"
    ),
    "training center" => array(
        "list" => 'Testing Center List',
        "add_edit" => 'Add New',
        "add_wallet" => "Add Wallet",
        "delete" => 'DELETE',
        "approved" => "Approve"
    ),
    "location city" => array(
        "list" => 'City List',
        "add_edit" => 'ADD / EDIT',
        "delete" => 'DELETE'
    ),
    "location" => array(
        "list" => 'Location List',
        "add_edit" => 'ADD / EDIT',
        "delete" => 'DELETE'
    ),
    "question bank" => array(
        "list" => 'Question List',
        "add_edit" => 'Add New',
        "delete" => 'DELETE'
    ),
    "exam" => array(
        "list" => 'Exam List',
        "add_edit" => 'Add New',
        "action_status" => 'Status'
    ),
    "transaction listing" => array(
        "list" => 'Transaction Listing'
    ),
    "mock exam result list" => array(
        "list" => 'Mock Exam Result List',
        "view_result" => 'View Result',
        "mark" => "Mark"
    ),
    "result list" => array(
        "list" => 'Result List',
        "view_result" => 'View Result',
        "delete_result" => "Delete Result"
    ),
    "student exam registration" => array(
        "list" => 'Applicant Exam Registration',
        "assign_user" => 'Assign User (Examiner & Rater)',
    )
);
