<?php

Class Schedule_model extends CI_Model {

    function verify_users($cond) {
        $this->db->where($cond);
        $query = $this->db->get('kams_users');
        return $query->num_rows();
    }

    function remove_schedule($schedule_id) {
        $this->db->where('schedule_id', $schedule_id);
        if ($this->db->delete('kams_schedule_quiz')) {
            return true;
        } else {
            return false;
        }
    }

    function update_user($uid) {
        $logged_in = $this->session->userdata('logged_in');


        $userdata = array(
            'first_name' => $this->input->post('first_name'),
            'last_name' => $this->input->post('last_name'),
            'skype_id' => $this->input->post('skype_id'),
            'contact_no' => $this->input->post('contact_no')
        );
        if ($logged_in['su'] == '1') {
            $userdata['email'] = $this->input->post('email');
            $userdata['gid'] = $this->input->post('gid');
            if ($this->input->post('subscription_expired') != '0') {
                $userdata['subscription_expired'] = strtotime($this->input->post('subscription_expired'));
            } else {
                $userdata['subscription_expired'] = '0';
            }
            $userdata['su'] = $this->input->post('su');
        }

        if ($this->input->post('password') != "") {
            $userdata['password'] = md5($this->input->post('password'));
        }
        if ($this->input->post('user_status')) {
            $userdata['user_status'] = $this->input->post('user_status');
        }
        $this->db->where('uid', $uid);
        if ($this->db->update('kams_users', $userdata)) {

            $this->db->where('uid', $uid);
            $this->db->delete('kams_users_custom');
            foreach ($_POST['custom'] as $ck => $cv) {
                if ($cv != '') {
                    $kams_users_custom = array(
                        'field_id' => $ck,
                        'uid' => $uid,
                        'field_values' => $cv
                    );
                    $this->db->insert('kams_users_custom', $kams_users_custom);
                }
            }
            return true;
        } else {

            return false;
        }
    }

}

?>
