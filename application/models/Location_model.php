<?php

Class Location_model extends CI_Model {

    function city_list($limit, $start, $cnt_flg = "") {
        $logged_in = $this->session->userdata('logged_in');
        $this->db->limit($limit, $start);
        $this->db->order_by('kams_city.id_city', 'desc');
        $query = $this->db->get('kams_city');
        if ($cnt_flg) {
            return $query->num_rows();
        } else {
            return $query->result_array();
        }
    }
    
    function location_list($limit, $start, $cnt_flg = "") {
        $logged_in = $this->session->userdata('logged_in');
        $this->db->limit($limit, $start);
        $this->db->order_by('kams_location.id_location', 'desc');
        $query = $this->db->get('kams_location');
        if ($cnt_flg) {
            return $query->num_rows();
        } else {
            return $query->result_array();
        }
    }
    
    function insert_city() {
        $logged_in = $this->session->userdata('logged_in');
        $city_id = $this->input->post('city_id');
        $ins_data = $this->input->post('city');
        $ins_data['added_by'] = $logged_in['uid'];
        if ($city_id) {
            $this->db->where('id_city', $city_id);
            $this->db->update('kams_city', $ins_data);
            return true;
        } else {
            if ($this->db->insert('kams_city', $ins_data)) {
                $city_id = $this->db->insert_id();
                return true;
            } else {
                return false;
            }
        }
    }

    function get_city($tbl, $fieldName, $fieldVal) {
        $this->db->where($fieldName, $fieldVal);
        $query = $this->db->get($tbl);
        return $query->row_array();
    }

    function remove_examiner($uid) {
        $this->db->where('uid', $uid);
        if ($this->db->delete('kams_users')) {
            return true;
        } else {

            return false;
        }
    }

}

?>
