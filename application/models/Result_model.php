<?php

Class Result_model extends CI_Model {

    function result_list($limit, $start, $cnt_flg = "", $status = '0') {
        $result_open = $this->lang->line('open');
        $logged_in = $this->session->userdata('logged_in');
        $uid = $logged_in['uid'];

        if ($this->input->post('search')) {
            $search = $this->input->post('search');
            $this->db->or_where('kams_users.email', $search);
            $this->db->or_where('kams_users.first_name', $search);
            $this->db->or_where('kams_users.last_name', $search);
            $this->db->or_where('kams_users.contact_no', $search);
            $this->db->or_where('kams_result.rid', $search);
            $this->db->or_where('kams_quiz.quiz_name', $search);
        } else {
            $this->db->where('kams_result.result_status !=', $result_open);
        }

//        if (!in_array('List_all', explode(',', $logged_in['results'])) && $logged_in['su'] == 1) {
//            $this->db->where('kams_result.uid', $uid);
//        }
        
        if($logged_in['su']) {
            $logged_in = $this->session->userdata('logged_in');

//            if ($logged_in['uid'] != '1' && $logged_in['su'] != '4') {
//                $uid = $logged_in['uid'];
//                $this->db->where('q.inserted_by', $uid);
//            }
//            if ($logged_in['su'] = '4') {
//                $this->db->where('q.inserted_by', $uid);
//            }
        }
        if ($status != '0') {
            $this->db->where('kams_result.result_status', $status);
        }
        $this->db->group_by('kams_result.rid');
        $this->db->limit($limit, $start);
        $this->db->order_by('rid', 'desc');
        $this->db->join('kams_users u', 'u.uid=kams_result.uid');
        $this->db->join('kams_quiz q', 'q.quid=kams_result.quid');
        if ($logged_in['su'] == 4) {
            $this->db->join('kams_students_exam_subscription s', 's.quiz_id=kams_result.quid and s.examiner_id = ' . $uid . '');
        }
        if ($logged_in['su'] == 3) {
            $this->db->join('kams_students_exam_subscription s', 's.quiz_id=kams_result.quid and s.agency_id = ' . $uid . '');
        }
        if ($logged_in['su'] == 5) {
            $this->db->join('kams_students_exam_subscription s', 's.quiz_id=kams_result.quid and s.scorer_id = ' . $uid . '');
        }
        if ($logged_in['su'] == 6) {
            $this->db->join('kams_students_exam_subscription s', 's.quiz_id=kams_result.quid and (s.scorer_id = ' . $uid . ' OR s.examiner_id = ' . $uid . ' )');
        }
        $query = $this->db->get('kams_result');
//        echo $this->db->last_query();exit();
        if ($cnt_flg) {
            return $query->num_rows();
        } else {
            return $query->result_array();
        }
    }

    function mockexam_result_list($limit, $start, $cnt_flg = "", $status = '0') {
        $result_open = $this->lang->line('open');
        $logged_in = $this->session->userdata('logged_in');
        $uid = $logged_in['uid'];

    //        if (!in_array('List_all', explode(',', $logged_in['results'])) && $logged_in['su'] == 1) {
    //            $this->db->where('kams_result.uid', $uid);
    //        } 
        if($logged_in['su']) {
            $logged_in = $this->session->userdata('logged_in');

//            if ($logged_in['uid'] != '1' && $logged_in['su'] != '4') {
//                $uid = $logged_in['uid'];
//                $this->db->where('q.inserted_by', $uid);
//            }
//            if ($logged_in['su'] = '4') {
//                $this->db->where('q.inserted_by', $uid);
//            }
        }
//        echo $limit . "+++" . $start;
        if ($status != '0') {
            $this->db->where('kams_result.result_status', $status);
        }
        $this->db->group_by('kams_result.rid');
        $this->db->where('q.exam_type', 1);
        $this->db->order_by('rid', 'desc');
        $this->db->limit($limit, $start);
        $this->db->join('kams_users u', 'u.uid=kams_result.uid');
        $this->db->join('kams_quiz q', 'q.quid=kams_result.quid');
        
        $query = $this->db->get('kams_result');
//        echo $this->db->last_query();exit();
        if ($cnt_flg) {
            return $query->num_rows();
        } else {
            return $query->result_array();
        }
    }

    function quiz_list($cond = "") {
        if ($cond) {
            $this->db->where($cond);
        }
        $this->db->order_by('quid', 'desc');
        $query = $this->db->get('kams_quiz');
        return $query->result_array();
    }

    function no_attempt($quid, $uid) {

        $query = $this->db->query(" select * from kams_result where uid='$uid' and quid='$quid' ");
        return $query->num_rows();
    }

    function remove_result($rid) {

        $this->db->where('kams_result.rid', $rid);
        if ($this->db->delete('kams_result')) {
            $this->db->where('rid', $rid);
            $this->db->delete('kams_answers');
            return true;
        } else {

            return false;
        }
    }

    function generate_report($quid, $gid) {
        $logged_in = $this->session->userdata('logged_in');
        $uid = $logged_in['uid'];
        $date1 = $this->input->post('date1');
        $date2 = $this->input->post('date2');

        if ($quid != '0') {
            $this->db->where('kams_result.quid', $quid);
        }
        if ($gid != '0') {
            $this->db->where('kams_users.gid', $gid);
        }
        if ($date1 != '') {
            $this->db->where('kams_result.start_time >=', strtotime($date1));
        }
        if ($date2 != '') {
            $this->db->where('kams_result.start_time <=', strtotime($date2));
        }

        $this->db->order_by('rid', 'desc');
        $this->db->join('kams_users', 'kams_users.uid=kams_result.uid');
        $this->db->join('kams_group', 'kams_group.gid=kams_users.gid');
        $this->db->join('kams_quiz', 'kams_quiz.quid=kams_result.quid');
        $query = $this->db->get('kams_result');
        return $query->result_array();
    }

    function get_result($rid) {
        $logged_in = $this->session->userdata('logged_in');
        $uid = $logged_in['uid'];
        if ($logged_in['su'] == '0') {
            $this->db->where('kams_result.uid', $uid);
        }
        $this->db->where('kams_result.rid', $rid);
        $this->db->join('kams_users', 'kams_users.uid=kams_result.uid');
        $this->db->join('kams_group', 'kams_group.gid=kams_users.gid');
        $this->db->join('kams_quiz', 'kams_quiz.quid=kams_result.quid');
        $query = $this->db->get('kams_result');
        return $query->row_array();
    }

    function last_ten_result($quid) {
        $this->db->order_by('percentage_obtained', 'desc');
        $this->db->limit(10);
        $this->db->where('kams_result.quid', $quid);
        $this->db->join('kams_users', 'kams_users.uid=kams_result.uid');
        $this->db->join('kams_quiz', 'kams_quiz.quid=kams_result.quid');
        $query = $this->db->get('kams_result');
        return $query->result_array();
    }

    function get_percentile($quid, $uid, $score) {
        $logged_in = $this->session->userdata('logged_in');
        $gid = $logged_in['gid'];
        $res = array();
        $this->db->select("kams_result.uid");
        $this->db->where("kams_result.quid", $quid);
        $this->db->group_by("kams_result.uid");
        // $this->db->order_by("kams_result.score_obtained",'DESC');
        $query = $this->db->get('kams_result');
        $res[0] = $query->num_rows();

        $this->db->select("kams_result.uid");

        $this->db->where("kams_result.quid", $quid);
        $this->db->where("kams_result.uid !=", $uid);
        $this->db->where("kams_result.score_obtained <=", $score);
        $this->db->group_by("kams_result.uid");
        // $this->db->order_by("kams_result.score_obtained",'DESC');
        $querys = $this->db->get('kams_result');
        $res[1] = $querys->num_rows();

        return $res;
    }

}

?>
