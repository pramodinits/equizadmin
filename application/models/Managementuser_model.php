<?php

Class Managementuser_model extends CI_Model {

    function verify_users($cond) {
        $this->db->where($cond);
        $query = $this->db->get('kams_users');
        return $query->num_rows();
    }

    function management_list($limit, $start, $cnt_flg = "", $searchdata = "") {
        $logged_in = $this->session->userdata('logged_in');
        /* if ($logged_in['uid'] != '1') {
          $uid = $logged_in['uid'];
          $this->db->where('kams_users.inserted_by', $uid);
          } */
//        print "<pre>";print_r($searchdata);
        $this->db->where('kams_users.inserted_by', $logged_in['uid']);
        if ($logged_in['su'] == 1) {
            $this->db->where('kams_users.su', 7);
        } elseif ($logged_in['su'] == 3) {
            $this->db->where('kams_users.su', 8);
        }
        //searching user
        if (!empty($searchdata['username'])) {
            $wherecond = "(kams_users.first_name LIKE '%" . $searchdata['username'] . "%' OR kams_users.last_name LIKE '%" . $searchdata['username'] . "%')";
            $this->db->where($wherecond);
//            $this->db->where('kams_users.first_name LIKE', '%'. $searchdata['username'].'%');
//            $this->db->or_where('kams_users.last_name LIKE', '%'. $searchdata['username'].'%');
        }
        if (!empty($searchdata['phone'])) {
            $this->db->where('kams_users.contact_no LIKE', '%' . $searchdata['phone'] . '%');
        }
        if (!empty($searchdata['email'])) {
            $this->db->where('kams_users.email LIKE', '%' . trim($searchdata['email']) . '%');
        }

        $this->db->limit($limit, $start);
        $this->db->order_by('kams_users.uid', 'desc');

//        $this->db->join('kams_group', 'kams_users.gid=kams_group.gid');
//        $this->db->join('account_type', 'kams_users.su=account_type.account_id');
        $query = $this->db->get('kams_users');
//        echo $this->db->last_query();exit();
        if ($cnt_flg) {
            return $query->num_rows();
        } else {
            return $query->result_array();
        }
//        print_r($query->result_array());exit;
    }

    function user_list_all() {
        $logged_in = $this->session->userdata('logged_in');
        if ($logged_in['uid'] != '1') {
            $uid = $logged_in['uid'];
            $this->db->where('kams_users.inserted_by', $uid);
        }

        $this->db->join('kams_group', 'kams_users.gid=kams_group.gid');
        $this->db->join('account_type', 'kams_users.su=account_type.account_id');
        $query = $this->db->get('kams_users');
        return $query->result_array();
    }

    function record_exist($fld_name, $fld_val, $tbl, $userId = '', $cond = '') {
        $data = array();
        $this->db->select('*');
        $this->db->from($tbl);
        $this->db->where("$fld_name = ", $fld_val);
        if (!empty($userId)) {
            $this->db->where($cond, $userId);
        }

        $this->db->limit(1);
        $query = $this->db->get();
        $row_cnt = $query->num_rows();
        return $row_cnt;
    }

    function insert_management_user() {
        $logged_in = $this->session->userdata('logged_in');
        $uid = $this->input->post('uid');
        $userdata = $this->input->post('management_user');
        $userdata['email'] = $this->input->post('email');
        if ($this->input->post('password')) {
            $userdata['password'] = md5($this->input->post('password'));
        }
        $userdata['inserted_by'] = $logged_in['uid'];
//        $userdata['su'] = 4;
//        if ($logged_in['uid'] != '1') {
//            $userdata['inserted_by'] = $logged_in['uid'];
//        }
        if ($uid) {
            $this->db->where('uid', $uid);
            $this->db->update('kams_users', $userdata);
            return true;
        } else {
            if ($this->db->insert('kams_users', $userdata)) {
                $uid = $this->db->insert_id();
                /* if ($logged_in['uid'] == '1') {
                  $su = $userdata['su'];
                  $seq = $this->db->query("select * from account_type where account_id='$su' ");
                  $seqr = $seq->row_array();
                  $acp = explode(',', $seqr['users']);
                  if (in_array('List_all', $acp)) {
                  $this->db->query(" update kams_users set inserted_by=uid where uid='$uid' ");
                  }
                  } */
                return true;
            } else {
                return false;
            }
        }
    }

    function get_examiner($uid) {
//        $data = array();
//        $this->db->select('*');
//        $this->db->from("kams_users");
//        $this->db->where("uid", $uid);
//        
//        $this->db->limit(1);
//        $query = $this->db->get();
//        return $query->row_array();

        $this->db->where('uid', $uid);
        $query = $this->db->get('kams_users');
        return $query->row_array();
    }

    function remove_examiner($uid) {
        $this->db->where('uid', $uid);
        if ($this->db->delete('kams_users')) {
            return true;
        } else {

            return false;
        }
    }

    function remove_students($student_id, $examiner_id) {
        $this->db->where('student_id', $student_id);
        $this->db->where('examiner_id', $examiner_id);
        if ($this->db->delete('kams_assign_students')) {
            return true;
        } else {

            return false;
        }
    }

    function dataListingKeyvalue($tbl, $fldkey = '', $fldvalue = '', $cond = "", $tblfldvalue = "", $ascdesc = "", $is_array = FALSE) {
        $data = array();
        if (!empty($cond)) {
            $condition = $cond;
        } else {
            $condition = "1";
        }
        if ($tblfldvalue) {
            $query = $this->db->query("SELECT $tblfldvalue FROM $tbl WHERE $condition $ascdesc");
        } else {
            $query = $this->db->query("SELECT * FROM $tbl WHERE $condition $ascdesc");
        }
        if ($fldkey) {
            $i = 0;
            foreach ($query->result_array() as $row) {
                if ($is_array) {
                    $data[$row[$fldkey]][$i] = $row;
                } else {
                    if ($fldvalue) {
                        $data[$row[$fldkey]] = $row[$fldvalue];
                    } else {
                        $data[$row[$fldkey]] = $row;
                    }
                }
                $i++;
            }
            return $data;
        } else {
            return $query->result_array();
        }
    }

}

?>
