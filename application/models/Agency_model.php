<?php

Class Agency_model extends CI_Model {

    function verify_users($cond) {
        $this->db->where($cond);
        $query = $this->db->get('kams_users');
        return $query->num_rows();
    }

    function agency_list($limit, $start, $cnt_flg = "", $searchdata = "") {
        $logged_in = $this->session->userdata('logged_in');
        /*if ($logged_in['uid'] != '1') {
            $uid = $logged_in['uid'];
            $this->db->where('kams_users.inserted_by', $uid);
        }*/
        if ($logged_in['su'] != 1 && $logged_in['su'] != 7) {
            $uid = $logged_in['uid'];
            $this->db->where('kams_users.inserted_by', $uid);
        }
        //searching user
        if (!empty($searchdata['username'])) {
            $wherecond = "(kams_users.first_name LIKE '%" . $searchdata['username'] . "%' OR kams_users.last_name LIKE '%" . $searchdata['username'] . "%')";
            $this->db->where($wherecond);
        }
        if (!empty($searchdata['phone'])) {
            $this->db->where('kams_users.contact_no LIKE', '%' . $searchdata['phone'] . '%');
        }
        if (!empty($searchdata['email'])) {
            $this->db->where('kams_users.email LIKE', '%' . trim($searchdata['email']) . '%');
        }

        $this->db->limit($limit, $start);
        $this->db->where('kams_users.su', '3');
        $this->db->order_by('kams_users.uid', 'desc');

//        $this->db->join('kams_group', 'kams_users.gid=kams_group.gid');
//        $this->db->join('account_type', 'kams_users.su=account_type.account_id');
        $query = $this->db->get('kams_users');
        if ($cnt_flg) {
            return $query->num_rows();
        } else {
            return $query->result_array();
        }
    }

    function transaction_list($limit, $start, $cnt_flg = "") {
        $logged_in = $this->session->userdata('logged_in');
//        if ($logged_in['uid'] != '1') {
//            $uid = $logged_in['uid'];
//            $this->db->where('kams_agency_transaction.inserted_by', $uid);
//        }

        $this->db->limit($limit, $start);
        if ($logged_in['su'] == 3) {
            $this->db->where('kams_agency_transaction.agency_id', $logged_in['uid']);
        } elseif ($logged_in['su'] == 8) {
            $this->db->where('kams_agency_transaction.agency_id', $logged_in['permission_given_by']);
        }
        $this->db->order_by('kams_agency_transaction.id', 'desc');

        $query = $this->db->get('kams_agency_transaction');
//        echo $this->db->last_query();exit();
        if ($cnt_flg) {
            return $query->num_rows();
        } else {
            return $query->result_array();
        }
    }

    function insert_agency() {
        $logged_in = $this->session->userdata('logged_in');
        $uid = $this->input->post('uid');
        $userdata = $this->input->post('agency');
        $userdata['email'] = $this->input->post('email');
        $userdata['password'] = md5($this->input->post('password'));
        $userdata['su'] = 3;
        if ($logged_in['uid'] != '1') {
            $userdata['inserted_by'] = $logged_in['uid'];
        }
        if ($uid) {
            $this->db->where('uid', $uid);
            $this->db->update('kams_users', $userdata);
            return true;
        } else {
            if ($this->db->insert('kams_users', $userdata)) {
                $uid = $this->db->insert_id();

                //update unique id
                /*$get_query = $this->db->query("select max(uid) as last_agency_id from kams_users WHERE su=3");
                $user_last_entry = $get_query->row_array();
                $unique_id = "GACATC" . $user_last_entry['last_agency_id'] < 100 ? "00" : "" . $user_last_entry['last_agency_id'];
                $unique_upd['unique_id'] = $unique_id;*/

                $get_query = $this->db->query("select max(uid) as last_agency_id from kams_users WHERE su=3");
                $user_last_entry = $get_query->row_array();
                $add_digit = $user_last_entry['last_agency_id'] < 100 ? '00' . $user_last_entry['last_agency_id'] : $user_last_entry['last_agency_id'];
                $unique_id = "GACATC" . $add_digit . "";


                $this->db->where('uid', $uid);
                $this->db->update('kams_users', $unique_upd);
                //update unique id

                /*if ($logged_in['uid'] == '1') {
                    $su = $userdata['su'];
                    $seq = $this->db->query("select * from account_type where account_id='$su' ");
                    $seqr = $seq->row_array();
                    $acp = explode(',', $seqr['users']);
                    if (in_array('List_all', $acp)) {
                        $this->db->query(" update kams_users set inserted_by=uid where uid='$uid' ");
                    }
                }*/
                return true;
            } else {
                return false;
            }
        }
    }

    function get_agency($uid) {
        $this->db->where('uid', $uid);
        $query = $this->db->get('kams_users');
        return $query->row_array();
    }

    function remove_examiner($uid) {
        $this->db->where('uid', $uid);
        if ($this->db->delete('kams_users')) {
            return true;
        } else {

            return false;
        }
    }

    function remove_students($student_id, $examiner_id) {
        $this->db->where('student_id', $student_id);
        $this->db->where('examiner_id', $examiner_id);
        if ($this->db->delete('kams_assign_students')) {
            return true;
        } else {

            return false;
        }
    }

    function dataListingKeyvalue($tbl, $fldkey = '', $fldvalue = '', $cond = "", $ascdesc = "", $is_array = FALSE) {
        $data = array();
        if (!empty($cond)) {
            $condition = $cond;
        } else {
            $condition = "1";
        }
        $query = $this->db->query("SELECT * FROM $tbl WHERE $condition $ascdesc");
        if ($fldkey) {
            $i = 0;
            foreach ($query->result_array() as $row) {
                if ($is_array) {
                    $data[$row[$fldkey]][$i] = $row;
                } else {
                    if ($fldvalue) {
                        $data[$row[$fldkey]] = $row[$fldvalue];
                    } else {
                        $data[$row[$fldkey]] = $row;
                    }
                }
                $i++;
            }
            return $data;
        } else {
            return $query->result_array();
        }
    }

}

?>
