<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Student_subscriptionexam extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->helper('url');
        $this->load->model("user_model");
        $this->load->model("examiner_model");
        $this->load->model("account_model");
        $this->lang->load('basic', $this->config->item('language'));
        $this->config->load('masterdata');
// redirect if not loggedin
        if (!$this->session->userdata('logged_in')) {
            redirect('login');
        }
        $logged_in = $this->session->userdata('logged_in');
        if ($logged_in['base_url'] != base_url()) {
            $this->session->unset_userdata('logged_in');
            redirect('login');
        }
    }

    public function test() {
        $this->load->view('header', $data);
        $this->load->view('test');
        $this->load->view('footer', $data);
    }

    public function index($limit = '0') {
        $logged_in = $this->session->userdata('logged_in');
        //$setting_p = explode(',', $logged_in['quiz']);
        //if (in_array('List', $setting_p) || in_array('List_all', $setting_p)) {
            $data['limit'] = $limit;
            $data['title'] = $this->lang->line('student_subscription_exam');
// fetching user list
            $list_cond = "id_city = " . $logged_in['id_city'] . "";
            $data['examiner_list'] = $this->examiner_model->dataListingKeyvalue("kams_users", "uid", "", "su = 4 OR su = 6 AND $list_cond", "uid, CONCAT(first_name, ' ', last_name) AS examiner_name");
            $data['scorer_list'] = $this->examiner_model->dataListingKeyvalue("kams_users", "uid", "", "su = 5 OR su = 6 AND $list_cond", "uid, CONCAT(first_name, ' ', last_name) AS scorer_name");
//fetching subscribed students list
//according to usertype
            $cond = "";
            if ($logged_in['su'] == 3) {
                $cond = "s.agency_id =" . $logged_in['uid'] . "";
            }
            if ($logged_in['su'] == 8) {
                $cond = "s.agency_id =" . $logged_in['permission_given_by'] . "";
            }
            if ($logged_in['su'] == 4) {
                $cond = "s.examiner_id =" . $logged_in['uid'] . "";
            }
            if ($logged_in['su'] == 5) {
                $cond = "s.scorer_id =" . $logged_in['uid'] . "";
            }
            if ($logged_in['su'] == 6) {
                $cond = "s.scorer_id =" . $logged_in['uid'] . " OR s.examiner_id =" . $logged_in['uid'] . "";
            }
            
            $this->db->select('q.quiz_name, q.quiz_price, s.*, CONCAT(ust.first_name, " ", ust.last_name) AS student_name, CONCAT(ue.first_name, " ", ue.last_name) AS examiner_name, CONCAT(us.first_name, " ", us.last_name) AS scorer_name');
            $this->db->from('kams_students_exam_subscription s');
            $this->db->join('kams_quiz q', 'q.quid = s.quiz_id', 'left');
            $this->db->join('kams_users ue', 'ue.uid = s.examiner_id', 'left');
            $this->db->join('kams_users us', 'us.uid = s.scorer_id', 'left');
            $this->db->join('kams_users ust', 'ust.uid = s.student_id', 'left');
            if ($logged_in['su'] != 1 && $logged_in['su'] != 7) {
                $this->db->where($cond);
            }
            //get search result
            $user = array();
            $user['exam_id'] = $this->input->get('exam_id');
            $user['studentname'] = $this->input->get('studentname');
            $user['subscriptiondate'] = $this->input->get('subscriptiondate');
            if ($user['exam_id']) {
                $this->db->where("q.exam_category", $user['exam_id']);
            }
            if($user['studentname']) {
                $this->db->where('ust.first_name LIKE', '%'. $user['studentname'].'%');
                $this->db->or_where('ust.last_name LIKE', '%'. $user['studentname'].'%');
            }
            if($user['subscriptiondate']) {
                $this->db->where('s.start_date =', date('Y-m-d', strtotime($user['subscriptiondate'])));
            }
            //get search result
            
            $query = $this->db->get();
//            echo $this->db->last_query();exit();
            $subscription_list = $query->result_array();
            $events[] = array();
            $colorcode = array();
            $colorcode[0] = "#6610f2"; //info
            $colorcode[1] = "#5ed84f"; //green
            $colorcode[2] = "#fa626b"; //red
            foreach ($subscription_list as $k => $v) {
                $events[] = array(
                    "subscription_id" => $v['subscription_id'],
                    "title" => $v['quiz_name'],
                    "student_name" => $v['student_name'],
                    "student_id" => $v['student_id'],
                    "quiz_price" => $v['quiz_price'],
                    "scorer_name" => $v['scorer_name'],
                    "examiner_name" => $v['examiner_name'],
                    "usertype" => $logged_in['su'],
                    "availableBalance" => $logged_in['wallet_balance'],
                    "start" => $v['start_date'] . " " . $v['start_time'],
                    "end" => $v['end_date'] . " " . $v['end_time'],
                    "start_date" => $v['start_date'],
                    "start_time" => $v['start_time'],
                    "approved_status" => $v['approved_status'],
                    "eventTextColor" => $colorcode[$v['approved_status']],
                    "backgroundColor" => $colorcode[$v['approved_status']],
                    'allDay' => false
                );
            }
            $data['events'] = json_encode($events);
            $data['table_events'] = json_encode($events);
            $data['exam_category'] = $this->config->item('exam_category');
            $data['user'] = $user;
            $this->load->view('header', $data);
            $this->load->view('student_subscription', $data);
            $this->load->view('footer', $data);
//            print "<pre>"; print_r($data);exit;
        /*} else {
            exit($this->lang->line('permission_denied'));
        }*/
    }

    public function assign_user() {
        $logged_in = $this->session->userdata('logged_in');
        $user_p = explode(',', $logged_in['users']);
//        if (!in_array('Add', $user_p)) {
//            exit($this->lang->line('permission_denied'));
//        }
        $subscription_id = $this->input->post('subscription_id');
        $upd['examiner_id'] = $this->input->post('examiner_id');
        $upd['scorer_id'] = $this->input->post('scorer_id');
        $upd['assigned_date'] = date("Y-m-d");
        $upd['start_date'] = date("Y-m-d", strtotime($this->input->post('start_date')));
        $upd['end_date'] = date("Y-m-d", strtotime($this->input->post('start_date')));
        $upd['start_time'] = $this->input->post('start_time');
        $timestamp = strtotime($upd['start_time']) + 60 * 60;
        $upd['end_time'] = date('H:i', $timestamp);
        $upd['assigned_date'] = date("Y-m-d");
        $upd['approved_status'] = 1;

//insert Agency
        $ins['agency_id'] = $logged_in['uid'];
        $ins['student_id'] = $this->input->post('student_id');
        $ins['amount'] = $this->input->post('quiz_price');
        $ins['flag'] = 2;
        $ins['subscription_id'] = $subscription_id;
        $ins['added_by'] = $logged_in['uid'];
        $ins['add_date'] = date("Y-m-d h:i:s");
        $ins['description'] = "Approve subscription";

//update user
        $upd_user['wallet_balance'] = $logged_in['wallet_balance'] - $ins['amount'];

        $this->db->where('uid', $logged_in['uid']);
        $this->db->update('kams_users', $upd_user);

        if ($upd && $subscription_id) {
            $this->db->insert('kams_agency_transaction', $ins);
            $this->db->where('subscription_id', $subscription_id);
            if ($this->db->update('kams_students_exam_subscription', $upd)) {
                $this->session->set_flashdata('message', "<div class='alert alert-success'>Exam assigned successfully! </div>");
            } else {
                $this->session->set_flashdata('message', "<div class='alert alert-danger'>" . $this->lang->line('error_to_add_data') . " </div>");
            }
            redirect('student_subscriptionexam/');
        }
    }

    public function updateSchedule1() {
        $logged_in = $this->session->userdata('logged_in');
        $user_p = explode(',', $logged_in['users']);
        if (!in_array('Add', $user_p)) {
            exit($this->lang->line('permission_denied'));
        }
        if (empty($quiz_id)) {
            exit($this->lang->line('permission_denied'));
        }
        $data['title'] = $this->lang->line('add') . ' ' . $this->lang->line('schedule_time');
        $data['quiz_id'] = $quiz_id;


//get scheduled event 
        $this->db->where('quiz_id', $quiz_id);
        $query = $this->db->get('kams_schedule_quiz');
        $schedule_list = $query->result_array();
        $events[] = array();
        foreach ($schedule_list as $k => $v) {
            $events[] = array(
                "scheduleId" => $v['schedule_id'],
                "title" => $v['schedule_title'],
                "start" => $v['start_schedule'],
                "end" => $v['end_schedule'],
                'allDay' => false,
                'overlap' => false
            );
        }
        $res = array();
        foreach ($schedule_list as $k => $v) {
            $k = array();
            $k['title'] = $v['schedule_title'];
            $k['insertId'] = $v['schedule_id'];
// $k['start'] = "2020-01-23 3:00:00";
            $k['start'] = $v['start_schedule'];
            $k['end'] = $v['end_schedule'];
            $k['allDay'] = false;
// $k['end'] = "2020-01-23 4:00:00";
            array_push($res, $k);
        }
        $data['events'] = json_encode($events);

        $this->load->view('header', $data);
        $this->load->view('add_schedule', $data);
        $this->load->view('footer', $data);
    }

    public function updateSchedule() {
        $ins['examiner_id'] = $this->input->post('examiner_id');
        $ins['start_date'] = $this->input->post('start_date');
        $ins['end_date'] = $this->input->post('end_date');
        $ins['start_time'] = $this->input->post('start_time');
        $ins['end_time'] = $this->input->post('end_time');

        if ($ins) {
            $this->db->insert('kams_students_exam_subscription', $ins);
            redirect('student_subscriptionexam/');
        }
    }

    public function getEventInfo() {
        $logged_in = $this->session->userdata('logged_in');
        $subscriptionid = $this->input->post('subscriptionid');
        $this->db->select('q.quiz_name, q.quiz_price, s.*, CONCAT(ue.first_name, " ", ue.last_name) AS examiner_name,'
                . ' CONCAT(ust.first_name, " ", ust.last_name) AS student_name, CONCAT(us.first_name, " ", us.last_name) AS scorer_name');
        $this->db->from('kams_students_exam_subscription s');
        $this->db->join('kams_quiz q', 'q.quid = s.quiz_id', 'left');
        $this->db->join('kams_users ue', 'ue.uid = s.examiner_id', 'left');
        $this->db->join('kams_users us', 'us.uid = s.scorer_id', 'left');
        $this->db->join('kams_users ust', 'ust.uid = s.student_id', 'left');
        $this->db->where('s.subscription_id = ' . $subscriptionid);
        $query = $this->db->get();
        $subscription_list = $query->row_array();

        //event info array
//        $events = array();
        $events = array(
            "subscription_id" => $subscription_list['subscription_id'],
            "quiz_name" => $subscription_list['quiz_name'],
            "user_name" => $subscription_list['user_name'],
            "student_id" => $subscription_list['student_id'],
            "quiz_price" => $subscription_list['quiz_price'],
            "scorer_name" => $subscription_list['scorer_name'],
            "examiner_name" => $subscription_list['examiner_name'],
            "student_name" => $subscription_list['student_name'],
            "usertype" => $logged_in['su'],
            "availableBalance" => $logged_in['wallet_balance'],
            "start_datetime" => $subscription_list['start_date'] . " " . $subscription_list['start_time'],
            "end_datetime" => $subscription_list['end_date'] . " " . $subscription_list['end_time'],
            "start_date" => $subscription_list['start_date'],
            "start_time" => $subscription_list['start_time'],
            "approved_status" => $subscription_list['approved_status'],
            "eventTextColor" => $colorcode[$v['approved_status']],
            "backgroundColor" => $colorcode[$v['approved_status']],
            'allDay' => false
        );
        echo json_encode($events);
        exit();
        //event info array
    }

    public function delete_schedule() {
        $schedule_id = $this->input->post('schedule_id');
        if ($this->schedule_model->remove_schedule($schedule_id)) {
            $result = array();
            $result['hasError'] = false;
            $result['Message'] = "Success";
        } else {
            $result = array();
            $result['hasError'] = true;
            $result['Message'] = "Failure";
        }
        echo json_encode($result);
        exit();
    }

}
