<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Agency extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->helper('url');
        $this->load->model(array('examiner_model', 'account_model', 'agency_model'));
        $this->lang->load('basic', $this->config->item('language'));
        $this->load->library("pagination");
        $this->config->load('masterdata');
        // redirect if not loggedin
        if (!$this->session->userdata('logged_in')) {
            redirect('login');
        }
        $logged_in = $this->session->userdata('logged_in');
        if ($logged_in['base_url'] != base_url()) {
            $this->session->unset_userdata('logged_in');
            redirect('login');
        }
    }

    public function paginatelinks() {
        $config['full_tag_open'] = '<div class="dataTables_paginate paging_bs_normal pull-right" id="datatable-default_paginate"><ul class="pagination justify-content-end p-4">';
        $config['full_tag_close'] = '</ul></div><!--pagination-->';

        $config['first_link'] = '&laquo; First';
        $config['first_tag_open'] = '<li class="page-item">';
        $config['first_tag_close'] = '</li>';

        $config['last_link'] = 'Last &raquo;';
        $config['last_tag_open'] = '<li class="page-item">';
        $config['last_tag_close'] = '</li>';

        $config['next_link'] = '<span class="page-link">&GT;</span>';
        $config['next_tag_open'] = '<li class="page-item">';
        $config['next_tag_close'] = '</li>';

        $config['prev_link'] = '<span class="page-link">&LT;</span>';
        $config['prev_tag_open'] = '<li class="page-item">';
        $config['prev_tag_close'] = '</li>';

        $config['cur_tag_open'] = '<li class="active"><span class="page-link-active">';
        $config['cur_tag_close'] = '</span></li>';

        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        return $config;
    }

    public function index($limit = '0') {
        $logged_in = $this->session->userdata('logged_in');

        /*$user_p = explode(',', $logged_in['users']);
        if (!in_array('List_all', $user_p)) {
            exit($this->lang->line('permission_denied'));
        }*/


        $data['limit'] = $limit;
        $data['title'] = $this->lang->line('agencylist');
        // fetching user list
        //search user
        $search = array();

        $inputname = $this->uri->segment(3) ? $this->uri->segment(3) : "Name";
        $inputphone = $this->uri->segment(4) ? $this->uri->segment(4) : "Phone";
        $inputemail = $this->uri->segment(5) ? $this->uri->segment(5) : "Email";

        $inputname = urldecode($inputname);
        $inputphone = urldecode($inputphone);
        $inputemail = urldecode($inputemail);

        $name = $this->input->get('username') ? $this->input->get('username') : $inputname;
        $phone = $this->input->get('phone') ? $this->input->get('phone') : $inputphone;
        $email = $this->input->get('email') ? $this->input->get('email') : $inputemail;

        $user = array();
        if ($name != "Name")
            $user['username'] = $name;
        if ($phone != "Phone")
            $user['phone'] = $phone;
        if ($email != "Email")
            $user['email'] = $email;

//pagination links
        $config = array();
        $config = $this->paginatelinks();
        $config["base_url"] = site_url("agency/index/$name/$phone/$email");
        $cnt_flg = "1";
        $config["total_rows"] = $this->agency_model->agency_list($config["per_page"], $start, $cnt_flg = 1);
        $config["per_page"] = 10;
        $config["uri_segment"] = 6;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = round($choice);
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(6)) ? $this->uri->segment(6) : 0;
        $start = $page;
        $data["links"] = $this->pagination->create_links();
        //pagination links

        $data['result'] = $this->agency_model->agency_list($config["per_page"], $start, "", $user);
        $data['exam_category'] = $this->config->item('exam_category');
        $data['city_list'] = $this->examiner_model->dataListingKeyvalue("kams_city", "id_city", "city_name");
        $data['location_list'] = $this->examiner_model->dataListingKeyvalue("kams_location", "id_location", "location_name");
        $data['user'] = $user;
        $this->load->view('header', $data);
        $this->load->view('agency_list', $data);
        $this->load->view('footer', $data);
    }

    public function new_agency($uid = "") {
        $logged_in = $this->session->userdata('logged_in');
        /*$user_p = explode(',', $logged_in['users']);
        if (!in_array('Add', $user_p)) {
            exit($this->lang->line('permission_denied'));
        }*/
        if ($uid) {
            $data = array();
            $data['result'] = $this->agency_model->get_agency($uid);
            $id_city = $data['result']['id_city'];
            $data['location_list'] = $this->examiner_model->dataListingKeyvalue("kams_location", "id_location", "location_name", "id_city=$id_city ORDER BY id_location ASC");
        $data['title'] = $this->lang->line('edit') . ' ' . $this->lang->line('agency');
        } else {
            $data['title'] = $this->lang->line('add_new') . ' ' . $this->lang->line('agency');
        }

        // fetching category list
        $data['account_type'] = $this->account_model->account_list(0);
        $data['exam_category'] = $this->config->item('exam_category');
        $data['city_list'] = $this->examiner_model->dataListingKeyvalue("kams_city", "id_city", "city_name");
        $this->load->view('header', $data);
        $this->load->view('new_agency', $data);
        $this->load->view('footer', $data);
    }

    public function getLocation() {
        $tbl = $this->input->post('tbl');
        $selected_val = $this->input->post('selected_val');
        $res = $this->examiner_model->dataListingKeyvalue($tbl, "id_location", "location_name", "id_city=$selected_val ORDER BY id_location ASC");
        $str = "<option value=''>Select Location</option>";
        foreach ($res as $k => $v) {
            $str .= "<option value='{$k}'";
            if ($k == $searchVal) {
                $str .= "selected";
            }
            $str .= ">" . ucwords(($v)) . "</option>";
        }
        echo $str;
        exit;
    }

    public function insert_agency() {
        $logged_in = $this->session->userdata('logged_in');
        /*$user_p = explode(',', $logged_in['users']);
        if (!in_array('Add', $user_p)) {
            exit($this->lang->line('permission_denied'));
        }*/
        $this->load->library('form_validation');
        if ($this->input->server('REQUEST_METHOD') == 'GET') {
            $this->session->set_flashdata('message', "<div class='alert alert-danger'>" . validation_errors() . " </div>");
            redirect('examiner/new_examiner');
        }
        if (!$this->input->post('uid')) {
            $email = $this->input->post('email');
            $record_exist = $this->examiner_model->record_exist('email', $email, 'kams_users');
            if ($record_exist) {
                $this->session->set_flashdata('message', "<div class='alert alert-danger'>Email address already exists.</div>");
                redirect('agency/new_agency');
            }
        }
        if ($this->agency_model->insert_agency()) {
            $this->session->set_flashdata('message', "<div class='alert alert-success'>" . $this->lang->line('data_added_successfully') . " </div>");
        } else {
            $this->session->set_flashdata('message', "<div class='alert alert-danger'>" . $this->lang->line('error_to_add_data') . " </div>");
        }
        redirect('agency/');
    }

    public function remove_agency() {
        $logged_in = $this->session->userdata('logged_in');
        /*$user_p = explode(',', $logged_in['users']);
        if (!in_array('Remove', $user_p)) {
            exit($this->lang->line('permission_denied'));
        }*/
        $uid = $this->input->post('uid');
        $this->db->where('uid', $uid);
        if ($this->db->delete('kams_users')) {
            echo "success";
        } else {
            echo "failure";
        }
        exit();
    }

    public function update_wallet() {
        $logged_in = $this->session->userdata('logged_in');
        $uid = $this->input->post('uid');
        $transaction_type = $this->input->post('transaction_type');
        $current_balance = $this->input->post('current_balance');
        $wallet_balance = $this->input->post('wallet_balance');
        if ($transaction_type == 1) {
            $total_balance = $current_balance + $wallet_balance;
        } else {
            $total_balance = $current_balance - $wallet_balance;
        }
        //update user
        $upd_user['wallet_balance'] = $total_balance;
        $this->db->where('uid', $uid);
        $this->db->update('kams_users', $upd_user);

        //update transaction
        $upd_transaction['agency_id'] = $uid;
        $upd_transaction['amount'] = $wallet_balance;
        $upd_transaction['flag'] = $transaction_type == 1 ? 1 : 2;
        $upd_transaction['add_date'] = date('Y-m-d h:i:s');
        $upd_transaction['description'] = $this->input->post('description');
        $this->db->insert('kams_agency_transaction', $upd_transaction);

        $this->session->set_flashdata('message', "<div class='alert alert-success'>Balance added successfully!</div>");
        redirect('agency/');
    }

    public function transaction_listing() {
        $logged_in = $this->session->userdata('logged_in');
        $user_p = explode(',', $logged_in['users']);
//        if (!in_array('List_all', $user_p)) {
//            exit($this->lang->line('permission_denied'));
//        }
        $data['limit'] = $limit;
        $data['title'] = $this->lang->line('transaction_listing');
        // fetching user list
        //pagination links
        $config = array();
        $config = $this->paginatelinks();
        $config["base_url"] = site_url('agency/transaction_listing');
        $cnt_flg = "1";
        $config["total_rows"] = $this->agency_model->transaction_list($config["per_page"], $start, $cnt_flg = 1);
        $config["per_page"] = 10;
        $config["uri_segment"] = 3;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = round($choice);
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $start = $page;
        $data["links"] = $this->pagination->create_links();
        //pagination links

        $data['result'] = $this->agency_model->transaction_list($config["per_page"], $start);
        $this->load->view('header', $data);
        $this->load->view('transaction_listing', $data);
        $this->load->view('footer', $data);
    }

    public function new_city($city_id = "") {
        $logged_in = $this->session->userdata('logged_in');
        /*$user_p = explode(',', $logged_in['users']);
        if (!in_array('Add', $user_p)) {
            exit($this->lang->line('permission_denied'));
        }*/
        if ($city_id) {
            $data = array();
            $data['result'] = $this->location_model->get_city($city_id);
        }

        $data['title'] = $this->lang->line('add_new') . ' ' . $this->lang->line('city');
        $this->load->view('header', $data);
        $this->load->view('new_city', $data);
        $this->load->view('footer', $data);
    }

}
