<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Management_user extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->helper('url');
        $this->load->model("user_model");
        $this->load->model("examiner_model");
        $this->load->model("account_model");
        $this->load->model("managementuser_model");
        $this->lang->load('basic', $this->config->item('language'));
        $this->load->library("pagination");
        $this->config->load('masterdata');
        // redirect if not loggedin
        if (!$this->session->userdata('logged_in')) {
            redirect('login');
        }
        $logged_in = $this->session->userdata('logged_in');
        if ($logged_in['base_url'] != base_url()) {
            $this->session->unset_userdata('logged_in');
            redirect('login');
        }
    }

    public function paginatelinks() {
        $config['full_tag_open'] = '<div class="dataTables_paginate paging_bs_normal pull-right" id="datatable-default_paginate"><ul class="pagination justify-content-end p-4">';
        $config['full_tag_close'] = '</ul></div><!--pagination-->';

        $config['first_link'] = '&laquo; First';
        $config['first_tag_open'] = '<li class="page-item">';
        $config['first_tag_close'] = '</li>';

        $config['last_link'] = 'Last &raquo;';
        $config['last_tag_open'] = '<li class="page-item">';
        $config['last_tag_close'] = '</li>';

        $config['next_link'] = '<span class="page-link">&GT;</span>';
        $config['next_tag_open'] = '<li class="page-item">';
        $config['next_tag_close'] = '</li>';

        $config['prev_link'] = '<span class="page-link">&LT;</span>';
        $config['prev_tag_open'] = '<li class="page-item">';
        $config['prev_tag_close'] = '</li>';

        $config['cur_tag_open'] = '<li class="active"><span class="page-link-active">';
        $config['cur_tag_close'] = '</span></li>';

        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        return $config;
    }

    public function index($limit = '0') {
        $logged_in = $this->session->userdata('logged_in');

        /*$user_p = explode(',', $logged_in['users']);
        if (!in_array('List_all', $user_p)) {
            exit($this->lang->line('permission_denied'));
        }*/


        $data['limit'] = $limit;
        $data['title'] = $this->lang->line('management_user') . " " . $this->lang->line('list');
        // fetching user list
        //search user
        $search = array();

        $inputname = $this->uri->segment(3) ? $this->uri->segment(3) : "Name";
        $inputphone = $this->uri->segment(4) ? $this->uri->segment(4) : "Phone";
        $inputemail = $this->uri->segment(5) ? $this->uri->segment(5) : "Email";

        $inputname = urldecode($inputname);
        $inputphone = urldecode($inputphone);
        $inputemail = urldecode($inputemail);

        $name = $this->input->get('username') ? $this->input->get('username') : $inputname;
        $phone = $this->input->get('phone') ? $this->input->get('phone') : $inputphone;
        $email = $this->input->get('email') ? $this->input->get('email') : $inputemail;

        $user = array();
        if ($name != "Name")
            $user['username'] = $name;
        if ($phone != "Phone")
            $user['phone'] = $phone;
        if ($email != "Email")
            $user['email'] = $email;
        //pagination links
        $config = array();
        $config = $this->paginatelinks();
        $config["base_url"] = site_url("management_user/index/$name/$phone/$email");
        $cnt_flg = "1";
        $config["total_rows"] = $this->managementuser_model->management_list($config["per_page"], $start, $cnt_flg = 1, $user);
        $config["per_page"] = 10;
        $config["uri_segment"] = 6;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = round($choice);
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(6)) ? $this->uri->segment(6) : 0;
        $start = $page;
        $data["links"] = $this->pagination->create_links();
        //pagination links

        $data['result'] = $this->managementuser_model->management_list($config["per_page"], $start, "", $user);
        $data['usertype'] = $this->config->item('usertype');
        $data['city_list'] = $this->examiner_model->dataListingKeyvalue("kams_city", "id_city", "city_name");
        $data['user'] = $user;
//        $data['role_menu'] = $this->config->item('role_menu');
        $this->load->view('header', $data);
        $this->load->view('management_user_list', $data);
        $this->load->view('footer', $data);
    }

    public function new_management_user($uid = "") {
        /*$user_p = explode(',', $logged_in['users']);
        if (!in_array('Add', $user_p)) {
            exit($this->lang->line('permission_denied'));
        }*/
        if ($uid) {
            $data = array();
            $data['result'] = $this->examiner_model->get_examiner($uid);
            $data['title'] = $this->lang->line('edit') . ' ' . $this->lang->line('management_user');
        } else {
           $data['title'] = $this->lang->line('add_new') . ' ' . $this->lang->line('management_user'); 
        }
        
        // fetching category list
        $data['account_type'] = $this->account_model->account_list(0);
        $data['usertype'] = $this->config->item('usertype');
        $data['city_list'] = $this->examiner_model->dataListingKeyvalue("kams_city", "id_city", "city_name");
        $data['logged_in'] = $this->session->userdata('logged_in');
        $this->load->view('header', $data);
        $this->load->view('new_management_user', $data);
        $this->load->view('footer', $data);
    }

    public function insert_management_user() {
        $logged_in = $this->session->userdata('logged_in');
        /*$user_p = explode(',', $logged_in['users']);
        if (!in_array('Add', $user_p)) {
            exit($this->lang->line('permission_denied'));
        }*/
        $this->load->library('form_validation');
        if ($this->input->server('REQUEST_METHOD') == 'GET') {
            $this->session->set_flashdata('message', "<div class='alert alert-danger'>" . validation_errors() . " </div>");
            redirect('management_user/new_management_user');
        }
        if (!$this->input->post('uid')) {
            $email = $this->input->post('email');
            $record_exist = $this->managementuser_model->record_exist('email', $email, 'kams_users');
            if ($record_exist) {
                $this->session->set_flashdata('message', "<div class='alert alert-danger'>Email address already exists.</div>");
                redirect('management_user/new_management_user');
            }
        }
        if ($this->managementuser_model->insert_management_user()) {
            $this->session->set_flashdata('message', "<div class='alert alert-success'>" . $this->lang->line('data_added_successfully') . " </div>");
        } else {
            $this->session->set_flashdata('message', "<div class='alert alert-danger'>" . $this->lang->line('error_to_add_data') . " </div>");
        }
        redirect('management_user/');
    }

    public function remove_management() {
        $logged_in = $this->session->userdata('logged_in');
        $user_p = explode(',', $logged_in['users']);
//        if (!in_array('Remove', $user_p)) {
//            exit($this->lang->line('permission_denied'));
//        }
        $uid = $this->input->get('id_management');
        $this->db->where('uid', $uid);
        if ($this->db->delete('kams_users')) {
            $this->session->set_flashdata('message', "<div class='alert alert-success'>Record removed successfully!</div>");
        } else {
            $this->session->set_flashdata('message', "<div class='alert alert-danger'>Error in deletion!</div>");
        }
        redirect('management_user/');
    }

    public function roles_permission($uid = "") {
        $logged_in = $this->session->userdata('logged_in');
        if($logged_in['su'] == 1) {
            $data['role_menu'] = $this->config->item('role_menu');
        } elseif($logged_in['su'] == 3) {
            $data['role_menu'] = $this->config->item('agency_menu');
        }
        
        $data['id_user'] = $uid; 
        $data['result'] = $this->examiner_model->get_examiner($uid);
        $data['title'] = "Roles & Permissions for " . $data['result']['first_name'] . " " . $data['result']['last_name'];
        $this->load->view('header', $data);
        $this->load->view('roles_permission', $data);
        $this->load->view('footer', $data);
    }
    
    public function insert_permission() {
        $logged_in = $this->session->userdata('logged_in');
       $data = array();
        $upd['permission_access'] = json_encode($this->input->post('role_menu'));
        $upd['permission_given_by'] = $logged_in['uid'];
        $id_user = $this->input->post('id_user');
        if($id_user) {
            $this->db->where('uid', $id_user);
            $this->db->update('kams_users', $upd);
            $this->session->set_flashdata('message', "<div class='alert alert-success'>" . $this->lang->line('permission_success') . " </div>");
            redirect("management_user");
        }
//        print_r($data['role_menu']);
    }

    function logout() {

        $this->session->unset_userdata('logged_in');
        if ($this->session->userdata('logged_in_raw')) {
            $this->session->unset_userdata('logged_in_raw');
        }
        redirect('login');
    }

}
