<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Examiner extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->helper('url');
        $this->load->model("user_model");
        $this->load->model("examiner_model");
        $this->load->model("account_model");
        $this->lang->load('basic', $this->config->item('language'));
        $this->load->library("pagination");
        $this->config->load('masterdata');
        // redirect if not loggedin
        if (!$this->session->userdata('logged_in')) {
            redirect('login');
        }
        $logged_in = $this->session->userdata('logged_in');
        if ($logged_in['base_url'] != base_url()) {
            $this->session->unset_userdata('logged_in');
            redirect('login');
        }
    }

    public function paginatelinks() {
        $config['full_tag_open'] = '<div class="dataTables_paginate paging_bs_normal pull-right" id="datatable-default_paginate"><ul class="pagination justify-content-end p-4">';
        $config['full_tag_close'] = '</ul></div><!--pagination-->';

        $config['first_link'] = '&laquo; First';
        $config['first_tag_open'] = '<li class="page-item">';
        $config['first_tag_close'] = '</li>';

        $config['last_link'] = 'Last &raquo;';
        $config['last_tag_open'] = '<li class="page-item">';
        $config['last_tag_close'] = '</li>';

        $config['next_link'] = '<span class="page-link">&GT;</span>';
        $config['next_tag_open'] = '<li class="page-item">';
        $config['next_tag_close'] = '</li>';

        $config['prev_link'] = '<span class="page-link">&LT;</span>';
        $config['prev_tag_open'] = '<li class="page-item">';
        $config['prev_tag_close'] = '</li>';

        $config['cur_tag_open'] = '<li class="active"><span class="page-link-active">';
        $config['cur_tag_close'] = '</span></li>';

        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        return $config;
    }

    public function index($limit = '0') {
        $logged_in = $this->session->userdata('logged_in');

        /*$user_p = explode(',', $logged_in['users']);
        if (!in_array('List_all', $user_p)) {
            exit($this->lang->line('permission_denied'));
        }*/


        $data['limit'] = $limit;
        $data['title'] = $this->lang->line('examinerlist');
        // fetching user list
        //search user
        $search = array();

        $inputname = $this->uri->segment(3) ? $this->uri->segment(3) : "Name";
        $inputphone = $this->uri->segment(4) ? $this->uri->segment(4) : "Phone";
        $inputemail = $this->uri->segment(5) ? $this->uri->segment(5) : "Email";

        $inputname = urldecode($inputname);
        $inputphone = urldecode($inputphone);
        $inputemail = urldecode($inputemail);

        $name = $this->input->get('username') ? $this->input->get('username') : $inputname;
        $phone = $this->input->get('phone') ? $this->input->get('phone') : $inputphone;
        $email = $this->input->get('email') ? $this->input->get('email') : $inputemail;

        $user = array();
        if ($name != "Name")
            $user['username'] = $name;
        if ($phone != "Phone")
            $user['phone'] = $phone;
        if ($email != "Email")
            $user['email'] = $email;
        //pagination links
        $config = array();
        $config = $this->paginatelinks();
        $config["base_url"] = site_url("examiner/index/$name/$phone/$email");
        $cnt_flg = "1";
        $config["total_rows"] = $this->examiner_model->examiner_list($config["per_page"], $start, $cnt_flg = 1, $user);
        $config["per_page"] = 10;
        $config["uri_segment"] = 6;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = round($choice);
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(6)) ? $this->uri->segment(6) : 0;
        $start = $page;
        $data["links"] = $this->pagination->create_links();
        //pagination links

        $data['result'] = $this->examiner_model->examiner_list($config["per_page"], $start, "", $user);
        $data['exam_category'] = $this->config->item('exam_category');
        $data['examiner_userrole'] = $this->config->item('examiner_userrole');
        $data['city_list'] = $this->examiner_model->dataListingKeyvalue("kams_city", "id_city", "city_name");
        $data['testcenter_list'] = $this->examiner_model->dataListingKeyvalue("kams_users", "uid", "company", "su=3");
        $data['user'] = $user;
        $this->load->view('header', $data);
        $this->load->view('examiner_list', $data);
        $this->load->view('footer', $data);
    }

    public function new_examiner($uid = "") {
        $logged_in = $this->session->userdata('logged_in');
        /*$user_p = explode(',', $logged_in['users']);
        if (!in_array('Add', $user_p)) {
            exit($this->lang->line('permission_denied'));
        }*/
        if ($uid) {
            $data = array();
            $data['result'] = $this->examiner_model->get_examiner($uid);
        }

        $data['title'] = $this->lang->line('add_new') . ' ' . $this->lang->line('examinerscorer');
        // fetching category list
        $data['account_type'] = $this->account_model->account_list(0);
        $data['exam_category'] = $this->config->item('exam_category');
        $data['examiner_userrole'] = $this->config->item('examiner_userrole');
        $data['city_list'] = $this->examiner_model->dataListingKeyvalue("kams_city", "id_city", "city_name");
        $data['testcenter_list'] = $this->examiner_model->dataListingKeyvalue("kams_users", "uid", "company", "su=3");
        $this->load->view('header', $data);
        $this->load->view('new_examiner', $data);
        $this->load->view('footer', $data);
    }

    public function insert_examiner() {
        $logged_in = $this->session->userdata('logged_in');
        /*$user_p = explode(',', $logged_in['users']);
        if (!in_array('Add', $user_p)) {
            exit($this->lang->line('permission_denied'));
        }*/
        $this->load->library('form_validation');
        if ($this->input->server('REQUEST_METHOD') == 'GET') {
            $this->session->set_flashdata('message', "<div class='alert alert-danger'>" . validation_errors() . " </div>");
            redirect('examiner/new_examiner');
        }
        if (!$this->input->post('uid')) {
            $email = $this->input->post('email');
            $record_exist = $this->examiner_model->record_exist('email', $email, 'kams_users');
            if ($record_exist) {
                $this->session->set_flashdata('message', "<div class='alert alert-danger'>Email address already exists.</div>");
                redirect('examiner/new_examiner');
            }
        }
        if ($this->examiner_model->insert_examiner()) {
            $this->session->set_flashdata('message', "<div class='alert alert-success'>" . $this->lang->line('data_added_successfully') . " </div>");
        } else {
            $this->session->set_flashdata('message', "<div class='alert alert-danger'>" . $this->lang->line('error_to_add_data') . " </div>");
        }
        redirect('examiner/');
    }

    public function getAssignStudents() {
        $examiner_id = $this->input->post('examiner_id');

        $this->db->select('a.*, CONCAT(u.first_name, " ", u.last_name) AS student_name, u.contact_no');
        $this->db->from('kams_assign_students a');
        $this->db->join('kams_users u', 'u.uid = a.student_id', 'left');
        $this->db->where('a.examiner_id', $examiner_id);
        $query = $this->db->get();
        $data['result'] = $query->result_array();
        $this->load->view('examiner_assignstudentlist', $data);
        //echo json_encode($data['result']);
        //exit();
    }

    public function assign_students() {
        $logged_in = $this->session->userdata('logged_in');
        /*$user_p = explode(',', $logged_in['users']);
        if (!in_array('Add', $user_p)) {
            exit($this->lang->line('permission_denied'));
        }*/
//        $assign_students = $this->input->post('assign_students');
        $student_id = $this->input->post('student_id');
        $stids = explode(',', $student_id);
        //$student_ids = explode(",", $ids);
        if ($stids) {
            foreach ($stids as $k => $v) {
                $ins['examiner_id'] = $this->input->post('examiner_id');
                $ins['student_id'] = $v;
                $ins['added_by'] = $logged_in['uid'];
                $this->db->insert('kams_assign_students', $ins);
            }
            $result = array();
            $result['hasError'] = false;
            $result['Message'] = "Success";
        } else {
            $result = array();
            $result['hasError'] = true;
            $result['Message'] = "Failure";
        }
        echo json_encode($result);
    }

    public function remove_examiner() {

        $logged_in = $this->session->userdata('logged_in');
        /*$user_p = explode(',', $logged_in['users']);
        if (!in_array('Remove', $user_p)) {
            exit($this->lang->line('permission_denied'));
        }*/
        $uid = $this->input->get('id_examiner');
        if ($uid == '1') {
            exit($this->lang->line('permission_denied'));
        }
        if ($this->examiner_model->remove_examiner($uid)) {
            $this->session->set_flashdata('message', "<div class='alert alert-success'>" . $this->lang->line('removed_successfully') . " </div>");
        } else {
            $this->session->set_flashdata('message', "<div class='alert alert-danger'>" . $this->lang->line('error_to_remove') . " </div>");
        }
        redirect('examiner');
    }

    public function exam_listing_original() {
        $logged_in = $this->session->userdata('logged_in');
        $data['title'] = $this->lang->line('exam_listing');
        //get subscribed video conference exam list

        $this->db->select('q.quiz_name, q.video_conference, s.*, '
                . 'CONCAT(u.first_name, " ", u.last_name) AS student_name');
        $this->db->from('kams_students_exam_subscription s');
        $this->db->join('kams_quiz q', 'q.quid = s.quiz_id', 'left');
        $this->db->join('kams_users u', 'u.uid = s.student_id', 'left');
//        $this->db->where('s.examiner_id', $logged_in['uid']);
//        $this->db->where('q.video_conference', 1);
//        $this->db->group_by('q.quid');
        $query = $this->db->get();
        $data['exam_list'] = $query->result_array();
//        echo $this->db->last_query();
        $this->load->view('header', $data);
        $this->load->view('exam_listing', $data);
        $this->load->view('footer', $data);
    }

    public function exam_listing() {
        $logged_in = $this->session->userdata('logged_in');
        $data['title'] = $this->lang->line('exam_listing');
        //get subscribed video conference exam list
        $sql = "SELECT ks.examiner_id, ks.scorer_id, ks.student_id, ku.first_name, ku.last_name,"
                . " kq.quiz_name,kq.vocabulary_flag,kq.structure_flag,kq.fluency_flag,kq.pronunciation_flag,kq.comprehension_flag,"
                . " p1.rid,p1.quid,p1.uid,p1.result_status,p1.video_conference_flag FROM kams_result p1"
                . " INNER JOIN (SELECT pi.rid, MAX(pi.rid) AS maxpostid"
                . " FROM kams_result pi GROUP BY pi.quid) p2 ON (p1.rid = p2.maxpostid)"
                . "LEFT JOIN kams_users AS ku ON ku.uid = p1.uid "
                . "LEFT JOIN kams_quiz AS kq ON kq.quid = p1.quid "
                . "LEFT JOIN kams_students_exam_subscription AS ks ON ks.subscription_id = p1.subscription_id";
//                    . " WHERE p1.uid = $userId";
        $query = $this->db->query($sql);
        $data['logged_in'] = $logged_in;
        $data['exam_list'] = $query->result_array();

        $this->load->view('header', $data);
        $this->load->view('exam_listing', $data);
        $this->load->view('footer', $data);
    }

    public function update_mark() {
        $logged_in = $this->session->userdata('logged_in');
        $result_id = $this->input->post('result_id');
        $quiz_id = $this->input->post('quiz_id');
        $score = $this->input->post('score');
        $video_exam = @$this->input->post('video_exam');
        $vocabulary_comment = @$this->input->post('vocabulary_comment');
        $structure_comment = @$this->input->post('structure_comment');
        $fluency_comment = @$this->input->post('fluency_comment');
        $pronunciation_comment = @$this->input->post('pronunciation_comment');
        $comprehension_comment = @$this->input->post('comprehension_comment');
        $vocabulary_interaction_percentage = $this->input->post('vocabulary_score');
        $structure_interaction_percentage = $this->input->post('structure_score');
        $fluency_interaction_percentage = $this->input->post('fluency_score');
        $pronunciation_interaction_percentage = $this->input->post('pronunciation_score');
        $comprehension_interaction_percentage = $this->input->post('comprehension_score');
        if (!empty($score)) {
            $upd = array(
                'video_conference_score' => $score,
                'vocabulary_comment' => $vocabulary_comment,
                'structure_comment' => $structure_comment,
                'fluency_comment' => $fluency_comment,
                'pronunciation_comment' => $pronunciation_comment,
                'comprehension_comment' => $comprehension_comment,
                'video_conference_flag' => 1,
                'video_conference_percentage' => ($score / 10) * 100,
                'vocabulary_interaction_percentage' => ($vocabulary_interaction_percentage / 10) * 100,
                'structure_interaction_percentage' => ($structure_interaction_percentage / 10) * 100,
                'fluency_interaction_percentage' => ($fluency_interaction_percentage / 10) * 100,
                'pronunciation_interaction_percentage' => ($pronunciation_interaction_percentage / 10) * 100,
                'comprehension_interaction_percentage' => ($comprehension_interaction_percentage / 10) * 100
            );
//            print "<pre>";print_r($upd);exit();
            $this->db->where('rid', $result_id);
            $this->db->update('kams_result', $upd);

            //create report 
            $this->createpdf($result_id);
            if(@$video_exam == 1) {
                $redirect_url = "result/mockexam_results/";
            } else {
                $redirect_url = "examiner/exam_listing/";
            }
            //end
            $this->session->set_flashdata('message', "<div class='alert alert-success'>Score added successfully!</div>");
            redirect($redirect_url);
        } else {
            $this->session->set_flashdata('message', "<div class='alert alert-error'>Please try once again!</div>");
            redirect($redirect_url);
        }
    }

    private function createpdf($rid) {
        $this->load->library('pdf');
        $this->load->model("result_model");
        $data = array();
        $data['result'] = $this->result_model->get_result($rid);
        $data['exam_category'] = $this->config->item('exam_category');
        $data['dob'] = date('d/m/Y', strtotime($data['result']['dob']));
        $data['start_date'] = date('d/m/Y', strtotime($data['result']['start_date_result']));
        $startdate = strtotime($data['result']['start_date_result']);
        $data['expirydate'] = date('d/m/Y', strtotime('+15 years', $startdate));
//        $a = $this->load->view('mypdf', $data,true);
//        print $a; exit();
        $this->pdf->load_view('mypdf', $data);
        $this->load->helper('file');
        $this->pdf->render();
        $output = $this->pdf->output();
        $pdfroot = base_url();
        $filename = time() . ".pdf";
        file_put_contents('./result_pdf/' . $filename, $output);

        $this->db->where('rid', $rid);
        $this->db->update('kams_result', array("report_pdf" => $filename));
        return true;
    }

    public function remove_students() {
        $student_id = $this->input->post('student_id');
        $examiner_id = $this->input->post('eid');
        if ($this->examiner_model->remove_students($student_id, $examiner_id)) {
            echo "success";
        } else {
            echo "failure";
        }
        exit();
    }

    function logout() {

        $this->session->unset_userdata('logged_in');
        if ($this->session->userdata('logged_in_raw')) {
            $this->session->unset_userdata('logged_in_raw');
        }
        redirect('login');
    }

}
