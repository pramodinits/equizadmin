<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Result extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->helper('url');
        $this->load->model("result_model");
        $this->load->model("social_model");
        $this->load->model("user_model");
        $this->config->load('masterdata');
        $this->lang->load('basic', $this->config->item('language'));
        $this->load->library("pagination");
        $logged_in = $this->session->userdata('logged_in');
        // redirect if not loggedin
    }

    public function paginatelinks() {
        $config['full_tag_open'] = '<div class="dataTables_paginate paging_bs_normal pull-right" id="datatable-default_paginate"><ul class="pagination justify-content-end p-4">';
        $config['full_tag_close'] = '</ul></div><!--pagination-->';

        $config['first_link'] = '&laquo; First';
        $config['first_tag_open'] = '<li class="page-item">';
        $config['first_tag_close'] = '</li>';

        $config['last_link'] = 'Last &raquo;';
        $config['last_tag_open'] = '<li class="page-item">';
        $config['last_tag_close'] = '</li>';

        $config['next_link'] = '<span class="page-link">&GT;</span>';
        $config['next_tag_open'] = '<li class="page-item">';
        $config['next_tag_close'] = '</li>';

        $config['prev_link'] = '<span class="page-link">&LT;</span>';
        $config['prev_tag_open'] = '<li class="page-item">';
        $config['prev_tag_close'] = '</li>';

        $config['cur_tag_open'] = '<li class="active"><span class="page-link-active">';
        $config['cur_tag_close'] = '</span></li>';

        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        return $config;
    }

    public function index($status = '0') {
        if (!$this->session->userdata('logged_in')) {
            redirect('login');
        }
        $logged_in = $this->session->userdata('logged_in');
        if ($logged_in['base_url'] != base_url()) {
            $this->session->unset_userdata('logged_in');
            redirect('login');
        }

        $logged_in = $this->session->userdata('logged_in');
        //$setting_p = explode(',', $logged_in['results']);

        /*if (in_array('List', $setting_p) || in_array('List_all', $setting_p)) {
        } else {
            exit($this->lang->line('permission_denied'));
        }*/

        $data['limit'] = $limit;
        $data['status'] = $status;
        $data['title'] = $this->lang->line('resultlist');
        // fetching quiz list
        $data['quiz_list'] = $this->result_model->quiz_list();
        // group list
        //pagination links
        $config = array();
        $config = $this->paginatelinks();
        $config["base_url"] = site_url("result/index/$status");
        $cnt_flg = "1";
        $config["total_rows"] = $this->result_model->result_list($config["per_page"], $start, $cnt_flg = 1);
        $config["per_page"] = 10;
        $config["uri_segment"] = 4;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = round($choice);
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $start = $page;
        $data["links"] = $this->pagination->create_links();
        $data['result'] = $this->result_model->result_list($config["per_page"], $start, "");
        //pagination links

        $this->load->model("user_model");
        $data['group_list'] = $this->user_model->group_list();
        $data['category_type'] = $this->config->item('exam_category');
        $this->load->view('header', $data);
        $this->load->view('result_list', $data);
        $this->load->view('footer', $data);
    }
    
    public function mockexam_results() {
        if (!$this->session->userdata('logged_in')) {
            redirect('login');
        }
        $logged_in = $this->session->userdata('logged_in');
        if ($logged_in['base_url'] != base_url()) {
            $this->session->unset_userdata('logged_in');
            redirect('login');
        }
        $logged_in = $this->session->userdata('logged_in');
        /*$setting_p = explode(',', $logged_in['results']);

        if (in_array('List', $setting_p) || in_array('List_all', $setting_p)) {
            
        } else {
            exit($this->lang->line('permission_denied'));
        }*/
        
        

        $data['limit'] = $limit;
        $data['status'] = $status;
        $data['title'] = $this->lang->line('mockexam_results');
        // fetching quiz list
        $data['quiz_list'] = $this->result_model->quiz_list('exam_type = 1');
        // group list
        //pagination links
        $config = array();
        $config = $this->paginatelinks();
        //$config["base_url"] = site_url("result/mockexam_results/$status");
        $config["base_url"] = site_url("result/mockexam_results");
        $cnt_flg = "1";
        $mockexam_cond = "kams_quiz.exam_type = 1";
        $config["total_rows"] = $this->result_model->mockexam_result_list($config["per_page"], $start, $cnt_flg = 1);
        $config["per_page"] = 10;
        $config["uri_segment"] = 3;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = round($choice);
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $start = $page;
        $data["links"] = $this->pagination->create_links();
        $data['result'] = $this->result_model->mockexam_result_list($config["per_page"], $start, "");
//        print "<pre>"; print_r($config); print_r($data["links"]);exit;
        //pagination links

        $this->load->model("user_model");
        $data['group_list'] = $this->user_model->group_list();
        $data['category_type'] = $this->config->item('exam_category');
        $this->load->view('header', $data);
        $this->load->view('mockexam_result_list', $data);
        $this->load->view('footer', $data);
    }

    public function remove_result($rid, $open = '0') {

        if (!$this->session->userdata('logged_in')) {
            redirect('login');
        }
        $logged_in = $this->session->userdata('logged_in');
        $acp = explode(',', $logged_in['results']);
       /* if (!in_array('List_all', $acp)) {
            exit($this->lang->line('permission_denied'));
        }

        if ($open != 0) {
            $this->db->query("delete from kams_result where result_status='Open'");
        }*/

        $logged_in = $this->session->userdata('logged_in');
        if ($logged_in['base_url'] != base_url()) {
            $this->session->unset_userdata('logged_in');
            redirect('login');
        }
        $logged_in = $this->session->userdata('logged_in');
        /*$acp = explode(',', $logged_in['results']);
        if (!in_array('Remove', $acp)) {
            exit($this->lang->line('permission_denied'));
        }*/

        if ($this->result_model->remove_result($rid)) {
            $this->session->set_flashdata('message', "<div class='alert alert-success'>" . $this->lang->line('removed_successfully') . " </div>");
        } else {
            $this->session->set_flashdata('message', "<div class='alert alert-danger'>" . $this->lang->line('error_to_remove') . " </div>");
        }
        redirect('result');
    }

    public function mypdf() {
        $this->load->library('pdf');
        $data = array();
        $data['result'] = $this->result_model->get_result(100);
        $data['exam_category'] = $this->config->item('exam_category');
        $data['dob'] = date('d/m/Y', strtotime($data['result']['dob']));
        $data['start_date'] = date('d/m/Y', strtotime($data['result']['start_date_result']));
        $startdate = strtotime($data['result']['start_date_result']);
        $data['expirydate'] = date('d/m/Y', strtotime('+15 years', $startdate));
//        $a = $this->load->view('mypdf', $data,true);
//        print $a; exit();
        $this->pdf->load_view('mypdf', $data);
        $this->load->helper('file');
        $this->pdf->render();
        $output = $this->pdf->output();
        $pdfroot = base_url();
        file_put_contents('./result_pdf/' . time() . '.pdf', $output);
    }

    function generate_report() {
        if (!$this->session->userdata('logged_in')) {
            redirect('login');
        }
        $logged_in = $this->session->userdata('logged_in');
        if ($logged_in['base_url'] != base_url()) {
            $this->session->unset_userdata('logged_in');
            redirect('login');
        }

        $logged_in = $this->session->userdata('logged_in');
        $acp = explode(',', $logged_in['results']);
        /*if (!in_array('List_all', $acp)) {
            exit($this->lang->line('permission_denied'));
        }*/

        $this->load->helper('download');

        $quid = $this->input->post('quid');
        $gid = $this->input->post('gid');
        $result = $this->result_model->generate_report($quid, $gid);
        $csvdata = $this->lang->line('result_id') . "," . $this->lang->line('email') . "," . $this->lang->line('first_name') . "," . $this->lang->line('last_name') . "," . $this->lang->line('group_name') . "," . $this->lang->line('quiz_name') . "," . $this->lang->line('score_obtained') . "," . $this->lang->line('percentage_obtained') . "," . $this->lang->line('status') . "\r\n";
        foreach ($result as $rk => $val) {
            $csvdata .= $val['rid'] . "," . $val['email'] . "," . $val['first_name'] . "," . $val['last_name'] . "," . $val['group_name'] . "," . $val['quiz_name'] . "," . $val['score_obtained'] . "," . $val['percentage_obtained'] . "," . $val['result_status'] . "\r\n";
        }
        $filename = time() . '.csv';
        force_download($filename, $csvdata);
    }

    function view_result($rid) {

        if (!$this->session->userdata('logged_in')) {
            if (!$this->session->userdata('logged_in_raw')) {
                redirect('login');
            }
        }
        if (!$this->session->userdata('logged_in')) {
            $logged_in = $this->session->userdata('logged_in_raw');
        } else {
            $logged_in = $this->session->userdata('logged_in');
        }
        if ($logged_in['base_url'] != base_url()) {
            $this->session->unset_userdata('logged_in');
            redirect('login');
        }
        $logged_in = $this->session->userdata('logged_in');
        $setting_p = explode(',', $logged_in['results']);
        /*if (in_array('List', $setting_p) || in_array('List_all', $setting_p)) {
            
        } else {
            exit($this->lang->line('permission_denied'));
        }*/

        // check any custom field pending to fill..

        $data['result'] = $this->result_model->get_result($rid);
        /*if (!in_array('List_all', $setting_p)) {
            if ($this->user_model->pending_custom($data['result']['uid']) >= 1) {
                redirect('user/edit_user_fill_custom/' . $data['result']['uid'] . '/' . $rid);
            }
        }*/
        $data['attempt'] = $this->result_model->no_attempt($data['result']['quid'], $data['result']['uid']);
        $data['title'] = $this->lang->line('result_id') . ' ' . $data['result']['rid'];

//        if ($data['result']['view_answer'] == '1' || $logged_in['su'] == '1') {
//            $this->load->model("quiz_model");
//            $data['saved_answers'] = $this->quiz_model->saved_answers($rid);
//            $data['questions'] = $this->quiz_model->get_questions($data['result']['english_question_ids']);
//            $data['options'] = $this->quiz_model->get_options($data['result']['r_qids']);
//        }
        // top 10 results of selected quiz
        $last_ten_result = $this->result_model->last_ten_result($data['result']['quid']);

        $value = array();
        $value[] = array('Quiz Name', 'Percentage (%)');
        foreach ($last_ten_result as $val) {
            $value[] = array($val['email'] . ' (' . $val['first_name'] . " " . $val['last_name'] . ')', intval($val['percentage_obtained']));
        }

        $data['value'] = json_encode($value);
        $data['percentile'] = $this->result_model->get_percentile($data['result']['quid'], $data['result']['uid'], $data['result']['score_obtained']);

        // time spent on individual questions
        $correct_incorrect = explode(',', $data['result']['score_individual']);
        $qtime[] = array($this->lang->line('category'), $this->lang->line('percentage'));

        //Assign values
        $qtime[] = array($this->lang->line('module_english'), intval($data['result']['english_percentage']));
        $qtime[] = array($this->lang->line('module_general'), intval($data['result']['general_percentage']));
        $qtime[] = array($this->lang->line('module_audio'), intval($data['result']['audio_percentage']));
        $qtime[] = array($this->lang->line('module_video'), intval($data['result']['video_percentage']));
//         print "<pre>";
//        print_r($qtime);exit();

        $graph = $bar = array();
        if ($data['result']['video_conference_flag'] == 1) {
            $bar['labels'] = array('Pronunciation', 'Structure', 'Vocabulary', 'Fluency', 'Comprehension');
            $bar['percent'] = array($data['result']['pronunciation_percentage'],
                $data['result']['structure_percentage'],
                $data['result']['vocabulary_percentage'],
                $data['result']['fluency_percentage'],
                $data['result']['comprehension_percentage']);
            $bar['interaction'] = array($data['result']['pronunciation_interaction_percentage'],
                $data['result']['structure_interaction_percentage'],
                $data['result']['vocabulary_interaction_percentage'],
                $data['result']['fluency_interaction_percentage'],
                $data['result']['comprehension_interaction_percentage']);
            $data['barchart'] = $bar;
        } else {
            $graph['Pronunciation']['percent'] = $data['result']['pronunciation_percentage'];
            $graph['Pronunciation']['color'] = intval($data['result']['pronunciation_percentage']) > 30 ? 'green' : 'red';
            $graph['Structure']['percent'] = $data['result']['structure_percentage'];
            $graph['Structure']['color'] = intval($data['result']['structure_percentage']) > 30 ? 'green' : 'red';
            $graph['Vocabulary']['percent'] = $data['result']['vocabulary_percentage'];
            $graph['Vocabulary']['color'] = intval($data['result']['vocabulary_percentage']) > 30 ? 'green' : 'red';
            $graph['Fluency']['percent'] = $data['result']['fluency_percentage'];
            $graph['Fluency']['color'] = intval($data['result']['fluency_percentage']) > 30 ? 'green' : 'red';
            $graph['Comprehension']['percent'] = $data['result']['comprehension_percentage'];
            $graph['Comprehension']['color'] = intval($data['result']['comprehension_percentage']) > 30 ? 'green' : 'red';
            $data['graph'] = $graph;
        }

//      
//       
//        foreach ($data['result'] as $key => $val) {
//            if ($val == '0') {
//                $val = 1;
//            }
//            if ($result['english_question_ids'] == "1") {
//                
//            } else if ($correct_incorrect[$key] == '2') {
//                $qtime[] = array($this->lang->line('q') . " " . ($key + 1) . ") - " . $this->lang->line('incorrect') . "", intval($val));
//            } else if ($correct_incorrect[$key] == '0') {
//                $qtime[] = array($this->lang->line('q') . " " . ($key + 1) . ") -" . $this->lang->line('unattempted') . " ", intval($val));
//            } else if ($correct_incorrect[$key] == '3') {
//                $qtime[] = array($this->lang->line('q') . " " . ($key + 1) . ") - " . $this->lang->line('pending_evaluation') . " ", intval($val));
//            }
//        }
        $data['qtime'] = json_encode($qtime);


        $uid = $data['result']['uid'];
        $quid = $data['result']['quid'];


        $this->load->view('header', $data);
        if ($this->session->userdata('logged_in')) {
            $this->load->view('view_result', $data);
        } else {
            $this->load->view('view_result_without_login', $data);
        }
        $this->load->view('footer', $data);
    }

    public function getQuestionDetails() {
        $question_ids = $this->input->post('question_ids');
        $result_id = $this->input->post('result_id');

        $this->db->select('r.quid, a.*, q.question');
        $this->db->from('kams_result r');
        $this->db->join('kams_answers a', 'r.rid = a.rid and a.qid IN (' . $question_ids . ')', 'left');
        $this->db->join('kams_qbank q', 'q.qid = a.qid', 'left');
        $this->db->where('r.rid', $result_id);
        $this->db->group_by("a.aid");
        $query = $this->db->get();
//        print_r($this->db->last_query());
//        exit();
        $data['result'] = $query->result_array();
        $this->load->view('category_ansdetails', $data);
    }

    function getscoresbysg($sg_id, $uid, $quid) {
        $data['members'] = $this->social_model->group_member($sg_id);
        $uids = array();
        foreach ($data['members'] as $k => $user) {
            $uids[] = $user['uid'];
        }
        $this->db->order_by('kams_result.score_obtained', 'desc');
        $this->db->where('kams_result.quid', $quid);
        $this->db->where_in('kams_result.uid', $uids);
        $this->db->join('kams_users', 'kams_users.uid=kams_result.uid');
        $query = $this->db->get("kams_result");
        $data['quiz'] = $query->result_array();

        $this->load->view('getscoresbysg', $data);
    }

    function generate_certificate($rid) {
        if (!$this->session->userdata('logged_in')) {
            redirect('login');
        }
        $logged_in = $this->session->userdata('logged_in');
        if ($logged_in['base_url'] != base_url()) {
            $this->session->unset_userdata('logged_in');
            redirect('login');
        }
        if (!$this->config->item('dompdf')) {
            exit('DOMPDF library disabled in config.php file');
        }
        $data['result'] = $this->result_model->get_result($rid);
        if ($data['result']['gen_certificate'] == '0') {
            exit();
        }
        // save qr 
        $enu = urlencode(site_url('login/verify_result/' . $rid));

        $qrname = "./upload/" . time() . '.jpg';
        $durl = "https://chart.googleapis.com/chart?chs=100x100&cht=qr&chl=" . $enu . "&choe=UTF-8";
        copy($durl, $qrname);


        $certificate_text = $data['result']['certificate_text'];
        $certificate_text = str_replace('{qr_code}', "<img src='" . $qrname . "'>", $certificate_text);
        $certificate_text = str_replace('{email}', $data['result']['email'], $certificate_text);
        $certificate_text = str_replace('{first_name}', $data['result']['first_name'], $certificate_text);
        $certificate_text = str_replace('{last_name}', $data['result']['last_name'], $certificate_text);
        $certificate_text = str_replace('{percentage_obtained}', $data['result']['percentage_obtained'], $certificate_text);
        $certificate_text = str_replace('{score_obtained}', $data['result']['score_obtained'], $certificate_text);
        $certificate_text = str_replace('{quiz_name}', $data['result']['quiz_name'], $certificate_text);
        $certificate_text = str_replace('{status}', $data['result']['result_status'], $certificate_text);
        $certificate_text = str_replace('{result_id}', $data['result']['rid'], $certificate_text);
        $certificate_text = str_replace('{generated_date}', date('Y-m-d H:i:s', $data['result']['end_time']), $certificate_text);

        $data['certificate_text'] = $certificate_text;
        // $this->load->view('view_certificate',$data);
        $this->load->library('pdf');
        $this->pdf->load_view('view_certificate', $data);
        $this->pdf->render();
        $filename = date('Y-M-d_H:i:s', time()) . ".pdf";
        $this->pdf->stream($filename);
    }

    function preview_certificate($quid) {
        if (!$this->session->userdata('logged_in')) {
            redirect('login');
        }
        $logged_in = $this->session->userdata('logged_in');
        if ($logged_in['base_url'] != base_url()) {
            $this->session->unset_userdata('logged_in');
            redirect('login');
        }

        $this->load->model("quiz_model");

        $data['result'] = $this->quiz_model->get_quiz($quid);
        if ($data['result']['gen_certificate'] == '0') {
            exit();
        }
        // save qr 
        $enu = urlencode(site_url('login/verify_result/0'));
        $tm = time();
        $qrname = "./upload/" . $tm . '.jpg';
        $durl = "https://chart.googleapis.com/chart?chs=100x100&cht=qr&chl=" . $enu . "&choe=UTF-8";
        copy($durl, $qrname);
        $qrname2 = base_url('/upload/' . $tm . '.jpg');


        $certificate_text = $data['result']['certificate_text'];
        $certificate_text = str_replace('{qr_code}', "<img src='" . $qrname2 . "'>", $certificate_text);
        $certificate_text = str_replace('{result_id}', '1023', $certificate_text);
        $certificate_text = str_replace('{generated_date}', date('Y-m-d H:i:s', time()), $certificate_text);

        $data['certificate_text'] = $certificate_text;
        $this->load->view('view_certificate_2', $data);
    }

}
