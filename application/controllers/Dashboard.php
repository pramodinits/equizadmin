<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->helper('url');
        $this->load->model("user_model");
        $this->load->model("qbank_model");
        $this->load->model("quiz_model");
        $this->load->model("result_model");
        $this->load->model("agency_model");
        $this->lang->load('basic', $this->config->item('language'));
        // redirect if not loggedin
        if (!$this->session->userdata('logged_in')) {
            redirect('login');
        }
        $logged_in = $this->session->userdata('logged_in');
        if ($logged_in['base_url'] != base_url()) {
            $this->session->unset_userdata('logged_in');
            redirect('login');
        }
    }

    public function index() {
        $logged_in = $this->session->userdata('logged_in');
        $acp = explode(',', $logged_in['setting']);
        $view_page = "dashboard";
        //if (in_array('All', $acp)) {
        if ($logged_in['su'] == 1 || $logged_in['su'] == 7) { //Admin dashboard
            $data['count_pilots'] = $this->user_model->count_rows("kams_quiz", "exam_category = 1");
            $data['count_agents'] = $this->user_model->count_rows("kams_quiz", "exam_category = 2");
            $data['count_atcos'] = $this->user_model->count_rows("kams_quiz", "exam_category = 3");
            $data['count_technicians'] = $this->user_model->count_rows("kams_quiz", "exam_category = 4");

            $data['total_subscription'] = $this->user_model->count_rows("kams_students_exam_subscription", "");
            $data['total_agency'] = $this->user_model->count_rows("kams_users", "su = 3");
            $data['num_users'] = $this->user_model->num_users();
            $data['verified_users'] = $this->user_model->verify_users('verify_code = 0');
            $data['unverified_users'] = $this->user_model->verify_users('verify_code != 0');
            $data['num_qbank'] = $this->user_model->count_rows("kams_qbank", "status = 1");
            $data['num_quiz_mock'] = $this->user_model->count_rows("kams_quiz", "exam_type = 1");
            $data['num_quiz_exam'] = $this->user_model->count_rows("kams_quiz", "exam_type = 2");

            $agency_query = "SELECT
   ku.first_name, ku.last_name, ku.wallet_balance,(SELECT COUNT(*) FROM kams_students_exam_subscription WHERE kams_students_exam_subscription.agency_id = ku.uid AND kams_students_exam_subscription.start_date BETWEEN NOW() - INTERVAL 30 DAY AND NOW()) AS totalexamsubscription FROM kams_users AS ku WHERE ku.su=3 LIMIT 0,5";
            $query = $this->db->query($agency_query);
            $data['agency_listing'] = $query->result_array();
            $view_page = "dashboard";
        } elseif ($logged_in['su'] == 3 || $logged_in['su'] == 8) { //Agency dashboard
            if ($logged_in['su'] == 3) {
                $total_balance = $logged_in['wallet_balance'];
            } elseif ($logged_in['su'] == 8) {
                $rq = $this->db->query("select wallet_balance from kams_users where uid = ".$logged_in['permission_given_by']."");
                $res = $rq->row_array();
                $total_balance = $res['wallet_balance'];
            }
            $data['total_balance'] = $total_balance;
            $agency_id = $logged_in['su'] == 3 ? $logged_in['uid'] : $logged_in['permission_given_by'];
            $data['completed_exam'] = "8"; //static for now
            $subscribed_exam = "SELECT COUNT(*) FROM kams_students_exam_subscription WHERE agency_id = " . $agency_id;
            $query = $this->db->query($subscribed_exam);
            $data['subscribed_exam'] = $query->num_rows();

            //transaction list
            $this->db->select('at.*, CONCAT(ku.first_name, " ", ku.last_name) AS center_name, ku.wallet_balance');
            $this->db->from('kams_agency_transaction at');
            $this->db->join('kams_users ku', 'ku.uid = at.agency_id', 'left');
            $this->db->where('at.agency_id =' . $agency_id);
            $this->db->limit(10, 0);
            $query = $this->db->get();
            $data['result'] = $query->result_array();
            //transaction list

            $view_page = "agency_dashboard";
        } elseif ($logged_in['su'] == 4) { //Examiner dashboard
            $assigned_exam = "SELECT COUNT(*) FROM kams_students_exam_subscription WHERE examiner_id = " . $logged_in['uid'];
            $query = $this->db->query($assigned_exam);
            $data['assigned_exam'] = $query->num_rows();
            $data['completed_exam'] = 12;
            $data['rating'] = 5;
            $data['result'] = $this->result_model->result_list(10, 0, "");
            $view_page = "examiner_dashboard";
        } elseif ($logged_in['su'] == 5) { //Scorer dashboard
            $assigned_exam = "SELECT COUNT(*) FROM kams_students_exam_subscription WHERE scorer_id = " . $logged_in['uid'];
            $query = $this->db->query($assigned_exam);
            $data['assigned_exam'] = $query->num_rows();
            $data['completed_exam'] = 12;
            $data['rating'] = 4;
            $data['result'] = $this->result_model->result_list(10, 0, "");
            $view_page = "scorer_dashboard";
        }
        //}
        $data['title'] = $this->lang->line('dashboard');
        $this->load->view('header', $data);
        $this->load->view($view_page, $data);
        $this->load->view('footer', $data);
    }

    public function config() {

        $logged_in = $this->session->userdata('logged_in');
        $acp = explode(',', $logged_in['setting']);
        if (!in_array('All', $acp)) {
            exit($this->lang->line('permission_denied'));
        }
        if ($this->config->item('frontend_write_admin') > $logged_in['su']) {
            exit($this->lang->line('permission_denied'));
        }

        if ($this->input->post('config_val')) {
            if ($this->input->post('force_write')) {
                if (chmod("./application/config/config.php", 0777)) {
                    
                }
            }

            $file = "./application/config/config.php";
            $content = $this->input->post('config_val');
            file_put_contents($file, $content);
            if ($this->input->post('force_write')) {
                if (chmod("./application/config/config.php", 0644)) {
                    
                }
            }

            redirect('dashboard/config');
        }

        $data['result'] = file_get_contents('./application/config/config.php');
        $data['title'] = $this->lang->line('config');
        $this->load->view('header', $data);
        $this->load->view('config', $data);
        $this->load->view('footer', $data);
    }

    public function css() {

        $logged_in = $this->session->userdata('logged_in');

        $logged_in = $this->session->userdata('logged_in');
        $acp = explode(',', $logged_in['setting']);
        if (!in_array('All', $acp)) {
            exit($this->lang->line('permission_denied'));
        }


        if ($this->input->post('config_val')) {
            if ($this->input->post('force_write')) {
                if (chmod("./css/style.css", 0777)) {
                    
                }
            }

            $file = "./css/style.css";
            $content = $this->input->post('config_val');
            file_put_contents($file, $content);
            if ($this->input->post('force_write')) {
                if (chmod("./css/style.css", 0644)) {
                    
                }
            }

            redirect('dashboard/css');
        }

        $data['result'] = file_get_contents('./css/style.css');
        $data['title'] = $this->lang->line('config');
        $this->load->view('header', $data);
        $this->load->view('css', $data);
        $this->load->view('footer', $data);
    }

}
