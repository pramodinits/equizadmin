<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {
    /* function __construct() {
      parent::__construct();
      $this->load->database();
      $this->load->helper('url');

      $this->load->model("user_model");
      $this->load->model("quiz_model");
      $this->load->model("api_model");
      $this->load->model("qbank_model");
      $this->load->model("result_model");
      $this->lang->load('basic', $this->config->item('language'));
      } */

    function __construct() {
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Headers: *");
        parent::__construct();
        $this->load->database();
        $this->load->helper('url');

        $controller = $this->router->fetch_class();
        $method = $this->router->fetch_method();

        ini_set('display_errors', 0);
        if (version_compare(PHP_VERSION, '5.3', '>=')) {
            error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED & ~E_STRICT & ~E_USER_NOTICE & ~E_USER_DEPRECATED);
        } else {
            error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_USER_NOTICE);
        }

//        if ($method != 'getToken') {
//            if (@$_SERVER['HTTP_EXAMTOKEN'] != 'f0e0ed4f0eddf542e27007a68b352635') {
//                $data['result'] = array();
//                $data['HasError'] = true;
//                $data['ErrorMessage'] = "Server key mismatch";
//                echo json_encode($data);
//                exit();
//            }
//        }

        $this->load->model("user_model");
        $this->load->model("quiz_model");
        $this->load->model("api_model");
        $this->load->model("qbank_model");
        $this->load->model("result_model");
//        $this->load->config('masterdata');
//        $this->config->load('masterdata', TRUE);
        $this->config->load('masterdata');
        $this->lang->load('basic', $this->config->item('language'));
    }

    public function getQuizInfo() {
        $data = file_get_contents('php://input');
        $json = json_decode($data);
        $license_type = array_values($this->config->item('license_type'));
        $payment_method = array_values($this->config->item('payment_method'));
        $license_country = $this->config->item('license_country');

        //get quiz info
        $quiz_id = $json->{'quiz_id'};
        $this->db->where('quid', $quiz_id);
        $auth = $this->db->get('kams_quiz');
        $quiz = $auth->row_array();

        //get time slot
        $this->db->where('qid', $quiz['quid']);
        $timeslot_query = $this->db->get('kams_quiz_duration');
        $timeslot_result = $timeslot_query->result_array();
        $quiz['timeslot'] = $timeslot_result;

        $result['StatusCode'] = "200";
        $result['HasError'] = false;
        $result['Message'] = "Master data found";
        $result['license_type'] = $license_type;
        $result['payment_method'] = $payment_method;
        $result['license_country'] = $license_country;
        $result['quiz_info'] = $quiz;
        echo json_encode($result);
        exit();
    }

    public function getToken() {
        $data['result']['tokenkey'] = "f0e0ed4f0eddf542e27007a68b352635";
        $data['HasError'] = false;
        $data['ErrorMessage'] = "Get your server key ";
        echo json_encode($data);
        exit();
    }

    public function index($api_key = '0') {
        exit('I am fine, No syntex error');
    }

    public function get_group() {
        $query = $this->db->query("select * from kams_group ");
        $result = $query->result_array();
        $groups = array();
        foreach ($result as $key => $val) {
            if ($val['price'] == 0) {
                $groups[] = array('gid' => $val['gid'], 'group_name' => $val['group_name']);
            } else {
                $groups[] = array('gid' => $val['gid'], 'group_name' => $val['group_name'] . ' Price:' . $val['price']);
            }
        }
        $group = array('groups' => $groups);
        print_r(json_encode($group));
    }

    public function reset_user() {
        $userdata = array(
            'first_name' => 'User',
            'last_name' => 'user',
            'password' => md5('12345')
        );
        $this->db->where('uid', '5');
        $this->db->update('kams_users', $userdata);
    }

    private function checkLogin($connection_key = "") {
        $this->db->where('connection_key', $connection_key);
        $auth = $this->db->get('kams_users');
//        $user = $auth->row_array();
        if ($auth->num_rows() == 0) {
            $result['StatusCode'] = "400";
            $result['HasError'] = true;
            $result['Message'] = "Invalid connection key.";
            echo json_encode($result);
            exit();
        } else {
            return true;
        }
    }

    public function login() {
        $data = file_get_contents('php://input');
        $json = json_decode($data);
        $email = $json->{'Email'};
        $password = $json->{'Password'};
        $ur = $this->user_model->login($email, $password);

        if ($ur['status'] == '1') {

            // row exist fetch userdata
            $user = $this->user_model->login($email, $password);


            // validate if user assigned to paid group
            if ($user['user']['price'] > '0') {

                // user assigned to paid group now validate expiry date.
                if ($user['user']['subscription_expired'] <= time()) {
                    // eubscription expired, redirect to payment page

                    echo $user['user']['uid'];
                    exit();
                }
            }
            $user['user']['base_url'] = base_url();
            $connection_key = $user['user']['uid'] . time();
            $uid = $user['user']['uid'];
            $user['user']['connection_key'] = md5($connection_key);
            // creating login cookie
            $userdata = array(
                'connection_key' => $user['user']['connection_key'],
                'connection_key_generatetime' => date('Y-m-d h:i:s'),
            );
            $this->db->where('uid', $uid);
            $this->db->update('kams_users', $userdata);
            $result['StatusCode'] = "200";
            $result['HasError'] = false;
            $result['Message'] = "Successfully Loggedin.";
            $result['result'] = $user;
            $result['result']['profileimagepath'] = base_url('/profileimage/');
        } else {
            $result['StatusCode'] = "400";
            $result['HasError'] = true;
            $result['Message'] = "Invalid email or password.";
        }
        echo json_encode($result);
        exit();
    }

    public function getCategoryDetails() {
        $data = file_get_contents('php://input');
        $json = json_decode($data);
        $exam_type = $json->{'exam_type'};
        $connection_key = $json->{'connection_key'};
        $this->checkLogin($connection_key);
        $country = $timeslot = array();

        //get quiz list
        $this->db->where('exam_type', $exam_type);
        //$this->db->join('kams_quiz_duration', 'kams_quiz_duration.qid=kams_quiz.quid');
        $auth = $this->db->get('kams_quiz');
        $quiz_list = $auth->result_array();
        $config['license_country'];
        $license_country = array_flip($this->config->item('license_country'));
        //get time slot
        foreach ($quiz_list as $key => $val) {
            $this->db->where('qid', $val['quid']);
            $timeslot_query = $this->db->get('kams_quiz_duration');
            $timeslot_result = $timeslot_query->result_array();

            if (!in_array($val['country'], $country)) {
                array_push($country, $val['country']);
            }
            $country_code[$val['country']] = $license_country[$val['country']];

            $quiz_list[$key]['timeslot'] = $timeslot_result;
        }
        if (!empty($quiz_list)) {
            $result['StatusCode'] = "200";
            $result['HasError'] = false;
            $result['Message'] = "Quiz list found";
            $result['quiz_list'] = $quiz_list;
            $result['country_list'] = $country;
            $result['country_code'] = $country_code;
        } else {
            $result['StatusCode'] = "200";
            $result['HasError'] = false;
            $result['Message'] = "No quiz list found";
        }
        echo json_encode($result);
        exit();
    }

    public function examRegister() {
        $data = file_get_contents('php://input');
        $json = json_decode($data);
        /*
          $connection_key = $json->{'connection_key'};
          if (!$connection_key) {
          $result['StatusCode'] = "400";
          $result['HasError'] = true;
          $result['Message'] = "No connecion key found";
          echo json_encode($result);
          exit();
          }
          $this->checkLogin($connection_key);
         */
        $update_id = $json->{'update_id'};
        $ins['company'] = $json->{'company'};
        $ins['license_type'] = $json->{'license_type'};
        $ins['license_number'] = $json->{'license_number'};
        $ins['license_country'] = $json->{'license_country'};
        $ins['vat'] = $json->{'vat'};
        $ins['address'] = $json->{'address'};
        $ins['postal_code'] = $json->{'postal_code'};
        $ins['city'] = $json->{'city'};
        $ins['country'] = $json->{'country'};
        $ins['dob'] = $json->{'dob'};
        $ins['payment_method'] = $json->{'payment_method'};
        $ins['first_name'] = $json->{'first_name'};
        $ins['last_name'] = $json->{'last_name'};
        $ins['contact_no'] = $json->{'contact_no'};
        $ins['email'] = $json->{'email'};
        $license_doc = $json->{'license_doc'};
        $profile_image = $json->{'profile_image'};
        $ins['password'] = md5($json->{'password'});
//        $ins['password'] = md5('123456');
        $ins['user_status'] = "Active";
        $ins['verify_code'] = 0;
        $ins['su'] = 2;

        //check email duplicacy
        $query = $this->db->query("select * from kams_users where email='{$ins['email']}' ");
        if ($query->num_rows() >= 1) {
            $result['StatusCode'] = "400";
            $result['HasError'] = true;
            $result['Message'] = "Email address already exist";
            echo json_encode($result);
            exit();
        }

        //user registration save/update
        if ($update_id) {
            $this->db->where('uid', $update_id);
            $this->db->update('kams_users', $ins);
        } else {
            $this->db->insert('kams_users', $ins);
            $ins_id = $this->db->insert_id();

            if ($ins_id) {
                //subscription information
                $ins_sub['quiz_id'] = $json->{'quiz_id'};
                $ins_sub['exam_type'] = $json->{'exam_type'};
                $ins_sub['exam_time'] = $json->{'exam_time'};
                $ins_sub['subscribe_date'] = date("Y-m-d");
                $ins_sub['user_id'] = $ins_id;
                $this->db->insert('kams_subscription', $ins_sub);
                $ins_id_sub = $this->db->insert_id();
            }
        }
        if (@$ins_id) {
            /* get user info */
            $this->db->where('uid', $ins_id);
            $auth = $this->db->get('kams_users');
            $user_info = $auth->row_array();
            /* get user info */
            $result['StatusCode'] = "200";
            $result['HasError'] = false;
            $result['Message'] = "Information saved successfully.";
            $result['user_info'] = $user_info;
        } elseif (@$update_id) {
            $result['StatusCode'] = "200";
            $result['HasError'] = false;
            $result['Message'] = "Information updated successfully.";
        } else {
            $result['StatusCode'] = "400";
            $result['HasError'] = false;
            $result['Message'] = "Error in saving.";
        }
        echo json_encode($result);
        exit();
    }
    
    private function base64_to_jpeg($base64_string) {
        $filter_data = explode(",", $base64_string);
        $data = str_replace(' ', '+', $filter_data[1]);
        $data = base64_decode($data);
        return $data;
    }

    public function fileUpload() {
       $data = file_get_contents('php://input');
       $json = json_decode($data);

       $uid = $json->{'user_id'};
       $uid = $_REQUEST['user_id'];
       $license_doc = @$_FILES['license_doc'];
       $profileimage = @$_FILES['profileimage'];

//        
//        $myfile = fopen("newfile.txt", "a+") or die("Unable to open file!");
//      
//        $txt = print_r($_FILES,true);
//        fwrite($myfile, $txt);
//        fclose($myfile);
//
//        $result['StatusCode'] = "400";
//        $result['HasError'] = false;
//        $result['Message'] = "Error in saving.";
//        echo json_encode($result);
//        exit();
       //save license doc & profile image
       if (!empty($license_doc)) {

           $config = array(
               'upload_path' => "./license_document/",
               'allowed_types' => "*"
           );

           $config['file_name'] = $uid . "_" . $_FILES["license_doc"]['name'];
           $this->load->library('upload', $config);

           if (!$this->upload->do_upload('license_doc')) {
               $error = array('error' => $this->upload->display_errors());
               $result = array();
               $result['StatusCode'] = "400";
               $result['HasError'] = true;
               $result['Message'] = "Error during file upload please try once again.";
               echo json_encode($result);
               exit();
           } else {
               $data = array('upload_data' => $this->upload->data());
           }
           $userdata_doc = array(
               'license_doc' => $config['file_name'],
               'licensedoc_verify' => 2 // 0->Not Uploaded 1-> varifify 2->Pending
           );
           $this->db->where('uid', $uid);
           $result = $this->db->update('kams_users', $userdata_doc);
           $result = array();
           $result['StatusCode'] = "200";
           $result['HasError'] = false;
           $result['Message'] = "Uploaded Successfully Please wait for admin Approveal";
           echo json_encode($result);
           exit();
       }
       if (!empty($profileimage)) {
           $config = array(
               'upload_path' => "./profileimage/",
               'allowed_types' => "*"
           );

           $config['file_name'] = $uid . "_" . $_FILES["profileimage"]['name'];
           $this->load->library('upload', $config);

           if (!$this->upload->do_upload('profileimage')) {
               $error = array('error' => $this->upload->display_errors());
               $result = array();
               $result['StatusCode'] = "400";
           }
       }
    }

    public function questionDetails() {
        $data = file_get_contents('php://input');
        $json = json_decode($data);
        $connection_key = $json->{'connection_key'};
        if (!$connection_key) {
            $result['StatusCode'] = "400";
            $result['HasError'] = true;
            $result['Message'] = "No connecion key found";
            echo json_encode($result);
            exit();
        }
        $this->checkLogin($connection_key);
        $question_id = $json->{'question_id'};
        $this->db->where('qid', $question_id);
        $auth = $this->db->get('kams_qbank');
        $qu_info = $auth->row_array();

        //question options list
        $this->db->where('qid', $qu_info['qid']);
        $auth = $this->db->get('kams_options');
        $option_info = $auth->result_array();

        $result['question_info'] = $qu_info;
        $result['options_info'] = $option_info;
        $result['video_base_url'] = base_url('/videos/');
        echo json_encode($result);
        exit();
    }

    public function viewRegister() {
        $data = file_get_contents('php://input');
        $json = json_decode($data);
        $connection_key = $json->{'connection_key'};
        if (!$connection_key) {
            $result['StatusCode'] = "400";
            $result['HasError'] = true;
            $result['Message'] = "No connecion key found";
            echo json_encode($result);
            exit();
        }
        $this->checkLogin($connection_key);
        $user_id = $json->{'user_id'};
        $this->db->where('uid', $user_id);
        $auth = $this->db->get('kams_users');
        $user = $auth->row_array();
        if ($auth->num_rows() == 0) {
            $result['StatusCode'] = "200";
            $result['HasError'] = false;
            $result['Message'] = "No user found.";
        } else {
            $result['StatusCode'] = "200";
            $result['HasError'] = false;
            $result['Message'] = "User Info found.";
            $result['user_info'] = $user;
        }
        echo json_encode($result);
        exit();
    }

    function quiz_list($connection_key = '', $uid = '', $limit = '0') {

        $this->db->where('uid', $uid);
        $this->db->where('connection_key', $connection_key);
        $this->db->join('account_type', 'account_type.account_id=kams_users.su');
        $auth = $this->db->get('kams_users');
        $user = $auth->row_array();
        if ($auth->num_rows() == 0) {
            exit('invalid Connection!');
        }
        $quiz = array('quiz' => $this->api_model->quiz_list($user, $limit));


        print_r(json_encode($quiz));
    }

    function stats($connection_key = '', $uid = '') {

        $this->db->where('uid', $uid);
        $this->db->where('connection_key', $connection_key);
        $this->db->join('account_type', 'account_type.account_id=kams_users.su');
        $auth = $this->db->get('kams_users');
        $user = $auth->row_array();
        if ($auth->num_rows() == 0) {
            exit('invalid Connection!');
        }

        $quiz = array(
            'no_quiz' => $this->api_model->no_quiz($user),
            'no_attempted' => $this->api_model->no_attempted($user),
            'no_pass' => $this->api_model->no_pass($user),
            'no_fail' => $this->api_model->no_fail($user)
        );


        print_r(json_encode($quiz));
    }

    function result_list($connection_key = '', $uid = '', $limit = '0') {

        $this->db->where('uid', $uid);
        $this->db->where('connection_key', $connection_key);
        $this->db->join('account_type', 'account_type.account_id=kams_users.su');
        $auth = $this->db->get('kams_users');
        $user = $auth->row_array();
        if ($auth->num_rows() == 0) {
            exit('invalid Connection!');
        }


        $result = array('result' => $this->api_model->result_list($user, $limit));


        print_r(json_encode($result));
    }

    function get_notification($connection_key = '', $uid = '', $limit = '0') {

        $this->db->where('uid', $uid);
        $this->db->where('connection_key', $connection_key);
        $auth = $this->db->get('kams_users');
        $user = $auth->row_array();
        if ($auth->num_rows() == 0) {
            exit('invalid Connection!');
        }


        $result = array('result' => $this->api_model->get_notification($user, $limit));


        print_r(json_encode($result));
    }

    public function myaccount($connection_key = '', $uid, $first_name, $last_name, $password) {

        $this->db->where('uid', $uid);
        $this->db->where('connection_key', $connection_key);
        $auth = $this->db->get('kams_users');
        $user = $auth->row_array();
        if ($auth->num_rows() == 0) {
            exit('invalid Connection!');
        }

        $userdata = array(
            'first_name' => urldecode($first_name),
            'last_name' => urldecode($last_name)
        );

        $this->db->where('uid', $uid);
        $this->db->where('connection_key', $connection_key);
        $this->db->update('kams_users', $userdata);
        exit("Information updated successfully");
    }

    public function validate_quiz($connection_key = '', $uid, $quid) {

        $this->db->where('uid', $uid);
        $this->db->where('connection_key', $connection_key);
        $this->db->join('account_type', 'account_type.account_id=kams_users.su');
        $auth = $this->db->get('kams_users');
        $user = $auth->row_array();
        if ($auth->num_rows() == 0) {
            exit('invalid Connection!');
        }
        $logged_in = $user;
        $gid = $logged_in['gid'];
        $uid = $logged_in['uid'];


        $data['quiz'] = $this->quiz_model->get_quiz($quid);
        // validate assigned group
        if (!in_array($gid, explode(',', $data['quiz']['gids']))) {
            exit('Quiz not assigned to your group');
        }
        // validate start end date/time
        if ($data['quiz']['start_date'] > time()) {
            exit('Quiz not available');
        }
        // validate start end date/time
        if ($data['quiz']['end_date'] < time()) {
            exit('Quiz ended');
        }

        // validate ip address
        if ($data['quiz']['ip_address'] != '') {
            $ip_address = explode(",", $data['quiz']['ip_address']);
            $myip = $_SERVER['REMOTE_ADDR'];
            if (!in_array($myip, $ip_address)) {
                exit('IP declined!');
            }
        }
        // validate maximum attempts
        $maximum_attempt = $this->quiz_model->count_result($quid, $uid);
        if ($data['quiz']['maximum_attempts'] <= $maximum_attempt) {
            exit('Reached maximum attempts!');
        }
        // if this quiz already opened by user then resume it
        $open_result = $this->quiz_model->open_result($quid, $uid);
        if ($open_result != '0') {
            $this->session->set_userdata('rid', $open_result);
            echo $open_result;
            exit();
        }
        // insert result row and get rid (result id)
        $rid = $this->quiz_model->insert_result($quid, $uid);

        echo $rid;
    }

    function attempt($connection_key = '', $uid, $rid) {

        $this->db->where('uid', $uid);
        $this->db->where('connection_key', $connection_key);
        $auth = $this->db->get('kams_users');
        $user = $auth->row_array();
        if ($auth->num_rows() == 0) {
            exit('invalid Connection!');
        }




        // get result and quiz info and validate time period
        $data['quiz'] = $this->quiz_model->quiz_result($rid);
        $data['saved_answers'] = $this->quiz_model->saved_answers($rid);




        // end date/time
        if ($data['quiz']['end_date'] < time()) {
            $this->api_model->submit_result($user, $rid);
            //$this->session->unset_userdata('rid');
            exit($this->lang->line('quiz_ended'));
        }


        // end date/time
        if (($data['quiz']['start_time'] + ($data['quiz']['duration'] * 60)) < time()) {
            $this->api_model->submit_result($user, $rid);
            //$this->session->unset_userdata('rid');

            exit($this->lang->line('time_over'));
        }
        // remaining time in seconds 
        $data['seconds'] = ($data['quiz']['duration'] * 60) - (time() - $data['quiz']['start_time']);
        // get questions
        $data['questions'] = $this->quiz_model->get_questions($data['quiz']['r_qids']);
        // get options
        $data['options'] = $this->quiz_model->get_options($data['quiz']['r_qids']);
        $data['title'] = $data['quiz']['quiz_name'];
        $data['connection_key'] = $connection_key;
        $data['uid'] = $uid;
        $data['rid'] = $rid;
        $this->load->view('quiz_attempt_android', $data);
    }

    function submit_quiz($connection_key = '', $uid, $rid) {

        $this->db->where('uid', $uid);
        $this->db->where('connection_key', $connection_key);
        $auth = $this->db->get('kams_users');
        $user = $auth->row_array();
        if ($auth->num_rows() == 0) {
            exit('invalid Connection!');
        }


        if ($this->api_model->submit_result($user, $rid)) {
            
        } else {
            
        }
        // $rid=$this->session->unset_userdata('rid');		

        echo "<script>Android.showToast('" . $rid . "');</script>";
    }

    function save_answer($connection_key = '', $uid, $rid) {

        $this->db->where('uid', $uid);
        $this->db->where('connection_key', $connection_key);
        $auth = $this->db->get('kams_users');
        $user = $auth->row_array();
        if ($auth->num_rows() == 0) {
            exit('invalid Connection!');
        }
        echo "<pre>";
        print_r($_POST);
        // insert user response and calculate scroe
        echo $this->api_model->insert_answer($user, $rid);
    }

    function set_ind_time($connection_key = '', $uid, $rid) {
        // update questions time spent
        $this->api_model->set_ind_time($rid);
    }

    function view_result($connection_key = '', $uid, $rid) {

        $this->db->where('uid', $uid);
        $this->db->where('connection_key', $connection_key);
        $auth = $this->db->get('kams_users');
        $user = $auth->row_array();
        if ($auth->num_rows() == 0) {
            exit('invalid Connection!');
        }

        $logged_in = $user;
        $data['logged_in'] = $user;
        $data['result'] = $this->result_model->get_result($rid);
        $data['title'] = $this->lang->line('result_id') . ' ' . $data['result']['rid'];
        if ($data['result']['view_answer'] == '1' || $logged_in['su'] == '1') {
            $this->load->model("quiz_model");
            $data['saved_answers'] = $this->quiz_model->saved_answers($rid);
            $data['questions'] = $this->quiz_model->get_questions($data['result']['r_qids']);
            $data['options'] = $this->quiz_model->get_options($data['result']['r_qids']);
        }
        // top 10 results of selected quiz
        $last_ten_result = $this->result_model->last_ten_result($data['result']['quid']);
        $value = array();
        $value[] = array('Quiz Name', 'Percentage (%)');
        foreach ($last_ten_result as $val) {
            $value[] = array($val['email'] . ' (' . $val['first_name'] . " " . $val['last_name'] . ')', intval($val['percentage_obtained']));
        }
        $data['value'] = json_encode($value);

        // time spent on individual questions
        $correct_incorrect = explode(',', $data['result']['score_individual']);
        $qtime[] = array($this->lang->line('question_no'), $this->lang->line('time_in_sec'));
        foreach (explode(",", $data['result']['individual_time']) as $key => $val) {
            if ($val == '0') {
                $val = 1;
            }
            if ($correct_incorrect[$key] == "1") {
                $qtime[] = array($this->lang->line('q') . " " . ($key + 1) . ") - " . $this->lang->line('correct') . " ", intval($val));
            } else if ($correct_incorrect[$key] == '2') {
                $qtime[] = array($this->lang->line('q') . " " . ($key + 1) . ") - " . $this->lang->line('incorrect') . "", intval($val));
            } else if ($correct_incorrect[$key] == '0') {
                $qtime[] = array($this->lang->line('q') . " " . ($key + 1) . ") -" . $this->lang->line('unattempted') . " ", intval($val));
            } else if ($correct_incorrect[$key] == '3') {
                $qtime[] = array($this->lang->line('q') . " " . ($key + 1) . ") - " . $this->lang->line('pending_evaluation') . " ", intval($val));
            }
        }
        $data['qtime'] = json_encode($qtime);
        $data['percentile'] = $this->result_model->get_percentile($data['result']['quid'], $data['result']['uid'], $data['result']['score_obtained']);




        $this->load->view('view_result_android', $data);
    }

    function register($email, $first_name, $last_name, $password, $contact_no, $gid) {

        if (!$this->config->item('user_registration')) {

            exit('Registration is closed by administrator');
        }
        $email = urldecode($email);
        $query = $this->db->query("select * from kams_users where email='$email' ");
        if ($query->num_rows() >= 1) {
            exit('Email address already exist');
        }
        if ($this->api_model->register($email, $first_name, $last_name, $password, $contact_no, $gid)) {
            if ($this->config->item('verify_email')) {
                exit($this->lang->line('account_registered_email_sent'));
            } else {
                exit($this->lang->line('account_registered'));
            }
        } else {
            exit($this->lang->line('error_to_add_data'));
        }
    }

    function forgot($user_email) {

        $user_email = urldecode($user_email);
        if ($this->api_model->reset_password($user_email)) {
            exit($this->lang->line('password_updated'));
        } else {
            exit($this->lang->line('email_doesnot_exist'));
        }
    }

    function logout($connection_key = '', $uid) {
        $userdata = array('connection_key' => '');
        $this->db->where('uid', $uid);
        $this->db->where('connection_key', $connection_key);
        $auth = $this->db->update('kams_users', $userdata);
        exit("Account logged out successfully!");
    }

}
