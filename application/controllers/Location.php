<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Location extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->helper('url');
//        $this->load->model("user_model");
//        $this->load->model("examiner_model");
//        $this->load->model("account_model");
        $this->load->model(array('location_model', 'user_model', 'examiner_model', 'account_model'));
        $this->lang->load('basic', $this->config->item('language'));
        $this->load->library("pagination");
        $this->config->load('masterdata');
        // redirect if not loggedin
        if (!$this->session->userdata('logged_in')) {
            redirect('login');
        }
        $logged_in = $this->session->userdata('logged_in');
        if ($logged_in['base_url'] != base_url()) {
            $this->session->unset_userdata('logged_in');
            redirect('login');
        }
    }

    public function paginatelinks() {
        $config['full_tag_open'] = '<div class="dataTables_paginate paging_bs_normal pull-right" id="datatable-default_paginate"><ul class="pagination justify-content-end p-4">';
        $config['full_tag_close'] = '</ul></div><!--pagination-->';

        $config['first_link'] = '&laquo; First';
        $config['first_tag_open'] = '<li class="page-item">';
        $config['first_tag_close'] = '</li>';

        $config['last_link'] = 'Last &raquo;';
        $config['last_tag_open'] = '<li class="page-item">';
        $config['last_tag_close'] = '</li>';

        $config['next_link'] = '<span class="page-link">&GT;</span>';
        $config['next_tag_open'] = '<li class="page-item">';
        $config['next_tag_close'] = '</li>';

        $config['prev_link'] = '<span class="page-link">&LT;</span>';
        $config['prev_tag_open'] = '<li class="page-item">';
        $config['prev_tag_close'] = '</li>';

        $config['cur_tag_open'] = '<li class="active"><span class="page-link-active">';
        $config['cur_tag_close'] = '</span></li>';

        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        return $config;
    }

    public function index($limit = '0') {
        $logged_in = $this->session->userdata('logged_in');

        /*$user_p = explode(',', $logged_in['users']);
        if (!in_array('List_all', $user_p)) {
            exit($this->lang->line('permission_denied'));
        }*/

        $data['limit'] = $limit;
        $data['title'] = $this->lang->line('citylist');
        // fetching user list
        //pagination links
        $config = array();
        $config = $this->paginatelinks();
        $config["base_url"] = site_url('location/index');
        $cnt_flg = "1";
        $config["total_rows"] = $this->location_model->city_list($config["per_page"], $start, $cnt_flg = 1);
        $config["per_page"] = 10;
        $config["uri_segment"] = 3;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = round($choice);
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $start = $page;
        $data["links"] = $this->pagination->create_links();
        //pagination links

        $data['result'] = $this->location_model->city_list($config["per_page"], $start);
        $this->load->view('header', $data);
        $this->load->view('city_list', $data);
        $this->load->view('footer', $data);
    }

    public function new_city($city_id = "") {
        $logged_in = $this->session->userdata('logged_in');
        /*$user_p = explode(',', $logged_in['users']);
        if (!in_array('Add', $user_p)) {
            exit($this->lang->line('permission_denied'));
        }*/
        if ($city_id) {
            $data = array();
            $data['result'] = $this->location_model->get_city('kams_city', 'id_city', $city_id);
        }

        $data['title'] = $this->lang->line('add_new') . ' ' . $this->lang->line('city');
        $this->load->view('header', $data);
        $this->load->view('new_city', $data);
        $this->load->view('footer', $data);
    }

    public function insert_city() {
        $logged_in = $this->session->userdata('logged_in');
        /*$user_p = explode(',', $logged_in['users']);
        if (!in_array('Add', $user_p)) {
            exit($this->lang->line('permission_denied'));
        }*/
        $this->load->library('form_validation');
        if ($this->input->server('REQUEST_METHOD') == 'GET') {
            $this->session->set_flashdata('message', "<div class='alert alert-danger'>" . validation_errors() . " </div>");
            redirect('location/new_city');
        }
        if ($this->location_model->insert_city()) {
            $this->session->set_flashdata('message', "<div class='alert alert-success'>" . $this->lang->line('data_added_successfully') . " </div>");
        } else {
            $this->session->set_flashdata('message', "<div class='alert alert-danger'>" . $this->lang->line('error_to_add_data') . " </div>");
        }
        redirect('location/');
    }

    public function remove_city() {
        $logged_in = $this->session->userdata('logged_in');
        /*$user_p = explode(',', $logged_in['users']);
        if (!in_array('Remove', $user_p)) {
            exit($this->lang->line('permission_denied'));
        }*/
        $city_id = $this->input->post('city_id');
        $this->db->where('id_city', $city_id);
        if ($this->db->delete('kams_city')) {
            echo "success";
        } else {
            echo "failure";
        }
        exit();
    }
    
    public function remove_location() {
        $logged_in = $this->session->userdata('logged_in');
        /*$user_p = explode(',', $logged_in['users']);
        if (!in_array('Remove', $user_p)) {
            exit($this->lang->line('permission_denied'));
        }*/
        $location_id = $this->input->post('location_id');
        $this->db->where('id_location', $location_id);
        if ($this->db->delete('kams_location')) {
            echo "success";
        } else {
            echo "failure";
        }
        exit();
    }
    
    public function location_list() {
        $logged_in = $this->session->userdata('logged_in');

        /*$user_p = explode(',', $logged_in['users']);
        if (!in_array('List_all', $user_p)) {
            exit($this->lang->line('permission_denied'));
        }*/

        $data['limit'] = $limit;
        $data['title'] = $this->lang->line('location_list');
        // fetching user list
        //pagination links
        $config = array();
        $config = $this->paginatelinks();
        $config["base_url"] = site_url('location/location_list');
        $cnt_flg = "1";
        $config["total_rows"] = $this->location_model->location_list($config["per_page"], $start, $cnt_flg = 1);
        $config["per_page"] = 10;
        $config["uri_segment"] = 3;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = round($choice);
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $start = $page;
        $data["links"] = $this->pagination->create_links();
        //pagination links

        $data['result'] = $this->location_model->location_list($config["per_page"], $start);
        $data['city_list'] = $this->examiner_model->dataListingKeyvalue("kams_city", "id_city", "city_name");
        $this->load->view('header', $data);
        $this->load->view('location_list', $data);
        $this->load->view('footer', $data);
    }
    
    public function new_location($location_id = "") {
        $logged_in = $this->session->userdata('logged_in');
        /*$user_p = explode(',', $logged_in['users']);
        if (!in_array('Add', $user_p)) {
            exit($this->lang->line('permission_denied'));
        }*/
        if ($location_id) {
            $data = array();
            $data['result'] = $this->location_model->get_city('kams_location', 'id_location', $location_id);
        }
        $data['city_list'] = $this->examiner_model->dataListingKeyvalue("kams_city", "id_city", "city_name");
        $data['title'] = $this->lang->line('location_new');
        $this->load->view('header', $data);
        $this->load->view('new_location', $data);
        $this->load->view('footer', $data);
    }
    
    public function insert_location() {
        $logged_in = $this->session->userdata('logged_in');
        /*$user_p = explode(',', $logged_in['users']);
        if (!in_array('Add', $user_p)) {
            exit($this->lang->line('permission_denied'));
        }*/
        $this->load->library('form_validation');
        if ($this->input->server('REQUEST_METHOD') == 'GET') {
            $this->session->set_flashdata('message', "<div class='alert alert-danger'>" . validation_errors() . " </div>");
            redirect('location/new_location');
        }
        //get records
        $location_id = $this->input->post('id_location');
        $ins_data = $this->input->post('location');
        $ins_data['added_by'] = $logged_in['uid'];
        if ($location_id) {
            $this->db->where('id_location', $location_id);
            $this->db->update('kams_location', $ins_data);
            $this->session->set_flashdata('message', "<div class='alert alert-success'>Data updated successfully! </div>");
            redirect('location/location_list');
        } else {
            if ($this->db->insert('kams_location', $ins_data)) {
                $location_id = $this->db->insert_id();
                $this->session->set_flashdata('message', "<div class='alert alert-success'>" . $this->lang->line('data_added_successfully') . " </div>");
                redirect('location/location_list');
            } else {
                $this->session->set_flashdata('message', "<div class='alert alert-danger'>" . $this->lang->line('error_to_add_data') . " </div>");
                redirect('location/new_location');
            }
        }
    }

}
