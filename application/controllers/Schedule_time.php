<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Schedule_time extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->helper('url');
        $this->load->model("user_model");
        $this->load->model("schedule_model");
        $this->load->model("account_model");
        $this->lang->load('basic', $this->config->item('language'));
        $this->config->load('masterdata');
        // redirect if not loggedin
        if (!$this->session->userdata('logged_in')) {
            redirect('login');
        }
        $logged_in = $this->session->userdata('logged_in');
        if ($logged_in['base_url'] != base_url()) {
            $this->session->unset_userdata('logged_in');
            redirect('login');
        }
    }

    public function index($limit = '0') {
        $logged_in = $this->session->userdata('logged_in');
        $user_p = explode(',', $logged_in['users']);
        if (!in_array('List_all', $user_p)) {
            exit($this->lang->line('permission_denied'));
        }
    }

    public function addSchedule($quiz_id = "") {
        $logged_in = $this->session->userdata('logged_in');
        $user_p = explode(',', $logged_in['users']);
        if (!in_array('Add', $user_p)) {
            exit($this->lang->line('permission_denied'));
        }
        if (empty($quiz_id)) {
            exit($this->lang->line('permission_denied'));
        }
        $data['title'] = $this->lang->line('add') . ' ' . $this->lang->line('schedule_time');
        $data['quiz_id'] = $quiz_id;


        //get scheduled event 
        $this->db->where('quiz_id', $quiz_id);
        $query = $this->db->get('kams_schedule_quiz');
        $schedule_list = $query->result_array();
        $events[] = array();
        foreach ($schedule_list as $k => $v) {
            $events[] = array(
                "scheduleId" => $v['schedule_id'],
                "title" => $v['schedule_title'],
                "start" => $v['start_schedule'],
                "end" => $v['end_schedule'],
                'allDay'=>false,
                'overlap'=>false
            );
        }
        $res = array();
       foreach ($schedule_list as $k => $v) {
           $k = array();
           $k['title'] = $v['schedule_title'];
           $k['insertId'] = $v['schedule_id'];
          // $k['start'] = "2020-01-23 3:00:00";
           $k['start'] =  $v['start_schedule'];
           $k['end'] = $v['end_schedule'];
           $k['allDay'] = false;
          // $k['end'] = "2020-01-23 4:00:00";
           array_push($res, $k);
       }
        $data['events'] = json_encode($events);

        $this->load->view('header', $data);
        $this->load->view('add_schedule', $data);
        $this->load->view('footer', $data);
    }

    public function insert_schedule() {
        $ins['quiz_id'] = $this->input->post('quiz_id');
        $ins['schedule_title'] = $this->input->post('schedule_title');
        $ins['schedule_date'] = $this->input->post('schedule_date');
        $ins['start_schedule'] = $this->input->post('start_schedule');
        $ins['end_schedule'] = $this->input->post('end_schedule');
        $ins['display_event_start'] = $this->input->post('display_event_start');
        $ins['display_event_end'] = $this->input->post('display_event_end');
        if ($ins['quiz_id']) {
            $this->db->insert('kams_schedule_quiz', $ins);
            $result = array();
            $result['hasError'] = false;
            $result['Message'] = "Success";
        } else {
            $result = array();
            $result['hasError'] = true;
            $result['Message'] = "Failure";
        }
        echo json_encode($result);
        exit();
    }
    
    public function delete_schedule() {
        $schedule_id = $this->input->post('schedule_id');
        if ($this->schedule_model->remove_schedule($schedule_id)) {
            $result = array();
            $result['hasError'] = false;
            $result['Message'] = "Success";
        } else {
            $result = array();
            $result['hasError'] = true;
            $result['Message'] = "Failure";
        }
        echo json_encode($result);
        exit();
    }

}
