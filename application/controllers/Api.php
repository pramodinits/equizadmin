<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {
    /* function __construct() {
      parent::__construct();
      $this->load->database();
      $this->load->helper('url');

      $this->load->model("user_model");
      $this->load->model("quiz_model");
      $this->load->model("api_model");
      $this->load->model("qbank_model");
      $this->load->model("result_model");
      $this->lang->load('basic', $this->config->item('language'));
      } */

    function __construct() {
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Headers: *");
        parent::__construct();
        $this->load->database();
        $this->load->helper('url');

        $controller = $this->router->fetch_class();
        $method = $this->router->fetch_method();

        ini_set('display_errors', 0);
        if (version_compare(PHP_VERSION, '5.3', '>=')) {
            error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED & ~E_STRICT & ~E_USER_NOTICE & ~E_USER_DEPRECATED);
        } else {
            error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_USER_NOTICE);
        }

//        if ($method != 'getToken') {
//            if (@$_SERVER['HTTP_EXAMTOKEN'] != 'f0e0ed4f0eddf542e27007a68b352635') {
//                $data['result'] = array();
//                $data['HasError'] = true;
//                $data['Message'] = "Server key mismatch";
//                echo json_encode($data);
//                exit();
//            }
//        }

        $this->load->model("user_model");
        $this->load->model("quiz_model");
        $this->load->model("api_model");
        $this->load->model("qbank_model");
        $this->load->model("result_model");
//        $this->load->config('masterdata');
//        $this->config->load('masterdata', TRUE);
        $this->config->load('masterdata');
        $this->lang->load('basic', $this->config->item('language'));
    }

    public function getQuizInfo() {
        $data = file_get_contents('php://input');
        $json = json_decode($data);
        $license_type = array_values($this->config->item('license_type'));
        $payment_method = array_values($this->config->item('payment_method'));
        $license_country = $this->config->item('license_country');

        //get quiz info
        $quiz_id = $json->{'quiz_id'};
        $this->db->where('quid', $quiz_id);
        $auth = $this->db->get('kams_quiz');
        $quiz = $auth->row_array();

        //get time slot
        $this->db->where('qid', $quiz['quid']);
        $timeslot_query = $this->db->get('kams_quiz_duration');
        $timeslot_result = $timeslot_query->result_array();
        $quiz['timeslot'] = $timeslot_result;

        $result['StatusCode'] = "200";
        $result['HasError'] = false;
        $result['Message'] = "Master data found";
        $result['license_type'] = $license_type;
        $result['id_type'] = array("National Id", "Iqama");
        $result['payment_method'] = $payment_method;
        $result['license_country'] = $license_country;
        $result['quiz_info'] = $quiz;
        echo json_encode($result);
        exit();
    }

    public function getToken() {
        $data['result']['tokenkey'] = "f0e0ed4f0eddf542e27007a68b352635";
        $data['HasError'] = false;
        $data['Message'] = "Get your server key ";
        echo json_encode($data);
        exit();
    }

    public function index($api_key = '0') {
        exit('I am fine, No syntex error');
    }

    public function get_group() {
        $query = $this->db->query("select * from kams_group ");
        $result = $query->result_array();
        $groups = array();
        foreach ($result as $key => $val) {
            if ($val['price'] == 0) {
                $groups[] = array('gid' => $val['gid'], 'group_name' => $val['group_name']);
            } else {
                $groups[] = array('gid' => $val['gid'], 'group_name' => $val['group_name'] . ' Price:' . $val['price']);
            }
        }
        $group = array('groups' => $groups);
        print_r(json_encode($group));
    }

    public function reset_user() {
        $userdata = array(
            'first_name' => 'User',
            'last_name' => 'user',
            'password' => md5('12345')
        );
        $this->db->where('uid', '5');
        $this->db->update('kams_users', $userdata);
    }

    private function checkLogin($connection_key = "") {
        $this->db->where('connection_key', $connection_key);
        $auth = $this->db->get('kams_users');
//        $user = $auth->row_array();
        if ($auth->num_rows() == 0) {
            $result['StatusCode'] = "400";
            $result['HasError'] = true;
            $result['Message'] = "Invalid connection key.";
            echo json_encode($result);
            exit();
        } else {
            return true;
        }
    }

    public function login() {
        $data = file_get_contents('php://input');
        $json = json_decode($data);
        $email = $json->{'Email'};
        $password = $json->{'Password'};
        $ur = $this->user_model->login($email, $password);

        if ($ur['status'] == '1') { //
            // row exist fetch userdata
            $user = $this->user_model->login($email, $password);

//print_r($user); exit();
            // validate if user assigned to paid group
            if ($user['user']['price'] > '0') {

                // user assigned to paid group now validate expiry date.
                if ($user['user']['subscription_expired'] <= time()) {
                    // eubscription expired, redirect to payment page

                    echo $user['user']['uid'];
                    exit();
                }
            }
            $user['user']['base_url'] = base_url();
            $connection_key = $user['user']['uid'] . time();
            $uid = $user['user']['uid'];
            $user['user']['connection_key'] = md5($connection_key);
            // creating login cookie
            $userdata = array(
                'connection_key' => $user['user']['connection_key'],
                'connection_key_generatetime' => date('Y-m-d h:i:s'),
            );
            $this->db->where('uid', $uid);
            $this->db->update('kams_users', $userdata);
            $result['StatusCode'] = "200";
            $result['HasError'] = false;
            $result['Message'] = "Successfully Loggedin.";
            $result['result'] = $user;
            $result['result']['user']['profileimagepath'] = base_url('/profileimage/');
        } else {
            $result['StatusCode'] = "400";
            $result['HasError'] = true;
            $result['Message'] = "Invalid email or password.";
        }
        echo json_encode($result);
        exit();
    }

    public function getCategoryDetails() {
        $data = file_get_contents('php://input');
        $json = json_decode($data);
        $exam_type = $json->{'exam_type'};
        $connection_key = $json->{'connection_key'};
        $this->checkLogin($connection_key);
        $country = $timeslot = array();

        //get quiz list
        $this->db->where('exam_type', $exam_type);
        //$this->db->join('kams_quiz_duration', 'kams_quiz_duration.qid=kams_quiz.quid');
        $auth = $this->db->get('kams_quiz');
        $quiz_list = $auth->result_array();
        $config['license_country'];
        $license_country = array_flip($this->config->item('license_country'));
        //get time slot
        foreach ($quiz_list as $key => $val) {
            $this->db->where('qid', $val['quid']);
            $timeslot_query = $this->db->get('kams_quiz_duration');
            $timeslot_result = $timeslot_query->result_array();

            if (!in_array($val['country'], $country)) {
                array_push($country, $val['country']);
            }
            $country_code[$val['country']] = $license_country[$val['country']];

            $quiz_list[$key]['timeslot'] = $timeslot_result;
        }
        if (!empty($quiz_list)) {
            $result['StatusCode'] = "200";
            $result['HasError'] = false;
            $result['Message'] = "Quiz list found";
            $result['quiz_list'] = $quiz_list;
            $result['country_list'] = $country;
            $result['country_code'] = $country_code;
        } else {
            $result['StatusCode'] = "200";
            $result['HasError'] = false;
            $result['Message'] = "No quiz list found";
        }
        echo json_encode($result);
        exit();
    }

    public function examRegister() {
        $data = file_get_contents('php://input');
        $json = json_decode($data);
        $update_id = $json->{'update_id'};
        if (@$json->{'email'} && @$json->{'first_name'} && @$json->{'password'}) {
            
        } else {
            $result = array();
            $result['StatusCode'] = "400";
            $result['HasError'] = true;
            $result['Message'] = "Invalid Parameter";
            echo json_encode($result);
            exit();
        }
        $ins['company'] = @$json->{'company'};
        $ins['id_type'] = @$json->{'id_type'};
        $ins['id_number'] = @$json->{'id_number'};
        $ins['license_type'] = @$json->{'license_type'};
        $ins['license_number'] = @$json->{'license_number'};
        $ins['license_country'] = @$json->{'license_country'};
        $ins['vat'] = @$json->{'vat'};
        $ins['address'] = @$json->{'address'};
        $ins['postal_code'] = @$json->{'postal_code'};
        $ins['passport_num'] = @$json->{'passport_number'};
        $ins['city'] = @$json->{'city'};
        $ins['country'] = @$json->{'country'};
        $ins['dob'] = @$json->{'dob'};
        $ins['exam_category'] = @$json->{'exam_category'};
        $ins['payment_method'] = @$json->{'payment_method'};
        $ins['first_name'] = @$json->{'first_name'};
        $ins['middle_name'] = @$json->{'middle_name'};
        $ins['last_name'] = @$json->{'last_name'};
        $ins['contact_no'] = @$json->{'contact_no'};
        $ins['email'] = @$json->{'email'};
        $license_doc = @$json->{'license_doc'};
        $profile_image = @$json->{'profile_image'};
        $pasword = @$json->{'password'};
        $ins['password'] = md5(trim($pasword));
        //$ins['password'] = trim($json->{'password'});
//        $ins['password'] = md5('123456');
        $ins['user_status'] = "Active";
        $ins['phone_verify'] = $phone_verify = rand(10000, 99999);
        $ins['email_verify'] = $email_verify = rand(10000, 99999);
        $ins['verify_code'] = 0;
        $ins['su'] = 2;

        //check email duplicacy
        $query = $this->db->query("select * from kams_users where email='{$ins['email']}' ");
        if ($query->num_rows() >= 1) {
            $result['StatusCode'] = "400";
            $result['HasError'] = true;
            $result['Message'] = "Email address already exist";
            echo json_encode($result);
            exit();
        }

        //user registration save/update
        if ($update_id) {
            $this->db->where('uid', $update_id);
            $this->db->update('kams_users', $ins);
        } else {
            $this->db->insert('kams_users', $ins);
            $ins_id = $this->db->insert_id();

            if ($ins_id) {
                //subscription information
                $ins_sub['quiz_id'] = $json->{'quiz_id'};
                $ins_sub['exam_type'] = $json->{'exam_type'};
                $ins_sub['exam_time'] = $json->{'exam_time'};
                $ins_sub['subscribe_date'] = date("Y-m-d");
                $ins_sub['user_id'] = $ins_id;
                $this->db->insert('kams_subscription', $ins_sub);
                $ins_id_sub = $this->db->insert_id();
            }
        }
        if (@$ins_id) {
            /* get user info */
            $this->db->where('uid', $ins_id);
            $auth = $this->db->get('kams_users');
            $user_info = $auth->row_array();
            /* get user info */

            $subject = "GACA Elpt - Registration";

            $dataEmail = array();
            $dataEmail['name'] = $user_info['first_name']; //$data_ins['firstname'];
            $dataEmail['phone_verify'] = $user_info['phone_verify']; //$data_ins['firstname'];
            $dataEmail['email_verify'] = $user_info['email_verify']; //$data_ins['firstname'];
            $dataEmail['password'] = $pasword; //$data_ins['firstname'];

            $body = $this->load->view('mail/registration', $dataEmail, true);
            $to = $user_info['email'];
            $this->sendEmail($subject, $body, $to);

            $result['StatusCode'] = "200";
            $result['HasError'] = false;
            $result['Message'] = "Information saved successfully.";
            $result['user_info'] = $user_info;
        } elseif (@$update_id) {
            $result['StatusCode'] = "200";
            $result['HasError'] = false;
            $result['Message'] = "Information updated successfully.";
        } else {
            $result['StatusCode'] = "400";
            $result['HasError'] = false;
            $result['Message'] = "Error in saving.";
        }
        echo json_encode($result);
        exit();
    }

    private function base64_to_jpeg($base64_string) {
        $filter_data = explode(",", $base64_string);
        $data = str_replace(' ', '+', $filter_data[1]);
        $data = base64_decode($data);
        return $data;
    }

    public function fileUpload() {
        $data = file_get_contents('php://input');
        $json = json_decode($data);

        $uid = $json->{'user_id'};
        $uid = $_REQUEST['user_id'];
        $license_doc = @$_FILES['license_doc'];
        $profileimage = @$_FILES['profileimage'];



//        $result['StatusCode'] = "400";
//        $result['HasError'] = false;
//        $result['Message'] = "Error in saving.";
//        echo json_encode($result);
//        exit();
        //save license doc & profile image
        if (!empty($license_doc)) {

            $config = array(
                'upload_path' => "./license_document/",
                'allowed_types' => "*"
            );

            $config['file_name'] = $uid . "_" . $_FILES["license_doc"]['name'];
            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('license_doc')) {
                $error = array('error' => $this->upload->display_errors());
                $result = array();
                $result['StatusCode'] = "400";
                $result['HasError'] = true;
                $result['Message'] = "Error during file upload please try once again.";
                echo json_encode($result);
                exit();
            } else {
                $data = array('upload_data' => $this->upload->data());
            }
            $userdata_doc = array(
                'license_doc' => $config['file_name'],
                'licensedoc_verify' => 2 // 0->Not Uploaded 1-> varifify 2->Pending
            );
            $this->db->where('uid', $uid);
            $result = $this->db->update('kams_users', $userdata_doc);
            $result = array();
            $result['StatusCode'] = "200";
            $result['HasError'] = false;
            $result['Message'] = "Uploaded Successfully Please wait for admin Approval";
            echo json_encode($result);
            exit();
        }
        if (!empty($profileimage)) {
            $config = array(
                'upload_path' => "./profileimage/",
                'allowed_types' => "*"
            );

            $config['file_name'] = $uid . "_" . $_FILES["profileimage"]['name'];
            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('profileimage')) {
                $error = array('error' => $this->upload->display_errors());
                $result = array();
                $result['StatusCode'] = "400";
            } else {
                $userdata_doc = array(
                    'photo' => $config['file_name'],
                );
                $this->db->where('uid', $uid);
                $result = $this->db->update('kams_users', $userdata_doc);
                $result = array();
                $result['StatusCode'] = "200";
                $result['HasError'] = false;
                $result['result']['photo'] = $config['file_name'];
                $result['result']['profileimagepath'] = base_url('/profileimage/');
                $result['Message'] = "Uploaded Successfully";
                echo json_encode($result);
                exit();
            }
        }
    }

    public function questionDetails() {
        $data = file_get_contents('php://input');
        $json = json_decode($data);
        $connection_key = $json->{'connection_key'};
        if (!$connection_key) {
            $result['StatusCode'] = "400";
            $result['HasError'] = true;
            $result['Message'] = "No connecion key found";
            echo json_encode($result);
            exit();
        }
        $this->checkLogin($connection_key);
        $question_id = $json->{'question_id'};
        $this->db->where('qid', $question_id);
        $auth = $this->db->get('kams_qbank');
        $qu_info = $auth->row_array();

        //question options list
        $this->db->where('qid', $qu_info['qid']);
        $auth = $this->db->get('kams_options');
        $option_info = $auth->result_array();

        $result['question_info'] = $qu_info;
        $result['options_info'] = $option_info;
        $result['video_base_url'] = base_url('/videos/');
        echo json_encode($result);
        exit();
    }

    public function viewRegister() {
        $data = file_get_contents('php://input');
        $json = json_decode($data);
        $connection_key = $json->{'connection_key'};
        if (!$connection_key) {
            $result['StatusCode'] = "400";
            $result['HasError'] = true;
            $result['Message'] = "No connecion key found";
            echo json_encode($result);
            exit();
        }
        $this->checkLogin($connection_key);
        $user_id = $json->{'user_id'};
        $this->db->where('uid', $user_id);
        $auth = $this->db->get('kams_users');
        $user = $auth->row_array();
        if ($auth->num_rows() == 0) {
            $result['StatusCode'] = "200";
            $result['HasError'] = false;
            $result['Message'] = "No user found.";
        } else {
            $result['StatusCode'] = "200";
            $result['HasError'] = false;
            $result['Message'] = "User Info found.";
            $result['user_info'] = $user;
        }
        echo json_encode($result);
        exit();
    }

    function quiz_list($connection_key = '', $uid = '', $limit = '0') {

        $this->db->where('uid', $uid);
        $this->db->where('connection_key', $connection_key);
        $this->db->join('account_type', 'account_type.account_id=kams_users.su');
        $auth = $this->db->get('kams_users');
        $user = $auth->row_array();
        if ($auth->num_rows() == 0) {
            exit('invalid Connection!');
        }
        $quiz = array('quiz' => $this->api_model->quiz_list($user, $limit));


        print_r(json_encode($quiz));
    }

    function stats($connection_key = '', $uid = '') {

        $this->db->where('uid', $uid);
        $this->db->where('connection_key', $connection_key);
        $this->db->join('account_type', 'account_type.account_id=kams_users.su');
        $auth = $this->db->get('kams_users');
        $user = $auth->row_array();
        if ($auth->num_rows() == 0) {
            exit('invalid Connection!');
        }

        $quiz = array(
            'no_quiz' => $this->api_model->no_quiz($user),
            'no_attempted' => $this->api_model->no_attempted($user),
            'no_pass' => $this->api_model->no_pass($user),
            'no_fail' => $this->api_model->no_fail($user)
        );


        print_r(json_encode($quiz));
    }

    function result_list($connection_key = '', $uid = '', $limit = '0') {

        $this->db->where('uid', $uid);
        $this->db->where('connection_key', $connection_key);
        $this->db->join('account_type', 'account_type.account_id=kams_users.su');
        $auth = $this->db->get('kams_users');
        $user = $auth->row_array();
        if ($auth->num_rows() == 0) {
            exit('invalid Connection!');
        }


        $result = array('result' => $this->api_model->result_list($user, $limit));


        print_r(json_encode($result));
    }

    function get_notification($connection_key = '', $uid = '', $limit = '0') {

        $this->db->where('uid', $uid);
        $this->db->where('connection_key', $connection_key);
        $auth = $this->db->get('kams_users');
        $user = $auth->row_array();
        if ($auth->num_rows() == 0) {
            exit('invalid Connection!');
        }


        $result = array('result' => $this->api_model->get_notification($user, $limit));


        print_r(json_encode($result));
    }

    public function myaccount($connection_key = '', $uid, $first_name, $last_name, $password) {

        $this->db->where('uid', $uid);
        $this->db->where('connection_key', $connection_key);
        $auth = $this->db->get('kams_users');
        $user = $auth->row_array();
        if ($auth->num_rows() == 0) {
            exit('invalid Connection!');
        }

        $userdata = array(
            'first_name' => urldecode($first_name),
            'last_name' => urldecode($last_name)
        );

        $this->db->where('uid', $uid);
        $this->db->where('connection_key', $connection_key);
        $this->db->update('kams_users', $userdata);
        exit("Information updated successfully");
    }

    public function validate_quiz($connection_key = '', $uid, $quid) {

        $this->db->where('uid', $uid);
        $this->db->where('connection_key', $connection_key);
        $this->db->join('account_type', 'account_type.account_id=kams_users.su');
        $auth = $this->db->get('kams_users');
        $user = $auth->row_array();
        if ($auth->num_rows() == 0) {
            exit('invalid Connection!');
        }
        $logged_in = $user;
        $gid = $logged_in['gid'];
        $uid = $logged_in['uid'];


        $data['quiz'] = $this->quiz_model->get_quiz($quid);
        // validate assigned group
        if (!in_array($gid, explode(',', $data['quiz']['gids']))) {
            exit('Quiz not assigned to your group');
        }
        // validate start end date/time
        if ($data['quiz']['start_date'] > time()) {
            exit('Quiz not available');
        }
        // validate start end date/time
        if ($data['quiz']['end_date'] < time()) {
            exit('Quiz ended');
        }

        // validate ip address
        if ($data['quiz']['ip_address'] != '') {
            $ip_address = explode(",", $data['quiz']['ip_address']);
            $myip = $_SERVER['REMOTE_ADDR'];
            if (!in_array($myip, $ip_address)) {
                exit('IP declined!');
            }
        }
        // validate maximum attempts
        $maximum_attempt = $this->quiz_model->count_result($quid, $uid);
        if ($data['quiz']['maximum_attempts'] <= $maximum_attempt) {
            exit('Reached maximum attempts!');
        }
        // if this quiz already opened by user then resume it
        $open_result = $this->quiz_model->open_result($quid, $uid);
        if ($open_result != '0') {
            $this->session->set_userdata('rid', $open_result);
            echo $open_result;
            exit();
        }
        // insert result row and get rid (result id)
        $rid = $this->quiz_model->insert_result($quid, $uid);

        echo $rid;
    }

    function attempt($connection_key = '', $uid, $rid) {

        $this->db->where('uid', $uid);
        $this->db->where('connection_key', $connection_key);
        $auth = $this->db->get('kams_users');
        $user = $auth->row_array();
        if ($auth->num_rows() == 0) {
            exit('invalid Connection!');
        }




        // get result and quiz info and validate time period
        $data['quiz'] = $this->quiz_model->quiz_result($rid);
        $data['saved_answers'] = $this->quiz_model->saved_answers($rid);




        // end date/time
        if ($data['quiz']['end_date'] < time()) {
            $this->api_model->submit_result($user, $rid);
            //$this->session->unset_userdata('rid');
            exit($this->lang->line('quiz_ended'));
        }


        // end date/time
        if (($data['quiz']['start_time'] + ($data['quiz']['duration'] * 60)) < time()) {
            $this->api_model->submit_result($user, $rid);
            //$this->session->unset_userdata('rid');

            exit($this->lang->line('time_over'));
        }
        // remaining time in seconds 
        $data['seconds'] = ($data['quiz']['duration'] * 60) - (time() - $data['quiz']['start_time']);
        // get questions
        $data['questions'] = $this->quiz_model->get_questions($data['quiz']['r_qids']);
        // get options
        $data['options'] = $this->quiz_model->get_options($data['quiz']['r_qids']);
        $data['title'] = $data['quiz']['quiz_name'];
        $data['connection_key'] = $connection_key;
        $data['uid'] = $uid;
        $data['rid'] = $rid;
        $this->load->view('quiz_attempt_android', $data);
    }

    function submit_quiz($connection_key = '', $uid, $rid) {

        $this->db->where('uid', $uid);
        $this->db->where('connection_key', $connection_key);
        $auth = $this->db->get('kams_users');
        $user = $auth->row_array();
        if ($auth->num_rows() == 0) {
            exit('invalid Connection!');
        }


        if ($this->api_model->submit_result($user, $rid)) {
            
        } else {
            
        }
        // $rid=$this->session->unset_userdata('rid');		

        echo "<script>Android.showToast('" . $rid . "');</script>";
    }

    function save_answer($connection_key = '', $uid, $rid) {

        $this->db->where('uid', $uid);
        $this->db->where('connection_key', $connection_key);
        $auth = $this->db->get('kams_users');
        $user = $auth->row_array();
        if ($auth->num_rows() == 0) {
            exit('invalid Connection!');
        }
        echo "<pre>";
        print_r($_POST);
        // insert user response and calculate scroe
        echo $this->api_model->insert_answer($user, $rid);
    }

    function set_ind_time($connection_key = '', $uid, $rid) {
        // update questions time spent
        $this->api_model->set_ind_time($rid);
    }

    function view_result($connection_key = '', $uid, $rid) {

        $this->db->where('uid', $uid);
        $this->db->where('connection_key', $connection_key);
        $auth = $this->db->get('kams_users');
        $user = $auth->row_array();
        if ($auth->num_rows() == 0) {
            exit('invalid Connection!');
        }

        $logged_in = $user;
        $data['logged_in'] = $user;
        $data['result'] = $this->result_model->get_result($rid);
        $data['title'] = $this->lang->line('result_id') . ' ' . $data['result']['rid'];
        if ($data['result']['view_answer'] == '1' || $logged_in['su'] == '1') {
            $this->load->model("quiz_model");
            $data['saved_answers'] = $this->quiz_model->saved_answers($rid);
            $data['questions'] = $this->quiz_model->get_questions($data['result']['r_qids']);
            $data['options'] = $this->quiz_model->get_options($data['result']['r_qids']);
        }
        // top 10 results of selected quiz
        $last_ten_result = $this->result_model->last_ten_result($data['result']['quid']);
        $value = array();
        $value[] = array('Quiz Name', 'Percentage (%)');
        foreach ($last_ten_result as $val) {
            $value[] = array($val['email'] . ' (' . $val['first_name'] . " " . $val['last_name'] . ')', intval($val['percentage_obtained']));
        }
        $data['value'] = json_encode($value);

        // time spent on individual questions
        $correct_incorrect = explode(',', $data['result']['score_individual']);
        $qtime[] = array($this->lang->line('question_no'), $this->lang->line('time_in_sec'));
        foreach (explode(",", $data['result']['individual_time']) as $key => $val) {
            if ($val == '0') {
                $val = 1;
            }
            if ($correct_incorrect[$key] == "1") {
                $qtime[] = array($this->lang->line('q') . " " . ($key + 1) . ") - " . $this->lang->line('correct') . " ", intval($val));
            } else if ($correct_incorrect[$key] == '2') {
                $qtime[] = array($this->lang->line('q') . " " . ($key + 1) . ") - " . $this->lang->line('incorrect') . "", intval($val));
            } else if ($correct_incorrect[$key] == '0') {
                $qtime[] = array($this->lang->line('q') . " " . ($key + 1) . ") -" . $this->lang->line('unattempted') . " ", intval($val));
            } else if ($correct_incorrect[$key] == '3') {
                $qtime[] = array($this->lang->line('q') . " " . ($key + 1) . ") - " . $this->lang->line('pending_evaluation') . " ", intval($val));
            }
        }
        $data['qtime'] = json_encode($qtime);
        $data['percentile'] = $this->result_model->get_percentile($data['result']['quid'], $data['result']['uid'], $data['result']['score_obtained']);




        $this->load->view('view_result_android', $data);
    }

    function register($email, $first_name, $last_name, $password, $contact_no, $gid) {

        if (!$this->config->item('user_registration')) {

            exit('Registration is closed by administrator');
        }
        $email = urldecode($email);
        $query = $this->db->query("select * from kams_users where email='$email' ");
        if ($query->num_rows() >= 1) {
            exit('Email address already exist');
        }
        if ($this->api_model->register($email, $first_name, $last_name, $password, $contact_no, $gid)) {
            if ($this->config->item('verify_email')) {
                exit($this->lang->line('account_registered_email_sent'));
            } else {
                exit($this->lang->line('account_registered'));
            }
        } else {
            exit($this->lang->line('error_to_add_data'));
        }
    }

    public function forgotPassword() {
        $data = file_get_contents('php://input');
        $json = json_decode($data);
        $email = $json->{'Email'};
        $this->forgot($user_email);
    }

    function forgot($user_email) {

        $user_email = urldecode($user_email);
        if ($this->api_model->reset_password($user_email)) {
            // exit($this->lang->line('password_updated'));
            $result = array();
            $result['StatusCode'] = "200";
            $result['HasError'] = false;
            $result['Message'] = "Successfully Password change and send to Register Email";

            echo json_encode($result);
            exit();
        } else {
            //exit($this->lang->line('email_doesnot_exist'));
            $result = array();
            $result['StatusCode'] = "200";
            $result['HasError'] = true;
            $result['Message'] = "Invalid Email ID";
            echo json_encode($result);
            exit();
        }
    }

    function logout($connection_key = '', $uid) {
        $userdata = array('connection_key' => '');
        $this->db->where('uid', $uid);
        $this->db->where('connection_key', $connection_key);
        $auth = $this->db->update('kams_users', $userdata);
        exit("Account logged out successfully!");
    }

//http://192.168.2.34/equizAdmin/api/verifyExam
    //{"userId":"52","examId":"18"}
    function verifyExam() {
        $data = file_get_contents('php://input');
        $json = json_decode($data);
        $connection_key = @$json->{'connection_key'};
        $userId = $json->{'userId'};
        $examId = $json->{'examId'};
        $examCode = $json->{'examCode'};
        $time = time();

        //$examId = 18;
        $this->db->where('quid', $examId);
        $auth = $this->db->get('kams_quiz');
        $quiz = $auth->row_array();

        $userinfo = $this->checkUser($userId);
        $cdate = date("Y-m-d");
        $chours = date("H");
        if ($quiz && $userinfo) {
            $subscriptionId = 0;
            if ($quiz['exam_category'] == $userinfo['exam_category']) {

                if ($quiz['exam_type'] == 2) {
                    //check Time
                    $this->db->where('student_id', $userId);
                    $this->db->where('quiz_id', $examId);
                    //$this->db->where('approved_status', 1);
                    $query = $this->db->get('kams_students_exam_subscription');
                    $quizSub = $query->row_array();

                    $subscriptionId = $quizSub['subscription_id'];
                    if ($quizSub) {
                        if ($quizSub['approved_status'] != 1) {
                            $result = array();
                            $result['StatusCode'] = "200";
                            $result['HasError'] = true;
                            $result['Message'] = "Your Exam Not Yet Approve";
                            echo json_encode($result);
                            exit();
                        }
                        if ($quizSub['unique_code'] == $examCode) {

                            if ($quizSub['start_date'] == $cdate) {
                                $stime = explode(":", $quizSub['start_time']);
                                $etime = explode(":", $quizSub['end_time']);
                                $intstime = (int) $stime[0];
                                $endtetime = (int) $etime[0];
                                //print_r($quizSub);
                                //print $cdate."++".$quizSub['start_date']."++";
                                //print $intetime."++".$chours."++".$quizSub['start_date'];
                                // print $intstime."++".$chours."++".$endtetime;
                                if (($intstime <= $chours) && ($chours <= $endtetime)) {
                                    //print "11"; exit();
                                } else {
                                    $result = array();
                                    $result['StatusCode'] = "200";
                                    $result['HasError'] = true;
                                    $result['Message'] = "Date not match";
                                    echo json_encode($result);
                                    exit();
                                }
                            } else {
                                $result = array();
                                $result['StatusCode'] = "200";
                                $result['HasError'] = true;
                                $result['Message'] = "Date not match";
                                echo json_encode($result);
                                exit();
                            }
                        } else {
                            $result = array();
                            $result['StatusCode'] = "200";
                            $result['HasError'] = true;
                            $result['Message'] = "Invalid Exam Code.";
                            echo json_encode($result);
                            exit();
                        }
                    } else {
                        $result = array();
                        $result['StatusCode'] = "200";
                        $result['HasError'] = true;
                        $result['Message'] = "Sorry You have not subscription this Exam.";
                        echo json_encode($result);
                        exit();
                    }
                }
            } else {
                $result = array();
                $result['StatusCode'] = "200";
                $result['HasError'] = true;
                $result['Message'] = "Invalid Exam Category";
                echo json_encode($result);
                exit();
            }
            //  print "33"; exit();

            $vocabulary_flag = $quiz['vocabulary_flag'];
            $structure_flag = $quiz['structure_flag'];
            $fluency_flag = $quiz['fluency_flag'];
            $pronunciation_flag = $quiz['pronunciation_flag'];
            $comprehension_flag = $quiz['comprehension_flag'];
            $video_conference = $quiz['video_conference'];

            $exam_category = $quiz['exam_category'];
            $exam_type = $quiz['exam_type'];
            $result = array();
            $result['result']['examdetails'] = $quiz;

            $result['result']['pronunciation_flag'] = $pronunciation_flag ? true : false;
            $result['result']['pronunciation_question'] = $pronunciation_flag;

            $result['result']['structure_flag'] = $structure_flag ? true : false;
            $result['result']['structure_question'] = $structure_flag;

            $result['result']['vocabulary_flag'] = $vocabulary_flag ? true : false;
            $result['result']['vocabulary_question'] = $vocabulary_flag;

            $result['result']['fluency_flag'] = $fluency_flag ? true : false;
            $result['result']['fluency_question'] = $fluency_flag;

            $result['result']['comprehension_flag'] = $comprehension_flag ? true : false;
            $result['result']['comprehension_question'] = $comprehension_flag;

            $result['result']['video_conference'] = ($video_conference == 1) ? true : false;



            $result['result']['generaltab'] = $module_general ? true : false;
            $result['result']['general_question'] = $module_general;
            $result['result']['audiotab'] = $module_audio ? true : false;
            $result['result']['audiotab_question'] = $module_audio;
            $result['result']['videotab'] = $module_video ? true : false;
            $result['result']['videotab_question'] = $module_video;

            //check varification
            $insertdata = array(
                'quid' => $examId,
                'uid' => $userId,
                'start_date_result' => date('Y-m-d'),
                'subscription_id' => $subscriptionId,
                'start_time' => time()
            );
            $this->db->insert('kams_result', $insertdata);
            $rid = $this->db->insert_id();
            if ($rid) {
                $result['result']['exam_start_id'] = $rid;
                $result['StatusCode'] = "200";
                $result['HasError'] = false;
                $result['Message'] = "Your Exam Start";
            } else {
                $result = array();
                $result['StatusCode'] = "200";
                $result['HasError'] = true;
                $result['Message'] = "Invalid Exam";
            }
        } else {
            $result = array();
            $result['StatusCode'] = "200";
            $result['HasError'] = true;
            $result['Message'] = "Invalid Exam";
        }
        echo json_encode($result);
        exit();
    }

    function verifyEmailPhone() {
        $data = file_get_contents('php://input');
        $json = json_decode($data);
        $connection_key = @$json->{'connection_key'};
        $userId = $json->{'userId'};
        $phone_verify_code = @$json->{'phone_verify_code'};
        $email_verify_code = @$json->{'email_verify_code'};
        $time = time();
        $userinfo = $this->checkUser($userId);

        if ($userinfo && ($email_verify_code || $phone_verify_code)) {

            if ($email_verify_code) {

                if ($email_verify_code == $userinfo['email_verify']) {
                    $updatedata = array(
                        'email_verify' => 1
                    );
                    $this->db->where('uid', $userId);
                    $this->db->update('kams_users', $updatedata);
                    //email fair

                    $dataEmail = array();
                    $dataEmail['name'] = $userinfo['first_name'];
                    $dataEmail['email_verify'] = 1;
                    $subject = "GACA Elpt:: Email Verified";
                    $body = $this->load->view('mail/emailphoneconfirmvarification', $dataEmail, true);
                    $to = $userinfo['email'];
                    $this->sendEmail($subject, $body, $to);
                    $result = array();
                    $result['StatusCode'] = "200";
                    $result['HasError'] = false;
                    $result['Message'] = "Successfully Verified.";
                    $result['result'] = array();
                    echo json_encode($result);
                    exit();
                    //end
                } else {
                    $result = array();
                    $result['StatusCode'] = "200";
                    $result['HasError'] = true;
                    $result['Message'] = "Invalid Email Varification Code.";
                    echo json_encode($result);
                    exit();
                }
            }

            if ($phone_verify_code) {
                if ($phone_verify_code == $userinfo['phone_verify']) {
                    $updatedata = array(
                        'phone_verify' => 1
                    );
                    $this->db->where('uid', $userId);
                    $this->db->update('kams_users', $updatedata);
                    //email 
                    $dataEmail = array();
                    $dataEmail['name'] = $userinfo['first_name'];
                    $dataEmail['phone_verify'] = 1;
                    $subject = "GACO Elpt:: Phone Verified";
                    $body = $this->load->view('mail/emailphoneconfirmvarification', $dataEmail, true);
                    $to = $userinfo['email'];
                    $this->sendEmail($subject, $body, $to);
                    $result = array();
                    $result['StatusCode'] = "200";
                    $result['HasError'] = false;
                    $result['Message'] = "Successfully Verified.";
                    $result['result'] = array();
                    echo json_encode($result);
                    exit();

                    //here
                } else {
                    $result = array();
                    $result['StatusCode'] = "200";
                    $result['HasError'] = true;
                    $result['Message'] = "Invalid Phone Varification Code.";
                    echo json_encode($result);
                    exit();
                }
            }
        } else {
            $result = array();
            $result['StatusCode'] = "200";
            $result['HasError'] = true;
            $result['Message'] = "Invalid Information";
            echo json_encode($result);
            exit();
        }
    }

    function examStart() {
        $data = file_get_contents('php://input');
        $json = json_decode($data);
        $connection_key = @$json->{'connection_key'};
        $userId = $json->{'userId'};
        $examId = $json->{'examId'};
        $exam_start_id = $json->{'exam_start_id'};
        //check valid exam or not 
        $quiz_id = $examId;
        $this->db->where('quid', $quiz_id);
        $auth = $this->db->get('kams_quiz');
        $quiz = $auth->row_array();

        if ($quiz) {

            $vocabulary_flag = $quiz['vocabulary_flag'];
            $structure_flag = $quiz['structure_flag'];
            $fluency_flag = $quiz['fluency_flag'];
            $pronunciation_flag = $quiz['pronunciation_flag'];
            $comprehension_flag = $quiz['comprehension_flag'];
            $video_conference = $quiz['video_conference'];


            $module_english = $quiz['module_english'];
            $module_video = $quiz['module_video'];
            $module_audio = $quiz['module_audio'];
            $module_general = $quiz['module_general'];
            $exam_category = $quiz['exam_category'];
            $exam_type = $quiz['exam_type'];

            //$exam_category = 
            $result = array();

            $result['result']['examdetails'] = $quiz;
            $result['result']['onlyquestion'] = 1;
            $result['result']['videoconference']['url'] = "";
            $result['result']['questions'] = array();
            $result['result']['totalquestions'] = 0;
            $result['result']['questionsection'] = 0;

            $result['result']['pronunciation_flag'] = $pronunciation_flag ? true : false;
            $result['result']['structure_flag'] = $structure_flag ? true : false;
            $result['result']['vocabulary_flag'] = $vocabulary_flag ? true : false;
            $result['result']['fluency_flag'] = $fluency_flag ? true : false;
            $result['result']['comprehension_flag'] = $comprehension_flag ? true : false;
            //$result['result']['video_conference'] = $video_conference ? true : false;
            $result['result']['video_conference'] = ($video_conference == 1) ? true : false;
            $result['result']['activetab'] = "";
            $result['result']['activetab'] = "";
            $activeflag = 0;

            if ($pronunciation_flag) {
                $question_section = 1;
                $activeflag = 1;
                $question_limit = $pronunciation_flag;

                $questions = $this->quiz_model->get_randamquestions($exam_category, $exam_type, $question_section, $question_limit);

                $slno = 1;
                $getquestionIDs = "";
                foreach ($questions as $key => $val) {
                    $getquestionIDs .= "," . $val['qid'];
                    $questions[$key]['slno'] = $slno;
                    $questions[$key]['video_url'] = ($val['video_flg'] == 1) ? base_url() . "videos/" . $val['video_url'] : $val['video_url'];
                    if ($val['qtype'] != 6) {
                        $questions[$key]['option'] = $this->quiz_model->get_options($val['qid']);
                    } else {
                        $questions[$key]['option'] = array();
                    }
                    $slno++;
                }
                $getquestionIDs = trim($getquestionIDs, ",");
                $updateExam = array();
                $updateExam['pronunciation_q_ids'] = $getquestionIDs;
                $this->db->where('rid', $exam_start_id);
                $this->db->update('kams_result', $updateExam);

                $result['result']['questions'] = $questions;
                $result['result']['totalquestions'] = ($slno - 1);
                $result['result']['activetab'] = "Pronunciation";
            }

            if ($structure_flag && $activeflag == 0) {
                $question_section = 2;
                $activeflag = 1;
                $question_limit = $structure_flag;

                $questions = $this->quiz_model->get_randamquestions($exam_category, $exam_type, $question_section, $question_limit);

                $slno = 1;
                $getquestionIDs = "";
                foreach ($questions as $key => $val) {
                    $getquestionIDs .= "," . $val['qid'];
                    $questions[$key]['slno'] = $slno;
                    $questions[$key]['video_url'] = ($val['video_flg'] == 1) ? base_url() . "videos/" . $val['video_url'] : $val['video_url'];

                    if ($val['qtype'] != 6) {
                        $questions[$key]['option'] = $this->quiz_model->get_options($val['qid']);
                    } else {
                        $questions[$key]['option'] = array();
                    }

                    //$questions[$key]['option'] = $this->quiz_model->get_options($val['qid']);
                    $slno++;
                }
                $getquestionIDs = trim($getquestionIDs, ",");
                $updateExam = array();
                $updateExam['structure_q_ids'] = $getquestionIDs;
                $this->db->where('rid', $exam_start_id);
                $this->db->update('kams_result', $updateExam);
                // $result['result']['general']['sql'] =$this->db->last_query();
                $result['result']['questions'] = $questions;
                $result['result']['totalquestions'] = ($slno - 1);
                $result['result']['activetab'] = "Structure";
                // = 
            }

            if ($vocabulary_flag && $activeflag == 0) {
                $question_section = 3;
                $activeflag = 1;
                $question_limit = $vocabulary_flag;

                $questions = $this->quiz_model->get_randamquestions($exam_category, $exam_type, $question_section, $question_limit);

                $slno = 1;
                $getquestionIDs = "";
                foreach ($questions as $key => $val) {
                    $getquestionIDs .= "," . $val['qid'];
                    $questions[$key]['slno'] = $slno;
                    $questions[$key]['video_url'] = ($val['video_flg'] == 1) ? base_url() . "videos/" . $val['video_url'] : $val['video_url'];
                    if ($val['qtype'] != 6) {
                        $questions[$key]['option'] = $this->quiz_model->get_options($val['qid']);
                    } else {
                        $questions[$key]['option'] = array();
                    }
                    //$questions[$key]['option'] = $this->quiz_model->get_options($val['qid']);
                    $slno++;
                }
                $getquestionIDs = trim($getquestionIDs, ",");
                $updateExam = array();
                $updateExam['vocabulary_q_ids'] = $getquestionIDs;
                $this->db->where('rid', $exam_start_id);
                $this->db->update('kams_result', $updateExam);
                // $result['result']['general']['sql'] =$this->db->last_query();
                $result['result']['questions'] = $questions;
                $result['result']['totalquestions'] = ($slno - 1);
                $result['result']['activetab'] = "Vocabulary";
                // = 
            }

            if ($fluency_flag && $activeflag == 0) {
                $question_section = 4;
                $activeflag = 1;
                $question_limit = $fluency_flag;

                $questions = $this->quiz_model->get_randamquestions($exam_category, $exam_type, $question_section, $question_limit);

                $slno = 1;
                $getquestionIDs = "";
                foreach ($questions as $key => $val) {
                    $getquestionIDs .= "," . $val['qid'];
                    $questions[$key]['slno'] = $slno;
                    $questions[$key]['video_url'] = ($val['video_flg'] == 1) ? base_url() . "videos/" . $val['video_url'] : $val['video_url'];
                    if ($val['qtype'] != 6) {
                        $questions[$key]['option'] = $this->quiz_model->get_options($val['qid']);
                    } else {
                        $questions[$key]['option'] = array();
                    }
                    //$questions[$key]['option'] = $this->quiz_model->get_options($val['qid']);
                    $slno++;
                }
                $getquestionIDs = trim($getquestionIDs, ",");
                $updateExam = array();
                $updateExam['fluency_q_ids'] = $getquestionIDs;
                $this->db->where('rid', $exam_start_id);
                $this->db->update('kams_result', $updateExam);
                // $result['result']['general']['sql'] =$this->db->last_query();
                $result['result']['questions'] = $questions;
                $result['result']['totalquestions'] = ($slno - 1);
                $result['result']['activetab'] = "Fluency";
                // = 
            }

            if ($comprehension_flag && $activeflag == 0) {
                $question_section = 5;
                $activeflag = 1;
                $question_limit = $comprehension_flag;

                $questions = $this->quiz_model->get_randamquestions($exam_category, $exam_type, $question_section, $question_limit);

                $slno = 1;
                $getquestionIDs = "";
                foreach ($questions as $key => $val) {
                    $getquestionIDs .= "," . $val['qid'];
                    $questions[$key]['slno'] = $slno;
                    $questions[$key]['video_url'] = ($val['video_flg'] == 1) ? base_url() . "videos/" . $val['video_url'] : $val['video_url'];
                    if ($val['qtype'] != 6) {
                        $questions[$key]['option'] = $this->quiz_model->get_options($val['qid']);
                    } else {
                        $questions[$key]['option'] = array();
                    }
                    //$questions[$key]['option'] = $this->quiz_model->get_options($val['qid']);
                    $slno++;
                }
                $getquestionIDs = trim($getquestionIDs, ",");
                $updateExam = array();
                $updateExam['comprehension_q_ids'] = $getquestionIDs;
                $this->db->where('rid', $exam_start_id);
                $this->db->update('kams_result', $updateExam);
                // $result['result']['general']['sql'] =$this->db->last_query();
                $result['result']['questions'] = $questions;
                $result['result']['totalquestions'] = ($slno - 1);
                $result['result']['activetab'] = "Comprehension";
                // = 
            }

            if ($video_conference && $activeflag == 0) {
                $updateExam = array();
                $updateExam['video_conference_flag'] = 4; //ready for start
                $this->db->where('rid', $exam_start_id);
                $this->db->update('kams_result', $updateExam);

                $result['result']['onlyquestion'] = 2;
                $result['result']['videoconference']['url'] = "https://www.thoughtspheres.com/testvideo/index.html";
                $result['result']['activetab'] = "Interactions";
            }

            $result['result']['questionsection'] = $question_section;

            if ($activeflag == 0) {
                $result = array();
                $result['StatusCode'] = "200";
                $result['HasError'] = true;
                $result['Message'] = "Invalid Exam";
            }
        } else {
            $result = array();
            $result['StatusCode'] = "200";
            $result['HasError'] = true;
            $result['Message'] = "Invalid Exam";
        }
        echo json_encode($result);
        exit();
    }

    public function submitExam() {

        $data = file_get_contents('php://input');
        $json = json_decode($data);

        $connection_key = @$json->{'connection_key'};
        $userId = $json->{'userId'};
        $examId = $json->{'examId'};
        $answer = $json->{'answer'};
        $module = $section = $json->{'section'};
        $question_section_list = $this->config->item('question_section');
        $error = true;
        $sectionkey = 0;
//        print_r($question_section);
//        print array_key_exists($section, $question_section); exit();
        if (array_key_exists($section, $question_section_list)) {
            $sectionkey = $section; //array_search($module, $question_section);
            $error = false;
        }
        //print $sectionkey; exit();

        if ($error) {
            $dataout = array();
            $dataout['result'] = array();
            $dataout['HasError'] = true;
            $dataout['Message'] = "Invalid Quiz";
            echo json_encode($dataout);
            exit();
        }


        $exam_start_id = $json->{'exam_start_id'};
        $getoptionIDs = "";
        $getquestionIDs = "";

        $this->db->where('quid', $examId);
        $auth = $this->db->get('kams_quiz');
        $quiz = $auth->row_array();

        $this->db->select('rid,'
                . 'pronunciation_q_ids,pronunciation_score,pronunciation_percentage,'
                . 'structure_q_ids,structure_score,structure_percentage,'
                . 'vocabulary_q_ids,vocabulary_score,vocabulary_percentage,'
                . 'fluency_q_ids,fluency_score,fluency_percentage,'
                . 'comprehension_q_ids,comprehension_score,comprehension_percentage,'
                . 'video_conference_flag,video_conference_score,video_conference_percentage'
        );
        $this->db->where('rid', $exam_start_id);
        $authresult = $this->db->get('kams_result');
        // echo $this->db->last_query(); exit();
        $examDetails = $authresult->row_array();


        if ($quiz && $examDetails) {

            $pronunciation_flag = $quiz['pronunciation_flag'];
            $structure_flag = $quiz['structure_flag'];
            $vocabulary_flag = $quiz['vocabulary_flag'];
            $fluency_flag = $quiz['fluency_flag'];
            $comprehension_flag = $quiz['comprehension_flag'];
            $video_conference = $quiz['video_conference'];
            $exam_category = $quiz['exam_category'];
            $exam_type = $quiz['exam_type'];
        } else {
            $dataout = array();
            $dataout['result'] = array();
            $dataout['HasError'] = true;
            $dataout['Message'] = "Invalid Quiz";
            echo json_encode($dataout);
            exit();
        }
        //print "11"; exit();

        $dataout = array();

        $dataout['result']['examdetails'] = $quiz;
        $dataout['result']['onlyquestion'] = 1;
        $dataout['result']['videoconference']['url'] = "";
        $dataout['result']['questions'] = array();
        $dataout['result']['totalquestions'] = 0;
        $dataout['result']['questionsection'] = 0;

        $dataout['result']['pronunciation_flag'] = $pronunciation_flag ? true : false;
        $dataout['result']['structure_flag'] = $structure_flag ? true : false;
        $dataout['result']['vocabulary_flag'] = $vocabulary_flag ? true : false;
        $dataout['result']['fluency_flag'] = $fluency_flag ? true : false;
        $dataout['result']['comprehension_flag'] = $comprehension_flag ? true : false;
        $dataout['result']['video_conference'] = $video_conference ? true : false;
        $dataout['result']['activetab'] = "";
        $fillinblank = "";
        foreach ($answer as $key => $val) {
            if (@$val->qtype == 6 && @$val->answer) {
                $myanswer = trim(@$val->answer);
                $sqlanswerMatch = "SELECT * FROM kams_options "
                        . "WHERE qid = $val->qid AND q_option = '$myanswer' LIMIT 1";
                $query = $this->db->query($sqlanswerMatch);
                $answerArea = $query->row();
                //print_r($answerArea);
                if ($answerArea) {
                    $getoptionIDs .= "," . $answerArea->oid;
                    $getquestionIDs .= "," . $answerArea->qid;
                }
            } else {
                $getoptionIDs .= "," . $val->oid;
                $getquestionIDs .= "," . $val->qid;
            }
        }
        //print $getoptionIDs; exit();
        $getoptionIDs = trim($getoptionIDs, ",");
        $getquestionIDs = trim($getquestionIDs, ",");

        if ($getoptionIDs) {
            $sqlansweroption = "INSERT INTO kams_answers (qid,q_option,uid,score_u,rid,exam_section) "
                    . "SELECT qid,oid,'$userId',score,'$exam_start_id',$sectionkey FROM kams_options WHERE oid IN ($getoptionIDs);";
            $this->db->query($sqlansweroption);
        }

        $updateExam = array();
        $nextflag = 0;
        $questionlimit = 0;
        $createreport = 0;

        $sessctionArray = array();
        $sessctionArray[1] = "pronunciation";
        $sessctionArray[2] = "structure";
        $sessctionArray[3] = "vocabulary";
        $sessctionArray[4] = "fluency";
        $sessctionArray[5] = "comprehension";

        $this->db->select_sum('score_u');
        $this->db->where('rid', $exam_start_id);
        $this->db->where('exam_section', $sectionkey);
        $resultscore = $this->db->get('kams_answers')->row();

        $sessctionvalue = $sessctionArray[$sectionkey];
        $sessction_q_ids = $sessctionArray[$sectionkey] . "_question_ids";
        //$sessction_question_idscount = count(explode(",", $examDetails[$sessction_q_ids]));
        $sessction_question_idscount = $quiz[$sessctionvalue . '_flag']; //count(explode(",", $examDetails[$sessction_q_ids]));

        $updateExam[$sessctionvalue . '_percentage'] = ($resultscore->score_u / $sessction_question_idscount) * 100;
        $updateExam[$sessctionvalue . '_score'] = $resultscore->score_u;



        if ($sectionkey == 1) {
            if ($structure_flag) {
                $nextflag = 2;
                $questionlimit = $structure_flag;
            } elseif ($vocabulary_flag) {
                $nextflag = 3;
                $questionlimit = $vocabulary_flag;
            } elseif ($fluency_flag) {
                $nextflag = 4;
                $questionlimit = $fluency_flag;
            } elseif ($comprehension_flag) {
                $nextflag = 5;
                $questionlimit = $comprehension_flag;
            }
        }

        if ($sectionkey == 2) {
            if ($vocabulary_flag) {
                $nextflag = 3;
                $questionlimit = $vocabulary_flag;
            } elseif ($fluency_flag) {
                $nextflag = 4;
                $questionlimit = $fluency_flag;
            } elseif ($comprehension_flag) {
                $questionlimit = $comprehension_flag;
                $nextflag = 5;
            }
        }
        if ($sectionkey == 3) {
            if ($fluency_flag) {
                $nextflag = 4;
                $questionlimit = $fluency_flag;
            } elseif ($comprehension_flag) {
                $questionlimit = $comprehension_flag;
                $nextflag = 5;
            }
        }
        if ($sectionkey == 4) {
            if ($comprehension_flag) {
                $nextflag = 5;
                $questionlimit = $comprehension_flag;
            }
        }

        //print $nextflag; exit();


        if ($nextflag == 0) { //exam end
            $this->db->select_sum('score_u');
            $this->db->where('rid', $exam_start_id);
            $result = $this->db->get('kams_answers')->row();

            $this->db->where('rid', $exam_start_id);
            $query = $this->db->get('kams_answers');
            $numberofquestion = $query->num_rows();
            $percentage_obtained = ($result->score_u / $numberofquestion) * 100;

            if ($percentage_obtained > 50) {
                $result_status = "Pass";
            } else {
                $result_status = "Fail";
            }
            $createreport = 1;

            $updateExam['end_time'] = time();
            $updateExam['score_obtained'] = $result->score_u;
            $updateExam['percentage_obtained'] = $percentage_obtained;
            $updateExam['result_status'] = $result_status;

            $dataout['result']['onlyquestion'] = 3;
            $dataout['result']['videoconference']['url'] = "";
            $dataout['result']['activetab'] = "";

            //$score_obtained = ""
        } else {
            $question_section = $nextflag;
            $activeflag = 1;
            $question_limit = $questionlimit;

            $questions = $this->quiz_model->get_randamquestions($exam_category, $exam_type, $question_section, $question_limit);

            $slno = 1;
            $getquestionIDs = "";
            foreach ($questions as $key => $val) {
                $getquestionIDs .= "," . $val['qid'];
                $questions[$key]['slno'] = $slno;
                $questions[$key]['video_url'] = ($val['video_flg'] == 1) ? base_url() . "videos/" . $val['video_url'] : $val['video_url'];
                if ($val['qtype'] != 6) {
                    $questions[$key]['option'] = $this->quiz_model->get_options($val['qid']);
                } else {
                    $questions[$key]['option'] = array();
                }
                // $questions[$key]['option'] = $this->quiz_model->get_options($val['qid']);
                $slno++;
            }
            $getquestionIDs = trim($getquestionIDs, ",");
            $updateExam2 = array();

            $sessctionvaluenext = $sessctionArray[$sectionkey + 1];
            $updateExam2[$sessctionvaluenext . '_q_ids'] = $getquestionIDs;

            $this->db->where('rid', $exam_start_id);
            $this->db->update('kams_result', $updateExam2);


            // $result['result']['general']['sql'] =$this->db->last_query();
//            $myfile = fopen("newfile.txt", "a+") or die("Unable to open file!");
//            $txt = $this->db->last_query();
//            fwrite($myfile, $txt);
//            fclose($myfile);

            $dataout['result']['questions'] = $questions;
            $dataout['result']['totalquestions'] = ($slno - 1);
            $dataout['result']['questionsection'] = $nextflag;
            $dataout['result']['activetab'] = $question_section_list[$nextflag];
        }

        if ($nextflag == 0 && $video_conference) {

            $roomID = $this->createroomID($exam_start_id);
            //$usertype = "moderator";
            $usertype = "participant";

            // $roomURL = "https://www.thoughtspheres.com/testvideo/index.html";
            //https://www.thoughtspheres.com/video_call/client/confo.html?roomId=5e5628cbcf8f4978a176a00d&usertype=participant&user_ref=Applicant"

            $roomURL = "https://www.thoughtspheres.com/video_call/client/confo.html?";
            $roomURL .= "roomId=" . $roomID;
            $roomURL .= "&usertype=" . $usertype;
            $roomURL .= "&user_ref=Applicant";

            $updateExam['video_conference_flag'] = 4; //ready for start
            $dataout['result']['questionsection'] = 6;
            $dataout['result']['onlyquestion'] = 2;
            $dataout['result']['videoconference']['url'] = $roomURL;
            $dataout['result']['activetab'] = "Interactions";
        }

        $this->db->where('rid', $exam_start_id);
        $this->db->update('kams_result', $updateExam);

        //create report
        if ($createreport) {
            $this->createpdf($exam_start_id);
        }
        //end
        //
//        $myfile = fopen("newfile.txt", "a+") or die("Unable to open file!");
//        $txt = $this->db->last_query();
//        fwrite($myfile, $txt);
//        fclose($myfile);

        $dataout['HasError'] = false;
        $dataout['Message'] = "Thanks for attend you Exam";
        echo json_encode($dataout);
        exit();
    }

    public function test() {
        $module = "Structure";
        $question_section = $this->config->item('question_section');
        $error = true;
        if (in_array($module, $question_section)) {
            echo array_search($module, $question_section);
        }
        exit();
    }

    function verifyExamBC() {
        $data = file_get_contents('php://input');
        $json = json_decode($data);
        $connection_key = @$json->{'connection_key'};
        $userId = $json->{'userId'};
        $examId = $json->{'examId'};
        $examCode = $json->{'examCode'};


        $time = time();

        //$examId = 18;
        $this->db->where('quid', $examId);
        $auth = $this->db->get('kams_quiz');
        $quiz = $auth->row_array();

        $userinfo = $this->checkUser($userId);
        $cdate = date("Y-m-d");
        $chours = date("H");
        if ($quiz && $userinfo) {
            if ($quiz['exam_category'] == $userinfo['exam_category']) {
                if ($quiz['exam_type'] == 2) {
                    //check Time
                    $this->db->where('student_id', $userId);
                    $this->db->where('quiz_id', $examId);
                    $query = $this->db->get('kams_students_exam_subscription');
                    $quizSub = $query->row_array();
                    if ($quizSub) {
                        if ($quizSub['unique_code'] == $examCode) {
                            if ($quizSub['start_date'] == $cdate) {
                                $stime = explode(":", $chours['start_time']);
                                $etime = $chours['end_time'];
                                $intstime = (int) $stime[0];
                                $intetime = (int) $etime[0];
                                if (($intstime <= $chours) && ($chours <= $intetime)) {
                                    
                                } else {
                                    $result = array();
                                    $result['StatusCode'] = "200";
                                    $result['HasError'] = true;
                                    $result['Message'] = "Date not match";
                                    echo json_encode($result);
                                    exit();
                                }
                            } else {
                                $result = array();
                                $result['StatusCode'] = "200";
                                $result['HasError'] = true;
                                $result['Message'] = "Date not match";
                                echo json_encode($result);
                                exit();
                            }
                        } else {
                            $result = array();
                            $result['StatusCode'] = "200";
                            $result['HasError'] = true;
                            $result['Message'] = "Invalid Exam Code.";
                            echo json_encode($result);
                            exit();
                        }
                    } else {
                        $result = array();
                        $result['StatusCode'] = "200";
                        $result['HasError'] = true;
                        $result['Message'] = "Sorry You have not subscription this Exam.";
                        echo json_encode($result);
                        exit();
                    }
                }
            } else {
                $result = array();
                $result['StatusCode'] = "200";
                $result['HasError'] = true;
                $result['Message'] = "Invalid Exam Category";
                echo json_encode($result);
                exit();
            }
            $module_english = $quiz['module_english'];
            $module_video = $quiz['module_video'];
            $module_audio = $quiz['module_audio'];
            $module_general = $quiz['module_general'];
            $exam_category = $quiz['exam_category'];
            $exam_type = $quiz['exam_type'];
            $result = array();
            $result['result']['examdetails'] = $quiz;
            $result['result']['englishtab'] = $module_english ? true : false;
            $result['result']['english_question'] = $module_english;
            $result['result']['generaltab'] = $module_general ? true : false;
            $result['result']['general_question'] = $module_general;
            $result['result']['audiotab'] = $module_audio ? true : false;
            $result['result']['audiotab_question'] = $module_audio;
            $result['result']['videotab'] = $module_video ? true : false;
            $result['result']['videotab_question'] = $module_video;

            //check varification
            $insertdata = array(
                'quid' => $examId,
                'uid' => $userId,
                'start_time' => time()
            );
            $this->db->insert('kams_result', $insertdata);
            $rid = $this->db->insert_id();
            if ($rid) {
                $result['result']['exam_start_id'] = $rid;
                $result['StatusCode'] = "200";
                $result['HasError'] = false;
                $result['Message'] = "Your Exam Start";
            } else {

                $result['StatusCode'] = "200";
                $result['HasError'] = true;
                $result['Message'] = "Invalid Exam";
            }
        } else {
            $result = array();
            $result['StatusCode'] = "200";
            $result['HasError'] = true;
            $result['Message'] = "Invalid Exam";
        }
        echo json_encode($result);
        exit();
    }

    public function getexamreport() {
        $data = file_get_contents('php://input');
        $json = json_decode($data);
        $connection_key = @$json->{'connection_key'};
        $userId = @$json->{'userId'};
        $examId = @$json->{'examId'};
        $exam_start_id = @$json->{'exam_start_id'};

        //check valid exam or not 
        //$examId = 18;
        //$exam_start_id = 153;
        $this->db->where('quid', $examId);
        $auth = $this->db->get('kams_quiz');
        $quiz = $auth->row_array();
        // echo $this->db->last_query() ;
        // $quiz_id = 18;
        $this->db->where('rid', $exam_start_id);
        $examId = $this->db->get('kams_result');
        $examDetails = $examId->row_array();

        // print_r($examDetails); exit();
        // echo $this->db->last_query() ;
        //print "11"; exit();
        if ($quiz && $examDetails) {

            $pronunciation_flag = $quiz['pronunciation_flag'];
            $structure_flag = $quiz['structure_flag'];
            $vocabulary_flag = $quiz['vocabulary_flag'];
            $fluency_flag = $quiz['fluency_flag'];
            $comprehension_flag = $quiz['comprehension_flag'];
            $video_conference = $quiz['video_conference'];
            //print $video_conference ; 
            $videoflag = 0;
            if ($video_conference == 1 && $examDetails['video_conference_flag'] == 1) { //video conference not present or complete
                $videoflag = 1;
                // print 22;
            } else if ($video_conference == 1 && $examDetails['video_conference_flag'] != 1) {
                $dataout = array();
                $dataout['result'] = array();
                $dataout['HasError'] = true;
                $dataout['Message'] = "Please Wait for examiner verification. ";
                echo json_encode($dataout);
                exit();
            }

            $examreportpdf = $examDetails['report_pdf'];

            $exam_category = $quiz['exam_category'];
            $exam_type = $quiz['exam_type'];

            //$exam_category = 
            $dataout = array();
            $dataout['result']['examdetails'] = $quiz;


            if ($pronunciation_flag) {
                $pronunciation_percentage = $examDetails['pronunciation_percentage'];
                if ($pronunciation_percentage < 50) {
                    $color = "#ff0000";
                    $bcolor = '#28160c';
                } elseif ($pronunciation_percentage < 80) {
                    $color = "#0058ff";
                    $bcolor = "#111937";
                } else {
                    $color = "#5ba432";
                    $bcolor = "#17270b";
                }
                $labels[] = 'Pronunciation';
                $datainput[] = $pronunciation_percentage;
                $backgroundColor[] = $color;
                $borderColor[] = $bcolor;
            }

            if ($structure_flag) {
                $structure_percentage = $examDetails['structure_percentage'];
                if ($structure_percentage < 50) {
                    $color = "#ff0000";
                    $bcolor = '#28160c';
                } elseif ($structure_percentage < 80) {
                    $color = "#0058ff";
                    $bcolor = "#111937";
                } else {
                    $color = "#5ba432";
                    $bcolor = "#17270b";
                }
                $labels[] = 'Structure';
                $datainput[] = $structure_percentage;
                $backgroundColor[] = $color;
                $borderColor[] = $bcolor;
            }

            if ($vocabulary_flag) {
                $vocabulary_percentage = $examDetails['vocabulary_percentage'];
                if ($vocabulary_percentage < 50) {
                    $color = "#ff0000";
                    $bcolor = '#28160c';
                } elseif ($vocabulary_percentage < 80) {
                    $color = "#0058ff";
                    $bcolor = "#111937";
                } else {
                    $color = "#5ba432";
                    $bcolor = "#17270b";
                }
                $labels[] = 'Vocabulary';
                $datainput[] = $vocabulary_percentage;
                $backgroundColor[] = $color;
                $borderColor[] = $bcolor;
            }

            if ($fluency_flag) {
                $fluency_percentage = $examDetails['fluency_percentage'];
                if ($fluency_percentage < 50) {
                    $color = "#ff0000";
                    $bcolor = '#28160c';
                } elseif ($fluency_percentage < 80) {
                    $color = "#0058ff";
                    $bcolor = "#111937";
                } else {
                    $color = "#5ba432";
                    $bcolor = "#17270b";
                }
                $labels[] = 'Fluency';
                $datainput[] = $fluency_percentage;
                $backgroundColor[] = $color;
                $borderColor[] = $bcolor;
            }

            if ($comprehension_flag) {
                $comprehension_percentage = $examDetails['comprehension_percentage'];
                if ($comprehension_percentage < 50) {
                    $color = "#ff0000";
                    $bcolor = '#28160c';
                } elseif ($comprehension_percentage < 80) {
                    $color = "#0058ff";
                    $bcolor = "#111937";
                } else {
                    $color = "#5ba432";
                    $bcolor = "#17270b";
                }
                $labels[] = 'Comprehension';
                $datainput[] = $comprehension_percentage;
                $backgroundColor[] = $color;
                $borderColor[] = $bcolor;
            }

            if ($videoflag) {
                $video_conference_percentage = $examDetails['video_conference_percentage'];
                if ($video_conference_percentage < 50) {
                    $color = "#ff0000";
                    $bcolor = '#28160c';
                } elseif ($video_conference_percentage < 80) {
                    $color = "#0058ff";
                    $bcolor = "#111937";
                } else {
                    $color = "#5ba432";
                    $bcolor = "#17270b";
                }
                $labels[] = 'Interaction';
                $datainput[] = $video_conference_percentage;
                $backgroundColor[] = $color;
                $borderColor[] = $bcolor;
            }


            $dataout['result']['labels'] = $labels;
            $dataout['result']['data'] = $datainput;
            $dataout['result']['backgroundColor'] = $backgroundColor;
            $dataout['result']['borderColor'] = $borderColor;
            $dataout['result']['certificate'] = base_url() . "/result_pdf/" . $examreportpdf;
            // $dataout['result']['certificate'] = base_url() . "upload/report.pdf";
            $dataout['HasError'] = false;
            $dataout['Message'] = "Gaphical Report";
            echo json_encode($dataout);
            exit();
        } else {
            $dataout = array();
            $dataout['result'] = array();
            $dataout['HasError'] = true;
            $dataout['Message'] = "Invalid ID";
            echo json_encode($dataout);
            exit();
        }
    }

    public function getmyresultlist() {
        $data = file_get_contents('php://input');
        $json = json_decode($data);
        $connection_key = @$json->{'connection_key'};
        $userId = @$json->{'userId'};
        $userinfo = $this->checkUser($userId);
        if ($userinfo) {
            $sql = "SELECT kq.quiz_name ,kq.description ,"
                    . " p1.rid AS exam_start_id ,p1.quid AS examId,"
                    . " p1.uid,p1.result_status,p1.video_conference_flag FROM kams_result p1"
                    . " INNER JOIN (SELECT pi.rid, MAX(pi.rid) AS maxpostid"
                    . " FROM kams_result pi GROUP BY pi.quid) p2 ON (p1.rid = p2.maxpostid)"
                    . " LEFT JOIN kams_quiz AS kq ON kq.quid = p1.quid"
                    . " WHERE p1.uid = $userId AND p1.result_status != 'Open' ";
            $query = $this->db->query($sql);
            // $query = $this->db->get('kams_quiz');
            $resultList = $query->result_array();
            foreach ($resultList as $key => $val) {
                if ($val['video_conference_flag'] == 0 || $val['video_conference_flag'] == 1) {
                    $resultList[$key]['showreport'] = true;
                } else {
                    $resultList[$key]['showreport'] = false;
                }
            }
            $result = array();
            $result['result']['list'] = $resultList;
            $result['StatusCode'] = "200";
            $result['HasError'] = false;
            $result['Message'] = "Result List";
        } else {
            $result = array();
            $result['StatusCode'] = "200";
            $result['HasError'] = true;
            $result['Message'] = "Invalid User";
        }
        echo json_encode($result);
        exit();
    }

    function examStartBC() {
        $data = file_get_contents('php://input');
        $json = json_decode($data);
        $connection_key = @$json->{'connection_key'};
        $userId = $json->{'userId'};
        $examId = $json->{'examId'};
        $exam_start_id = $json->{'exam_start_id'};
        //check valid exam or not 
        $quiz_id = $examId;
        $this->db->where('quid', $quiz_id);
        $auth = $this->db->get('kams_quiz');
        $quiz = $auth->row_array();

        if ($quiz) {
            $module_english = $quiz['module_english'];
            $module_video = $quiz['module_video'];
            $module_audio = $quiz['module_audio'];
            $module_general = $quiz['module_general'];
            $exam_category = $quiz['exam_category'];
            $exam_type = $quiz['exam_type'];

            //$exam_category = 
            $result = array();
            $result['result']['examdetails'] = $quiz;
            $result['result']['englishtab'] = $module_english ? true : false;
            $result['result']['generaltab'] = $module_general ? true : false;
            $result['result']['audiotab'] = $module_audio ? true : false;
            $result['result']['videotab'] = $module_video ? true : false;
            if ($module_english) {
                $question_module = 1;
                //$inputarray = array();
                //$inputarray['']
                $questions = $this->quiz_model->get_randamquestions($exam_category, $exam_type, $question_module, $module_english);

                $slno = 1;
                $getquestionIDs = "";
                foreach ($questions as $key => $val) {
                    $getquestionIDs .= "," . $val['qid'];
                    $questions[$key]['slno'] = $slno;
                    if ($val['qtype'] != 6) {
                        $questions[$key]['option'] = $this->quiz_model->get_options($val['qid']);
                    } else {
                        $questions[$key]['option'] = array();
                    }
                    //$questions[$key]['option'] = $this->quiz_model->get_options($val['qid']);
                    $slno++;
                }
                $getquestionIDs = trim($getquestionIDs, ",");
                $updateExam = array();
                $updateExam['english_question_ids'] = $getquestionIDs;
                $this->db->where('rid', $exam_start_id);
                $this->db->update('kams_result', $updateExam);


                //update
                //$questionslist
                //$exam_start_id
                //$result['result']['english']['sql'] =$this->db->last_query();
                $result['result']['english']['questions'] = $questions;
                $result['result']['english']['totalquestions'] = ($slno - 1);
                $result['result']['activetab'] = "english";

                // = 
            } else {
                $result['result']['english'] = array();
            }

            if ($module_general && !$module_english) {
                $question_module = 2;
                //$inputarray = array();
                //$inputarray['']
                $questions = $this->quiz_model->get_randamquestions($exam_category, $exam_type, $question_module, $module_general);

                $slno = 1;
                $getquestionIDs = "";
                foreach ($questions as $key => $val) {
                    $getquestionIDs .= "," . $val['qid'];
                    $questions[$key]['slno'] = $slno;
                    if ($val['qtype'] != 6) {
                        $questions[$key]['option'] = $this->quiz_model->get_options($val['qid']);
                    } else {
                        $questions[$key]['option'] = array();
                    }
                    //$questions[$key]['option'] = $this->quiz_model->get_options($val['qid']);
                    $slno++;
                }
                $getquestionIDs = trim($getquestionIDs, ",");
                $updateExam = array();
                $updateExam['general_question_ids'] = $getquestionIDs;
                $this->db->where('rid', $exam_start_id);
                $this->db->update('kams_result', $updateExam);
                // $result['result']['general']['sql'] =$this->db->last_query();
                $result['result']['general']['questions'] = $questions;
                $result['result']['general']['totalquestions'] = ($slno - 1);
                $result['result']['activetab'] = "general";
                // = 
            } else {
                $result['result']['general'] = array();
            }
            if ($module_audio && !$module_general && !$module_english) {
                $question_module = 3;
                //$inputarray = array();
                //$inputarray['']
                $questions = $this->quiz_model->get_randamquestions($exam_category, $exam_type, $question_module, $module_audio);

                $slno = 1;
                $getquestionIDs = "";
                foreach ($questions as $key => $val) {
                    $getquestionIDs .= "," . $val['qid'];
                    $questions[$key]['slno'] = $slno;
                    $questions[$key]['video_url'] = ($val['video_flg'] == 1) ? base_url() . "videos/" . $val['video_url'] : $val['video_url'];
                    if ($val['qtype'] != 6) {
                        $questions[$key]['option'] = $this->quiz_model->get_options($val['qid']);
                    } else {
                        $questions[$key]['option'] = array();
                    }
//$questions[$key]['option'] = $this->quiz_model->get_options($val['qid']);
                    $slno++;
                }
                $getquestionIDs = trim($getquestionIDs, ",");
                $updateExam = array();
                $updateExam['audio_question_ids'] = $getquestionIDs;
                $this->db->where('rid', $exam_start_id);
                $this->db->update('kams_result', $updateExam);

                //$result['result']['audio']['sql'] =$this->db->last_query();
                $result['result']['audio']['questions'] = $questions;
                $result['result']['audio']['totalquestions'] = ($slno - 1);
                $result['result']['activetab'] = "audio";
            } else {
                $result['result']['audio'] = array();
            }
            if ($module_video && !$module_audio && !$module_general && !$module_english) {
                // print "11"; exit();
                $question_module = 4;
                $questions = $this->quiz_model->get_randamquestions($exam_category, $exam_type, $question_module, $module_video);
                $slno = 1;
                //print_r($questions); exit();
                $getquestionIDs = "";
                foreach ($questions as $key => $val) {
                    $getquestionIDs .= "," . $val['qid'];
                    $questions[$key]['slno'] = $slno;
                    $questions[$key]['video_url'] = ($val['video_flg'] == 1) ? base_url() . "videos/" . $val['video_url'] : $val['video_url'];
                    if ($val['qtype'] != 6) {
                        $questions[$key]['option'] = $this->quiz_model->get_options($val['qid']);
                    } else {
                        $questions[$key]['option'] = array();
                    }
                    $slno++;
                }
                $getquestionIDs = trim($getquestionIDs, ",");
                $updateExam = array();
                $updateExam['video_question_ids'] = $getquestionIDs;
                $this->db->where('rid', $exam_start_id);
                $this->db->update('kams_result', $updateExam);
                //$result['result']['video']['sql'] =$this->db->last_query();
                $result['result']['video']['questions'] = $questions;
                $result['result']['video']['totalquestions'] = ($slno - 1);
                $result['result']['activetab'] = "video";
            } else {
                $result['result']['video'] = array();
            }
        } else {
            $result = array();
            $result['StatusCode'] = "200";
            $result['HasError'] = true;
            $result['Message'] = "Invalid Exam";
        }
        echo json_encode($result);
        exit();
    }

    public function examSubmit() {
        $result = array();
        $result['StatusCode'] = "200";
        $result['HasError'] = true;
        $result['Message'] = "Invalid Exam";
        echo json_encode($result);
        exit();
    }

    public function submitExamBC() {

        $data = file_get_contents('php://input');
        $json = json_decode($data);

//        $myfile = fopen("newfile.txt", "a+") or die("Unable to open file!");
//        $txt = print_r($json,true);
//        fwrite($myfile, $txt);
//        $txt = "Jane Doe\n";
//        fwrite($myfile, $txt);
//        fclose($myfile);

        $connection_key = @$json->{'connection_key'};
        $userId = $json->{'userId'};
        $examId = $json->{'examId'};
        $answer = $json->{'answer'};
        $module = $json->{'module'};
        $exam_start_id = $json->{'exam_start_id'};
        $getoptionIDs = "";
        $getquestionIDs = "";

        $this->db->where('quid', $examId);
        $auth = $this->db->get('kams_quiz');
        $quiz = $auth->row_array();

        $this->db->select('rid,english_question_ids,english_score,english_percentage,'
                . 'general_question_ids,general_score,general_percentage,'
                . 'audio_question_ids,audio_score,audio_percentage,'
                . 'video_question_ids,video_score,video_percentage');
        $this->db->where('rid', $exam_start_id);
        $authresult = $this->db->get('kams_result');
        $examDetails = $authresult->row_array();

        if ($quiz && $examDetails) {
            $module_english = $quiz['module_english'];
            $module_video = $quiz['module_video'];
            $module_audio = $quiz['module_audio'];
            $module_general = $quiz['module_general'];
            $exam_category = $quiz['exam_category'];
            $exam_type = $quiz['exam_type'];
        } else {
            $dataout = array();
            $dataout['result'] = array();
            $dataout['HasError'] = true;
            $dataout['Message'] = "Invalid Quiz";
            echo json_encode($dataout);
            exit();
        }
        foreach ($answer as $key => $val) {
            $getoptionIDs .= "," . $val->oid;
            $getquestionIDs .= "," . $val->qid;
        }
        $getoptionIDs = trim($getoptionIDs, ",");
        $getquestionIDs = trim($getquestionIDs, ",");
        $exam_module = 0;
        if ($module == "english") {
            $exam_module = 1;
        } elseif ($module == "general") {
            $exam_module = 2;
        } elseif ($module == "audio") {
            $exam_module = 3;
        } elseif ($module == "video") {
            $exam_module = 4;
        }
        $sqlansweroption = "INSERT INTO kams_answers (qid,q_option,uid,score_u,rid,exam_module) "
                . "SELECT qid,oid,'$userId',score,'$exam_start_id',$exam_module FROM kams_options WHERE oid IN ($getoptionIDs);";
        $this->db->query($sqlansweroption);

        $updateExam = array();
        $nextflag = "";
        if ($module == "english") {
            //$updateExam['english_question_ids'] = $getquestionIDs;

            $this->db->select_sum('score_u');
            $this->db->where('rid', $exam_start_id);
            $this->db->where('exam_module', $exam_module);
            $resultscore = $this->db->get('kams_answers')->row();

            $english_question_idscount = count(explode(",", $examDetails['english_question_ids']));

            $updateExam['english_percentage'] = ($resultscore->score_u / $english_question_idscount) * 100;
            $updateExam['english_score'] = $resultscore->score_u;
            if ($module_general) {
                $nextflag = "general";
            } elseif ($module_audio) {
                $nextflag = "audio";
            } elseif ($module_video) {
                $nextflag = "video";
            }
            //$updateExam['userscore'] = $myscrore;
        } elseif ($module == "general") {
            // $updateExam['general_question_ids'] = $getquestionIDs;
            $this->db->select_sum('score_u');
            $this->db->where('rid', $exam_start_id);
            $this->db->where('exam_module', $exam_module);
            $resultscore = $this->db->get('kams_answers')->row();

            $general_question_idscount = count(explode(",", $examDetails['general_question_ids']));

            $updateExam['general_percentage'] = ($resultscore->score_u / $general_question_idscount) * 100;
            $updateExam['general_score'] = $resultscore->score_u;

            if ($module_audio) {
                $nextflag = "audio";
            } elseif ($module_video) {
                $nextflag = "video";
            }
        } elseif ($module == "audio") {
            // $updateExam['audio_question_ids'] = $getquestionIDs;

            $this->db->select_sum('score_u');
            $this->db->where('rid', $exam_start_id);
            $this->db->where('exam_module', $exam_module);
            $resultscore = $this->db->get('kams_answers')->row();

            $audio_question_idscount = count(explode(",", $examDetails['audio_question_ids']));

            $updateExam['audio_percentage'] = ($resultscore->score_u / $audio_question_idscount) * 100;
            $updateExam['audio_score'] = $resultscore->score_u;


            if ($module_video) {
                $nextflag = "video";
            }
        } elseif ($module == "video") {
            //$updateExam['video_question_ids'] = $getquestionIDs;
            $this->db->select_sum('score_u');
            $this->db->where('rid', $exam_start_id);
            $this->db->where('exam_module', $exam_module);
            $resultscore = $this->db->get('kams_answers')->row();

            $video_question_idscount = count(explode(",", $examDetails['video_question_ids']));

            $updateExam['video_percentage'] = ($resultscore->score_u / $video_question_idscount) * 100;
            $updateExam['video_score'] = $resultscore->score_u;
        }
        if ($nextflag == "") {
            $this->db->select_sum('score_u');
            $this->db->where('rid', $exam_start_id);
            $result = $this->db->get('kams_answers')->row();

            $this->db->where('rid', $exam_start_id);
            $query = $this->db->get('kams_answers');
            $numberofquestion = $query->num_rows();
            $percentage_obtained = ($result->score_u / $numberofquestion) * 100;

            //evaluate Pass or fail here 
            //
            //
            //$result = $this->db->get('kams_answers')->row();
            if ($percentage_obtained > 50) {
                $result_status = "Pass";
            } else {
                $result_status = "Fail";
            }
            $updateExam['end_time'] = time();
            $updateExam['score_obtained'] = $result->score_u;
            $updateExam['percentage_obtained'] = $percentage_obtained;
            $updateExam['result_status'] = $result_status;
            //$score_obtained = ""
        }

        $this->db->where('rid', $exam_start_id);
        $this->db->update('kams_result', $updateExam);

        $dataout = array();
        $dataout['result'] = array();
        if ($nextflag == "general") {
            $question_module = 2;
            $questions = $this->quiz_model->get_randamquestions($exam_category, $exam_type, $question_module, $module_general);

            $slno = 1;

            $getquestionIDs = "";
            foreach ($questions as $key => $val) {
                $getquestionIDs .= "," . $val['qid'];
                $questions[$key]['slno'] = $slno;
                if ($val['qtype'] != 6) {
                    $questions[$key]['option'] = $this->quiz_model->get_options($val['qid']);
                } else {
                    $questions[$key]['option'] = array();
                }
                $slno++;
            }
            $getquestionIDs = trim($getquestionIDs, ",");
            $updateExamNew = array();
            $updateExamNew['general_question_ids'] = $getquestionIDs;
            $this->db->where('rid', $exam_start_id);
            $this->db->update('kams_result', $updateExamNew);

            // $dataout['result']['general']['sql'] =$this->db->last_query();
            $dataout['result']['general']['questions'] = $questions;
            $dataout['result']['general']['totalquestions'] = ($slno - 1);
            $dataout['result']['activetab'] = "general";
        }

        if ($nextflag == "audio") {
            $question_module = 3;
            $questions = $this->quiz_model->get_randamquestions($exam_category, $exam_type, $question_module, $module_audio);

            $slno = 1;
            $getquestionIDs = "";
            foreach ($questions as $key => $val) {
                $getquestionIDs .= "," . $val['qid'];
                $questions[$key]['slno'] = $slno;
                $questions[$key]['video_url'] = ($val['video_flg'] == 1) ? base_url() . "videos/" . $val['video_url'] : $val['video_url'];
                if ($val['qtype'] != 6) {
                    $questions[$key]['option'] = $this->quiz_model->get_options($val['qid']);
                } else {
                    $questions[$key]['option'] = array();
                }
                $slno++;
            }
            $getquestionIDs = trim($getquestionIDs, ",");
            $updateExamNew = array();
            $updateExamNew['audio_question_ids'] = $getquestionIDs;
            $this->db->where('rid', $exam_start_id);
            $this->db->update('kams_result', $updateExamNew);

            //$dataout['result']['audio']['sql'] =$this->db->last_query();
            $dataout['result']['audio']['questions'] = $questions;
            $dataout['result']['audio']['media_path'] = base_url() . 'videos/';
            $dataout['result']['audio']['totalquestions'] = ($slno - 1);
            $dataout['result']['activetab'] = "audio";
        }
        if ($nextflag == "video") {
            $question_module = 4;
            $questions = $this->quiz_model->get_randamquestions($exam_category, $exam_type, $question_module, $module_video);
            $slno = 1;

            $getquestionIDs = "";
            foreach ($questions as $key => $val) {
                $getquestionIDs .= "," . $val['qid'];
                $questions[$key]['slno'] = $slno;
                $questions[$key]['video_url'] = ($val['video_flg'] == 1) ? base_url() . "videos/" . $val['video_url'] : $val['video_url'];
                if ($val['qtype'] != 6) {
                    $questions[$key]['option'] = $this->quiz_model->get_options($val['qid']);
                } else {
                    $questions[$key]['option'] = array();
                }
                $slno++;
            }
            $getquestionIDs = trim($getquestionIDs, ",");
            $updateExamNew = array();
            $updateExamNew['video_question_ids'] = $getquestionIDs;
            $this->db->where('rid', $exam_start_id);
            $this->db->update('kams_result', $updateExamNew);

            //$dataout['result']['video']['sql'] =$this->db->last_query();
            $dataout['result']['video']['questions'] = $questions;
            $dataout['result']['video']['media_path'] = base_url() . 'videos/';
            $dataout['result']['video']['totalquestions'] = ($slno - 1);
            $dataout['result']['activetab'] = "video";
        }
        $dataout['result']['activetab'] = $nextflag;
        $dataout['HasError'] = false;
        $dataout['Message'] = "Thanks for attend you Exam";
        echo json_encode($dataout);
        exit();
    }

    public function getexamreportBC() {
        $data = file_get_contents('php://input');
        $json = json_decode($data);
        $connection_key = @$json->{'connection_key'};
        $userId = @$json->{'userId'};
        $examId = @$json->{'examId'};
        $exam_start_id = @$json->{'exam_start_id'};

        //check valid exam or not 
        //$examId = 18;
        //$exam_start_id = 153;
        $this->db->where('quid', $examId);
        $auth = $this->db->get('kams_quiz');
        $quiz = $auth->row_array();
        // echo $this->db->last_query() ;
        // $quiz_id = 18;
        $this->db->where('rid', $exam_start_id);
        $examId = $this->db->get('kams_result');
        $examDetails = $examId->row_array();

        // print_r($examDetails); exit();
        // echo $this->db->last_query() ;
        //print "11"; exit();
        if ($quiz && $examDetails) {
            $module_english = $quiz['module_english'];
            $module_video = $quiz['module_video'];
            $module_audio = $quiz['module_audio'];
            $module_general = $quiz['module_general'];
            $exam_category = $quiz['exam_category'];
            $exam_type = $quiz['exam_type'];

            //$exam_category = 
            $dataout = array();
            $dataout['result']['examdetails'] = $quiz;

//            $labels[] = '';
//            $datainput[] = 0;
//            $backgroundColor[] = "#fff";
//            $borderColor[] = "#fff";


            if ($module_english) {

                $english_percentage = $examDetails['english_percentage'];

                if ($english_percentage < 50) {
                    $color = "#ff0000";
                    $bcolor = '#28160c';
                } elseif ($english_percentage < 80) {
                    $color = "#0058ff";
                    $bcolor = "#111937";
                } else {
                    $color = "#5ba432";
                    $bcolor = "#17270b";
                }

                //$question_module = 1;
                $labels[] = 'English';
                $datainput[] = $examDetails['english_percentage'];
                $backgroundColor[] = $color;
                $borderColor[] = $bcolor;
                // print "33"; exit();
            }
            if ($module_general) {
                $general_percentage = $examDetails['general_percentage'];
                if ($general_percentage < 50) {
                    $color = "#ff0000";
                    $bcolor = '#28160c';
                } elseif ($general_percentage < 80) {
                    $color = "#0058ff";
                    $bcolor = "#111937";
                } else {
                    $color = "#5ba432";
                    $bcolor = "#17270b";
                }
                //$question_module = 1;
                $labels[] = 'General';
                $datainput[] = 30; //$examDetails['general_percentage'];
                $backgroundColor[] = "red"; //$color;
                $borderColor[] = $bcolor;
            }
            if ($module_audio) {
                $audio_percentage = $examDetails['audio_percentage'];
                if ($audio_percentage < 50) {
                    $color = "#ff0000";
                    $bcolor = '#28160c';
                } elseif ($audio_percentage < 80) {
                    $color = "#0058ff";
                    $bcolor = "#111937";
                } else {
                    $color = "#5ba432";
                    $bcolor = "#17270b";
                }
                //$question_module = 1;
                $labels[] = 'Audio';
                $datainput[] = $examDetails['audio_percentage'];
                $backgroundColor[] = $color;
                $borderColor[] = $bcolor;
            }

            if ($module_video) {
                $video_percentage = $examDetails['video_percentage'];
                if ($video_percentage < 50) {
                    $color = "#ff0000";
                    $bcolor = '#28160c';
                } elseif ($video_percentage < 80) {
                    $color = "#0058ff";
                    $bcolor = "#111937";
                } else {
                    $color = "#5ba432";
                    $bcolor = "#17270b";
                }
                //$question_module = 1;
                $labels[] = 'Video';
                $datainput[] = $examDetails['video_percentage'];
                $backgroundColor[] = $color;
                $borderColor[] = $bcolor;
            }


            $dataout['result']['labels'] = $labels;
            $dataout['result']['data'] = $datainput;
            $dataout['result']['backgroundColor'] = $backgroundColor;
            $dataout['result']['borderColor'] = $borderColor;
            $dataout['HasError'] = false;
            $dataout['Message'] = "Gaphical Report";
            echo json_encode($dataout);
            exit();
        } else {
            $dataout = array();
            $dataout['result'] = array();
            $dataout['HasError'] = true;
            $dataout['Message'] = "Invalid ID";
            echo json_encode($dataout);
            exit();
        }
    }

    public function getexamsubscription() {
        $data = file_get_contents('php://input');
        $json = json_decode($data);
        $connection_key = @$json->{'connection_key'};
        $userId = $json->{'userId'};
        $examId = $json->{'examId'};
        $cityId = @$json->{'cityId'};
        $agencyId = @$json->{'agencyId'};

        $date = @$json->{'date'};
        $date = $date ? $date : date("Y-m-d");

        // $examId = 18;
        $this->db->where('quid', $examId);
        $auth = $this->db->get('kams_quiz');
        $quiz = $auth->row_array();

        if ($quiz) {
            $module_english = $quiz['module_english'];
            $module_video = $quiz['module_video'];
            $module_audio = $quiz['module_audio'];
            $module_general = $quiz['module_general'];
            $exam_category = $quiz['exam_category'];
            $exam_type = $quiz['exam_type'];

            if ($cityId)
                $this->db->where('id_city', $cityId);

            $this->db->where('status', 1);
            $cityQuery = $this->db->get('kams_city');
            $citylist = $cityQuery->result_array();

            $selectCity = array();

            $quizprice = $quiz['quiz_price'] ? $quiz['quiz_price'] : 0;
            foreach ($citylist as $key => $val) {
                $cityId = $val['id_city'];
                $this->db->select('uid,first_name,last_name,company,address,email,contact_no');
                $this->db->where('su', 3);
                // $this->db->where('user_status', "Active");
                $this->db->where('id_city', $cityId);
                $this->db->where("wallet_balance >= $quizprice");
                $userQuery = $this->db->get('kams_users');
                // echo $this->db->last_query();
                $userlist = $userQuery->result_array();
                // echo $this->db->last_query();
                if ($userlist) {
                    $val['agencyList'] = $userlist;
                    array_push($selectCity, $val);
                }
            }

            $result = array();
            $result['result']['examdetails'] = $quiz;
            $result['result']['agencyList'] = $selectCity;
            $result['result']['locationlist'] = array("All", "Location-1", "Location-2");
            $result['result']['examdate'] = $date ? $date : date("Y-m-d");

            //free or packed calculate Here 
            $this->db->where('student_id', $userId);
            $this->db->where('quiz_id', $examId);
            $this->db->where('start_date', $date);
            $timeslot_query = $this->db->get('kams_students_exam_subscription');
            $timeslot_result = $timeslot_query->result_array();
            $timebookArray = array();

            foreach ($timeslot_result as $k => $v) {
                $hourArray = explode(" ", $v['start_time']);
                $hour = (int) $hourArray[0];
                $timebookArray[$hour] = true;
            }

            $slotArray = array();
            if (!@$timebookArray[9]) {
                $timearray = array("time" => "9",
                    "viewtime" => "9 AM",
                    "available" => true);
                array_push($slotArray, $timearray);
            }
            if (!@$timebookArray[10]) {
                $timearray = array("time" => "10", "viewtime" => "10 AM",
                    "available" => true);
                array_push($slotArray, $timearray);
            }
            if (!@$timebookArray[11]) {
                $timearray = array("time" => "11", "viewtime" => "11 AM",
                    "available" => true);
                array_push($slotArray, $timearray);
            }
            if (!@$timebookArray[12]) {
                $timearray = array("time" => "12", "viewtime" => "12 PM",
                    "available" => true);
                array_push($slotArray, $timearray);
            }
            if (!@$timebookArray[13]) {
                $timearray = array("time" => "13", "viewtime" => "1 PM",
                    "available" => true);
                array_push($slotArray, $timearray);
            }
            if (!@$timebookArray[14]) {
                $timearray = array("time" => "14", "viewtime" => "2 PM",
                    "available" => true);
                array_push($slotArray, $timearray);
            }

            if (!@$timebookArray[15]) {
                $timearray = array("time" => "15", "viewtime" => "3 PM",
                    "available" => true);
                array_push($slotArray, $timearray);
            }

            if (!@$timebookArray[16]) {
                $timearray = array("time" => "16", "viewtime" => "4 PM",
                    "available" => true);
                array_push($slotArray, $timearray);
            }

            $result['result']['slottime'] = $slotArray;
            $result['StatusCode'] = "200";
            $result['HasError'] = false;
            $result['Message'] = "Invalid Exam";
        } else {
            $result = array();
            $result['StatusCode'] = "200";
            $result['HasError'] = true;
            $result['Message'] = "Invalid Exam";
        }
        echo json_encode($result);
        exit();
    }

    public function submitexamsubscription() {
        $data = file_get_contents('php://input');
        $json = json_decode($data);
        $connection_key = @$json->{'connection_key'};
        $userId = $json->{'userId'};
        $examId = $json->{'examId'};
        $cityId = $json->{'cityId'};
        $agencyId = $json->{'agencyId'};
        $date = $json->{'date'};
        $time = $json->{'time'};
        if ($userId && $examId && $cityId && $agencyId) {
            
        } else {
            $result = array();
            $result['StatusCode'] = "200";
            $result['HasError'] = true;
            $result['Message'] = "Invalid Exam Parameter ";
            echo json_encode($result);
            exit();
        }
        // $examId = 18;
        $userinfo = $this->checkUser($userId);

        $this->db->where('quid', $examId);
        $auth = $this->db->get('kams_quiz');
        $quiz = $auth->row_array();
        $examiner_id = 0; //67;
        if ($quiz && $userinfo) {
            $starttime = $time < 10 ? "0" . $time . ":00:00" : $time . ":00:00";
            $etime = $time + 1;
            $endtime = $etime < 10 ? "0" . $etime . ":00:00" : $etime . ":00:00";
            $ins_sub = array();
            $ins_sub['quiz_id'] = $examId;
            $ins_sub['student_id'] = $userId;
            $ins_sub['agency_id'] = $agencyId;
            $ins_sub['city_id'] = $cityId;
            $ins_sub['start_date'] = $date;
            $ins_sub['end_date'] = $date;
            $ins_sub['start_time'] = $starttime;
            $ins_sub['end_time'] = $endtime;
            $ins_sub['examiner_id'] = $examiner_id;
            $ins_sub['approved_status'] = 0;
            $activationcode = rand(10000, 99999);
            $ins_sub['unique_code'] = $activationcode;
            $ins_sub['added_date'] = date("Y-m-d H:i:s");
            $this->db->insert('kams_students_exam_subscription', $ins_sub);
            $ins_id_sub = $this->db->insert_id();

            $subject = "GACA Elpt - Subscription Email";

            $dataEmail = array();
            $dataEmail['name'] = $userinfo['first_name']; //$data_ins['firstname'];
            $dataEmail['examname'] = $quiz['quiz_name'];
            $examstartdate = $this->saudidateformate($date);
            $examstarttime = $this->sauditimeformate($starttime);
            $dataEmail['examdatetime'] = $examstartdate . " " . $examstarttime;
            $dataEmail['activationcode'] = $activationcode;
            $body = $this->load->view('mail/examSubscription', $dataEmail, true);
            $to = $userinfo['email'];
            $this->sendEmail($subject, $body, $to);

            $result = array();
            $result['StatusCode'] = "200";
            $result['HasError'] = false;
            $result['Message'] = "Subscription Successfully";
        } else {
            $result = array();
            $result['StatusCode'] = "200";
            $result['HasError'] = true;
            $result['Message'] = "Invalid Exam";
        }
        echo json_encode($result);
        exit();
    }

    public function getexamlistDetails() {
        $data = file_get_contents('php://input');
        $json = json_decode($data);
        $connection_key = @$json->{'connection_key'};
        $userId = $json->{'userId'};
        $userinfo = $this->checkUser($userId);
        //print_r($userinfo);
        if ($userinfo) {

            $result = array();
            $this->db->where('exam_type', 1);
            $this->db->where('status', 1);
            $this->db->where('exam_category', $userinfo['exam_category']);
            $this->db->select('quid,exam_category,exam_type,quiz_name,description');
            $query = $this->db->get('kams_quiz');
            $dummyExamList = $query->result_array();

            $result['result']['dummyExamList'] = $dummyExamList;

            $this->db->where('q.exam_type', 2);
            $this->db->where('q.status', 1);
            $this->db->where('q.exam_category', $userinfo['exam_category']);
            $this->db->where('qs.student_id', $userId);
            // $this->db->where('qs.approved_status', 1);
            $this->db->select('q.quid,q.exam_category,q.exam_type,q.quiz_name,q.description,'
                    . 'qs.approved_status, qs.start_date AS examstartdate,qs.start_time AS examstarttime');
            $this->db->from('kams_quiz AS q');
            $this->db->join('kams_students_exam_subscription AS qs', 'qs.quiz_id = q.quid');
            $query = $this->db->get();
            // print $this->db->last_query();
            $subcriptionExamList = $query->result_array();
//print "22"; exit();
            //$result['result']['query'] = $this->db->last_query();
            foreach ($subcriptionExamList as $key => $val) {
                $examstartdate = $this->saudidateformate($val['examstartdate']);
                $examstarttime = $this->sauditimeformate($val['examstarttime']);
                $subcriptionExamList[$key]['datetimeview'] = $examstartdate . " " . $examstarttime;
            }


            // print_r($subcriptionExamList);exit();

            $result['result']['subcriptionExamList'] = $subcriptionExamList;


//            $this->db->where('q.exam_type', 2);
//            $this->db->where('q.exam_category', $userinfo['exam_category']);
//            $this->db->where('qs.student_id', $userId);
//            $this->db->where('qs.approved_status', 0);
//            $this->db->select('q.quid,q.exam_category,q.exam_type,q.quiz_name,q.description,'
//                    . 'qs.start_date AS examstartdate,qs.start_time AS examstarttime');
//            $this->db->from('kams_quiz AS q');
//            $this->db->join('kams_students_exam_subscription AS qs', 'qs.quiz_id = q.quid');
//            $query = $this->db->get();
//            $subcriptionpendingExamList = $query->result_array();
//
//            foreach ($subcriptionpendingExamList as $key => $val) {
//                $examstartdate = $this->saudidateformate($val['examstartdate']);
//                $examstarttime = $this->sauditimeformate($val['examstarttime']);
//                $subcriptionpendingExamList[$key]['datetimeview'] = $examstartdate . " " . $examstarttime;
//            }
//            
//            $result['result']['subcriptionpendingExamList'] = $subcriptionpendingExamList;

            $this->db->where('exam_type', 2);
            $this->db->where('status', 1);
            $this->db->where('exam_category', $userinfo['exam_category']);
            $this->db->where("quid NOT IN (SELECT quiz_id FROM kams_students_exam_subscription WHERE student_id = $userId )");
            $this->db->select('quid,exam_category,exam_type,quiz_name,description');
            $query = $this->db->get('kams_quiz');
            $notsubcriptionExamList = $query->result_array();

            $result['result']['notsubcriptionExamList'] = $notsubcriptionExamList;
        } else {
            $result = array();
            $result['StatusCode'] = "200";
            $result['HasError'] = true;
            $result['Message'] = "Invalid Exam";
        }
        echo json_encode($result);
        exit();
    }

    public function examFeedbackbyUser() {
        $data = file_get_contents('php://input');
        $json = json_decode($data);
        $connection_key = @$json->{'connection_key'};
        $userId = $json->{'userId'};
        $examId = $json->{'examId'};
        $exam_start_id = $json->{'exam_start_id'};
        $feedback = $json->{'feedback'};

        $userinfo = $this->checkUser($userId);
        if ($userinfo) {
            $insertArray = array();
            $insertArray['exam_id'] = $examId;
            $insertArray['result_id'] = $exam_start_id;
            $insertArray['student_id'] = $userId;
            $insertArray['student_comments'] = $feedback;
            $insertArray['add_date'] = date("Y-m-d H:i:s");
            $insertArray['status'] = 0;
            $this->db->insert('kams_result_feedback', $insertArray);

            $result = array();
            $result['StatusCode'] = "200";
            $result['HasError'] = false;
            $result['Message'] = "Thank you for feedback.";
        } else {
            $result = array();
            $result['StatusCode'] = "200";
            $result['HasError'] = true;
            $result['Message'] = "Invalid Information ";
        }

        echo json_encode($result);
        exit();
    }

    private function checkUser($userId) {
        $this->db->where('uid', $userId);
        $auth = $this->db->get('kams_users');
        return $auth->row_array();
    }

    private function saudidateformate($date) {
        $datef = explode("-", $date); //yy-mm-dd
        return $datef[2] . "/" . $datef[1] . "/" . $datef[0]; //mm/dd/yy
    }

    private function sauditimeformate($time) {
        $datef = explode(":", $time); //yy-mm-dd
        $hour = (int) $datef[0];
        if ($hour < 12) {
            $time = $hour < 10 ? "0" . $hour . " AM" : $hour . " AM";
        } else {
            $hour = $hour - 12;
            $time = $hour < 10 ? "0" . $hour . " PM" : $hour . " PM";
        }
        return $time; //mm/dd/yy
    }

    public function testEmail() {
        $subject = "Kams Global - Contact Us";
        $dataEmail = array();
        $dataEmail['name'] = "Pramodini"; //$data_ins['firstname'];
        $dataEmail['link'] = "Link";
        $body = $this->load->view('mail/contactus', $dataEmail, true);
        $to = "applicantelpt2020@gmail.com";
        $a = $this->sendEmail($subject, $body, $to);
        print $a;
    }

    public function sendEmail($subject, $body, $to, $cc = null, $filepath = null, $flg = 0) {
        //$flg = 1;
        if (!$to || !$subject || !$body) {
            return true;
        }

        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'lnx25.securehostdns.com',
            // 'smtp_host' => 'ssl://smtp.zoho.com',
            'smtp_port' => 587,
            'smtp_user' => 'smtp@thoughtspheres.com',
            'smtp_pass' => 'smtp@#$2016',
            'mailtype' => 'html',
                //'smtp_user' => 'alert@iliftserviceapp.com',
                //'smtp_pass' => 'Ali#241@dkj'
        );
        $config2 = Array(
            'protocol' => 'tls',
            'smtp_host' => 'smtp.gmail.com',
            // 'smtp_host' => 'ssl://smtp.zoho.com',
            'smtp_port' => 587,
            'smtp_user' => 'smptelpt2020@gmail.com',
            'smtp_pass' => 'Pass@1234',
            'mailtype' => 'html',
                //'smtp_user' => 'alert@iliftserviceapp.com',
                //'smtp_pass' => 'Ali#241@dkj'
        );


        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from('smtpgoamrut@gmail.com', 'Admin');
        if ($to) {
            $this->email->to($to);
        }

        if ($cc) {
            $this->email->cc($cc);
        }
        if ($filepath) {
            $this->email->attach($filepath);
        }

        $this->email->subject($subject);
        $this->email->message($body);

        if ($flg) {
            if (!$this->email->send()) {
                print_r($this->email->print_debugger());
            } else {
                echo 'Your e-mail has been sent!';
            }
        } else {
            return $this->email->send();
        }


        return true;
    }

    public function textreport() {
        //print "22"; exit();
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);

        $this->createpdf(117);
    }

    private function createpdf($rid) {
        $this->load->library('pdf');
        $this->load->model("result_model");
        $data = array();
        $data['result'] = $this->result_model->get_result($rid);
        // print_r($data); exit();
        $data['exam_category'] = $this->config->item('exam_category');
        $data['dob'] = date('d/m/Y', strtotime($data['result']['dob']));
        $data['start_date'] = date('d/m/Y', strtotime($data['result']['start_date_result']));
        $startdate = strtotime($data['result']['start_date_result']);
        $data['expirydate'] = date('d/m/Y', strtotime('+15 years', $startdate));
//        $a = $this->load->view('mypdf', $data,true);
//        print $a; exit();
        $this->pdf->load_view('mypdf', $data);
        $this->load->helper('file');
        $this->pdf->render();
        $output = $this->pdf->output();
        $pdfroot = base_url();
        $filename = time() . ".pdf";
        file_put_contents('./result_pdf/' . $filename, $output);

        $this->db->where('rid', $rid);
        $this->db->update('kams_result', array("report_pdf" => $filename));
        return true;
    }

    public function testroom() {
        $rid = 122;
        $r = $this->createroomID($rid);
        echo $r;
    }

    private function createroomID($rid) {
//        ini_set('display_errors', 1);
//        ini_set('display_startup_errors', 1);
//        error_reporting(E_ALL);
        define('VIDEOCALLPATH', $_SERVER['DOCUMENT_ROOT'] . '/equizAdmindesign/video_call/'); // OR exit('No direct script access allowed');
        include VIDEOCALLPATH . 'api/create-room/videoAPI.php';

        $vAPI = new videoAPI();
        $inputarray = array();
        $inputarray['roomname'] = "Test";
        $inputarray['description'] = "Description";
        $res = $vAPI->Createroom($inputarray);
        $roomdetails = json_decode($res, true);
        $roomID = $roomdetails['room']['room_id'];
        $updateroom = array();
        $updateroom['video_room_id'] = $roomID;
        $updateroom['video_room_details'] = $res;

        $this->db->where('rid', $rid);
        $this->db->update('kams_result', $updateroom);

        $myfile = fopen("newfile.txt", "a+") or die("Unable to open file!");
        $txt = print_r($updateroom, true);
        fwrite($myfile, $txt);
        $txt = print_r($roomdetails, true);
        fwrite($myfile, $txt);
        $txt = $this->db->last_query();
        fwrite($myfile, $txt);
        fclose($myfile);

        return $roomID;
    }

    public function getRecord() {
        $roomID = $_GET['roomid'];
        $login = '5e316b0f90ef809cde545f05';
        $password = 'uyVynu3u3y2aDejumyLy9ezu5uReYuWaYevy';
        $url = 'https://api.enablex.io/v1/archive/room/' . $roomID;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, "$login:$password");
        $result = curl_exec($ch);

        curl_close($ch);
        $res = json_decode($result, true);
        //echo($result);
        print "<pre>";
        print_r($res);
    }

}
